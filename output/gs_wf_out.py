#
# Python-matplotlib script for producing wavefunction plot on a grid
# Last update: Dec 13th 2013, Riku Tuovinen <riku.m.tuovinen@jyu.fi>
#

import sys
import numpy as np
import matplotlib as mpl
from matplotlib.patches import Circle, Arrow
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from ase.visualize import view
from ase.io import read, write
from math import pow

fig_width_pt  = 500.0                       # Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 1.0/72.27                   # Convert pt to inches
golden_mean   = (np.sqrt(5)-1.0)/2.0        # Aesthetic ratio
fig_width     = fig_width_pt*inches_per_pt  # width in inches
fig_height    = fig_width*golden_mean       # height in inches
fig_size      = [fig_width,fig_height]

params = {'backend': 'pdf',                      # ps pdf svg
          'axes.labelsize': 8,
          'text.fontsize': 8,
          'legend.fontsize': 8,
          'xtick.labelsize': 6,
          'xtick.major.size': 4,
          'xtick.minor.size': 2,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.5,
          'xtick.major.pad': 4,
          'xtick.minor.pad': 4,
          'ytick.labelsize': 6,
          'ytick.major.size': 4,
          'ytick.minor.size': 2,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.5,
          'ytick.major.pad': 4,
          'ytick.minor.pad': 4,
          'text.usetex': True,
          'ps.usedistiller': 'xpdf',            # None ghostscript xpdf
          'lines.linewidth': 1.0,
          'lines.dash_capstyle': 'butt',        # butt round projecting
          'lines.dash_joinstyle': 'miter',      # miter round bevel
          'figure.figsize': fig_size}

structure_from_file = read('../input/structure.xyz')
pos = structure_from_file.get_positions()

def dist(j,k):
    return np.sqrt(  (pos[j][0]-pos[k][0])**2
                   + (pos[j][1]-pos[k][1])**2
                   + (pos[j][2]-pos[k][2])**2 )

wf_from_file = np.loadtxt("gs_wf.out")
norm_from_file = np.loadtxt("gs_norm.out")

#print(wf_from_file[:,1])

x = pos[:,0]
z = pos[:,1]
y = pos[:,2]

min1 = 1.0
min2 = 1.0
min3 = 1.0
min4 = 1.0
min5 = 1.0

max1 = -1.0
max2 = -1.0
max3 = -1.0
max4 = -1.0
max5 = -1.0

min1n = 1.0
min2n = 1.0
min3n = 1.0
min4n = 1.0
min5n = 1.0

max1n = 0.0
max2n = 0.0
max3n = 0.0
max4n = 0.0
max5n = 0.0

for j in range(len(pos)):
    if (wf_from_file[:,1][j] < min1):
        min1 = wf_from_file[:,1][j]
    if (wf_from_file[:,3][j] < min2):
        min2 = wf_from_file[:,3][j]
    if (wf_from_file[:,5][j] < min3):
        min3 = wf_from_file[:,5][j]
    if (wf_from_file[:,7][j] < min4):
        min4 = wf_from_file[:,7][j]
    if (wf_from_file[:,9][j] < min5):
        min5 = wf_from_file[:,9][j]
    if (wf_from_file[:,1][j] > max1):
        max1 = wf_from_file[:,1][j]
    if (wf_from_file[:,3][j] > max2):
        max2 = wf_from_file[:,3][j]
    if (wf_from_file[:,5][j] > max3):
        max3 = wf_from_file[:,5][j]
    if (wf_from_file[:,7][j] > max4):
        max4 = wf_from_file[:,7][j]
    if (wf_from_file[:,9][j] > max5):
        max5 = wf_from_file[:,9][j]
    if (norm_from_file[:,1][j] < min1n):
        min1n = norm_from_file[:,1][j]
    if (norm_from_file[:,2][j] < min2n):
        min2n = norm_from_file[:,2][j]
    if (norm_from_file[:,3][j] < min3n):
        min3n = norm_from_file[:,3][j]
    if (norm_from_file[:,4][j] < min4n):
        min4n = norm_from_file[:,4][j]
    if (norm_from_file[:,5][j] < min5n):
        min5n = norm_from_file[:,5][j]
    if (norm_from_file[:,1][j] > max1n):
        max1n = norm_from_file[:,1][j]
    if (norm_from_file[:,2][j] > max2n):
        max2n = norm_from_file[:,2][j]
    if (norm_from_file[:,3][j] > max3n):
        max3n = norm_from_file[:,3][j]
    if (norm_from_file[:,4][j] > max4n):
        max4n = norm_from_file[:,4][j]
    if (norm_from_file[:,5][j] > max5n):
        max5n = norm_from_file[:,5][j]

print(min2,max2)
print(min2n,max2n)

N = len(x)

radius = 0.7
delta = 1.0e-4
delta2 = 1.0e-1
patchesc1 = []
patchesc2 = []
patchesc3 = []
patchesc4 = []
patchesc5 = []
patchesc1n = []
patchesc2n = []
patchesc3n = []
patchesc4n = []
patchesc5n = []

colours1 = wf_from_file[:,1]
colours2 = wf_from_file[:,3]
colours3 = wf_from_file[:,5]
colours4 = wf_from_file[:,7]
colours5 = wf_from_file[:,9]
colours1n = norm_from_file[:,1]
colours2n = norm_from_file[:,2]
colours3n = norm_from_file[:,3]
colours4n = norm_from_file[:,4]
colours5n = norm_from_file[:,5]

clw = 0.0
opacity = 0.95

for j, k, c in zip(range(N), range(N), mpl.cm.RdBu_r(colours1)):
    circle1 = Circle((x[j], z[k]), radius, fc=c, lw=clw)
    patchesc1.append(circle1)

pc1 = PatchCollection(patchesc1, cmap=mpl.cm.RdBu_r, match_original=False)
pc1.set_alpha(opacity)

plt.rcParams.update(params)
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.set_title('$\psi_j(k)$', fontsize = 8)

ax1.add_collection(pc1)
ax1.set_xticks([])
ax1.set_yticks([])
plt.xlim([min(x)-2*radius,max(x)+2*radius])
plt.ylim([min(z)-2*radius,max(z)+2*radius])
ax1.set_aspect('equal')

pc1.set_array(colours1)
pc1.set_clim([float(min1),float(max1)])

divider1 = make_axes_locatable(plt.gca())
cax1 = divider1.append_axes("right", "5%", pad="3%")
plt.colorbar(pc1, cax1, ticks=[min1, min1 + (max1 - min1)/4, min1 + 2*(max1 - min1)/4, min1 + 3*(max1 - min1)/4, max1], format='%0.2f')

plt.savefig('gs_wf1.pdf', bbox_inches='tight')

#

for j, k, c in zip(range(N), range(N), mpl.cm.RdBu_r(colours2)):
    circle2 = Circle((x[j], z[k]), radius, fc=c, lw=clw)
    patchesc2.append(circle2)

pc2 = PatchCollection(patchesc2, cmap=mpl.cm.RdBu_r, match_original=False)
pc2.set_alpha(opacity)

plt.rcParams.update(params)
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
ax2.set_title('$\psi_j(k)$', fontsize = 8)

ax2.add_collection(pc2)
ax2.set_xticks([])
ax2.set_yticks([])
plt.xlim([min(x)-2*radius,max(x)+2*radius])
plt.ylim([min(z)-2*radius,max(z)+2*radius])
ax2.set_aspect('equal')

pc2.set_array(colours2)
pc2.set_clim([float(min2),float(max2)])

divider2 = make_axes_locatable(plt.gca())
cax2 = divider2.append_axes("right", "5%", pad="3%")
plt.colorbar(pc2, cax2, ticks=[min2, min2 + (max2 - min2)/4, min2 + 2*(max2 - min2)/4, min2 + 3*(max2 - min2)/4, max2], format='%0.2f')

plt.savefig('gs_wf2.pdf', bbox_inches='tight')

#

for j, k, c in zip(range(N), range(N), mpl.cm.RdBu_r(colours3)):
    circle3 = Circle((x[j], z[k]), radius, fc=c, lw=clw)
    patchesc3.append(circle3)

pc3 = PatchCollection(patchesc3, cmap=mpl.cm.RdBu_r, match_original=False)
pc3.set_alpha(opacity)

plt.rcParams.update(params)
fig3 = plt.figure()
ax3 = fig3.add_subplot(111)
ax3.set_title('$\psi_j(k)$', fontsize = 8)

ax3.add_collection(pc3)
ax3.set_xticks([])
ax3.set_yticks([])
plt.xlim([min(x)-2*radius,max(x)+2*radius])
plt.ylim([min(z)-2*radius,max(z)+2*radius])
ax3.set_aspect('equal')

pc3.set_array(colours3)
pc3.set_clim([float(min3),float(max3)])

divider3 = make_axes_locatable(plt.gca())
cax3 = divider3.append_axes("right", "5%", pad="3%")
plt.colorbar(pc3, cax3, ticks=[min3, min3 + (max3 - min3)/4, min3 + 2*(max3 - min3)/4, min3 + 3*(max3 - min3)/4, max3], format='%0.2f')

plt.savefig('gs_wf3.pdf', bbox_inches='tight')

#

for j, k, c in zip(range(N), range(N), mpl.cm.RdBu_r(colours4)):
    circle4 = Circle((x[j], z[k]), radius, fc=c, lw=clw)
    patchesc4.append(circle4)

pc4 = PatchCollection(patchesc4, cmap=mpl.cm.RdBu_r, match_original=False)
pc4.set_alpha(opacity)

plt.rcParams.update(params)
fig4 = plt.figure()
ax4 = fig4.add_subplot(111)
ax4.set_title('$\psi_j(k)$', fontsize = 8)

ax4.add_collection(pc4)
ax4.set_xticks([])
ax4.set_yticks([])
plt.xlim([min(x)-2*radius,max(x)+2*radius])
plt.ylim([min(z)-2*radius,max(z)+2*radius])
ax4.set_aspect('equal')

pc4.set_array(colours4)
pc4.set_clim([float(min4),float(max4)])

divider4 = make_axes_locatable(plt.gca())
cax4 = divider4.append_axes("right", "5%", pad="3%")
plt.colorbar(pc4, cax4, ticks=[min4, min4 + (max4 - min4)/4, min4 + 2*(max4 - min4)/4, min4 + 3*(max4 - min4)/4, max4], format='%0.2f')

plt.savefig('gs_wf4.pdf', bbox_inches='tight')

#

for j, k, c in zip(range(N), range(N), mpl.cm.RdBu_r(colours5)):
    circle5 = Circle((x[j], z[k]), radius, fc=c, lw=clw)
    patchesc5.append(circle5)

pc5 = PatchCollection(patchesc5, cmap=mpl.cm.RdBu_r, match_original=False)
pc5.set_alpha(opacity)

plt.rcParams.update(params)
fig5 = plt.figure()
ax5 = fig5.add_subplot(111)
ax5.set_title('$\psi_j(k)$', fontsize = 8)

ax5.add_collection(pc5)
ax5.set_xticks([])
ax5.set_yticks([])
plt.xlim([min(x)-2*radius,max(x)+2*radius])
plt.ylim([min(z)-2*radius,max(z)+2*radius])
ax5.set_aspect('equal')

pc5.set_array(colours5)
pc5.set_clim([float(min5),float(max5)])

divider5 = make_axes_locatable(plt.gca())
cax5 = divider5.append_axes("right", "5%", pad="3%")
plt.colorbar(pc5, cax5, ticks=[min5, min5 + (max5 - min5)/4, min5 + 2*(max5 - min5)/4, min5 + 3*(max5 - min5)/4, max5], format='%0.2f')

plt.savefig('gs_wf5.pdf', bbox_inches='tight')

###
###
###

for j, k, c in zip(range(N), range(N), mpl.cm.hot_r(colours1n)):
    circle1n = Circle((x[j], z[k]), radius, fc=c, lw=clw)
    patchesc1n.append(circle1n)

pc1n = PatchCollection(patchesc1n, cmap=mpl.cm.hot_r, match_original=False)
pc1n.set_alpha(opacity)

plt.rcParams.update(params)
fig1n = plt.figure()
ax1n = fig1n.add_subplot(111)
ax1n.set_title('$|\psi_j(k)|^2$', fontsize = 8)

ax1n.add_collection(pc1n)
ax1n.set_xticks([])
ax1n.set_yticks([])
plt.xlim([min(x)-2*radius,max(x)+2*radius])
plt.ylim([min(z)-2*radius,max(z)+2*radius])
ax1n.set_aspect('equal')

pc1n.set_array(colours1n)
pc1n.set_clim([float(min1n),float(max1n)])

divider1n = make_axes_locatable(plt.gca())
cax1n = divider1n.append_axes("right", "5%", pad="3%")
plt.colorbar(pc1n, cax1n, ticks=[min1n, min1n + (max1n - min1n)/4, min1n + 2*(max1n - min1n)/4, min1n + 3*(max1n - min1n)/4, max1n], format='%0.2f')

plt.savefig('gs_norm1.pdf', bbox_inches='tight')

#

for j, k, c in zip(range(N), range(N), mpl.cm.hot_r(colours2n)):
    circle2n = Circle((x[j], z[k]), radius, fc=c, lw=clw)
    patchesc2n.append(circle2n)

pc2n = PatchCollection(patchesc2n, cmap=mpl.cm.hot_r, match_original=False)
pc2n.set_alpha(opacity)

plt.rcParams.update(params)
fig2n = plt.figure()
ax2n = fig2n.add_subplot(111)
ax2n.set_title('$|\psi_j(k)|^2$', fontsize = 8)

ax2n.add_collection(pc2n)
ax2n.set_xticks([])
ax2n.set_yticks([])
plt.xlim([min(x)-2*radius,max(x)+2*radius])
plt.ylim([min(z)-2*radius,max(z)+2*radius])
ax2n.set_aspect('equal')

pc2n.set_array(colours2n)
pc2n.set_clim([float(min2n),float(max2n)])

divider2n = make_axes_locatable(plt.gca())
cax2n = divider2n.append_axes("right", "5%", pad="3%")
plt.colorbar(pc2n, cax2n, ticks=[min2n, min2n + (max2n - min2n)/4, min2n + 2*(max2n - min2n)/4, min2n + 3*(max2n - min2n)/4, max2n], format='%0.2f')

plt.savefig('gs_norm2.pdf', bbox_inches='tight')

#

for j, k, c in zip(range(N), range(N), mpl.cm.hot_r(colours3n)):
    circle3n = Circle((x[j], z[k]), radius, fc=c, lw=clw)
    patchesc3n.append(circle3n)

pc3n = PatchCollection(patchesc3n, cmap=mpl.cm.hot_r, match_original=False)
pc3n.set_alpha(opacity)

plt.rcParams.update(params)
fig3n = plt.figure()
ax3n = fig3n.add_subplot(111)
ax3n.set_title('$|\psi_j(k)|^2$', fontsize = 8)

ax3n.add_collection(pc3n)
ax3n.set_xticks([])
ax3n.set_yticks([])
plt.xlim([min(x)-2*radius,max(x)+2*radius])
plt.ylim([min(z)-2*radius,max(z)+2*radius])
ax3n.set_aspect('equal')

pc3n.set_array(colours3n)
pc3n.set_clim([float(min3n),float(max3n)])

divider3n = make_axes_locatable(plt.gca())
cax3n = divider3n.append_axes("right", "5%", pad="3%")
plt.colorbar(pc3n, cax3n, ticks=[min3n, min3n + (max3n - min3n)/4, min3n + 2*(max3n - min3n)/4, min3n + 3*(max3n - min3n)/4, max3n], format='%0.2f')

plt.savefig('gs_norm3.pdf', bbox_inches='tight')

#

for j, k, c in zip(range(N), range(N), mpl.cm.hot_r(colours4n)):
    circle4n = Circle((x[j], z[k]), radius, fc=c, lw=clw)
    patchesc4n.append(circle4n)

pc4n = PatchCollection(patchesc4n, cmap=mpl.cm.hot_r, match_original=False)
pc4n.set_alpha(opacity)

plt.rcParams.update(params)
fig4n = plt.figure()
ax4n = fig4n.add_subplot(111)
ax4n.set_title('$|\psi_j(k)|^2$', fontsize = 8)

ax4n.add_collection(pc4n)
ax4n.set_xticks([])
ax4n.set_yticks([])
plt.xlim([min(x)-2*radius,max(x)+2*radius])
plt.ylim([min(z)-2*radius,max(z)+2*radius])
ax4n.set_aspect('equal')

pc4n.set_array(colours4n)
pc4n.set_clim([float(min4n),float(max4n)])

divider4n = make_axes_locatable(plt.gca())
cax4n = divider4n.append_axes("right", "5%", pad="3%")
plt.colorbar(pc4n, cax4n, ticks=[min4n, min4n + (max4n - min4n)/4, min4n + 2*(max4n - min4n)/4, min4n + 3*(max4n - min4n)/4, max4n], format='%0.2f')

plt.savefig('gs_norm4.pdf', bbox_inches='tight')

#

for j, k, c in zip(range(N), range(N), mpl.cm.hot_r(colours5n)):
    circle5n = Circle((x[j], z[k]), radius, fc=c, lw=clw)
    patchesc5n.append(circle5n)

pc5n = PatchCollection(patchesc5n, cmap=mpl.cm.hot_r, match_original=False)
pc5n.set_alpha(opacity)

plt.rcParams.update(params)
fig5n = plt.figure()
ax5n = fig5n.add_subplot(111)
ax5n.set_title('$|\psi_j(k)|^2$', fontsize = 8)

ax5n.add_collection(pc5n)
ax5n.set_xticks([])
ax5n.set_yticks([])
plt.xlim([min(x)-2*radius,max(x)+2*radius])
plt.ylim([min(z)-2*radius,max(z)+2*radius])
ax5n.set_aspect('equal')

pc5n.set_array(colours5n)
pc5n.set_clim([float(min5n),float(max5n)])

divider5n = make_axes_locatable(plt.gca())
cax5n = divider5n.append_axes("right", "5%", pad="3%")
plt.colorbar(pc5n, cax5n, ticks=[min5n, min5n + (max5n - min5n)/4, min5n + 2*(max5n - min5n)/4, min5n + 3*(max5n - min5n)/4, max5n], format='%0.2f')

plt.savefig('gs_norm5.pdf', bbox_inches='tight')
