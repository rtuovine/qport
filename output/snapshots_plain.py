#!/usr/bin/env python

from __future__ import print_function
import os
import matplotlib.pyplot as plt
import numpy as np
import sys
import matplotlib as mpl
from matplotlib.patches import Circle, Arrow
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt
from ase.visualize import view
from ase.io import read, write
from math import pow

'''
    python animation_plain.py 100 10 0.1 90
'''

Nframes = int(sys.argv[1])  # Number of frames
fps     = int(sys.argv[2])  # Frames per second
dt      = float(sys.argv[3])  # Time step
dpi     = int(sys.argv[4])  # Dots per inch

fig_width_pt  = 500.0                       # Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 1.0/72.27                   # Convert pt to inches
golden_mean   = (np.sqrt(5)-1.0)/2.0        # Aesthetic ratio
fig_width     = fig_width_pt*inches_per_pt  # width in inches
fig_height    = fig_width*golden_mean       # height in inches
fig_size      = [fig_width,fig_height]

params = {'backend': 'pdf',                      # ps pdf svg
          'axes.labelsize': 8,
          'text.fontsize': 8,
          'legend.fontsize': 8,
          'xtick.labelsize': 6,
          'xtick.major.size': 4,
          'xtick.minor.size': 2,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.5,
          'xtick.major.pad': 4,
          'xtick.minor.pad': 4,
          'ytick.labelsize': 6,
          'ytick.major.size': 4,
          'ytick.minor.size': 2,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.5,
          'ytick.major.pad': 4,
          'ytick.minor.pad': 4,
          'text.usetex': False,
          'ps.usedistiller': 'xpdf',            # None ghostscript xpdf
          'lines.linewidth': 1.0,
          'lines.dash_capstyle': 'butt',        # butt round projecting
          'lines.dash_joinstyle': 'miter',      # miter round bevel
          'figure.figsize': fig_size}


structure_from_file = read('../input/structure.xyz')
pos = structure_from_file.get_positions()

x = pos[:,0]
z = pos[:,1]
y = pos[:,2]

N = len(x)

Hure = [[0.0 for j in range(len(pos))] for k in range(len(pos))]
Huim = [[0.0 for j in range(len(pos))] for k in range(len(pos))]

with open('../input/matrix_unpert.in') as inf:
    for line in inf:
        parts = line.split() # split line into parts
        Hure[int(parts[0])][int(parts[1])]=float(parts[2])
        Huim[int(parts[0])][int(parts[1])]=float(parts[3])

cdict = {
'red'  :  ((0., 0., 0.), (0.5, 0.25, 0.25), (1., 1., 1.)),
'green':  ((0., 1., 1.), (0.7, 0.0, 0.5), (1., 1., 1.)),
'blue' :  ((0., 1., 1.), (0.5, 0.0, 0.0), (1., 1., 1.))
}

my_cmap = mpl.colors.LinearSegmentedColormap('my_colormap', cdict, 256)

radius = 0.7
delta = 1.0e-4
delta2 = 1.0e-1
clw = 0.0
alw = 0.0
il = 1.5 # Scaling exponent (arrow widths) for illustration purposes
scale = 2.0

def dist(j,k):
    return np.sqrt(  (pos[j][0]-pos[k][0])**2
                   + (pos[j][1]-pos[k][1])**2
                   + (pos[j][2]-pos[k][2])**2 )

files = []

for i in range(1,Nframes+1):
    densmatrixre_from_file = np.loadtxt('snapshot_re%s.out'%i)
    densmatrixim_from_file = np.loadtxt('snapshot_im%s.out'%i)

    if(i==1):
        init_dens = np.diag(densmatrixre_from_file)

    colours = np.zeros(len(init_dens))
    for j in range(len(init_dens)):
        colours[j] = densmatrixre_from_file[j][j]-init_dens[j]

    max_bc = 0.0
    
    for j in range(len(pos)):
        for k in range(len(pos)):
            if ( j!=k and dist(j,k) < 1.43):
#                if (abs(2.0*(-2.7)*densmatrixim_from_file[j][k]) > max_bc):
                if (abs(2.0*( Hure[j][k]*densmatrixim_from_file[k][j] + Huim[j][k]*densmatrixre_from_file[k][j] )) > max_bc):
#                    max_bc = 2.0*(-2.7)*densmatrixim_from_file[j][k]
                    max_bc = 2.0*( Hure[j][k]*densmatrixim_from_file[k][j] + Huim[j][k]*densmatrixre_from_file[k][j] )

    patchesc = []
    patchesa = []

    for j, k, c in zip(range(N), range(N), mpl.cm.OrRd(colours)):
        circle = Circle((x[j], z[k]), radius, fc=c, lw=clw)
        patchesc.append(circle)

    for j in range(len(pos)):
        for k in range(len(pos)):
            if ( j!=k and dist(j,k) < 1.43 and (x[j] < x[k] or ( x[j] == x[k] and z[j] < z[k] ) ) ):

#                comp = scale*2.0*(-2.7)*densmatrixim_from_file[j][k]/max_bc
                comp = scale*2.0*( Hure[j][k]*densmatrixim_from_file[k][j] + Huim[j][k]*densmatrixre_from_file[k][j] )/max_bc

#                if (2.0*(-2.7)*densmatrixim_from_file[j][k] < 0):
                if (2.0*( Hure[j][k]*densmatrixim_from_file[k][j] + Huim[j][k]*densmatrixre_from_file[k][j] ) > 0):
                    arrow1 = Arrow(x[k]+(x[j]-x[k])*delta2, z[k]+(z[j]-z[k])*delta2, (x[j]-x[k])*(1-2*delta2), (z[j]-z[k])*(1-2*delta2), width=pow(abs(comp),il), fc='black', lw=alw)
                    patchesa.append(arrow1)
                else:
                    arrow2 = Arrow(x[j]+(x[k]-x[j])*delta2, z[j]+(z[k]-z[j])*delta2, (x[k]-x[j])*(1-2*delta2), (z[k]-z[j])*(1-2*delta2), width=pow(abs(comp),il), fc='black', lw=alw)
                    patchesa.append(arrow2)

    pc = PatchCollection(patchesc, cmap=mpl.cm.OrRd, match_original=True)
    pc.set_alpha(0.75)
    pa = PatchCollection(patchesa, match_original=True)
    pa.set_alpha(0.95)

    t=(float(i)/(1.0/dt))*10.0*0.658212

    plt.rcParams.update(params)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_title('t = %.1f fs'%t, fontsize = 8)

    ax.add_collection(pc)
    ax.add_collection(pa)
    ax.set_xticks([])
    ax.set_yticks([])
    plt.xlim([min(x)-2*radius,max(x)+2*radius])
    plt.ylim([min(z)-2*radius,max(z)+2*radius])
    ax.set_aspect('equal')

    pc.set_array(colours)
    pc.set_clim([-0.002,0.002])

    fname = 'dens_map%03d.png'%i
    print('Saving frame', fname)
    plt.savefig(fname, bbox_inches='tight', dpi=dpi)
    files.append(fname)
    plt.close(fig)
