#!/usr/bin/env python
# -*- noplot -*-

from __future__ import print_function
import os
import matplotlib.pyplot as plt
import numpy as np
import sys
import matplotlib as mpl
import pylab as pyl
from matplotlib import cm, rc
from matplotlib.patches import Circle, Arrow
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from ase.visualize import view
from ase.io import read, write
from math import pow

'''
    python animation.py 100 10 0.1 90
'''

Nframes = int(sys.argv[1])      # Number of frames
my_fps  = int(sys.argv[2])      # Frames per second
dt      = float(sys.argv[3])    # Time step
my_dpi  = int(sys.argv[4])      # Dots per inch

fig_width_pt  = 500.0                       # Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 1.0/72.27                   # Convert pt to inches
golden_mean   = (np.sqrt(5)-1.0)/2.0        # Aesthetic ratio
fig_width     = fig_width_pt*inches_per_pt  # width in inches
fig_height    = fig_width*golden_mean       # height in inches
fig_size      = [fig_width,fig_height]

params = {'backend': 'pdf',                      # ps pdf svg
          'axes.labelsize': 8,
          'text.fontsize': 8,
          'legend.fontsize': 8,
          'xtick.labelsize': 6,
          'xtick.major.size': 4,
          'xtick.minor.size': 2,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.5,
          'xtick.major.pad': 4,
          'xtick.minor.pad': 4,
          'ytick.labelsize': 6,
          'ytick.major.size': 4,
          'ytick.minor.size': 2,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.5,
          'ytick.major.pad': 4,
          'ytick.minor.pad': 4,
          'text.usetex': True,
          'ps.usedistiller': 'xpdf',            # None ghostscript xpdf
          'lines.linewidth': 1.0,
          'lines.dash_capstyle': 'butt',        # butt round projecting
          'lines.dash_joinstyle': 'miter',      # miter round bevel
          'figure.figsize': fig_size}

structure_from_file = read('../input/structure.xyz')
pos = structure_from_file.get_positions()

x = pos[:,0]
z = pos[:,1]
y = pos[:,2]

N = len(x)

Hure = [[0.0 for j in range(len(pos))] for k in range(len(pos))]
Huim = [[0.0 for j in range(len(pos))] for k in range(len(pos))]

with open('../input/matrix_unpert.in') as inf:
    for line in inf:
        parts = line.split() # split line into parts
        Hure[int(parts[0])][int(parts[1])]=float(parts[2])
        Huim[int(parts[0])][int(parts[1])]=float(parts[3])

cdict = {
'red'  :  ((0., 0., 0.), (0.5, 0.25, 0.25), (1., 1., 1.)),
'green':  ((0., 1., 1.), (0.7, 0.0, 0.5), (1., 1., 1.)),
'blue' :  ((0., 1., 1.), (0.5, 0.0, 0.0), (1., 1., 1.))
}

my_cmap = mpl.colors.LinearSegmentedColormap('my_colormap', cdict, 256)

radius  = 0.7       # Circle radius
delta   = 1.0e-4
delta2  = 1.0e-1    # Arrow spacing from circles
clw     = 0.0       # Circle edge linewidth
alw     = 0.0       # Arrow edge linewidth
il      = 1.0       # Scaling exponent (arrow widths) for illustration purposes
scale   = 1.0       # Arrow width multiplier

def dist(j,k):
    return np.sqrt(  (pos[j][0]-pos[k][0])**2
                   + (pos[j][1]-pos[k][1])**2
                   + (pos[j][2]-pos[k][2])**2 )

files = []

supermatrix_from_file = np.loadtxt('supermatrix.out')

#

max_array_e = np.empty(Nframes)
max_array_h = np.empty(Nframes)

for i in range(1, Nframes+1):
    densmatrixre_from_file = np.loadtxt('snapshot_re%s.out'%i)
    max_array_e[i-1] = np.amax(abs(densmatrixre_from_file))
    holematrixre_from_file = np.loadtxt('snapshot_hole_re%s.out'%i)
    max_array_h[i-1] = np.amax(abs(holematrixre_from_file))

max_e = np.amax(max_array_e)
max_h = np.amax(max_array_h)
max_s = np.amax(np.delete(supermatrix_from_file, np.s_[0], axis=1))

#print(supermatrix_from_file)
#print("")
#print(np.delete(supermatrix_from_file, np.s_[0], axis=1))
#print(max_e,max_h,max_s)
#sys.exit()

#

for i in range(1,Nframes+1):
    densmatrixre_from_file = np.loadtxt('snapshot_re%s.out'%i)
    densmatrixim_from_file = np.loadtxt('snapshot_im%s.out'%i)

    holematrixre_from_file = np.loadtxt('snapshot_hole_re%s.out'%i)
    holematrixim_from_file = np.loadtxt('snapshot_hole_im%s.out'%i)

    if(i==1):
        init_dens_re = densmatrixre_from_file
        init_dens_im = densmatrixim_from_file
        init_hole_re = holematrixre_from_file
        init_hole_im = holematrixim_from_file

    colours_e = np.zeros(len(init_dens_re))
    for j in range(len(init_dens_re)):
        colours_e[j] = densmatrixre_from_file[j][j]-init_dens_re[j][j]

    colours_h = np.zeros(len(init_hole_re))
    for j in range(len(init_hole_re)):
        colours_h[j] = holematrixre_from_file[j][j]-init_hole_re[j][j]

    colours_s = np.zeros(len(init_dens_re))
    for j in range(len(init_dens_re)):
        colours_s[j] = supermatrix_from_file[i*10][j+1]

    max_bc = 0.0
    
    for j in range(len(pos)):
        for k in range(len(pos)):
            if ( j!=k and dist(j,k) < 1.43):
#                if (abs(2.0*(-2.7)*densmatrixim_from_file[j][k]) > max_bc):
                if (abs(2.0*( Hure[j][k]*(abs(densmatrixim_from_file[k][j]-init_dens_im[k][j])) + Huim[j][k]*(abs(densmatrixre_from_file[k][j]-init_dens_re[k][j])) )) > max_bc):
#                    max_bc = 2.0*(-2.7)*densmatrixim_from_file[j][k]
                    max_bc = 2.0*( Hure[j][k]*(abs(densmatrixim_from_file[k][j]-init_dens_im[k][j])) + Huim[j][k]*(abs(densmatrixre_from_file[k][j]-init_dens_re[k][j])) )

    patchesc_e = []
    patchesc_h = []
    patchesc_s = []
    patchesa = []

    for j, k, c in zip(range(N), range(N), mpl.cm.OrRd(colours_e)):
        circle_e = Circle((x[j], z[k]), radius, fc=c, lw=clw)
        patchesc_e.append(circle_e)

    for j, k, c in zip(range(N), range(N), mpl.cm.OrRd(colours_h)):
        circle_h = Circle((x[j], z[k]), radius, fc=c, lw=clw)
        patchesc_h.append(circle_h)

    for j, k, c in zip(range(N), range(N), mpl.cm.OrRd(colours_s)):
        circle_s = Circle((x[j], z[k]), radius, fc=c, lw=clw)
        patchesc_s.append(circle_s)

    for j in range(len(pos)):
        for k in range(len(pos)):
            if ( j!=k and dist(j,k) < 1.43 and (x[j] < x[k] or ( x[j] == x[k] and z[j] < z[k] ) ) ):

#                comp = scale*2.0*(-2.7)*densmatrixim_from_file[j][k]/max_bc
                comp = scale*2.0*( Hure[j][k]*(abs(densmatrixim_from_file[k][j]-init_dens_im[k][j])) + Huim[j][k]*(abs(densmatrixre_from_file[k][j]-init_dens_re[k][j])) )/max_bc

#                if (2.0*(-2.7)*densmatrixim_from_file[j][k] < 0):
                if (2.0*( Hure[j][k]*(abs(densmatrixim_from_file[k][j]-init_dens_im[k][j])) + Huim[j][k]*(abs(densmatrixre_from_file[k][j]-init_dens_re[k][j])) ) > 0):
                    arrow1 = Arrow(x[k]+(x[j]-x[k])*delta2, z[k]+(z[j]-z[k])*delta2, (x[j]-x[k])*(1-2*delta2), (z[j]-z[k])*(1-2*delta2), width=pow(abs(comp),il), fc='black', lw=alw)
                    patchesa.append(arrow1)
                else:
                    arrow2 = Arrow(x[j]+(x[k]-x[j])*delta2, z[j]+(z[k]-z[j])*delta2, (x[k]-x[j])*(1-2*delta2), (z[k]-z[j])*(1-2*delta2), width=pow(abs(comp),il), fc='black', lw=alw)
                    patchesa.append(arrow2)

    pc_e = PatchCollection(patchesc_e, cmap=mpl.cm.OrRd, match_original=True)
    pc_e.set_alpha(0.75)
    pc_h = PatchCollection(patchesc_h, cmap=mpl.cm.OrRd, match_original=True)
    pc_h.set_alpha(0.75)
    pc_s = PatchCollection(patchesc_s, cmap=mpl.cm.OrRd, match_original=True)
    pc_s.set_alpha(0.75)
    pa = PatchCollection(patchesa, match_original=True)
    pa.set_alpha(0.95)

    t=(float(i)/(1.0/dt))*10.0*0.658212  # one snapshot in 10 time steps

    pyl.rcParams.update(params)
    rc('text.latex', preamble = \
        '\usepackage{amsmath},' \
        '\usepackage{amssymb},' \
        '\usepackage{color},' )
    fig = plt.figure()
    fig.clf()

    ax1 = fig.add_subplot(311)
    ax1.set_title('$t = %.1f \ \mathrm{fs}$'%t, fontsize = 8)

    ax1.add_collection(pc_e)
    #ax1.add_collection(pa)
    ax1.set_xticks([])
    ax1.set_yticks([])
    ax1.set_ylabel(r'$n_{\text{e}}$')
    plt.xlim([min(x)-2*radius,max(x)+2*radius])
    plt.ylim([min(z)-2*radius,max(z)+2*radius])
    ax1.set_aspect('equal')

    pc_e.set_array(colours_e)
    pc_e.set_clim([-0.001,0.001])
    #pc_e.set_clim([-max_e+0.5,max_e-0.5])
    Delta_e = abs(max_e - 0.5)/4

    divider_e = make_axes_locatable(plt.gca())
    cax_e = divider_e.append_axes("right", "5%", pad="3%")
    plt.colorbar(pc_e, cax_e, ticks=[-0.0010, -0.0005, 0.0000, 0.0005, 0.0010], format='%0.4f')
    #plt.colorbar(pc_e, cax_e, ticks=[-2.0*Delta_e, -Delta_e, 0.0, Delta_e, 2.0*Delta_e], format='%0.3f')

#

    ax2 = fig.add_subplot(312)
    ax2.add_collection(pc_h)
    ax2.set_xticks([])
    ax2.set_yticks([])
    ax2.set_ylabel(r'$n_{\text{h}}$')
    plt.xlim([min(x)-2*radius,max(x)+2*radius])
    plt.ylim([min(z)-2*radius,max(z)+2*radius])
    ax2.set_aspect('equal')

    pc_h.set_array(colours_h)
    pc_h.set_clim([-0.001,0.001])
    #pc_h.set_clim([-max_h+0.5,max_h-0.5])
    Delta_h = abs(max_h - 0.5)/4

    divider_h = make_axes_locatable(plt.gca())
    cax_h = divider_h.append_axes("right", "5%", pad="3%")
    plt.colorbar(pc_h, cax_h, ticks=[-0.0010, -0.0005, 0.0000, 0.0005, 0.0010], format='%0.4f')
    #plt.colorbar(pc_h, cax_h, ticks=[-2.0*Delta_h, -Delta_h, 0.0, Delta_h, 2.0*Delta_h], format='%0.3f')

#

    ax3 = fig.add_subplot(313)
    ax3.add_collection(pc_s)
    ax3.set_xticks([])
    ax3.set_yticks([])
    ax3.set_ylabel(r'$\varDelta^* P \text{e}^{-2\text{i}Ht}$')
    plt.xlim([min(x)-2*radius,max(x)+2*radius])
    plt.ylim([min(z)-2*radius,max(z)+2*radius])
    ax3.set_aspect('equal')

    pc_s.set_array(colours_s)
    pc_s.set_clim([-0.00001,0.00001])
    #pc_s.set_clim([-max_s,max_s])
    Delta_s = abs(max_s)/4

    divider_s = make_axes_locatable(plt.gca())
    cax_s = divider_s.append_axes("right", "5%", pad="3%")
    plt.colorbar(pc_s, cax_s, ticks=[-0.000010, -0.000005, 0.000000, 0.000005, 0.000010], format='%0.1e')
    #plt.colorbar(pc_s, cax_s, ticks=[-2.0*Delta_s, -Delta_s, 0.0, Delta_s, 2.0*Delta_s], format='%0.3f')

    fname = 'dens_map%03d.png'%i
    print('Saving frame', fname)
    plt.savefig(fname, bbox_inches='tight', dpi=my_dpi)
    files.append(fname)
    plt.close(fig)

print('Rendering an animation - this might take a while...')
os.system("mencoder 'mf://dens_map*.png' -mf type=png:fps=%s -ovc lavc -lavcopts vcodec=wmv2 -oac copy -o animation.mpg"%my_fps)

'''
# cleanup
print('Done - cleaning up the individual frames...')
for fname in files: os.remove(fname)
'''
