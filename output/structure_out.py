#
# Python-matplotlib script for producing density maps
# Last update: Jun 3rd 2013, Riku Tuovinen <riku.m.tuovinen@jyu.fi>
#

import sys
import numpy as np
import matplotlib as mpl
from matplotlib.patches import Circle, Arrow
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from ase.visualize import view
from ase.io import read, write
from math import pow

fig_width_pt  = 500.0                       # Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 1.0/72.27                   # Convert pt to inches
golden_mean   = (np.sqrt(5)-1.0)/2.0        # Aesthetic ratio
fig_width     = fig_width_pt*inches_per_pt  # width in inches
fig_height    = fig_width*golden_mean       # height in inches
fig_size      = [fig_width,fig_height]

params = {'backend': 'pdf',                      # ps pdf svg
          'axes.labelsize': 8,
          'text.fontsize': 8,
          'legend.fontsize': 8,
          'xtick.labelsize': 6,
          'xtick.major.size': 4,
          'xtick.minor.size': 2,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.5,
          'xtick.major.pad': 4,
          'xtick.minor.pad': 4,
          'ytick.labelsize': 6,
          'ytick.major.size': 4,
          'ytick.minor.size': 2,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.5,
          'ytick.major.pad': 4,
          'ytick.minor.pad': 4,
          'text.usetex': True,
          'ps.usedistiller': 'xpdf',            # None ghostscript xpdf
          'lines.linewidth': 1.0,
          'lines.dash_capstyle': 'butt',        # butt round projecting
          'lines.dash_joinstyle': 'miter',      # miter round bevel
          'figure.figsize': fig_size}

structure_from_file = read('../input/structure.xyz')
pos = structure_from_file.get_positions()

def dist(j,k):
    return np.sqrt(  (pos[j][0]-pos[k][0])**2
                   + (pos[j][1]-pos[k][1])**2
                   + (pos[j][2]-pos[k][2])**2 )

densmatrix1re_from_file = np.loadtxt("snapshot_re0.out")
densmatrix2re_from_file = np.loadtxt("snapshot_re1.out")
densmatrix3re_from_file = np.loadtxt("snapshot_re2.out")
densmatrix4re_from_file = np.loadtxt("snapshot_re3.out")

densmatrix1im_from_file = np.loadtxt("snapshot_im0.out")
densmatrix2im_from_file = np.loadtxt("snapshot_im1.out")
densmatrix3im_from_file = np.loadtxt("snapshot_im2.out")
densmatrix4im_from_file = np.loadtxt("snapshot_im3.out")

mindens1 = np.amin(np.diag(densmatrix1re_from_file))
mindens2 = np.amin(np.diag(densmatrix2re_from_file))
mindens3 = np.amin(np.diag(densmatrix3re_from_file))
mindens4 = np.amin(np.diag(densmatrix4re_from_file))

maxdens1 = np.amax(np.diag(densmatrix1re_from_file))
maxdens2 = np.amax(np.diag(densmatrix2re_from_file))
maxdens3 = np.amax(np.diag(densmatrix3re_from_file))
maxdens4 = np.amax(np.diag(densmatrix4re_from_file))

x = pos[:,0]
z = pos[:,1]
y = pos[:,2]

mindens1_ess = 1.0
mindens2_ess = 1.0
mindens3_ess = 1.0
mindens4_ess = 1.0

maxdens1_ess = 0.0
maxdens2_ess = 0.0
maxdens3_ess = 0.0
maxdens4_ess = 0.0

for j in range(len(pos)):
    if (x[j] > min(x)+5.0 and x[j] < max(x)-5.0):
        if (densmatrix1re_from_file[j][j] < mindens1_ess):
            mindens1_ess = densmatrix1re_from_file[j][j]
        if (densmatrix2re_from_file[j][j] < mindens2_ess):
            mindens2_ess = densmatrix2re_from_file[j][j]
        if (densmatrix3re_from_file[j][j] < mindens3_ess):
            mindens3_ess = densmatrix3re_from_file[j][j]
        if (densmatrix4re_from_file[j][j] < mindens4_ess):
            mindens4_ess = densmatrix4re_from_file[j][j]
        if (densmatrix1re_from_file[j][j] > maxdens1_ess):
            maxdens1_ess = densmatrix1re_from_file[j][j]
        if (densmatrix2re_from_file[j][j] > maxdens2_ess):
            maxdens2_ess = densmatrix2re_from_file[j][j]
        if (densmatrix3re_from_file[j][j] > maxdens3_ess):
            maxdens3_ess = densmatrix3re_from_file[j][j]
        if (densmatrix4re_from_file[j][j] > maxdens4_ess):
            maxdens4_ess = densmatrix4re_from_file[j][j]

N = len(x)

colours1 = np.diag(densmatrix1re_from_file)
colours2 = np.diag(densmatrix2re_from_file)
colours3 = np.diag(densmatrix3re_from_file)
colours4 = np.diag(densmatrix4re_from_file)

max_bc1 = 0.0
max_bc2 = 0.0
max_bc3 = 0.0
max_bc4 = 0.0
for j in range(len(pos)):
    for k in range(len(pos)):
        if ( j!=k and dist(j,k) < 1.43):
            if (abs(2.0*(-2.7)*densmatrix1im_from_file[j][k]) > max_bc1):
                max_bc1 = 2.0*(-2.7)*densmatrix1im_from_file[j][k]
            if (abs(2.0*(-2.7)*densmatrix2im_from_file[j][k]) > max_bc2):
                max_bc2 = 2.0*(-2.7)*densmatrix2im_from_file[j][k]
            if (abs(2.0*(-2.7)*densmatrix3im_from_file[j][k]) > max_bc3):
                max_bc3 = 2.0*(-2.7)*densmatrix3im_from_file[j][k]
            if (abs(2.0*(-2.7)*densmatrix4im_from_file[j][k]) > max_bc4):
                max_bc4 = 2.0*(-2.7)*densmatrix4im_from_file[j][k]

# print max_bc1, max_bc2, max_bc3, max_bc4

cdict = {
'red'  :  ((0., 0., 0.), (0.5, 0.25, 0.25), (1., 1., 1.)),
'green':  ((0., 1., 1.), (0.7, 0.0, 0.5), (1., 1., 1.)),
'blue' :  ((0., 1., 1.), (0.5, 0.0, 0.0), (1., 1., 1.))
}

my_cmap = mpl.colors.LinearSegmentedColormap('my_colormap', cdict, 256)

radius = 0.7
delta = 1.0e-4
delta2 = 1.0e-1
patchesc1 = []
patchesc2 = []
patchesc3 = []
patchesc4 = []

patchesa1 = []
patchesa2 = []
patchesa3 = []
patchesa4 = []

clw = 0.0
alw = 0.0

for j, k, c in zip(range(N), range(N), mpl.cm.OrRd(colours1)):
    circle1 = Circle((x[j], z[k]), radius, fc=c, lw=clw)
    patchesc1.append(circle1)

for j, k, c in zip(range(N), range(N), mpl.cm.OrRd(colours2)):
    circle2 = Circle((x[j], z[k]), radius, fc=c, lw=clw)
    patchesc2.append(circle2)

for j, k, c in zip(range(N), range(N), mpl.cm.OrRd(colours3)):
    circle3 = Circle((x[j], z[k]), radius, fc=c, lw=clw)
    patchesc3.append(circle3)

for j, k, c in zip(range(N), range(N), mpl.cm.OrRd(colours4)):
    circle4 = Circle((x[j], z[k]), radius, fc=c, lw=clw)
    patchesc4.append(circle4)

il = 1.0 # Scaling exponent (arrow widths) for illustration purposes

for j in range(len(pos)):
    for k in range(len(pos)):
        if ( j!=k and dist(j,k) < 1.43 and (x[j] < x[k] or ( x[j] == x[k] and z[j] < z[k] ) ) ):

#            comp1 = 2.0*(-2.7)*densmatrix1im_from_file[j][k]/max_bc1
#            comp2 = 2.0*(-2.7)*densmatrix2im_from_file[j][k]/max_bc2
#            comp3 = 2.0*(-2.7)*densmatrix3im_from_file[j][k]/max_bc3
#            comp4 = 2.0*(-2.7)*densmatrix4im_from_file[j][k]/max_bc4

            comp1 = 2.0*(-2.7)*densmatrix1im_from_file[j][k]
            comp2 = 2.0*(-2.7)*densmatrix2im_from_file[j][k]
            comp3 = 2.0*(-2.7)*densmatrix3im_from_file[j][k]
            comp4 = 2.0*(-2.7)*densmatrix4im_from_file[j][k]

            if (2.0*(-2.7)*densmatrix1im_from_file[j][k] < 0):
#                arrow11 = Arrow(x[k]+(x[j]-x[k])*radius, z[k]+(z[j]-z[k])*radius, (x[j]-x[k])*(1-2*radius), (z[j]-z[k])*(1-2*radius), width=pow(abs(comp1),il), fc='gray', lw=alw)
                arrow11 = Arrow(x[k]+(x[j]-x[k])*delta2, z[k]+(z[j]-z[k])*delta2, (x[j]-x[k])*(1-2*delta2), (z[j]-z[k])*(1-2*delta2), width=pow(abs(comp1),il), fc='black', lw=alw)
                patchesa1.append(arrow11)
            else:
#                arrow21 = Arrow(x[j]+(x[k]-x[j])*radius, z[j]+(z[k]-z[j])*radius, (x[k]-x[j])*(1-2*radius), (z[k]-z[j])*(1-2*radius), width=pow(abs(comp1),il), fc='gray', lw=alw)
                arrow21 = Arrow(x[j]+(x[k]-x[j])*delta2, z[j]+(z[k]-z[j])*delta2, (x[k]-x[j])*(1-2*delta2), (z[k]-z[j])*(1-2*delta2), width=pow(abs(comp1),il), fc='black', lw=alw)
                patchesa1.append(arrow21)

            if (2.0*(-2.7)*densmatrix2im_from_file[j][k] < 0):
#                arrow12 = Arrow(x[k]+(x[j]-x[k])*radius, z[k]+(z[j]-z[k])*radius, (x[j]-x[k])*(1-2*radius), (z[j]-z[k])*(1-2*radius), width=pow(abs(comp2),il), fc='gray', lw=alw)
                arrow12 = Arrow(x[k]+(x[j]-x[k])*delta2, z[k]+(z[j]-z[k])*delta2, (x[j]-x[k])*(1-2*delta2), (z[j]-z[k])*(1-2*delta2), width=pow(abs(comp2),il), fc='black', lw=alw)
                patchesa2.append(arrow12)
            else:
#                arrow22 = Arrow(x[j]+(x[k]-x[j])*radius, z[j]+(z[k]-z[j])*radius, (x[k]-x[j])*(1-2*radius), (z[k]-z[j])*(1-2*radius), width=pow(abs(comp2),il), fc='gray', lw=alw)
                arrow22 = Arrow(x[j]+(x[k]-x[j])*delta2, z[j]+(z[k]-z[j])*delta2, (x[k]-x[j])*(1-2*delta2), (z[k]-z[j])*(1-2*delta2), width=pow(abs(comp2),il), fc='black', lw=alw)
                patchesa2.append(arrow22)

            if (2.0*(-2.7)*densmatrix3im_from_file[j][k] < 0):
#                arrow13 = Arrow(x[k]+(x[j]-x[k])*radius, z[k]+(z[j]-z[k])*radius, (x[j]-x[k])*(1-2*radius), (z[j]-z[k])*(1-2*radius), width=pow(abs(comp3),il), fc='gray', lw=alw)
                arrow13 = Arrow(x[k]+(x[j]-x[k])*delta2, z[k]+(z[j]-z[k])*delta2, (x[j]-x[k])*(1-2*delta2), (z[j]-z[k])*(1-2*delta2), width=pow(abs(comp3),il), fc='black', lw=alw)
                patchesa3.append(arrow13)
            else:
#                arrow23 = Arrow(x[j]+(x[k]-x[j])*radius, z[j]+(z[k]-z[j])*radius, (x[k]-x[j])*(1-2*radius), (z[k]-z[j])*(1-2*radius), width=pow(abs(comp3),il), fc='gray', lw=alw)
                arrow23 = Arrow(x[j]+(x[k]-x[j])*delta2, z[j]+(z[k]-z[j])*delta2, (x[k]-x[j])*(1-2*delta2), (z[k]-z[j])*(1-2*delta2), width=pow(abs(comp3),il), fc='black', lw=alw)
                patchesa3.append(arrow23)

            if (2.0*(-2.7)*densmatrix4im_from_file[j][k] < 0):
#                arrow14 = Arrow(x[k]+(x[j]-x[k])*radius, z[k]+(z[j]-z[k])*radius, (x[j]-x[k])*(1-2*radius), (z[j]-z[k])*(1-2*radius), width=pow(abs(comp4),il), fc='gray', lw=alw)
                arrow14 = Arrow(x[k]+(x[j]-x[k])*delta2, z[k]+(z[j]-z[k])*delta2, (x[j]-x[k])*(1-2*delta2), (z[j]-z[k])*(1-2*delta2), width=pow(abs(comp4),il), fc='black', lw=alw)
                patchesa4.append(arrow14)
            else:
#                arrow24 = Arrow(x[j]+(x[k]-x[j])*radius, z[j]+(z[k]-z[j])*radius, (x[k]-x[j])*(1-2*radius), (z[k]-z[j])*(1-2*radius), width=pow(abs(comp4),il), fc='gray', lw=alw)
                arrow24 = Arrow(x[j]+(x[k]-x[j])*delta2, z[j]+(z[k]-z[j])*delta2, (x[k]-x[j])*(1-2*delta2), (z[k]-z[j])*(1-2*delta2), width=pow(abs(comp4),il), fc='black', lw=alw)
                patchesa4.append(arrow24)

pc1 = PatchCollection(patchesc1, cmap=mpl.cm.OrRd, match_original=True)
pc1.set_alpha(0.4)
pa1 = PatchCollection(patchesa1, match_original=True)
pa1.set_alpha(0.8)

pc2 = PatchCollection(patchesc2, cmap=mpl.cm.OrRd, match_original=True)
pc2.set_alpha(0.4)
pa2 = PatchCollection(patchesa2, match_original=True)
pa2.set_alpha(0.8)

pc3 = PatchCollection(patchesc3, cmap=mpl.cm.OrRd, match_original=True)
pc3.set_alpha(0.4)
pa3 = PatchCollection(patchesa3, match_original=True)
pa3.set_alpha(0.8)

pc4 = PatchCollection(patchesc4, cmap=mpl.cm.OrRd, match_original=True)
pc4.set_alpha(0.4)
pa4 = PatchCollection(patchesa4, match_original=True)
pa4.set_alpha(0.8)

plt.rcParams.update(params)
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.set_title('$t = 0.0 \ \mathrm{fs}$', fontsize = 8)

ax1.add_collection(pc1)
ax1.add_collection(pa1)
ax1.set_xticks([])
ax1.set_yticks([])
plt.xlim([min(x)-2*radius,max(x)+2*radius])
plt.ylim([min(z)-2*radius,max(z)+2*radius])
ax1.set_aspect('equal')

pc1.set_array(colours1)
pc1.set_clim([float(mindens1_ess)-delta,float(maxdens1_ess)+delta])

divider1 = make_axes_locatable(plt.gca())
cax1 = divider1.append_axes("right", "5%", pad="3%")
plt.colorbar(pc1, cax1, ticks=[mindens1_ess, mindens1_ess + (maxdens1_ess - mindens1_ess)/4, mindens1_ess + 2*(maxdens1_ess - mindens1_ess)/4, mindens1_ess + 3*(maxdens1_ess - mindens1_ess)/4, maxdens1_ess], format='%0.4f')

#plt.show()
plt.savefig('dens_map1.pdf', bbox_inches='tight')

plt.rcParams.update(params)
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
ax2.set_title('$t = T/3$', fontsize = 8)

ax2.add_collection(pc2)
ax2.add_collection(pa2)
ax2.set_xticks([])
ax2.set_yticks([])
plt.xlim([min(x)-2*radius,max(x)+2*radius])
plt.ylim([min(z)-2*radius,max(z)+2*radius])
ax2.set_aspect('equal')

pc2.set_array(colours2)
pc2.set_clim([float(mindens2_ess)-delta,float(maxdens2_ess)+delta])

divider2 = make_axes_locatable(plt.gca())
cax2 = divider2.append_axes("right", "5%", pad="3%")
plt.colorbar(pc2, cax2, ticks=[mindens2_ess, mindens2_ess + (maxdens2_ess - mindens2_ess)/4, mindens2_ess + 2*(maxdens2_ess - mindens2_ess)/4, mindens2_ess + 3*(maxdens2_ess - mindens2_ess)/4, maxdens2_ess], format='%0.4f')

#plt.show()
plt.savefig('dens_map2.pdf', bbox_inches='tight')

plt.rcParams.update(params)
fig3 = plt.figure()
ax3 = fig3.add_subplot(111)
ax3.set_title('$t = 2T/3$', fontsize = 8)

ax3.add_collection(pc3)
ax3.add_collection(pa3)
ax3.set_xticks([])
ax3.set_yticks([])
plt.xlim([min(x)-2*radius,max(x)+2*radius])
plt.ylim([min(z)-2*radius,max(z)+2*radius])
ax3.set_aspect('equal')

pc3.set_array(colours3)
pc3.set_clim([float(mindens3_ess)-delta,float(maxdens3_ess)+delta])

divider3 = make_axes_locatable(plt.gca())
cax3 = divider3.append_axes("right", "5%", pad="3%")
plt.colorbar(pc3, cax3, ticks=[mindens3_ess, mindens3_ess + (maxdens3_ess - mindens3_ess)/4, mindens3_ess + 2*(maxdens3_ess - mindens3_ess)/4, mindens3_ess + 3*(maxdens3_ess - mindens3_ess)/4, maxdens3_ess], format='%0.4f')

#plt.show()
plt.savefig('dens_map3.pdf', bbox_inches='tight')

plt.rcParams.update(params)
fig4 = plt.figure()
ax4 = fig4.add_subplot(111)
ax4.set_title('$t = T$', fontsize = 8)

ax4.add_collection(pc4)
ax4.add_collection(pa4)
ax4.set_xticks([])
ax4.set_yticks([])
plt.xlim([min(x)-2*radius,max(x)+2*radius])
plt.ylim([min(z)-2*radius,max(z)+2*radius])
ax4.set_aspect('equal')

pc4.set_array(colours4)
pc4.set_clim([float(mindens4_ess)-delta,float(maxdens4_ess)+delta])

divider4 = make_axes_locatable(plt.gca())
cax4 = divider4.append_axes("right", "5%", pad="3%")
plt.colorbar(pc4, cax4, ticks=[mindens4_ess, mindens4_ess + (maxdens4_ess - mindens4_ess)/4, mindens4_ess + 2*(maxdens4_ess - mindens4_ess)/4, mindens4_ess + 3*(maxdens4_ess - mindens4_ess)/4, maxdens4_ess], format='%0.4f')

#plt.show()
plt.savefig('dens_map4.pdf', bbox_inches='tight')
