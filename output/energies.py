#!/usr/bin/python
import numpy as np
import sys
#from scipy.optimize import root

# System
Nwid = int(sys.argv[1])  # width
Nlen = int(sys.argv[2])  # length

def func(kappaj,j):
    return np.sin(kappaj*Nlen)/np.sin(kappaj*(Nlen+0.5))+2.0*np.cos((np.pi*float(j))/(2.0*(Nwid+1.0)))

dkk = (np.pi/np.sqrt(3))/Nlen

fs = open("energies.out", "w")

count = 0
for j in range(Nwid):
    kk = 0.0
    for k in range(Nlen):
        fs.write(str(float(count)) + "\t" + str('%.5f' %(2.7*np.sqrt(1.0+4.0*np.cos(np.pi*float(j)/(2.0*(float(Nwid)+1.0)))*np.cos(np.sqrt(3.0)*kk/2.0)+4.0*np.power(np.cos(np.pi*float(j)/(2.0*(float(Nwid)+1.0))),2)))) + "\n")
        count += 1
        fs.write(str(float(count)) + "\t" + str('%.5f' %(2.7*np.sqrt(1.0-4.0*np.cos(np.pi*float(j)/(2.0*(float(Nwid)+1.0)))*np.cos(np.sqrt(3.0)*kk/2.0)+4.0*np.power(np.cos(np.pi*float(j)/(2.0*(float(Nwid)+1.0))),2)))) + "\n")
        count += 1
        fs.write(str(float(count)) + "\t" + str('%.5f' %(-2.7*np.sqrt(1.0+4.0*np.cos(np.pi*float(j)/(2.0*(float(Nwid)+1.0)))*np.cos(np.sqrt(3.0)*kk/2.0)+4.0*np.power(np.cos(np.pi*float(j)/(2.0*(float(Nwid)+1.0))),2)))) + "\n")
        count += 1
        fs.write(str(float(count)) + "\t" + str('%.5f' %(-2.7*np.sqrt(1.0-4.0*np.cos(np.pi*float(j)/(2.0*(float(Nwid)+1.0)))*np.cos(np.sqrt(3.0)*kk/2.0)+4.0*np.power(np.cos(np.pi*float(j)/(2.0*(float(Nwid)+1.0))),2)))) + "\n")
        count += 1
        kk += dkk

fs.close()

Ee = np.genfromtxt('energies.out', dtype=None)[:,1]

sortcount = 0
fss = open("gs_energy_onipko.out", "w")
for j in range(len(Ee)):
    fss.write(str(float(sortcount)) + "\t" + str('%.5f' %np.sort(Ee)[j]) + "\n")
    sortcount += 1
fss.close()
