SRCDIR = src
OBJDIR = obj
SOURCE = $(wildcard $(SRCDIR)/*.cpp)
OBJECT = $(addprefix $(OBJDIR)/,$(notdir $(SOURCE:.cpp=.o)))
EXECUTABLE = qport

.PHONY: default desktop laptop mpsd electra taito sisu hydra qport clean

default:
	@cxx="mpiCC";\
	cflags="-g -c -O3 -Wno-write-strings";\
	lflags="-larmadillo -lgsl -lgslcblas";\
	$(MAKE) CXX="$$cxx" CFLAGS="$$cflags" LDFLAGS="$$lflags" $(EXECUTABLE)

desktop:
	@cxx="mpiCC";\
	cflags="-g -c -O3 -Wno-write-strings";\
	lflags="-larmadillo -lgsl -lgslcblas";\
	$(MAKE) CXX="$$cxx" CFLAGS="$$cflags" LDFLAGS="$$lflags" $(EXECUTABLE)

laptop:
	@cxx="mpiCC";\
	cflags="-g -c -O3 -Wno-write-strings";\
	lflags="-larmadillo -lgsl -lgslcblas";\
	$(MAKE) CXX="$$cxx" CFLAGS="$$cflags" LDFLAGS="$$lflags" $(EXECUTABLE)

jyu:
	@cxx="mpiCC";\
	cflags="-g -c -O3 -Wno-write-strings -I/home/rimasatu/repos/armadillo-6.100.0/local/usr/local/include";\
	lflags="-lgsl -fopenmp -lpthread -L/home/rimasatu/repos/intel/oneapi/mkl/2022.2.0/lib/intel64 -lmkl_blas95_lp64 -lmkl_lapack95_lp64 -lmkl_sequential -lmkl_core -lmkl_gf_lp64 -lmkl_gnu_thread -lmkl_rt -L/home/rimasatu/repos/armadillo-6.100.0/local/usr/local/lib64 -larmadillo";\
	$(MAKE) CXX="$$cxx" CFLAGS="$$cflags" LDFLAGS="$$lflags" $(EXECUTABLE)

$(EXECUTABLE): $(OBJECT)
	@echo ""
	@echo "Building the target $@ ..."
	$(CXX) $(OBJECT) $(LDFLAGS) -o $(EXECUTABLE)
	@echo ""
	@echo "Build complete!"
	@echo ""

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	@echo ""
	@echo "Compiling $< ..."
	$(CXX) $(CFLAGS) $< -o $@

clean:
	rm -rf $(OBJDIR)/*.o $(EXECUTABLE)
	rm -f output/*.out
