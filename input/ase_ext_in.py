#!/usr/bin/python
from ase.visualize import view
from ase.io import read, write
from ase.atoms import Atoms, string2symbols
from ase.data import covalent_radii
from ase.utils import gcd
import numpy as np
from numpy import sqrt, cos, pi, exp
import sys
import warnings

'''

E.g. command

    python ase_ext_in.py t 1 1 u 6 0.5 10.0
 
loads the structure: t=nanotube, takes 1st nearest neighbours into account,
shows it and sets gate=0.5 and B=10.0 in the nonequilibrium Hamiltonian

'''

# System
systype         = str(sys.argv[1])      # r or t (ribbon or tube)
nn              = int(sys.argv[2])      # 1, 2, 3
show            = int(sys.argv[3])      # 1
pert            = str(sys.argv[4])      # u or p
prof            = int(sys.argv[5])      # 1=constant, 2=linear, 3=shifted linear, 4=smooth, 5=gaussian, 6=bump
gate            = float(sys.argv[6])    # 0.5
B               = float(sys.argv[7])    # 0.1
echarge         = 1.60217657e-19 
hbar            = 1.05457173e-34
Ang_to_meter    = 1.00000000e-10

# Get structure
gnr = read('extstruct.xyz')
pos = gnr.get_positions()

# View structure
if(show == 1):
    view(gnr)

# Function for calculating distances
def dist(j,k):
    return sqrt(  (pos[j][0]-pos[k][0])**2
                + (pos[j][1]-pos[k][1])**2
                + (pos[j][2]-pos[k][2])**2 )

def phase(j,k):
    # Return the phases Phi_jk (Dimensionless)
    return ((B/2)*(echarge/hbar)*(Ang_to_meter**2)
               *(pos[k][0]*pos[j][1] - pos[j][0]*pos[k][1]))

# Tight-binding hopping parameters
if(nn == 1):
    hop1 = -2.70
    hop2 =  0.00
    hop3 =  0.00
elif(nn == 2):
    hop1 = -2.70
    hop2 = -0.20
    hop3 =  0.00
elif(nn == 3):
    hop1 = -2.70
    hop2 = -0.20
    hop3 = -0.18
else:
    print 'TB hoppings only for 1st, 2nd or 3rd nearest neighbours...'
    exit(1)

CH_hop   = 2.2*hop1
CH_phase = 0.0
C_onsite = -0.0*hop1
H_onsite = -0.66*hop1

# Bump function parameters:
barrwidth=6;
if(systype == 'r'):
    bumpa = (max(pos[:,0])-min(pos[:,0]))/barrwidth
elif(systype == 't'):
    bumpa = (max(pos[:,2])-min(pos[:,2]))/barrwidth
else:
    print 'Unknown system type, exiting...'
    exit(1)
bumpb = bumpa/5.0

# Hamiltonian matrix
Hure = [[0.0 for j in range(len(pos))] for k in range(len(pos))]
Huim = [[0.0 for j in range(len(pos))] for k in range(len(pos))]
Hpre = [[0.0 for j in range(len(pos))] for k in range(len(pos))]
Hpim = [[0.0 for j in range(len(pos))] for k in range(len(pos))]

# Open files for writing the Hamiltonian, coupling and bridge matrices
fu = open("matrix_unpert.in", "w")
fp = open("matrix_pert.in", "w")
profile = open("profile.out", "w")

# Loop over the matrix dimensions
for j in range(len(pos)):
    if (gnr[j].get_symbol() == 'H'):
        Hure[j][j] = np.real(H_onsite)
    if (gnr[j].get_symbol() == 'C'):
        Hure[j][j] = np.real(C_onsite)
    if(systype == 'r'):
        if(pert == 'u'):        # On-site terms to the gs Hamiltonian
            if(prof == 1):      # Constant gate
                Hure[j][j] += np.real( gate )
                profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 2):    # Linear gate
                Hure[j][j] += np.real( (-2.0*gate/(max(pos[:,0])-min(pos[:,0])))*(pos[j][0]-min(pos[:,0])) + gate )
                profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 3):    # Shifted linear gate
                if (pos[j][0] < (1.0/3.0)*max(pos[:,0])):
                    Hure[j][j] += np.real(gate)
                elif (pos[j][0] < (2.0/3.0)*max(pos[:,0])):
                    Hure[j][j] += np.real(3.0*gate - (6.0*gate/max(pos[:,0]))*pos[j][0])
                else:
                    Hure[j][j] += -np.real(gate)
                profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 4):    # Smooth potential
                if (pos[j][0] < (1.0/10.0)*max(pos[:,0])):
                    Hure[j][j] += np.real(gate)
                elif (pos[j][0] < (9.0/10.0)*max(pos[:,0])):
                    Hure[j][j] += np.real(gate*(cos(((10.0*pi)/(8.0*(max(pos[:,0])-min(pos[:,0]))))*pos[j][0]-pi/8.0)))
                else:
                    Hure[j][j] += -np.real(gate)
                profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 5):    # Gaussian barrier
                if( pos[j][0] > (1.0/3.0)*max(pos[:,0]) and pos[j][0] < (2.0/3.0)*max(pos[:,0]) ):
                    Hure[j][j] += np.real(gate*exp(-((pos[j][0]-max(pos[:,0])/2)**2/((max(pos[:,0])/10)**2))))
                profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 6):    # Bump function barrier
                if( abs(pos[j][0]-(max(pos[:,0])-min(pos[:,0]))/2) < bumpa ):
                    Hure[j][j] += np.real(gate*exp(-bumpb**2/(bumpa**2-(pos[j][0]-(max(pos[:,0])-min(pos[:,0]))/2)**2))/exp(-bumpb**2/bumpa**2))
                profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
            else:
                print 'Unknown profile, exiting...'
                exit(1)
        elif(pert == 'p'):      # On-site terms to the perturbative Hamiltonian
            if(prof == 1):      # Constant gate
                Hpre[j][j] += np.real( gate )
                profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 2):    # Linear gate
                Hpre[j][j] += np.real( (-2.0*gate/(max(pos[:,0])-min(pos[:,0])))*(pos[j][0]-min(pos[:,0])) + gate )
                profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 3):    # Shifted linear gate
                if (pos[j][0] < (1.0/3.0)*max(pos[:,0])):
                    Hpre[j][j] += np.real(gate)
                elif (pos[j][0] < (2.0/3.0)*max(pos[:,0])):
                    Hpre[j][j] += np.real(3.0*gate - (6.0*gate/max(pos[:,0]))*pos[j][0])
                else:
                    Hpre[j][j] += -np.real(gate)
                profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 4):    # Smooth potential
                if (pos[j][0] < (1.0/10.0)*max(pos[:,0])):
                    Hpre[j][j] += np.real(gate)
                elif (pos[j][0] < (9.0/10.0)*max(pos[:,0])):
                    Hpre[j][j] += np.real(gate*(cos(((10.0*pi)/(8.0*(max(pos[:,0])-min(pos[:,0]))))*pos[j][0]-pi/8.0)))
                else:
                    Hpre[j][j] += -np.real(gate)
                profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 5):    # Gaussian barrier
                if( pos[j][0] > (1.0/3.0)*max(pos[:,0]) and pos[j][0] < (2.0/3.0)*max(pos[:,0]) ):
                    Hpre[j][j] += np.real(gate*exp(-((pos[j][0]-max(pos[:,0])/2)**2/((max(pos[:,0])/10)**2))))
                profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 6):    # Bump function barrier
                if( abs(pos[j][0]-(max(pos[:,0])-min(pos[:,0]))/2) < bumpa ):
                    Hpre[j][j] += np.real(gate*exp(-bumpb**2/(bumpa**2-(pos[j][0]-(max(pos[:,0])-min(pos[:,0]))/2)**2))/exp(-bumpb**2/bumpa**2))
                profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
            else:
                print 'Unknown profile, exiting...'
                exit(1)
        else:
            print 'Unknown perturbation, exiting...'
            exit(1)
    elif(systype == 't'):
        if(pert == 'u'):        # On-site terms to the gs Hamiltonian
            if(prof == 1):      # Constant gate
                Hure[j][j] += np.real( gate )
                profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 2):    # Linear gate
                Hure[j][j] += np.real( (-2.0*gate/(max(pos[:,2])-min(pos[:,2])))*(pos[j][2]-min(pos[:,2])) + gate )
                profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 3):    # Shifted linear gate
                if (pos[j][2] < (1.0/3.0)*max(pos[:,2])):
                    Hure[j][j] += np.real(gate)
                elif (pos[j][2] < (2.0/3.0)*max(pos[:,2])):
                    Hure[j][j] += np.real(3.0*gate - (6.0*gate/max(pos[:,2]))*pos[j][2])
                else:
                    Hure[j][j] += -np.real(gate)
                profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 4):    # Smooth potential
                if (pos[j][2] < (1.0/10.0)*max(pos[:,2])):
                    Hure[j][j] += np.real(gate)
                elif (pos[j][2] < (9.0/10.0)*max(pos[:,2])):
                    Hure[j][j] += np.real(gate*(cos(((10.0*pi)/(8.0*(max(pos[:,2])-min(pos[:,2]))))*pos[j][2]-pi/8.0)))
                else:
                    Hure[j][j] += -np.real(gate)
                profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 5):    # Gaussian barrier
                if( pos[j][2] > (1.0/3.0)*max(pos[:,2]) and pos[j][2] < (2.0/3.0)*max(pos[:,2]) ):
                    Hure[j][j] += np.real(gate*exp(-((pos[j][2]-max(pos[:,2])/2)**2/((max(pos[:,2])/10)**2))))
                profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 6):    # Bump function barrier
                if( abs(pos[j][2]-(max(pos[:,2])-min(pos[:,2]))/2) < bumpa ):
                    Hure[j][j] += np.real(gate*exp(-bumpb**2/(bumpa**2-(pos[j][2]-(max(pos[:,2])-min(pos[:,2]))/2)**2))/exp(-bumpb**2/bumpa**2))
                profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
            else:
                print 'Unknown profile, exiting...'
                exit(1)
        elif(pert == 'p'):      # On-site terms to the perturbative Hamiltonian
            if(prof == 1):      # Constant gate
                Hpre[j][j] += np.real( gate )
                profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 2):    # Linear gate
                Hpre[j][j] += np.real( (-2.0*gate/(max(pos[:,2])-min(pos[:,2])))*(pos[j][2]-min(pos[:,2])) + gate )
                profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 3):    # Shifted linear gate
                if (pos[j][2] < (1.0/3.0)*max(pos[:,2])):
                    Hpre[j][j] += np.real(gate)
                elif (pos[j][2] < (2.0/3.0)*max(pos[:,2])):
                    Hpre[j][j] += np.real(3.0*gate - (6.0*gate/max(pos[:,2]))*pos[j][2])
                else:
                    Hpre[j][j] += -np.real(gate)
                profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 4):    # Smooth potential
                if (pos[j][2] < (1.0/10.0)*max(pos[:,2])):
                    Hpre[j][j] += np.real(gate)
                elif (pos[j][2] < (9.0/10.0)*max(pos[:,2])):
                    Hpre[j][j] += np.real(gate*(cos(((10.0*pi)/(8.0*(max(pos[:,2])-min(pos[:,2]))))*pos[j][2]-pi/8.0)))
                else:
                    Hpre[j][j] += -np.real(gate)
                profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 5):    # Gaussian barrier
                if( pos[j][2] > (1.0/3.0)*max(pos[:,2]) and pos[j][2] < (2.0/3.0)*max(pos[:,2]) ):
                    Hpre[j][j] += np.real(gate*exp(-((pos[j][2]-max(pos[:,2])/2)**2/((max(pos[:,2])/10)**2))))
                profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 6):    # Bump function barrier
                if( abs(pos[j][2]-(max(pos[:,2])-min(pos[:,2]))/2) < bumpa ):
                    Hpre[j][j] += np.real(gate*exp(-bumpb**2/(bumpa**2-(pos[j][2]-(max(pos[:,2])-min(pos[:,2]))/2)**2))/exp(-bumpb**2/bumpa**2))
                profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
            else:
                print 'Unknown profile, exiting...'
                exit(1)
        else:
            print 'Unknown perturbation, exiting...'
            exit(1)
    else:
        print 'Unknown system type, exiting...'
        exit(1)

    for k in range(len(pos)):
        # CH-distance = 1
        if ( j!=k and dist(j,k) <= 1.0 ):
            Hure[j][k] = np.real(CH_hop)
            Huim[j][k] = np.imag(CH_hop)
            if(pert == 'u'):        # Peierls phases terms to the gs Hamiltonian
                Hure[j][k] = np.real(CH_phase*np.exp(complex(0,1)*phase(j,k)))
                Huim[j][k] = np.imag(CH_phase*np.exp(complex(0,1)*phase(j,k)))
            elif(pert == 'p'):        # Peierls phases terms to the perturbative Hamiltonian
                Hpre[j][k] = np.real(CH_phase*np.exp(complex(0,1)*phase(j,k)))
                Hpim[j][k] = np.imag(CH_phase*np.exp(complex(0,1)*phase(j,k)))
            else:
                print 'Unknown perturbation, exiting...'
                exit(1)
        # Nearest neighbours <= 1.43 Angstrom
        elif ( j!=k and dist(j,k) < 1.43 ):
            Hure[j][k] = np.real(hop1)
            Huim[j][k] = np.imag(hop1)
            if(pert == 'u'):        # Peierls phases terms to the gs Hamiltonian
                Hure[j][k] = np.real(hop1*np.exp(complex(0,1)*phase(j,k)))
                Huim[j][k] = np.imag(hop1*np.exp(complex(0,1)*phase(j,k)))
            elif(pert == 'p'):        # Peierls phases terms to the perturbative Hamiltonian
                Hpre[j][k] = np.real(hop1*np.exp(complex(0,1)*phase(j,k)))
                Hpim[j][k] = np.imag(hop1*np.exp(complex(0,1)*phase(j,k)))
            else:
                print 'Unknown perturbation, exiting...'
                exit(1)
        # 2nd-nearest neighbours <= 2.46 Angstrom
        elif ( j!=k and dist(j,k) < 2.46 ):
            Hure[j][k] = np.real(hop2)
            Huim[j][k] = np.imag(hop2)
            if(pert == 'u'):        # Peierls phases terms to the gs Hamiltonian
                Hure[j][k] = np.real(hop2*np.exp(complex(0,1)*phase(j,k)))
                Huim[j][k] = np.imag(hop2*np.exp(complex(0,1)*phase(j,k)))
            elif(pert == 'p'):        # Peierls phases terms to the perturbative Hamiltonian
                Hpre[j][k] = np.real(hop2*np.exp(complex(0,1)*phase(j,k)))
                Hpim[j][k] = np.imag(hop2*np.exp(complex(0,1)*phase(j,k)))
            else:
                print 'Unknown perturbation, exiting...'
                exit(1)
        # 3rd-nearest neighbours <= 2.85 Angstrom
        elif ( j!=k and dist(j,k) < 2.85 ):
            Hure[j][k] = np.real(hop3)
            Huim[j][k] = np.imag(hop3)
            if(pert == 'u'):        # Peierls phases terms to the gs Hamiltonian
                Hure[j][k] = np.real(hop3*np.exp(complex(0,1)*phase(j,k)))
                Huim[j][k] = np.imag(hop3*np.exp(complex(0,1)*phase(j,k)))
            elif(pert == 'p'):        # Peierls phases terms to the perturbative Hamiltonian
                Hpre[j][k] = np.real(hop3*np.exp(complex(0,1)*phase(j,k)))
                Hpim[j][k] = np.imag(hop3*np.exp(complex(0,1)*phase(j,k)))
            else:
                print 'Unknown perturbation, exiting...'
                exit(1)
        fu.write(str(j) + "\t" + str(k) + "\t" + str(Hure[j][k]) + "\t" + str(Huim[j][k]) + "\n")
        fp.write(str(j) + "\t" + str(k) + "\t" + str(Hpre[j][k]) + "\t" + str(Hpim[j][k]) + "\n")

# Close the files
fu.close()
fp.close()
profile.close()

fm = open("misc.in", "w")
fm.write(str(len(pos)) + "\n")
fm.write(str(hop1) + "\n")
fm.write(str(0) + "\n")
fm.write(str(0) + "\n")
fm.close()

# Print the size of the ribbon in nm
if(show == 1):
    print 'Dimensions (nm): ', (max(pos[:,0])-min(pos[:,0]))/10, '', (max(pos[:,1])-min(pos[:,1]))/10, '', (max(pos[:,2])-min(pos[:,2]))/10

fs = open("size", "w")
fs.write("Dimensions (nm): " + str((max(pos[:,0])-min(pos[:,0]))/10) + " " + str((max(pos[:,1])-min(pos[:,1]))/10) + " " + str((max(pos[:,2])-min(pos[:,2]))/10))

# Write the structure to a file
write('structure.xyz', gnr)
