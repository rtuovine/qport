#!/usr/bin/python
from ase.visualize import view
from ase.structure import graphene_nanoribbon
from ase.io import read, write
from ase.atoms import Atoms, string2symbols
from ase.data import covalent_radii
from ase.utils import gcd
import numpy as np
from numpy import sqrt, cos, pi, exp
import sys
import warnings

'''

E.g. command

    python ase_gnr_in.py a 3 14 3 1 0 p 1 0.5 10.0
 
gives aGNR of width 3, length 14, 3rd nn-hopping, shows the structure, 
does not load structure from external file, and puts constant perturbative
gate=0.5 and B=10.0

'''

# System
systype         = str(sys.argv[1])      # 'z' 'a'
width           = int(sys.argv[2])      # 3
length          = int(sys.argv[3])      # 14
nn              = int(sys.argv[4])      # 3
show            = int(sys.argv[5])      # 1
ext             = int(sys.argv[6])      # 0 or 1
pert            = str(sys.argv[7])      # u or p
prof            = int(sys.argv[8])      # 1=constant, 2=linear, 3=shifted linear, 4=smooth, 5=gaussian, 6=bump, 7=bias
gate            = float(sys.argv[9])    # 0.5
B               = float(sys.argv[10])    # 0.1
echarge         = 1.60217657e-19 
hbar            = 1.05457173e-34
Ang_to_meter    = 1.00000000e-10

# Get structure
'''

graphene_nanoribbon(n, m, type='zigzag', saturated=False, C_H=1.09, C_C=1.42, vacuum=2.5, magnetic=None,
                    initial_mag=1.12, sheet=False, main_element='C', saturate_element='H', vacc=None)

n:              int     The width of the nanoribbon.
m:              int     The length of the nanoribbon.
type:           str     The orientation of the ribbon. Must be either 'zigzag' or 'armchair'.
saturated:      bool    If 'True', hydrogen atoms are placed along the edge.
C_H:            float   Carbon-hydrogen bond length. Default: 1.09 Angstrom.
C_C:            float   Carbon-carbon bond length. Default: 1.42 Angstrom.
vacuum:         float   Amount of vacuum added to both sides. Default 2.5 Angstrom.
magnetic:       bool    Make the edges magnetic.
initial_mag:    float   Magnitude of magnetic moment if magnetic=True.
sheet:          bool    If True, make an infinite sheet instead of a ribbon.

'''

if(ext == 0):
    if(systype == 'a'):
        gnr = graphene_nanoribbon(width, length, type='armchair', saturated=False)
    elif(systype == 'z'):
        gnr = graphene_nanoribbon(width, length, type='zigzag', saturated=False)
    # Get coordinates
    pos = gnr.get_positions()


elif(ext == 1):
    gnr = read('extstruct.xyz')
    pos = gnr.get_positions()

else:
    print 'Structure from external file: choose either 1 or 0'
    exit(1)

# View structure
if(show == 1):
    view(gnr)

# Function for calculating distances
def dist(j,k):
    return sqrt(  (pos[j][0]-pos[k][0])**2
                + (pos[j][1]-pos[k][1])**2
                + (pos[j][2]-pos[k][2])**2 )

def phase(j,k):
    # Return the phases Phi_jk (Dimensionless)
    if(ext == 0):
        return ((B/2)*(echarge/hbar)*(Ang_to_meter**2)
               *(pos[k][0]*pos[j][2] - pos[j][0]*pos[k][2]))
    elif(ext == 1):
        return ((B/2)*(echarge/hbar)*(Ang_to_meter**2)
               *(pos[k][0]*pos[j][1] - pos[j][0]*pos[k][1]))
    else:
        print 'Structure from external file: choose either 1 or 0'
        exit(1)

# Tight-binding hopping parameters
if(nn == 1):
    hop1 = -2.70
    hop2 =  0.00
    hop3 =  0.00
elif(nn == 2):
    hop1 = -2.70
    hop2 = -0.20
    hop3 =  0.00
elif(nn == 3):
    hop1 = -2.70
    hop2 = -0.20
    hop3 = -0.18
else:
    print 'TB hoppings only for 1st, 2nd or 3rd nearest neighbours...'
    exit(1)

# Bump function parameters:
barrwidth=4;
if(ext == 0):
    bumpa = (max(pos[:,2])-min(pos[:,2]))/barrwidth
elif(ext == 1):
    bumpa = (max(pos[:,0])-min(pos[:,0]))/barrwidth
bumpb = bumpa/10.0

# Hamiltonian matrix
Hure = [[0.0 for j in range(len(pos))] for k in range(len(pos))]
Huim = [[0.0 for j in range(len(pos))] for k in range(len(pos))]
Hpre = [[0.0 for j in range(len(pos))] for k in range(len(pos))]
Hpim = [[0.0 for j in range(len(pos))] for k in range(len(pos))]

# Open file for writing the matrix
fu = open("matrix_unpert.in", "w")
fp = open("matrix_pert.in", "w")
profile = open("profile.out", "w")
localfield = open("local.out", "w")
locdiv = 3

# Loop over the matrix dimensions
for j in range(len(pos)):
    if(ext == 0):
        if(pert == 'u'):        # On-site terms to the gs Hamiltonian
            if(prof == 1):      # Constant gate
                Hure[j][j] = np.real( gate )
                profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 2):    # Linear gate
                Hure[j][j] = np.real( (-2.0*gate/(max(pos[:,2])-min(pos[:,2])))*(pos[j][2]-min(pos[:,2])) + gate )
                profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 3):    # Shifted linear gate
                if (pos[j][2] < (1.0/3.0)*max(pos[:,2])):
                    Hure[j][j] = np.real(gate)
                elif (pos[j][2] < (2.0/3.0)*max(pos[:,2])):
                    Hure[j][j] = np.real(3.0*gate - (6.0*gate/max(pos[:,2]))*pos[j][2])
                else:
                    Hure[j][j] = -np.real(gate)
                profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 4):    # Smooth potential
                if (pos[j][2] < (1.0/10.0)*max(pos[:,2])):
                    Hure[j][j] = np.real(gate)
                elif (pos[j][2] < (9.0/10.0)*max(pos[:,2])):
                    Hure[j][j] = np.real(gate*(cos(((10.0*pi)/(8.0*(max(pos[:,2])-min(pos[:,2]))))*pos[j][2]-pi/8.0)))
                else:
                    Hure[j][j] = -np.real(gate)
                profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 5):    # Gaussian barrier
                if( pos[j][2] > (1.0/3.0)*max(pos[:,2]) and pos[j][2] < (2.0/3.0)*max(pos[:,2]) ):
                    Hure[j][j] = np.real(gate*exp(-((pos[j][2]-max(pos[:,2])/2)**2/((max(pos[:,2])/10)**2))))
                profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 6):    # Bump function barrier
                if( abs(pos[j][2]-(max(pos[:,2])-min(pos[:,2]))/2) < bumpa ):
                    Hure[j][j] = np.real(gate*exp(-bumpb**2/(bumpa**2-(pos[j][2]-(max(pos[:,2])-min(pos[:,2]))/2)**2))/exp(-bumpb**2/bumpa**2))
                profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 7):    # Left side of the central region gated with the lead bias
                for l in range(locdiv):
                    if(l == 0):
                        if( pos[j][2] < (max(pos[:,2])-min(pos[:,2]))/locdiv ):
                            Hure[j][j] = np.real(gate)
                            profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
                    elif(l%2 != 0):
                        if( pos[j][2] >= float(l)*(max(pos[:,2])-min(pos[:,2]))/locdiv and pos[j][2] < float(l+1)*(max(pos[:,2])-min(pos[:,2]))/locdiv ):
                            Hure[j][j] = np.real(0.0)
                            profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
                    else:
                        if( pos[j][2] >= float(l)*(max(pos[:,2])-min(pos[:,2]))/locdiv and pos[j][2] < float(l+1)*(max(pos[:,2])-min(pos[:,2]))/locdiv ):
                            Hure[j][j] = np.real(0.0)
                            profile.write(str(pos[j][2]) + "\t" + str(Hure[j][j]) + "\n")
            else:
                print 'Unknown profile, exiting...'
                exit(1)
        elif(pert == 'p'):      # On-site terms to the perturbative Hamiltonian
            if(prof == 1):      # Constant gate
                Hpre[j][j] = np.real( gate )
                profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 2):    # Linear gate
                Hpre[j][j] = np.real( (-2.0*gate/(max(pos[:,2])-min(pos[:,2])))*(pos[j][2]-min(pos[:,2])) + gate )
                profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 3):    # Shifted linear gate
                if (pos[j][2] < (1.0/3.0)*max(pos[:,2])):
                    Hpre[j][j] = np.real(gate)
                elif (pos[j][2] < (2.0/3.0)*max(pos[:,2])):
                    Hpre[j][j] = np.real(3.0*gate - (6.0*gate/max(pos[:,2]))*pos[j][2])
                else:
                    Hpre[j][j] = -np.real(gate)
                profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 4):    # Smooth potential
                if (pos[j][2] < (1.0/10.0)*max(pos[:,2])):
                    Hpre[j][j] = np.real(gate)
                elif (pos[j][2] < (9.0/10.0)*max(pos[:,2])):
                    Hpre[j][j] = np.real(gate*(cos(((10.0*pi)/(8.0*(max(pos[:,2])-min(pos[:,2]))))*pos[j][2]-pi/8.0)))
                else:
                    Hpre[j][j] = -np.real(gate)
                profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 5):    # Gaussian barrier
                if( pos[j][2] > (1.0/3.0)*max(pos[:,2]) and pos[j][2] < (2.0/3.0)*max(pos[:,2]) ):
                    Hpre[j][j] = np.real(gate*exp(-((pos[j][2]-max(pos[:,2])/2)**2/((max(pos[:,2])/10)**2))))
                profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 6):    # Bump function barrier
                if( abs(pos[j][2]-(max(pos[:,2])-min(pos[:,2]))/2) < bumpa ):
                    Hpre[j][j] = np.real(gate*exp(-bumpb**2/(bumpa**2-(pos[j][2]-(max(pos[:,2])-min(pos[:,2]))/2)**2))/exp(-bumpb**2/bumpa**2))
                profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 7):    # Left side of the central region gated with the lead bias
                for l in range(locdiv):
                    if(l == 0):
                        if( pos[j][2] < (max(pos[:,2])-min(pos[:,2]))/locdiv ):
                            Hpre[j][j] = np.real(gate)
                            profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
                    elif(l%2 != 0):
                        if( pos[j][2] >= float(l)*(max(pos[:,2])-min(pos[:,2]))/locdiv and pos[j][2] < float(l+1)*(max(pos[:,2])-min(pos[:,2]))/locdiv ):
                            Hpre[j][j] = np.real(0.0)
                            profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
                    else:
                        if( pos[j][2] >= float(l)*(max(pos[:,2])-min(pos[:,2]))/locdiv and pos[j][2] < float(l+1)*(max(pos[:,2])-min(pos[:,2]))/locdiv ):
                            Hpre[j][j] = np.real(0.0)
                            profile.write(str(pos[j][2]) + "\t" + str(Hpre[j][j]) + "\n")
            else:
                print 'Unknown profile, exiting...'
                exit(1)
        else:
            print 'Unknown perturbation, exiting...'
            exit(1)

        for k in range(len(pos)):
            # Nearest neighbours <= 1.43 Angstrom
            if ( j!=k and dist(j,k) < 1.43 ):
                Hure[j][k] = np.real(hop1)
                Huim[j][k] = np.imag(hop1)
                if(pert == 'u'):        # Peierls phases terms to the gs Hamiltonian
                    if( pos[j][2] < 2.0*(max(pos[:,2])-min(pos[:,2]))/barrwidth ):  # Magnetic confinement
                        Hure[j][k] = np.real(hop1*np.exp(complex(0,1)*phase(j,k)))
                        Huim[j][k] = np.imag(hop1*np.exp(complex(0,1)*phase(j,k)))
                    else:
                        Hure[j][k] = hop1
                        Huim[j][k] = 0.0
                elif(pert == 'p'):        # Peierls phases terms to the perturbative Hamiltonian
                    if( pos[j][2] < 2.0*(max(pos[:,2])-min(pos[:,2]))/barrwidth ):  # Magnetic confinement
                        Hpre[j][k] = np.real(hop1*np.exp(complex(0,1)*phase(j,k)))
                        Hpim[j][k] = np.imag(hop1*np.exp(complex(0,1)*phase(j,k)))
                    else:
                        Hpre[j][k] = hop1
                        Hpim[j][k] = 0.0
                else:
                    print 'Unknown perturbation, exiting...'
                    exit(1)

                for l in range(locdiv):
                    if(l == 0):
                        if( pos[j][2] < (max(pos[:,2])-min(pos[:,2]))/locdiv ):
                            print l, j, pos[j][2], 'S'
                            localfield.write(str(j) + "\t" + str(1) + "\t" + str(pos[j][2]) + "\n")
                    elif(l%2 != 0):
                        if( pos[j][2] >= float(l)*(max(pos[:,2])-min(pos[:,2]))/locdiv and pos[j][2] < float(l+1)*(max(pos[:,2])-min(pos[:,2]))/locdiv ):
                            print l, j, pos[j][2], 'N'
                            localfield.write(str(j) + "\t" + str(0) + "\t" + str(pos[j][2]) + "\n")
                    else:
                        if( pos[j][2] >= float(l)*(max(pos[:,2])-min(pos[:,2]))/locdiv and pos[j][2] < float(l+1)*(max(pos[:,2])-min(pos[:,2]))/locdiv ):
                            print l, j, pos[j][2], 'S'
                            localfield.write(str(j) + "\t" + str(2) + "\t" + str(pos[j][2]) + "\n")

            # 2nd-nearest neighbours <= 2.46 Angstrom
            elif ( j!=k and dist(j,k) < 2.46 ):
                Hure[j][k] = np.real(hop2)
                Huim[j][k] = np.imag(hop2)
                if(pert == 'u'):        # Peierls phases terms to the gs Hamiltonian
                    if( pos[j][2] < 2.0*(max(pos[:,2])-min(pos[:,2]))/barrwidth ):  # Magnetic confinement
                        Hure[j][k] = np.real(hop2*np.exp(complex(0,1)*phase(j,k)))
                        Huim[j][k] = np.imag(hop2*np.exp(complex(0,1)*phase(j,k)))
                    else:
                        Hure[j][k] = hop2
                        Huim[j][k] = 0.0
                elif(pert == 'p'):        # Peierls phases terms to the perturbative Hamiltonian
                    if( pos[j][2] < 2.0*(max(pos[:,2])-min(pos[:,2]))/barrwidth ):  # Magnetic confinement
                        Hpre[j][k] = np.real(hop2*np.exp(complex(0,1)*phase(j,k)))
                        Hpim[j][k] = np.imag(hop2*np.exp(complex(0,1)*phase(j,k)))
                    else:
                        Hpre[j][k] = hop2
                        Hpim[j][k] = 0.0
                else:
                    print 'Unknown perturbation, exiting...'
                    exit(1)
            # 3rd-nearest neighbours <= 2.85 Angstrom
            elif ( j!=k and dist(j,k) < 2.85 ):
                Hure[j][k] = np.real(hop3)
                Huim[j][k] = np.imag(hop3)
                if(pert == 'u'):        # Peierls phases terms to the gs Hamiltonian
                    if( pos[j][2] < 2.0*(max(pos[:,2])-min(pos[:,2]))/barrwidth ):  # Magnetic confinement
                        Hure[j][k] = np.real(hop3*np.exp(complex(0,1)*phase(j,k)))
                        Huim[j][k] = np.imag(hop3*np.exp(complex(0,1)*phase(j,k)))
                    else:
                        Hure[j][k] = hop3
                        Huim[j][k] = 0.0
                elif(pert == 'p'):        # Peierls phases terms to the perturbative Hamiltonian
                    if( pos[j][2] < 2.0*(max(pos[:,2])-min(pos[:,2]))/barrwidth ):  # Magnetic confinement
                        Hpre[j][k] = np.real(hop3*np.exp(complex(0,1)*phase(j,k)))
                        Hpim[j][k] = np.imag(hop3*np.exp(complex(0,1)*phase(j,k)))
                    else:
                        Hpre[j][k] = hop3
                        Hpim[j][k] = 0.0
                else:
                    print 'Unknown perturbation, exiting...'
                    exit(1)

            fu.write(str(j) + "\t" + str(k) + "\t" + str(Hure[j][k]) + "\t" + str(Huim[j][k]) + "\n")
            fp.write(str(j) + "\t" + str(k) + "\t" + str(Hpre[j][k]) + "\t" + str(Hpim[j][k]) + "\n")

    elif(ext == 1):
        if(pert == 'u'):        # On-site terms to the gs Hamiltonian
            if(prof == 1):      # Constant gate
                Hure[j][j] = np.real( gate )
                profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 2):    # Linear gate
                Hure[j][j] = np.real( (-2.0*gate/(max(pos[:,0])-min(pos[:,0])))*(pos[j][0]-min(pos[:,0])) + gate )
                profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 3):    # Shifted linear gate
                if (pos[j][0] < (1.0/3.0)*max(pos[:,0])):
                    Hure[j][j] = np.real(gate)
                elif (pos[j][0] < (2.0/3.0)*max(pos[:,0])):
                    Hure[j][j] = np.real(3.0*gate - (6.0*gate/max(pos[:,0]))*pos[j][0])
                else:
                    Hure[j][j] = -np.real(gate)
                profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 4):    # Smooth potential
                if (pos[j][0] < (1.0/10.0)*max(pos[:,0])):
                    Hure[j][j] = np.real(gate)
                elif (pos[j][0] < (9.0/10.0)*max(pos[:,0])):
                    Hure[j][j] = np.real(gate*(cos(((10.0*pi)/(8.0*(max(pos[:,0])-min(pos[:,0]))))*pos[j][0]-pi/8.0)))
                else:
                    Hure[j][j] = -np.real(gate)
                profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 5):    # Gaussian barrier
                if( pos[j][0] > (1.0/3.0)*max(pos[:,0]) and pos[j][0] < (2.0/3.0)*max(pos[:,0]) ):
                    Hure[j][j] = np.real(gate*exp(-((pos[j][0]-max(pos[:,0])/2)**2/((max(pos[:,0])/10)**2))))
                profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 6):    # Bump function barrier
                if( abs(pos[j][0]-(max(pos[:,0])-min(pos[:,0]))/2) < bumpa ):
                    Hure[j][j] = np.real(gate*exp(-bumpb**2/(bumpa**2-(pos[j][0]-(max(pos[:,0])-min(pos[:,0]))/2)**2))/exp(-bumpb**2/bumpa**2))
                profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
            elif(prof == 7):    # Left side of the central region gated with the lead bias
                for l in range(locdiv):
                    if(l == 0):
                        if( pos[j][0] < (max(pos[:,0])-min(pos[:,0]))/locdiv ):
                            Hure[j][j] = np.real(gate)
                            profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
                    elif(l%2 != 0):
                        if( pos[j][0] >= float(l)*(max(pos[:,0])-min(pos[:,0]))/locdiv and pos[j][0] < float(l+1)*(max(pos[:,0])-min(pos[:,0]))/locdiv ):
                            Hure[j][j] = np.real(0.0)
                            profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
                    else:
                        if( pos[j][0] >= float(l)*(max(pos[:,0])-min(pos[:,0]))/locdiv and pos[j][0] < float(l+1)*(max(pos[:,0])-min(pos[:,0]))/locdiv ):
                            Hure[j][j] = np.real(0.0)
                            profile.write(str(pos[j][0]) + "\t" + str(Hure[j][j]) + "\n")
            else:
                print 'Unknown profile, exiting...'
                exit(1)
        elif(pert == 'p'):      # On-site terms to the perturbative Hamiltonian
            if(prof == 1):      # Constant gate
                Hpre[j][j] = np.real( gate )
                profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 2):    # Linear gate
                Hpre[j][j] = np.real( (-2.0*gate/(max(pos[:,0])-min(pos[:,0])))*(pos[j][0]-min(pos[:,0])) + gate )
                profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 3):    # Shifted linear gate
                if (pos[j][0] < (1.0/3.0)*max(pos[:,0])):
                    Hpre[j][j] = np.real(gate)
                elif (pos[j][0] < (2.0/3.0)*max(pos[:,0])):
                    Hpre[j][j] = np.real(3.0*gate - (6.0*gate/max(pos[:,0]))*pos[j][0])
                else:
                    Hpre[j][j] = -np.real(gate)
                profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 4):    # Smooth potential
                if (pos[j][0] < (1.0/10.0)*max(pos[:,0])):
                    Hpre[j][j] = np.real(gate)
                elif (pos[j][0] < (9.0/10.0)*max(pos[:,0])):
                    Hpre[j][j] = np.real(gate*(cos(((10.0*pi)/(8.0*(max(pos[:,0])-min(pos[:,0]))))*pos[j][0]-pi/8.0)))
                else:
                    Hpre[j][j] = -np.real(gate)
                profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 5):    # Gaussian barrier
                if( pos[j][0] > (1.0/3.0)*max(pos[:,0]) and pos[j][0] < (2.0/3.0)*max(pos[:,0]) ):
                    Hpre[j][j] = np.real(gate*exp(-((pos[j][0]-max(pos[:,0])/2)**2/((max(pos[:,0])/10)**2))))
                profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 6):    # Bump function barrier
                if( abs(pos[j][0]-(max(pos[:,0])-min(pos[:,0]))/2) < bumpa ):
                    Hpre[j][j] = np.real(gate*exp(-bumpb**2/(bumpa**2-(pos[j][0]-(max(pos[:,0])-min(pos[:,0]))/2)**2))/exp(-bumpb**2/bumpa**2))
                profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
            elif(prof == 7):    # Left side of the central region gated with the lead bias
                for l in range(locdiv):
                    if(l == 0):
                        if( pos[j][0] < (max(pos[:,0])-min(pos[:,0]))/locdiv ):
                            Hpre[j][j] = np.real(gate)
                            profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
                    elif(l%2 != 0):
                        if( pos[j][0] >= float(l)*(max(pos[:,0])-min(pos[:,0]))/locdiv and pos[j][0] < float(l+1)*(max(pos[:,0])-min(pos[:,0]))/locdiv ):
                            Hpre[j][j] = np.real(0.0)
                            profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
                    else:
                        if( pos[j][0] >= float(l)*(max(pos[:,0])-min(pos[:,0]))/locdiv and pos[j][0] < float(l+1)*(max(pos[:,0])-min(pos[:,0]))/locdiv ):
                            Hpre[j][j] = np.real(0.0)
                            profile.write(str(pos[j][0]) + "\t" + str(Hpre[j][j]) + "\n")
            else:
                print 'Unknown profile, exiting...'
                exit(1)
        else:
            print 'Unknown perturbation, exiting...'
            exit(1)

        for k in range(len(pos)):
            # Nearest neighbours <= 1.43 Angstrom
            if ( j!=k and dist(j,k) < 1.43 ):
                Hure[j][k] = np.real(hop1)
                Huim[j][k] = np.imag(hop1)
                if(pert == 'u'):        # Peierls phases terms to the gs Hamiltonian
                    if( pos[j][0] < 2.0*(max(pos[:,0])-min(pos[:,0]))/barrwidth ):  # Magnetic confinement
                        #print pos[j][0]
                        Hure[j][k] = np.real(hop1*np.exp(complex(0,1)*phase(j,k)))
                        Huim[j][k] = np.imag(hop1*np.exp(complex(0,1)*phase(j,k)))
                        #localfield.write(str(j) + "\t" + str(0) + "\n")
                    else:
                        Hure[j][k] = hop1
                        Huim[j][k] = 0.0
                        #localfield.write(str(j) + "\t" + str(1) + "\n")
                elif(pert == 'p'):        # Peierls phases terms to the perturbative Hamiltonian
                    if( pos[j][0] < 2.0*(max(pos[:,0])-min(pos[:,0]))/barrwidth ):  # Magnetic confinement
                        Hpre[j][k] = np.real(hop1*np.exp(complex(0,1)*phase(j,k)))
                        Hpim[j][k] = np.imag(hop1*np.exp(complex(0,1)*phase(j,k)))
                        #localfield.write(str(j) + "\t" + str(0) + "\n")
                    else:
                        Hpre[j][k] = hop1
                        Hpim[j][k] = 0.0
                        #localfield.write(str(j) + "\t" + str(1) + "\n")
                else:
                    print 'Unknown perturbation, exiting...'
                    exit(1)

                for l in range(locdiv):
                    if(l == 0):
                        if( pos[j][0] < (max(pos[:,0])-min(pos[:,0]))/locdiv ):
                            print l, j, pos[j][0], 'S'
                            localfield.write(str(j) + "\t" + str(1) + "\n")
                    elif(l%2 != 0):
                        if( pos[j][0] >= float(l)*(max(pos[:,0])-min(pos[:,0]))/locdiv and pos[j][0] < float(l+1)*(max(pos[:,0])-min(pos[:,0]))/locdiv ):
                            print l, j, pos[j][0], 'N'
                            localfield.write(str(j) + "\t" + str(0) + "\n")
                    else:
                        if( pos[j][0] >= float(l)*(max(pos[:,0])-min(pos[:,0]))/locdiv and pos[j][0] < float(l+1)*(max(pos[:,0])-min(pos[:,0]))/locdiv ):
                            print l, j, pos[j][0], 'S'
                            localfield.write(str(j) + "\t" + str(1) + "\n")
#                if( pos[j][1] > 25.0 ):
#                    localfield.write(str(j) + "\t" + str(1) + "\n")
#                elif( pos[j][1] < 17.0 ):
#                    localfield.write(str(j) + "\t" + str(2) + "\n")
#                else:
#                    localfield.write(str(j) + "\t" + str(0) + "\n")

            # 2nd-nearest neighbours <= 2.46 Angstrom
            elif ( j!=k and dist(j,k) < 2.46 ):
                Hure[j][k] = np.real(hop2)
                Huim[j][k] = np.imag(hop2)
                if(pert == 'u'):        # Peierls phases terms to the gs Hamiltonian
                    if( pos[j][0] < 2.0*(max(pos[:,0])-min(pos[:,0]))/barrwidth ):  # Magnetic confinement
                        Hure[j][k] = np.real(hop2*np.exp(complex(0,1)*phase(j,k)))
                        Huim[j][k] = np.imag(hop2*np.exp(complex(0,1)*phase(j,k)))
                    else:
                        Hure[j][k] = hop2
                        Huim[j][k] = 0.0
                elif(pert == 'p'):        # Peierls phases terms to the perturbative Hamiltonian
                    if( pos[j][0] < 2.0*(max(pos[:,0])-min(pos[:,0]))/barrwidth ):  # Magnetic confinement
                        Hpre[j][k] = np.real(hop2*np.exp(complex(0,1)*phase(j,k)))
                        Hpim[j][k] = np.imag(hop2*np.exp(complex(0,1)*phase(j,k)))
                    else:
                        Hpre[j][k] = hop2
                        Hpim[j][k] = 0.0
                else:
                    print 'Unknown perturbation, exiting...'
                    exit(1)
            # 3rd-nearest neighbours <= 2.85 Angstrom
            elif ( j!=k and dist(j,k) < 2.85 ):
                Hure[j][k] = np.real(hop3)
                Huim[j][k] = np.imag(hop3)
                if(pert == 'u'):        # Peierls phases terms to the gs Hamiltonian
                    if( pos[j][0] < 2.0*(max(pos[:,0])-min(pos[:,0]))/barrwidth ):  # Magnetic confinement
                        Hure[j][k] = np.real(hop3*np.exp(complex(0,1)*phase(j,k)))
                        Huim[j][k] = np.imag(hop3*np.exp(complex(0,1)*phase(j,k)))
                    else:
                        Hure[j][k] = hop3
                        Huim[j][k] = 0.0
                elif(pert == 'p'):        # Peierls phases terms to the perturbative Hamiltonian
                    if( pos[j][0] < 2.0*(max(pos[:,0])-min(pos[:,0]))/barrwidth ):  # Magnetic confinement
                        Hpre[j][k] = np.real(hop3*np.exp(complex(0,1)*phase(j,k)))
                        Hpim[j][k] = np.imag(hop3*np.exp(complex(0,1)*phase(j,k)))
                    else:
                        Hpre[j][k] = hop3
                        Hpim[j][k] = 0.0
                else:
                    print 'Unknown perturbation, exiting...'
                    exit(1)
            fu.write(str(j) + "\t" + str(k) + "\t" + str(Hure[j][k]) + "\t" + str(Huim[j][k]) + "\n")
            fp.write(str(j) + "\t" + str(k) + "\t" + str(Hpre[j][k]) + "\t" + str(Hpim[j][k]) + "\n")

    else:
        print 'Structure from external file: choose either 1 or 0'
        exit(1)



# Close the file
fu.close()
fp.close()
profile.close()
localfield.close()

# Open file for writing the coupling and bridge matrices
fl = open("lcouple.in", "w")
fr = open("rcouple.in", "w")
fbone = open("bridgeone.in", "w")
fbtwo = open("bridgetwo.in", "w")
fbthree = open("bridgethree.in", "w")
countl = 0
countr = 0
countb = 0
delta = 1.0e-2

for j in range(len(pos)):
    if(ext == 0):
        if( abs(pos[j][2] - min(pos[:,2])) < delta ):
            fl.write(str(j) + "\n")
            countl += 1
            if(systype == 'a'):
                fbone.write(str((j+(2*length-1)/2)) + "\n")
                fbone.write(str((j+(2*length-1)/2+1)) + "\n")
                fbtwo.write(str((j+(4*length-1)/2)) + "\n")
                fbtwo.write(str((j+(4*length-1)/2+1)) + "\n")
                fbthree.write(str((j+(6*length-1)/2)) + "\n")
                fbthree.write(str((j+(6*length-1)/2+1)) + "\n")
                countb += 2
            elif(systype == 'z'):
                if(j % 2 == 0):
                    fbone.write(str((j+(length-1)/2)) + "\n")
                    fbone.write(str((j+(length-1)/2+1)) + "\n")
                    fbtwo.write(str((j+(2*length-1)/2)) + "\n")
                    fbtwo.write(str((j+(2*length-1)/2+1)) + "\n")
                    fbthree.write(str((j+(3*length-1)/2)) + "\n")
                    fbthree.write(str((j+(3*length-1)/2+1)) + "\n")
                    countb += 2
                else:
                    fbone.write(str(int(j-1.5*length-1)) + "\n")
                    fbone.write(str(int(j-1.5*length)) + "\n")
                    fbtwo.write(str(j-length-1) + "\n")
                    fbtwo.write(str(j-length) + "\n")
                    fbthree.write(str(int(j-0.5*length-1)) + "\n")
                    fbthree.write(str(int(j-0.5*length)) + "\n")
                    countb += 2
        if( abs(pos[j][2] - max(pos[:,2])) < delta ):
            fr.write(str(j) + "\n")
            countr += 1
    elif(ext == 1):
        if( abs(pos[j][0] - min(pos[:,0])) < delta ):
            fl.write(str(j) + "\n")
            countl += 1
        if(systype == 'a'):     # TODO: cleaner expressions
            if( abs(pos[j][0] - (max(pos[:,0])-min(pos[:,0]))/2) < delta ):
                fbone.write(str(int(j-((width+1)*3+width-1)*(length/4)-((width+1)*3+width-2))) + "\n")
                fbone.write(str(int(j-((width+1)*3+width-1)*(length/4))) + "\n")
                fbtwo.write(str(j-((width+1)*3+width-2)) + "\n")
                fbtwo.write(str(j) + "\n")
                fbthree.write(str(int(j+((width+1)*3+width-1)*(length/4)-((width+1)*3+width-2))) + "\n")
                fbthree.write(str(int(j+((width+1)*3+width-1)*(length/4))) + "\n")
                countb += 2
        if(systype == 'z'):     # TODO: these might not work
            if( abs(pos[j][0] - (max(pos[:,0])+1.23-min(pos[:,0]))/2 + 1.23) < delta  and j%2!=0):
                fbone.write(str(j) + "\n")
                fbone.write(str(j-1) + "\n")
                fbone.write(str(j+1) + "\n")
                fbone.write(str(j+2) + "\n")
                fbtwo.write(str((j+(4*length-1)/2)) + "\n")
                fbtwo.write(str((j+(4*length-1)/2+1)) + "\n")
                fbtwo.write(str((j+(4*length-1)/2)) + "\n")
                fbtwo.write(str((j+(4*length-1)/2+1)) + "\n")
                fbthree.write(str((j+(4*length-1)/2)) + "\n")
                fbthree.write(str((j+(4*length-1)/2+1)) + "\n")
                fbthree.write(str((j+(4*length-1)/2)) + "\n")
                fbthree.write(str((j+(4*length-1)/2+1)) + "\n")
                countb += 4
        if( abs(pos[j][0] - max(pos[:,0])) < delta ):
            fr.write(str(j) + "\n")
            countr += 1
    else:
        print 'Structure from external file: choose either 1 or 0'
        exit(1)

if(ext == 0):
    if(countl != width):
        print countl
    if(countr != width):
        print countr
    if(countb != 2*width):
        print countb, "bridge"
elif(ext == 1):
    if(systype == 'a'):
        if(countl != width):
            print countl
        if(countr != width):
            print countr
        if(length%2 != 0):
            if(countb != 2*(width+1)):
                print countb, "bridge"
        elif(length%2 == 0):
            if(countb != 2*width):
                print countb, "bridge"
    elif(systype == 'z'):
        if(countl != (width+1)):
            print countl
        if(countr != (width+1)):
            print countr
        if(countb != 2*(width+1)):
            print countb, "bridge"
else:
    print 'Structure from external file: choose either 1 or 0'
    exit(1)

# Close the files
fl.close()
fr.close()
fbone.close()
fbtwo.close()
fbthree.close()

fm = open("misc.in", "w")
fm.write(str(len(pos)) + "\n")
fm.write(str(hop1) + "\n")
fm.write(str(width) + "\n")
if(ext == 0):
    fm.write(str(2*width) + "\n")
elif(ext == 1):
    if(systype == 'a'):
        if(length%2 != 0):
            fm.write(str(2*(width+1)) + "\n")
        elif(length%2 == 0):
            fm.write(str(2*width) + "\n")
    elif(systype == 'z'):
        fm.write(str(2*width) + "\n")
fm.close()

# Print the size of the ribbon in nm
if(show == 1):
    print 'Dimensions (nm): ', (max(pos[:,0])-min(pos[:,0]))/10, '', (max(pos[:,1])-min(pos[:,1]))/10, '', (max(pos[:,2])-min(pos[:,2]))/10
    if(systype == 'a'):
        print 'Magnetic flux through the whole structure: ', B*(2*width*length-width-length+1)*1.5*sqrt(3.0)*(1.42e-10)**2, ' Wb; ', (B*(2*width*length-width-length+1)*1.5*sqrt(3.0)*(1.42e-10)**2)/(4.13566923956e-15), ' per flux quantum.'
    elif(systype == 'z'):
        print 'Magnetic flux through the whole structure: ', B*(width-1)*(length-1)*1.5*sqrt(3.0)*(1.42e-10)**2, ' Wb; ', (B*(width-1)*(length-1)*1.5*sqrt(3.0)*(1.42e-10)**2)/(4.13566923956e-15), ' per flux quantum.'

fs = open("size", "w")
fs.write("Dimensions (nm): " + str((max(pos[:,0])-min(pos[:,0]))/10) + " " + str((max(pos[:,1])-min(pos[:,1]))/10) + " " + str((max(pos[:,2])-min(pos[:,2]))/10) + "\n")
if(systype == 'a'):
    fs.write("Magnetic flux through the whole structure: " + str(B*(2*width*length-width-length+1)*1.5*sqrt(3.0)*(1.42e-10)**2) + " Wb; " + str((B*(2*width*length-width-length+1)*1.5*sqrt(3.0)*(1.42e-10)**2)/(4.13566923956e-15)) + " per flux quantum.")
elif(systype == 'z'):
    fs.write("Magnetic flux through the whole structure: " + str(B*(width-1)*(length-1)*1.5*sqrt(3.0)*(1.42e-10)**2) + " Wb; " + str((B*(width-1)*(length-1)*1.5*sqrt(3.0)*(1.42e-10)**2)/(4.13566923956e-15)) + " per flux quantum.")
fs.close()

# Write the structure to a file
write('structure.xyz', gnr)
