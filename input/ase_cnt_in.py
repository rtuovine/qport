#!/usr/bin/python
from ase.visualize import view
from ase.structure import nanotube
from ase.io import read, write
from ase.atoms import Atoms, string2symbols
from ase.data import covalent_radii
from ase.utils import gcd
import numpy as np
from numpy import sqrt, cos, pi
import sys
import warnings

'''

E.g. command

    python ase_cnt_in.py c 3 0 4 3 1 0 0.5 10.0
 
gives CNT(n,m) of n=3, m=0, length=4, 3rd nn-hopping, shows the structure, 
does not load structure from external file, and puts gate=0.5 and B=10.0

'''

# System
systype         = str(sys.argv[1])      # 'c'
n               = int(sys.argv[2])      # 1,2,3, ...
m               = int(sys.argv[3])      # 0,1,2, ...
tubelen         = int(sys.argv[4])      # 1,2,3, ...
nn              = int(sys.argv[5])      # 1, 2 or 3
show            = int(sys.argv[6])      # 0 or 1
ext             = int(sys.argv[7])      # 0 or 1
gate            = float(sys.argv[8])    # 0.5
B               = float(sys.argv[9])    # 0.1
echarge         = 1.60217657e-19 
hbar            = 1.05457173e-34
Ang_to_meter    = 1.00000000e-10

# Get structure
'''

nanotube(n, m, length=1, bond=1.42, symbol='C', verbose=False)

'''

if(ext == 0):
    if(systype == 'c'):
        cnt = nanotube(n, m, length=tubelen, bond=1.42, symbol='C', verbose=False)
    # Get coordinates
    pos = cnt.get_positions()


elif(ext == 1):
    cnt = read('extstruct.xyz')
    pos = cnt.get_positions()

else:
    print 'Structure from external file: choose either 1 or 0'
    exit(1)

# View structure
if(show == 1):
    view(cnt)

# Function for calculating distances
def dist(j,k):
    return sqrt(  (pos[j][0]-pos[k][0])**2
                + (pos[j][1]-pos[k][1])**2
                + (pos[j][2]-pos[k][2])**2 )

def phase(j,k): # TODO: Needs thinking
    # Return the phases Phi_jk (Dimensionless)
    if(ext == 0):
        return ((B/2)*(echarge/hbar)*(Ang_to_meter**2)
               *(pos[k][0]*pos[j][2] - pos[j][0]*pos[k][2]))
    elif(ext == 1):
        return ((B/2)*(echarge/hbar)*(Ang_to_meter**2)
               *(pos[k][0]*pos[j][1] - pos[j][0]*pos[k][1]))
    else:
        print 'Structure from external file: choose either 1 or 0'
        exit(1)

# Tight-binding hopping parameters
if(nn == 1):
    hop1 = -2.70
    hop2 =  0.00
    hop3 =  0.00
elif(nn == 2):
    hop1 = -2.70
    hop2 = -0.20
    hop3 =  0.00
elif(nn == 3):
    hop1 = -2.70
    hop2 = -0.20
    hop3 = -0.18
else:
    print 'TB hoppings only for 1st, 2nd or 3rd nearest neighbours...'
    exit(1)

# Hamiltonian matrix
Hure = [[0.0 for j in range(len(pos))] for k in range(len(pos))]
Huim = [[0.0 for j in range(len(pos))] for k in range(len(pos))]
Hpre = [[0.0 for j in range(len(pos))] for k in range(len(pos))]
Hpim = [[0.0 for j in range(len(pos))] for k in range(len(pos))]

# Open file for writing the matrix
fu = open("matrix_unpert.in", "w")
fp = open("matrix_pert.in", "w")

# Loop over the matrix dimensions
for j in range(len(pos)):
    if(ext == 0):
        # Linear gate
#        Hpre[j][j] = np.real( (-2.0*gate/(max(pos[:,2])-min(pos[:,2])))*(pos[j][2]-min(pos[:,2])) + gate )
        # Constant gate
#        Hpre[j][j] = np.real( gate )
        # Another linear gate
        '''
        if (pos[j][2] < (1.0/3.0)*max(pos[:,2])):
            Hpre[j][j] = np.real(gate)
        elif (pos[j][2] < (2.0/3.0)*max(pos[:,2])):
            Hpre[j][j] = np.real(3.0*gate - (6.0*gate/max(pos[:,2]))*pos[j][2])
        else:
            Hpre[j][j] = -np.real(gate)
        '''
        # Smooth potential
#        '''
        if (pos[j][2] < (1.0/10.0)*max(pos[:,2])):
            Hpre[j][j] = np.real(gate)
        elif (pos[j][2] < (9.0/10.0)*max(pos[:,2])):
            Hpre[j][j] = np.real(gate*(cos(((10.0*pi)/(8.0*(max(pos[:,2])-min(pos[:,2]))))*pos[j][2]-pi/8.0)))
        else:
            Hpre[j][j] = -np.real(gate)
#        '''
    elif(ext == 1):
        # Linear gate
        Hpre[j][j] = np.real( (-2.0*gate/(max(pos[:,0])-min(pos[:,0])))*(pos[j][0]-min(pos[:,0])) + gate )
        # Constant gate
#        Hpre[j][j] = np.real( gate )
        # Another linear gate
        '''
        if (pos[j][0] < (1.0/3.0)*max(pos[:,0])):
            Hpre[j][j] = np.real(gate)
        elif (pos[j][0] < (2.0/3.0)*max(pos[:,0])):
            Hpre[j][j] = np.real(3.0*gate - (6.0*gate/max(pos[:,0]))*pos[j][0])
        else:
            Hpre[j][j] = -np.real(gate)
        '''
        # Smooth potential
        '''
        if (pos[j][0] < (1.0/10.0)*max(pos[:,0])):
            Hpre[j][j] = np.real(gate)
        elif (pos[j][0] < (9.0/10.0)*max(pos[:,0])):
            Hpre[j][j] = np.real(gate*(cos(((10.0*pi)/(8.0*(max(pos[:,0])-min(pos[:,0]))))*pos[j][0]-pi/8.0)))
        else:
            Hpre[j][j] = -np.real(gate)
        '''
    else:
        print 'Structure from external file: choose either 1 or 0'
        exit(1)

    for k in range(len(pos)):
        # Nearest neighbours <= 1.43 Angstrom
        if ( j!=k and dist(j,k) < 1.43 ):
            Hure[j][k] = np.real(hop1)
            Huim[j][k] = np.imag(hop1)
            Hpre[j][k] = np.real(hop1*np.exp(complex(0,1)*phase(j,k)))
            Hpim[j][k] = np.imag(hop1*np.exp(complex(0,1)*phase(j,k)))
        # 2nd-nearest neighbours <= 2.46 Angstrom
        elif ( j!=k and dist(j,k) < 2.46 ):
            Hure[j][k] = np.real(hop2)
            Huim[j][k] = np.imag(hop2)
            Hpre[j][k] = np.real(hop2*np.exp(complex(0,1)*phase(j,k)))
            Hpim[j][k] = np.imag(hop2*np.exp(complex(0,1)*phase(j,k)))
        # 3rd-nearest neighbours <= 2.85 Angstrom
        elif ( j!=k and dist(j,k) < 2.85 ):
            Hure[j][k] = np.real(hop3)
            Huim[j][k] = np.imag(hop3)
            Hpre[j][k] = np.real(hop3*np.exp(complex(0,1)*phase(j,k)))
            Hpim[j][k] = np.imag(hop3*np.exp(complex(0,1)*phase(j,k)))
        fu.write(str(j) + "\t" + str(k) + "\t" + str(Hure[j][k]) + "\t" + str(Huim[j][k]) + "\n")
        fp.write(str(j) + "\t" + str(k) + "\t" + str(Hpre[j][k]) + "\t" + str(Hpim[j][k]) + "\n")

# Close the file
fu.close()
fp.close()

# Open file for writing the coupling and bridge matrices
fl = open("lcouple.in", "w")
fr = open("rcouple.in", "w")
fbone = open("bridgeone.in", "w")
fbtwo = open("bridgetwo.in", "w")
fbthree = open("bridgethree.in", "w")
countl = 0
countr = 0
countbone = 0
countbtwo = 0
countbthree = 0
delta = 1.0e-2

for j in range(len(pos)):
    if(ext == 0):
        if( abs(pos[j][2] - min(pos[:,2])) < delta ):
            fl.write(str(j) + "\n")
            countl += 1
        if( abs( (pos[j][2]/(max(pos[:,2]) - min(pos[:,2]))) - 0.25) < delta ):
            fbone.write(str(j) + "\n")
            fbone.write(str(j) + "\n")
            countbone += 2
        if( abs( (pos[j][2]/(max(pos[:,2]) - min(pos[:,2]))) - 0.5) < delta ):
            fbtwo.write(str(j) + "\n")
            fbtwo.write(str(j) + "\n")
            countbtwo += 2
        if( abs( (pos[j][2]/(max(pos[:,2]) - min(pos[:,2]))) - 0.75) < delta ):
            fbthree.write(str(j) + "\n")
            fbthree.write(str(j) + "\n")
            countbthree += 2
        if( abs(pos[j][2] - max(pos[:,2])) < delta ):
            fr.write(str(j) + "\n")
            countr += 1
    elif(ext == 1): # TODO
        print "TODO"
    else:
        print 'Structure from external file: choose either 1 or 0'
        exit(1)

if(ext == 0):
    if(countl != n):
        print countl
    if(countr != n):
        print countr
    if(countbone != 2*n):
        print countbone, "bridgeone"
    if(countbtwo != 2*n):
        print countbtwo, "bridgetwo"
    if(countbthree != 2*n):
        print countbtwo, "bridgethree"
elif(ext == 1): # TODO
    print "TODO"
else:
    print 'Structure from external file: choose either 1 or 0'
    exit(1)

# Close the files
fl.close()
fr.close()
fbone.close()
fbtwo.close()
fbthree.close()

fm = open("misc.in", "w")
fm.write(str(len(pos)) + "\n")
fm.write(str(hop1) + "\n")
fm.write(str(n) + "\n")
if(ext == 0):
    fm.write(str(2*n) + "\n")
elif(ext == 1): #TODO
    print "TODO"
fm.close()

# Print the size of the ribbon in nm
if(show == 1):
    print 'Dimensions (nm): ', (max(pos[:,0])-min(pos[:,0]))/10, '', (max(pos[:,1])-min(pos[:,1]))/10, '', (max(pos[:,2])-min(pos[:,2]))/10

fs = open("size", "w")
fs.write("Dimensions (nm): " + str((max(pos[:,0])-min(pos[:,0]))/10) + " " + str((max(pos[:,1])-min(pos[:,1]))/10) + " " + str((max(pos[:,2])-min(pos[:,2]))/10))

# Write the structure to a file
write('structure.xyz', cnt)
