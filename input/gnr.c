/*

Imported Claudia's code

Compile with: gcc gnr.c -o gnr -lm

*/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#define NN 40000
#define ON   1
#define OFF  0

int main(int argc, const char *argv[])
{
    if (argc == 1 )
    {
        printf(" - - - - \n");
        printf("H O W   T O   U S E\n");
        printf(" - - - - \n");
        printf("\nThis utility adds cluster to specified cartecian\n");
        printf("Example:\n\n");
        printf("add-layer original.xyz adding.xyz -x 3.0 -y 4.0 -z 5.0 \n");
        printf("\nAbove case, adding.xyz is translated for 3.0 in x-dirextion, ");
        printf(",4.0 in y-dirextion, and 5.0 in z-direction and added to original.xyz\n");
        printf("This code is not flexible so that please keep this format.\n");
    }

    int atom_num; // the number of atoms
    int w;
    int l;
    double xyz1[3];
    double xyz2[3];
    double xyz3[3];
    double xyz4[3];
    double xyz5[3];
    double xyz6[3];
    double r = 1.42;
    double x1 = 0.0;
    double y1 = 0.0;
    double z1 = 0.0;
    double A = 1;
    double a = 0.0;

    unsigned char a_flag, z_flag, h_flag, r_flag=OFF;
    char str1[256];
    char element[NN][10];
    int  i, j, k, n;
    int amari = 0;

    // option check
    for(i=1; i<argc; i++)
    {
        if(strcmp("-a",argv[i]) == 0)
        {
            a_flag = ON;
            w = atoi(argv[i+1]);
            l = atoi(argv[i+2]);
        }
        else if(strcmp("-z",argv[i]) == 0)
        {
            z_flag = ON;
            w = atoi(argv[i+1]);
            l = atoi(argv[i+2]);
        }
        else if(strcmp("-h",argv[i]) == 0)
        {
            h_flag = ON;
        }
        else if(strcmp("-r",argv[i]) == 0)
        {
            r_flag = ON;
            if(i+1 > argc)
            {
                printf("there is no argument after -r option.\n");
                exit(0);
            }
            r = atof(argv[i+1]);
        }
    }

    // option check
    if(z_flag == ON && a_flag == ON)
    {
        printf("-a and -z options coexist. \n");
        exit(0);
    }

    if(w == 0  || l == 0 )
    {
        printf("width or length of gnr is zero!\n");
        exit(0);
    }

    if(z_flag == ON)
    {
        k = (w-((w-1)%2))/2;
        amari = (w-1)%2;

        xyz1[0] = 0.0;              xyz1[1] = 0.0;        xyz1[2] = 0.0;
        xyz2[0] = -sqrt(3.0)/2.0*r; xyz2[1] = -1.0/2.0*r; xyz2[2] = 0.0;
        xyz3[0] = -sqrt(3.0)/2.0*r; xyz3[1] = -3.0/2.0*r; xyz3[2] = 0.0;
        xyz4[0] = 0.0;              xyz4[1] = -2.0*r;     xyz4[2] = 0.0;

        A = sqrt(3)*r;
        if(h_flag == ON)
        {
            atom_num = l*(2*w+4);
        }
        else
        {
            atom_num = l*(2*w+2);
        }
        printf("%d\n\n",atom_num);

        for(i=0; i<l; i++)   // loop for length
        {
            // w = 1
            printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz1[0]+A*i, xyz1[1], xyz1[2]);
            printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz2[0]+A*i, xyz2[1], xyz2[2]);
            printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz3[0]+A*i, xyz3[1], xyz3[2]);
            printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz4[0]+A*i, xyz4[1], xyz4[2]);

            a = 0.0;
            for(j=0; j<k; j++)    // loop for width
            {
                a = (double)(j+1)*3.0*r; // printf("a=%lf\n",a);
                printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz1[0]+A*i, xyz1[1]-a, xyz1[2]);
                printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz2[0]+A*i, xyz2[1]-a, xyz2[2]);
                printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz3[0]+A*i, xyz3[1]-a, xyz3[2]);
                printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz4[0]+A*i, xyz4[1]-a, xyz4[2]);
            }
            if(amari == 1)
            {
                a = (double)(k+1)*3.0*r;
                printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz1[0]+A*i, xyz1[1]-a, xyz1[2]);
                printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz2[0]+A*i, xyz2[1]-a, xyz2[2]);

                if(h_flag == ON)
                {
                    printf(" H  %13.8lf  %13.8lf  %13.8lf\n",xyz1[0]+A*i, xyz1[1]+1.089, xyz1[2]);
                    printf(" H  %13.8lf  %13.8lf  %13.8lf\n",xyz2[0]+A*i, xyz2[1]-a-1.089, xyz2[2]);
                }
            }
            if(h_flag == ON && amari == 0)
            {
                printf(" H  %13.8lf  %13.8lf  %13.8lf\n",xyz1[0]+A*i, xyz1[1]+1.089, xyz1[2]);
                printf(" H  %13.8lf  %13.8lf  %13.8lf\n",xyz4[0]+A*i, xyz4[1]-a-1.089, xyz4[2]);
            }
        }
    }

    if(a_flag == ON)
    {
        k = (w-((w-1)%2))/2;
        amari = (w-1)%2;

        xyz1[0] = 0.0;              xyz1[1] = 0.0;              xyz1[2] = 0.0;
        xyz2[0] = r;                xyz2[1] = 0.0;              xyz2[2] = 0.0;
        xyz3[0] = -1.0/2.0*r;       xyz3[1] = -sqrt(3.0)/2.0*r; xyz3[2] = 0.0;
        xyz4[0] = 3.0/2.0*r;        xyz4[1] = -sqrt(3.0)/2.0*r; xyz4[2] = 0.0;
        xyz5[0] = 0.0;              xyz5[1] = -sqrt(3.0)*r;     xyz5[2] = 0.0;
        xyz6[0] = r;                xyz6[1] = -sqrt(3.0)*r;     xyz6[2] = 0.0;

        A = 3.0*r;
        if(h_flag == ON)
        {
            atom_num = l*(2*w+8);
        }
        else
        {
            atom_num = l*(2*w+4);
        }
        printf("%d\n\n",atom_num);
        for(i=0; i<l; i++)   // loop for length
        {
            // w = 1
            printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz1[0]+A*i, xyz1[1], xyz1[2]);
            printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz2[0]+A*i, xyz2[1], xyz2[2]);
            printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz3[0]+A*i, xyz3[1], xyz3[2]);
            printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz4[0]+A*i, xyz4[1], xyz4[2]);
            printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz5[0]+A*i, xyz5[1], xyz5[2]);
            printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz6[0]+A*i, xyz6[1], xyz6[2]);

            a = 0.0;
            for(j=0; j<k; j++)    // loop for width
            {
                a = (double)(j+1)*sqrt(3.0)*r; // printf("a=%lf\n",a);
                printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz3[0]+A*i, xyz3[1]-a, xyz3[2]);
                printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz4[0]+A*i, xyz4[1]-a, xyz4[2]);
                printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz5[0]+A*i, xyz5[1]-a, xyz5[2]);
                printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz6[0]+A*i, xyz6[1]-a, xyz6[2]);
            }
            if(amari == 1)
            {
                a = (double)(k+1)*sqrt(3.0)*r;
                printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz3[0]+A*i, xyz3[1]-a, xyz3[2]);
                printf(" C  %13.8lf  %13.8lf  %13.8lf\n",xyz4[0]+A*i, xyz4[1]-a, xyz4[2]);

                if(h_flag == ON)
                {
                    printf(" H  %13.8lf  %13.8lf  %13.8lf\n",xyz1[0]+A*i-0.5*1.089, xyz1[1]+sqrt(3.0)*0.5*1.089, xyz1[2]);
                    printf(" H  %13.8lf  %13.8lf  %13.8lf\n",xyz2[0]+A*i+0.5*1.089, xyz2[1]+sqrt(3.0)*0.5*1.089, xyz2[2]);
                    printf(" H  %13.8lf  %13.8lf  %13.8lf\n",xyz3[0]+A*i+0.5*1.089, xyz3[1]-a-sqrt(3.0)*0.5*1.089, xyz3[2]);
                    printf(" H  %13.8lf  %13.8lf  %13.8lf\n",xyz4[0]+A*i-0.5*1.089, xyz4[1]-a-sqrt(3.0)*0.5*1.089, xyz4[2]);
                }
            }
            if(h_flag == ON && amari == 0)
            {
                printf(" H  %13.8lf  %13.8lf  %13.8lf\n",xyz1[0]+A*i-0.5*1.089, xyz1[1]+sqrt(3.0)*0.5*1.089, xyz1[2]);
                printf(" H  %13.8lf  %13.8lf  %13.8lf\n",xyz2[0]+A*i+0.5*1.089, xyz2[1]+sqrt(3.0)*0.5*1.089, xyz2[2]);
                printf(" H  %13.8lf  %13.8lf  %13.8lf\n",xyz5[0]+A*i-0.5*1.089, xyz5[1]-a-sqrt(3.0)*0.5*1.089, xyz5[2]);
                printf(" H  %13.8lf  %13.8lf  %13.8lf\n",xyz6[0]+A*i+0.5*1.089, xyz6[1]-a-sqrt(3.0)*0.5*1.089, xyz6[2]);
            }
        }
    }
    printf("\n");
}   
