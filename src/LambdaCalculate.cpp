#include "Main.hpp"
cx_double LambdaCalculate(void *param)
{
    ParamsL &funcpara= *reinterpret_cast<ParamsL *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    unsigned int alpha = funcpara.alpha;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;
    cx_vec eps_peff_l = funcpara.eps_peff_l;
    double mu = funcpara.mu;
    int biastype = funcpara.biastype;
    int pert = funcpara.pert;
    double bias_strength = funcpara.bias_strength;
    int finitet = funcpara.finitet;
    double beta = funcpara.beta;

    cx_double I(0.0,1.0);

    if(pert == 0 && finitet == 0) return (1.0
                                          /(2.0*datum::pi*(conj(eps_ueff_l(k)) - eps_ueff_l(j)))
                                         )
                                         *(
                                           log(conj(eps_ueff_l(k))-mu-V(alpha,biastype,bias_strength))
                                          -log(eps_ueff_l(j)-mu-V(alpha,biastype,bias_strength))
                                          );


    if(pert == 0 && finitet == 1)
    {
        return (I/(conj(eps_ueff_l(k)) - eps_ueff_l(j)))
              *(
                Fermi(beta,conj(eps_ueff_l(k))-mu-V(alpha,biastype,bias_strength))
               -(I/(2.0*datum::pi))*(Psi(-(conj(eps_ueff_l(k))-mu-V(alpha,biastype,bias_strength)),beta)
                                     -Psi(-(eps_ueff_l(j)-mu-V(alpha,biastype,bias_strength)),beta))
                );
    }

    if(pert == 1 && finitet == 0) return (1.0
                                          /(2.0*datum::pi*(conj(eps_peff_l(k)) - eps_peff_l(j)))
                                         )
                                         *(
                                           log(conj(eps_peff_l(k))-mu-V(alpha,biastype,bias_strength))
                                          -log(eps_peff_l(j)-mu-V(alpha,biastype,bias_strength))
                                          );

    if(pert == 1 && finitet == 1)
    {
        return (I/(conj(eps_peff_l(k)) - eps_peff_l(j)))
              *(
                Fermi(beta,conj(eps_peff_l(k))-mu-V(alpha,biastype,bias_strength))
               -(I/(2.0*datum::pi))*(Psi(-(conj(eps_peff_l(k))-mu-V(alpha,biastype,bias_strength)),beta)
                                     -Psi(-(eps_peff_l(j)-mu-V(alpha,biastype,bias_strength)),beta))
                );
    }
}
