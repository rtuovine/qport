#include "Main.hpp"
void CalculateGS(int nsite, int ntstep, double &mu, double gamma_strength, double bias_strength, 
                 cx_mat &Hu, int systype, int finitet, double beta, vec &gs_eigval, cx_mat &gs_eigvec)
{
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if(rank == 0) 
    {
        cout << endl;
        cout << "Calculating the ground state..." << endl;
    }

    const double difference = 1.0e-8;

    int bandstructure=0;
    ofstream bandfile("output/bandstructure.out", ios::out);
    double jj, Nss;

// Diagonalize the ground state Hamiltonian
    gs_eigval.zeros();
    gs_eigvec.zeros();
    eig_sym(gs_eigval, gs_eigvec, Hu);

    if(rank == 0) 
    {
    // Write the eigenvalues into file "gs_energy.out"
        double wmax(0.0);
        ofstream gs_energy("output/gs_energy.out", ios::out);
        gs_energy <<"#k" << "\t" << "epsilon_k" << endl;
        for(unsigned int k=0; k<nsite; k++)
        {
            gs_energy << setprecision(5) << scientific << k << "\t" << gs_eigval(k) << endl;
            if(gs_eigval(k) > wmax) wmax = gs_eigval(k);
        }
        wmax*=1.5;
        gs_energy.close();

    // Write the ground-state vectors into file "gs_vector.out"
        ofstream gs_vector("output/gs_vector.out", ios::out);
        gs_vector <<"#j" << "\t" <<"psi_j: Re & Im" << endl;

/*
    // Relative phases (for periodic systems)
        cx_double I(0.0,1.0);
        int equiv_atom_a = 0;   // Choose equivalent atoms
        int equiv_atom_b = 0;   // Choose equivalent atoms
        ofstream rel_phase("output/rel_phase.out", ios::out);
        rel_phase <<"#j" << "\t" << "exp(i*k_y*d)" << "\t\t\t" << "k_y*d" << endl;
        for(unsigned int j=0; j<nsite; j++)
        {
            gs_vector << setprecision(5) << scientific << j;
            for(unsigned int k=0; k<nsite; k++)
            {
                gs_vector << setprecision(5) << scientific << "\t" << real(gs_eigvec(k,j)) << "\t" << imag(gs_eigvec(k,j));
            }
            gs_vector << endl;
            rel_phase << setprecision(5) << scientific << j << "\t" << gs_eigvec(equiv_atom_a,j)/gs_eigvec(equiv_atom_b,j)
                      << "\t" << -I*log(gs_eigvec(equiv_atom_a,j)/gs_eigvec(equiv_atom_b,j)) << endl;
        }
        gs_vector.close();
        rel_phase.close();

    // Find the states close to Fermi energy and write into files "gs_wf.out" and "gs_norm.out"
        ofstream gs_wf("output/gs_wf.out", ios::out);
        gs_wf <<"#k" << "\t" << "Re[psi_j(k)]" << "\t" << "Im[psi_j(k)]" << endl;
        ofstream gs_norm("output/gs_norm.out", ios::out);
        gs_norm <<"#k" << "\t" << "|psi_j(k)|^2" << endl;
        for(unsigned int j=0; j<nsite; j++)
        {
            if(j == nsite/2-1 && abs(gs_eigval(j)) < 1.0e-4)
            {
                for(unsigned int k=0; k<nsite; k++)
                {
                    gs_wf << setprecision(5) << scientific << k << "\t" 
                          << real(gs_eigvec.col(j-1)(k)) << "\t" << imag(gs_eigvec.col(j-1)(k)) << "\t"
                          << real(gs_eigvec.col(j)(k)) << "\t" << imag(gs_eigvec.col(j)(k)) << "\t"
                          << real(gs_eigvec.col(j+1)(k)) << "\t" << imag(gs_eigvec.col(j+1)(k)) << "\t"
                          << real(gs_eigvec.col(j+2)(k)) << "\t" << imag(gs_eigvec.col(j+2)(k)) << "\t"
                          << real(gs_eigvec.col(j)(k)+gs_eigvec.col(j+1)(k)) << "\t"
                          << imag(gs_eigvec.col(j)(k)+gs_eigvec.col(j+1)(k))
                          << endl;

                    gs_norm << setprecision(5) << scientific << k << "\t"
                            << real(gs_eigvec.col(j-1)(k)*conj(gs_eigvec.col(j-1)(k))) << "\t"
                            << real(gs_eigvec.col(j)(k)*conj(gs_eigvec.col(j)(k))) << "\t"
                            << real(gs_eigvec.col(j+1)(k)*conj(gs_eigvec.col(j+1)(k))) << "\t"
                            << real(gs_eigvec.col(j+2)(k)*conj(gs_eigvec.col(j+2)(k))) << "\t"
                            << real((gs_eigvec.col(j)(k)+gs_eigvec.col(j+1)(k))
                               *conj(gs_eigvec.col(j)(k)+gs_eigvec.col(j+1)(k)))
                            << endl;
                }
            }
        }
        gs_wf.close();
        gs_norm.close();
*/

    // Calculate the ground-state spectral function and write into file "spectral_gs.out"
        int spacing;
        if(ntstep < 100) spacing = 1000;
        else if(ntstep < 1000) spacing = 100;
        else spacing = 10;

        double wu, dwu;
        if(gamma_strength < 0.01)
        {
            wu = -wmax-double(spacing)*gamma_strength;
            dwu = (2.0*(wmax+double(spacing)*gamma_strength)/ntstep)/double(spacing);
        }
        else if(gamma_strength < 0.1)
        {
            wu = -wmax-double(spacing)*gamma_strength/10.0;
            dwu = (2.0*(wmax+double(spacing)*gamma_strength/10.0)/ntstep)/double(spacing);
        }
        else
        {
            wu = -wmax-double(spacing)*gamma_strength/100.0;
            dwu = (2.0*(wmax+double(spacing)*gamma_strength/100.0)/ntstep)/double(spacing);
        }

        cx_double tmpdbl(0.0,0.0);
        double tmpsum = 0.0;
        cx_double one(1.0,0.0);
        cx_double Ieta(0.0,pow((gamma_strength/2.0),2.0));
        ofstream spectral("output/spectral_gs.out", ios::out);
        spectral <<"#w" << "\t\t" << "A_gs(w)" << endl;
        for(unsigned int i=0; i<spacing*ntstep; i++)
        {
            for(unsigned int j=0; j<nsite; j++)
            {
                tmpdbl += (one/(wu-gs_eigval(j)+Ieta))*cdot(gs_eigvec.col(j),gs_eigvec.col(j));
            }
            spectral << setprecision(5) << scientific << wu << "\t"
                     << (-1.0/datum::pi)*imag(tmpdbl) << endl;
            tmpsum += (-1.0/datum::pi)*imag(tmpdbl)*dwu;
            tmpdbl = cx_double(0.0,0.0);
            wu += dwu;
        }
        spectral.close();
        cout << setprecision(5) << "Integral A_gs(w) dw = " << tmpsum
             << " (Normalized: " << tmpsum/nsite << ")" << endl;

        if(bandstructure)
        {
            cx_mat h01st(nsite/2,nsite/2), h02nd(nsite/2,nsite/2);
            h01st = Hu.submat(0, 0, nsite/2-1, nsite/2-1);
            h02nd = Hu.submat(nsite/2, nsite/2, nsite-1, nsite-1);
            vec eval1st(nsite/2), eval2nd(nsite/2);
            cx_mat evec1st(nsite/2,nsite/2), evec2nd(nsite/2,nsite/2);
            eig_sym(eval1st, evec1st, h01st);
            eig_sym(eval2nd, evec2nd, h02nd);

            bandfile << setprecision(10) << scientific << "#k" << "\t" << "eps_k" << endl;
            Nss = static_cast<double>(nsite);
            for(unsigned int j=0; j<nsite/2; j++)
            {
                jj = static_cast<double>(j);
                if(j%2==0) bandfile << (jj/Nss)*2.0*datum::pi << "\t" << eval1st(j) << "\t" << eval2nd(j) << endl;
                else bandfile << ((jj+1)/Nss)*(-2.0*datum::pi) << "\t" << eval1st(j) << "\t" << eval2nd(j) << endl;
            }
        }
    }

// Determine the value for chemical potential in half-filling
    int degsite;
    if(systype == 1 || systype == 2)
    {
        // In TB equal amount of negative and positive eigenvalues -> mu = 0.
        //mu = (gs_eigval(int(nsite/2)-1)+gs_eigval(int(nsite/2)))/2.0;
        mu = 0.0;
    }

    else if(systype == 3)   // Check when the flat band starts, and set mu accordingly.
    {
        bool degeneracy = false;
        for(unsigned int j=2; j<nsite; j++)
        {
            if(gs_eigval(j) > 0 && abs(gs_eigval(j)-gs_eigval(j-1)) < difference)
            {
                if(degeneracy == false)
                {
                    mu = 0.5*(gs_eigval(j-2) + gs_eigval(j-1));
                    degeneracy = true;
                    degsite = j-1;
                }
            }
            else degeneracy = false;
        }
        if(degeneracy == false) mu = (gs_eigval(int(nsite/2)-1)+gs_eigval(int(nsite/2)))/2.0;
    }

    else if(systype == 4)   // Check when the flat band starts, and set mu accordingly.
    {
        bool degeneracy = false;
        for(unsigned int j=2; j<nsite; j++)
        {
            if(gs_eigval(j) > 0 && abs(gs_eigval(j)-gs_eigval(j-1)) < difference)
            {
                if(degeneracy == false)
                {
                    mu = 0.5*(gs_eigval(j-2) + gs_eigval(j-1));
                    degeneracy = true;
                    degsite = j-1;
                }
            }
            else degeneracy = false;
        }
        if(degeneracy == false) mu = (gs_eigval(int(nsite/2)-1)+gs_eigval(int(nsite/2)))/2.0;
    }

    else if(systype == 5 || systype == 6)   // Two-site molecule, mu arbitrary.
    {
        mu = 0.0;
    }

    else if(systype == 7 || systype == 8)   // One-level system, mu arbitrary.
    {
        mu = 0.0;
    }

    else if(systype == 9)   // Kagome lattice
    {
        mu = (gs_eigval(int(nsite/2)-1)+gs_eigval(int(nsite/2)))/2.0;;
    }

    else if(systype == 10)   // From file
    {
      //        mu = (gs_eigval(int(nsite/2)-1)+gs_eigval(int(nsite/2)))/2.0;
        mu = 0.0;
        // HACK
        //double ef=0.01;
        //mu = bias_strength-ef;
    }

    else if(systype == 11)   // Rectangular grid
    {
        mu = (gs_eigval(int(nsite/2)-1)+gs_eigval(int(nsite/2)))/2.0;;
    }

    else if(systype == 12)   // Majorana
    {
	//        mu = (gs_eigval(int(nsite/2)-1)+gs_eigval(int(nsite/2)))/2.0;
        mu = 0.0;
    }

    else{ cout << "Erroneous system type in GSCalculation.cpp, exiting ..." << endl; exit(1);}

// Calculate the ground state density from the eigenvectors an write into file "gs_density.out"
    if(rank == 0)
    {
        ofstream gs_density("output/gs_density.out", ios::out);
        gs_density << "#j" << "\t" << "dens_j" << endl;
    
        for(unsigned int j=0; j<nsite; j++)
        {
            cx_double gs_dens = 0.0;
            if(finitet == 0)
            {
                if(nsite > 1)
                {
                    if(systype == 3 || systype == 4)
                    {
                        for(unsigned int k=degsite; k<nsite; k++)   // Calculate the density from flat-band states
                        {
                            gs_dens += gs_eigvec(j,k)*conj(gs_eigvec(j,k));
                        }
                    }
                    else
                    {
                        for(unsigned int k=0; k<nsite/2; k++)   // Fermi level at k=nsite/2 (half-filling)
                        {
                            gs_dens += gs_eigvec(j,k)*conj(gs_eigvec(j,k));
                        }
                    }
                }
                else
                {
                    for(unsigned int k=0; k<nsite; k++)
                    {
                        gs_dens += gs_eigvec(j,k)*conj(gs_eigvec(j,k));
                    }
                }
            }
            else if(finitet == 1)
            {
                for(unsigned int k=0; k<nsite; k++)
                {
                    gs_dens += Fermi(beta, gs_eigval(k)-mu)*gs_eigvec(j,k)*conj(gs_eigvec(j,k));
                }
            }
            else{ cout << "Erroneous temperature parameter in GSCalculation.cpp, exiting ..." << endl; exit(1);}

            gs_density << setprecision(5) << scientific << j << "\t" << real(gs_dens) << endl;
        }
        gs_density.close();

        cout << "Chemical potential: " << setprecision(6) << mu << endl;

        if(finitet == 1)    // Write the Fermi function into file
        {
            cx_double kk;
            ofstream test("output/fermi.out", ios::out);
            test << "#w" << "\t" << "f(w) = [ exp(beta*w) + 1 ]^-1" << endl;
            for(unsigned int k=0; k<nsite; k++)
            {
                kk = cx_double(k-nsite/2.0,0.0);
                test << setprecision(5) << scientific << real(kk) << "\t" << real(Fermi(1.0,kk)) << endl;
            }
            test.close();
        }
    }

/*
// Return the Hamiltonian in its eigenbasis to the main programme
// (This should be uncommented if in Coupling.cpp it is chosen to calculate Gamma in eigenbasis)

    Hu.zeros();
    for(unsigned int j=0; j<nsite; j++)
    {
        Hu(j,j) = gs_eigval(j);
    }

// For small enough central systems, print the real part of the Hamiltonian matrix
    mat Hreal = real(Hu);
    if(rank == 0) 
    {
        cout << "Diagonalized Hamiltonian:" << endl;
        if(nsite <= 14) Hreal.print(cout, "H =");
        else cout << "(not shown)" << endl;
    }
*/
}
