#include "Main.hpp"
void PrintTime(const unsigned int value)
{
    if(value == 0)
    {
    	cout << "00";
    }
    else if(value < 10)
    {
    	cout << "0" << value;
    }
    else
    {
    	cout << value;
    }
}

void StopWatch(double start, double stop)
{
    const static unsigned int s_in_h = 3600;
    const static unsigned int s_in_min = 60;    
    unsigned int runtime = ceil(stop-start);
    cout << "Time wasted: ";
    if( runtime > 0 )
    {
        PrintTime((unsigned int)(runtime / s_in_h));
        cout << ":";
        PrintTime((unsigned int)((runtime % s_in_h) / s_in_min));
        cout << ":";
        PrintTime((unsigned int)((runtime % s_in_h) % (s_in_min)));
    }
    else
    {
    	cout << "00:00:00";
    }
    cout << endl; cout << endl;
}
