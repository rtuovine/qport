#include "Main.hpp"
cx_double OmegaCalculate(void *param)
{
    ParamsO &funcpara= *reinterpret_cast<ParamsO *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    unsigned int m = funcpara.m;
    unsigned int n = funcpara.n;
    unsigned int p = funcpara.p;
    unsigned int q = funcpara.q;
    unsigned int alpha = funcpara.alpha;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;
    cx_vec eps_peff_l = funcpara.eps_peff_l;
    double mu = funcpara.mu;
    int biastype = funcpara.biastype;
    int pert = funcpara.pert;
    double bias_strength = funcpara.bias_strength;
    int finitet = funcpara.finitet;
    double beta = funcpara.beta;

    cx_double I(0.0,1.0);

    if(pert == 0 && finitet == 0)
    {
        return (1.0
                /(2.0*datum::pi
                 *(
                   (conj(eps_ueff_l(k))-eps_ueff_l(j))*pow(V(alpha,biastype,bias_strength),3.0)
                  -pow((conj(eps_ueff_l(k))-eps_ueff_l(j)),3.0)*V(alpha,biastype,bias_strength)
                  )
                 )
               )
              *(
                 (conj(eps_ueff_l(k))-eps_ueff_l(j)+V(alpha,biastype,bias_strength))
                 *(
                   log(conj(eps_ueff_l(k))-mu-V(alpha,biastype,bias_strength))
                  -log(eps_ueff_l(j)-mu)
                  )
               + (conj(eps_ueff_l(k))-eps_ueff_l(j)-V(alpha,biastype,bias_strength))
                 *(
                   log(eps_ueff_l(j)-mu-V(alpha,biastype,bias_strength))
                  -log(conj(eps_ueff_l(k))-mu)
                  )
               );
    }

    if(pert == 0 && finitet == 1)
    {
        return ( I/(
                    (conj(eps_ueff_l(k))-eps_ueff_l(j))
                   *(conj(eps_ueff_l(k))-eps_ueff_l(j)+V(alpha,biastype,bias_strength))
                   *V(alpha,biastype,bias_strength)
                   )
                 *(  Fermi(beta,conj(eps_ueff_l(k))-mu) 
                   + (I/(2.0*datum::pi))*(Psi(-(eps_ueff_l(j)-mu-V(alpha,biastype,bias_strength)),beta)
                                          -Psi(-(conj(eps_ueff_l(k))-mu),beta))
                  )
                -I/(
                    (conj(eps_ueff_l(k))-eps_ueff_l(j))
                   *(conj(eps_ueff_l(k))-eps_ueff_l(j)-V(alpha,biastype,bias_strength))
                   *V(alpha,biastype,bias_strength)
                   )
                 *(  Fermi(beta,conj(eps_ueff_l(k))-mu-V(alpha,biastype,bias_strength)) 
                   + (I/(2.0*datum::pi))*(Psi(-(eps_ueff_l(j)-mu),beta)
                                          -Psi(-(conj(eps_ueff_l(k))-mu-V(alpha,biastype,bias_strength)),beta))
                  )
               );
    }

    if(pert == 1 && finitet == 0)
    {
        return (1.0/(2.0*datum::pi))*
        ( 
            (
             log(eps_ueff_l(m)-mu)
             /(
               (eps_ueff_l(m)-conj(eps_ueff_l(q)))
              *(eps_ueff_l(m)-eps_peff_l(n)+V(alpha,biastype,bias_strength))
              *(eps_ueff_l(m)-conj(eps_peff_l(p))+V(alpha,biastype,bias_strength))
              )
            )

          + (
             log(eps_peff_l(n)-mu-V(alpha,biastype,bias_strength))
             /(
               (eps_peff_l(n)-conj(eps_peff_l(p)))
              *(conj(eps_ueff_l(q))-eps_peff_l(n)+V(alpha,biastype,bias_strength))
              *(eps_ueff_l(m)-eps_peff_l(n)+V(alpha,biastype,bias_strength))
              )
            )

          + (
             log(conj(eps_ueff_l(q))-mu)
             /(
               (conj(eps_ueff_l(q))-eps_ueff_l(m))
              *(conj(eps_ueff_l(q))-eps_peff_l(n)+V(alpha,biastype,bias_strength))
              *(conj(eps_ueff_l(q))-conj(eps_peff_l(p))+V(alpha,biastype,bias_strength))
              )
            )

          + (
             log(conj(eps_peff_l(p))-mu-V(alpha,biastype,bias_strength))
             /(
               (conj(eps_peff_l(p))-eps_peff_l(n))
              *(eps_ueff_l(m)-conj(eps_peff_l(p))+V(alpha,biastype,bias_strength))
              *(conj(eps_ueff_l(q))-conj(eps_peff_l(p))+V(alpha,biastype,bias_strength))
              )
            )
        );
    }

    if(pert == 1 && finitet == 1)
    {
        return ( 1.0/(
                    (eps_ueff_l(m)-eps_peff_l(n)+V(alpha,biastype,bias_strength))
                   *(eps_ueff_l(m)-conj(eps_peff_l(p))+V(alpha,biastype,bias_strength))
                   *(eps_ueff_l(m)-conj(eps_ueff_l(q)))
                   )
                 *(
                     (1.0/(2.0*datum::pi))*(Psi(-(eps_ueff_l(m)-mu),beta))
                  )
                +1.0/(
                    (eps_peff_l(n)-eps_ueff_l(m)-V(alpha,biastype,bias_strength))
                   *(eps_peff_l(n)-conj(eps_peff_l(p)))
                   *(eps_peff_l(n)-conj(eps_ueff_l(q))-V(alpha,biastype,bias_strength))
                   )
                 *(
                     (1.0/(2.0*datum::pi))*(Psi(-(eps_peff_l(n)-mu-V(alpha,biastype,bias_strength)),beta))
                  )
                +1.0/(
                    (conj(eps_ueff_l(q))-eps_ueff_l(m))
                   *(conj(eps_ueff_l(q))-eps_peff_l(n)+V(alpha,biastype,bias_strength))
                   *(conj(eps_ueff_l(q))-conj(eps_peff_l(p))+V(alpha,biastype,bias_strength))
                   )
                 *(  I*Fermi(beta,conj(eps_ueff_l(q))-mu) 
                   + (1.0/(2.0*datum::pi))*(Psi(-(conj(eps_ueff_l(q))-mu),beta))
                  )
                +1.0/(
                    (conj(eps_peff_l(p))-eps_ueff_l(m)-V(alpha,biastype,bias_strength))
                   *(conj(eps_peff_l(p))-eps_peff_l(n))
                   *(conj(eps_peff_l(p))-conj(eps_ueff_l(q))-V(alpha,biastype,bias_strength))
                   )
                 *(  I*Fermi(beta,conj(eps_peff_l(p))-mu-V(alpha,biastype,bias_strength)) 
                   + (1.0/(2.0*datum::pi))*(Psi(-(conj(eps_peff_l(p))-mu-V(alpha,biastype,bias_strength)),beta))
                  )
               );
    }
}
