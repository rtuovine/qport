#include "Main.hpp"
double V(int nlead, int biastype, double bias_strength)
{
    if(biastype == 1)
    {
        return (bias_strength+1.0e-15)*pow(-1.0,nlead);		// (Anti-)symmetric constant field
        // This also makes alternating source-drain-source-drain-... for n-lead system
    }
    else if(biastype == 2)
    {
        if(nlead == 0) return bias_strength+1.0e-15;        // Sudden (constant) one-sided bias for two leads.
        else if(nlead == 1) return 1.0e-15;                 // Numerically zero = positive infinitesimal in
                                                            // order to avoid singularities.
	}
	else if(biastype == 3)                                  // All leads biased to same value
	{
        return bias_strength+1.0e-15;
	}
	else if(biastype == 4)                                  // 2 source-drain, 2 probes
	{
        if(nlead == 0) return bias_strength+1.0e-15;
        else if(nlead == 2) return -(bias_strength+1.0e-15);
        else if(nlead == 1 || nlead == 3) return 1.0e-15;
	}
    else{ cout << "Erroneous bias type, exiting ..." << endl; exit(1);}
    // In all cases V=0 is written as 1.0e-15 to avoid singularities in the exponential integral.
}
