#include "Main.hpp"
double Cosech(double tau, double taubar, double beta, int padeorder, 
              vec pade_eta, vec pade_zeta)
{
/*
Returns Csch[(tau-taubar)*pi/beta]theta(tau-taubar)
       -Csch[(taubar-tau)*pi/beta]theta(taubar-tau)

Can be implemented either by 1/Sinh (numerically problematic due to 
divergent behaviour) or by the Padé expansion.
*/

/*
    if(tau==taubar) return 0.0;
    else return (1.0/(sinh((datum::pi/beta)*(tau-taubar))))*Theta(tau-taubar)
               -(1.0/(sinh((datum::pi/beta)*(taubar-tau))))*Theta(taubar-tau);
*/

    double tempsum = 0.0;
    for(unsigned int j=0; j<padeorder; j++)
    {
        tempsum += 2.0*pade_eta(j)*(exp(-pade_zeta(j)*(tau-taubar)/beta)*Theta(tau-taubar)
                                   -exp(-pade_zeta(j)*(taubar-tau)/beta)*Theta(taubar-tau));
    }

    return tempsum;
}

double Integrand111re(double tauprime, void *param)
{
/*
This is a gsl integrand for the function 
psi_b(tau,taubar) = int_taubar^tau dt' V_b(t').

This is called from Integrand11re(), Integrand11im(), Integrand2re(),
Integrand2im(), Integrand3re(), Integrand3im().

Also, other forms than V(t) = V_b + A_b*Cos(w_b * t + phase_b) can be 
introduced here.
*/
    ParamsTimeIntegral &funcpara= *reinterpret_cast<ParamsTimeIntegral *>(param);

    unsigned int be = funcpara.be;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;

    return constpart[be] + amplitude[be]*cos(frequency[be]*tauprime + phase[be]);
}

double Integrand11re(double taubar, void *param)
{
/*
This is a gsl integrand (real part) for the double (tau,taubar) 
integral. This is called from the outer integral Integrand1re().
*/
    ParamsTimeIntegral &funcpara= *reinterpret_cast<ParamsTimeIntegral *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    unsigned int be = funcpara.be;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    double tau = funcpara.tau;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;
    int padeorder = funcpara.padeorder;
    vec pade_eta = funcpara.pade_eta;
    vec pade_zeta = funcpara.pade_zeta;
    int subs = funcpara.subs;
    double acc = funcpara.acc;
    int rule = funcpara.rule;

    cx_double I(0.0,1.0);

/* 
This integrand depends on the phase psi which again is a time integral
of the bias profile over the range [taubar,tau].
*/

    gsl_function F111re;
    gsl_integration_workspace *ws111re;
    double result111re, error111re;

    const double epsabs = acc;
    const double epsrel = acc;
    const size_t wslimit = subs;
    const int key = rule;
    int integral_status;
    double epsabsvar, epsrelvar;

    ParamsTimeIntegral para11;
    para11.be = be;
    para11.constpart = constpart;
    para11.amplitude = amplitude;
    para11.frequency = frequency;
    para11.phase = phase;

    double lo = taubar;
    double up = tau;

    F111re.function = &Integrand111re;
    F111re.params = reinterpret_cast<void *>(&para11);
    ws111re = gsl_integration_workspace_alloc(wslimit);
    gsl_error_handler_t *old_handler111 = gsl_set_error_handler_off();
    integral_status = 1;
    epsabsvar = epsabs;
    epsrelvar = epsrel;
    while(integral_status)
    {
        integral_status = gsl_integration_qag(&F111re, lo, up, epsabsvar, epsrelvar, wslimit, 
                                              key, ws111re, &result111re, &error111re);

        epsabsvar *= 1.1;
        epsrelvar *= 1.1;

        if(integral_status) {cout << setprecision(3) << scientific
                         << "\tProblems in integration; increasing tolerance to "
                         << epsabsvar << endl;}
    }
    gsl_set_error_handler(old_handler111);
    gsl_integration_workspace_free(ws111re);

    return real(exp(I*(eps_ueff_l(j)-mu)*tau)
               *exp(-I*(conj(eps_ueff_l(k))-mu)*taubar)
               *exp(-I*result111re)*Cosech(tau,taubar,beta,padeorder,pade_eta,pade_zeta));
}

double Integrand11im(double taubar, void *param)
{
/*
This is a gsl integrand (imaginary part) for the double (tau,taubar) 
integral. This is called from the outer integral Integrand1im().
*/
    ParamsTimeIntegral &funcpara= *reinterpret_cast<ParamsTimeIntegral *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    unsigned int be = funcpara.be;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    double tau = funcpara.tau;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;
    int padeorder = funcpara.padeorder;
    vec pade_eta = funcpara.pade_eta;
    vec pade_zeta = funcpara.pade_zeta;
    int subs = funcpara.subs;
    double acc = funcpara.acc;
    int rule = funcpara.rule;

    cx_double I(0.0,1.0);

/* 
This integrand depends on the phase psi which again is a time integral
of the bias profile over the range [taubar,tau].
*/

    gsl_function F111re;
    gsl_integration_workspace *ws111re;
    double result111re, error111re;

    const double epsabs = acc;
    const double epsrel = acc;
    const size_t wslimit = subs;
    const int key = rule;
    int integral_status;
    double epsabsvar, epsrelvar;

    ParamsTimeIntegral para11;
    para11.be = be;
    para11.constpart = constpart;
    para11.amplitude = amplitude;
    para11.frequency = frequency;
    para11.phase = phase;

    double lo = taubar;
    double up = tau;

    F111re.function = &Integrand111re;
    F111re.params = reinterpret_cast<void *>(&para11);
    ws111re = gsl_integration_workspace_alloc(wslimit);
    gsl_error_handler_t *old_handler111 = gsl_set_error_handler_off();
    integral_status = 1;
    epsabsvar = epsabs;
    epsrelvar = epsrel;
    while(integral_status)
    {
        integral_status = gsl_integration_qag(&F111re, lo, up, epsabsvar, epsrelvar, wslimit, 
                                              key, ws111re, &result111re, &error111re);

        epsabsvar *= 1.1;
        epsrelvar *= 1.1;

        if(integral_status) {cout << setprecision(3) << scientific
                         << "\tProblems in integration; increasing tolerance to "
                         << epsabsvar << endl;}
    }
    gsl_set_error_handler(old_handler111);
    gsl_integration_workspace_free(ws111re);

    return imag(exp(I*(eps_ueff_l(j)-mu)*tau)
               *exp(-I*(conj(eps_ueff_l(k))-mu)*taubar)
               *exp(-I*result111re)*Cosech(tau,taubar,beta,padeorder,pade_eta,pade_zeta));
}

double Integrand1re(double tau, void *param)
{
/*
This is the outer integrand (real part) for the double (tau,taubar) 
integral. This is called from the evaluation of Glss,Ggtr.
*/
    ParamsTimeIntegral &funcpara= *reinterpret_cast<ParamsTimeIntegral *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    unsigned int be = funcpara.be;
    unsigned int ii = funcpara.ii;
    int tgridtype = funcpara.tgridtype;
    int ntstep = funcpara.ntstep;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    double dt = funcpara.dt;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;
    int padeorder = funcpara.padeorder;
    vec pade_eta = funcpara.pade_eta;
    vec pade_zeta = funcpara.pade_zeta;
    int subs = funcpara.subs;
    double acc = funcpara.acc;
    int rule = funcpara.rule;

    cx_double I(0.0,1.0);

/*
The inner taubar integrand is called from here.
*/
    gsl_function F11re;
    gsl_integration_workspace *ws11re;
    double result11re, error11re;

    const double epsabs = acc;
    const double epsrel = acc;
    const size_t wslimit = subs;
    const int key = rule;
    int integral_status;
    double epsabsvar, epsrelvar;

    ParamsTimeIntegral para1;
    para1.j = j;
    para1.k = k;
    para1.be = be;
    para1.ii = ii;
    para1.mu = mu;
    para1.beta = beta;
    para1.tau = tau;
    para1.constpart = constpart;
    para1.amplitude = amplitude;
    para1.frequency = frequency;
    para1.phase = phase;
    para1.eps_ueff_l = eps_ueff_l;
    para1.padeorder = padeorder;
    para1.pade_eta = pade_eta;
    para1.pade_zeta = pade_zeta;
    para1.subs = subs;
    para1.acc = acc;
    para1.rule = rule;

    double lo = 0.0;
    double up = Time(ntstep, tgridtype, dt, ii);

    F11re.function = &Integrand11re;
    F11re.params = reinterpret_cast<void *>(&para1);
    ws11re = gsl_integration_workspace_alloc(wslimit);
    gsl_error_handler_t *old_handler11 = gsl_set_error_handler_off();
    integral_status = 1;
    epsabsvar = epsabs;
    epsrelvar = epsrel;
    while(integral_status)
    {
        integral_status = gsl_integration_qag(&F11re, lo, up, epsabsvar, epsrelvar, wslimit, 
                                              key, ws11re, &result11re, &error11re);

        epsabsvar *= 1.1;
        epsrelvar *= 1.1;

        if(integral_status) {cout << setprecision(3) << scientific
                         << "\tProblems in integration; increasing tolerance to "
                         << epsabsvar << endl;}
    }
    gsl_set_error_handler(old_handler11);
    gsl_integration_workspace_free(ws11re);

    return result11re;
}

double Integrand1im(double tau, void *param)
{
/*
This is the outer integrand (imaginary part) for the double (tau,taubar) 
integral. This is called from the evaluation of Glss,Ggtr.
*/
    ParamsTimeIntegral &funcpara= *reinterpret_cast<ParamsTimeIntegral *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    unsigned int be = funcpara.be;
    unsigned int ii = funcpara.ii;
    int tgridtype = funcpara.tgridtype;
    int ntstep = funcpara.ntstep;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    double dt = funcpara.dt;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;
    int padeorder = funcpara.padeorder;
    vec pade_eta = funcpara.pade_eta;
    vec pade_zeta = funcpara.pade_zeta;
    int subs = funcpara.subs;
    double acc = funcpara.acc;
    int rule = funcpara.rule;

    cx_double I(0.0,1.0);

/*
The inner taubar integrand is called from here.
*/
    gsl_function F11im;
    gsl_integration_workspace *ws11im;
    double result11im, error11im;

    const double epsabs = 1.0e-3;
    const double epsrel = 1.0e-3;
    const size_t wslimit = 1000;
    const int key = 1;
    int integral_status;
    double epsabsvar, epsrelvar;

    ParamsTimeIntegral para1;
    para1.j = j;
    para1.k = k;
    para1.be = be;
    para1.ii = ii;
    para1.mu = mu;
    para1.beta = beta;
    para1.tau = tau;
    para1.constpart = constpart;
    para1.amplitude = amplitude;
    para1.frequency = frequency;
    para1.phase = phase;
    para1.eps_ueff_l = eps_ueff_l;
    para1.padeorder = padeorder;
    para1.pade_eta = pade_eta;
    para1.pade_zeta = pade_zeta;
    para1.subs = subs;
    para1.acc = acc;
    para1.rule = rule;

    double lo = 0.0;
    double up = Time(ntstep, tgridtype, dt, ii);

    F11im.function = &Integrand11im;
    F11im.params = reinterpret_cast<void *>(&para1);
    ws11im = gsl_integration_workspace_alloc(wslimit);
    gsl_error_handler_t *old_handler22 = gsl_set_error_handler_off();
    integral_status = 1;
    epsabsvar = epsabs;
    epsrelvar = epsrel;
    while(integral_status)
    {
        integral_status = gsl_integration_qag(&F11im, lo, up, epsabsvar, epsrelvar, wslimit, 
                                              key, ws11im, &result11im, &error11im);

        epsabsvar *= 1.1;
        epsrelvar *= 1.1;

        if(integral_status) {cout << setprecision(3) << scientific
                         << "\tProblems in integration; increasing tolerance to "
                         << epsabsvar << endl;}
    }
    gsl_set_error_handler(old_handler22);
    gsl_integration_workspace_free(ws11im);

    return result11im;
}

void TwoTimeGreen(int nsite, int nlead, vector<int> lcontact, vector<int> rcontact, 
                  vector<int> lcontact2, vector<int> rcontact2, int lead_row, 
                  double bias_strength, int ntstep, int tgridtype, int sc, double dt, 
                  double lambda_strength, double gamma_strength, double lead_hop, 
                  double mu, double beta, cx_mat Hu, int systype, vec gs_eigval, 
                  cx_mat gs_eigvec, field<cx_mat> &Glss, field<cx_mat> &Ggtr,
                  vector<double> constpart, vector<double> amplitude, vector<double> frequency, 
                  vector<double> phase, int padeorder, vec pade_eta, vec pade_zeta,
                  int subs, double acc, int rule)
{
	// MPI variables
	int rank, size;

	//  Get the number of processes and the individual process ID
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if(rank == 0)
    {
        cout << endl;
        cout << "Two-time Green's functions G^{<,>}(t1,t2)" << endl;
    }

// Allocate arrays
    cx_double I(0.0,1.0);
    cx_vec eps_ueff_r(nsite), eps_ueff_l(nsite);
    cx_mat hu_eff(nsite,nsite), temp_mat(nsite,nsite), 
           psi_ueff_r(nsite,nsite), psi_ueff_l(nsite,nsite);
    cx_cube gamma_basis(nsite,nsite,nlead);

    hu_eff.zeros();
    temp_mat.zeros();
    eps_ueff_l.zeros();
    eps_ueff_r.zeros();
    psi_ueff_l.zeros();
    psi_ueff_r.zeros();
    gamma_basis.zeros();

	double tmpdble1, tmpdble2;
    tmpdble1 = MPI_Wtime();

    for(unsigned int alpha=0; alpha<nlead; alpha++)
    {
        temp_mat += Gamma(nsite,nlead,lcontact,rcontact,lcontact2,
                          rcontact2,lead_row,lambda_strength,
                          gamma_strength,lead_hop,gs_eigvec,systype,sc)
                          .slice(alpha);
    }

    hu_eff = Hu - 0.5*I*temp_mat;

    eig_gen(eps_ueff_r, psi_ueff_r, hu_eff, 'r');  // h_eff |psi_eff_r> = eps_eff_r |psi_eff_r>
    eig_gen(eps_ueff_l, psi_ueff_l, hu_eff, 'l');  // <psi_eff_l| h_eff = eps_eff_l <psi_eff_l|

    cx_mat overlap_uu_lr(nsite,nsite), invover_uu_lr(nsite,nsite), 
           overlap_uu_rl(nsite,nsite), invover_uu_rl(nsite,nsite);

    overlap_uu_lr = trans(psi_ueff_l)*psi_ueff_r;
    overlap_uu_rl = trans(psi_ueff_r)*psi_ueff_l;

    invover_uu_lr = inv(overlap_uu_lr);
    invover_uu_rl = inv(overlap_uu_rl);

    for(unsigned int alpha=0; alpha<nlead; alpha++)
    {
        gamma_basis.slice(alpha) = trans(psi_ueff_l)
                                  *Gamma(nsite,nlead,lcontact,rcontact,
                                         lcontact2,rcontact2,lead_row,
                                         lambda_strength,gamma_strength,
                                         lead_hop,gs_eigvec,systype,sc)
                                         .slice(alpha)*psi_ueff_l;
    } // alpha<nlead

	unsigned int elems = ntstep/size;
    cx_mat temp(nsite,nsite), temp2(nsite,nsite);

    gsl_function F1re, F2re, F3re, F1im, F2im, F3im;
    gsl_integration_workspace *ws1re, *ws2re, *ws3re, *ws1im, *ws2im, *ws3im;
    double result1re, result2re, result3re, error1re, error2re, error3re, 
           result1im, result2im, result3im, error1im, error2im, error3im;

    const double epsabs = acc;
    const double epsrel = acc;
    const size_t wslimit = subs;
    double lo = 0.0;
    double up = 1.0;
    const int key = rule;
    int integral_status;
    double epsabsvar, epsrelvar;

	for(unsigned int i=0; i<elems; i++) // Outer time loop (t1) [PARALLELIZED!]
    {
        unsigned int l = elems*rank + i; // Index i runs up to divided steps,
                                         // index l accounts for individual steps
        for(unsigned int ii=0; ii<ntstep; ii++) // Inner time loop (t2) [NOT PARALLELIZED! TODO: threading perhaps?]
        {
            if(ii == l) // Evaluating the time-diagonal only (for testing)
            {
                for(unsigned int j=0; j<nsite; j++)
                {
                    for(unsigned int k=0; k<nsite; k++)
                    {
                        for(unsigned int be=0; be<nlead; be++)
                        {
                            // The double and single tau integrals are called from here
                            ParamsTimeIntegral para;
                            para.j = j;
                            para.k = k;
                            para.be = be;
                            para.ii = ii;
                            para.mu = mu;
                            para.beta = beta;
                            para.dt = dt;
                            para.tgridtype = tgridtype;
                            para.ntstep = ntstep;
                            para.constpart = constpart;
                            para.amplitude = amplitude;
                            para.frequency = frequency;
                            para.phase = phase;
                            para.eps_ueff_l = eps_ueff_l;
                            para.padeorder = padeorder;
                            para.pade_eta = pade_eta;
                            para.pade_zeta = pade_zeta;
                            para.subs = subs;
                            para.acc = acc;
                            para.rule = rule;

                            lo = 0.0;
                            up = Time(ntstep, tgridtype, dt, l);

                            F1re.function = &Integrand1re;
                            F1re.params = reinterpret_cast<void *>(&para);
                            ws1re = gsl_integration_workspace_alloc(wslimit);
                            gsl_error_handler_t *old_handler1 = gsl_set_error_handler_off();
                            integral_status = 1;
                            epsabsvar = epsabs;
                            epsrelvar = epsrel;
                            while(integral_status)
                            {
                                integral_status = gsl_integration_qag(&F1re, lo, up, epsabsvar, epsrelvar, wslimit, 
                                                                      key, ws1re, &result1re, &error1re);

                                epsabsvar *= 1.1;
                                epsrelvar *= 1.1;

                                if(integral_status) {cout << setprecision(3) << scientific
                                                 << "\tProblems in integration; increasing tolerance to "
                                                 << epsabsvar << endl;}
                            }
                            gsl_set_error_handler(old_handler1);
                            gsl_integration_workspace_free(ws1re);

                            F1im.function = &Integrand1im;
                            F1im.params = reinterpret_cast<void *>(&para);
                            ws1im = gsl_integration_workspace_alloc(wslimit);
                            gsl_error_handler_t *old_handler2 = gsl_set_error_handler_off();
                            integral_status = 1;
                            epsabsvar = epsabs;
                            epsrelvar = epsrel;
                            while(integral_status)
                            {
                                integral_status = gsl_integration_qag(&F1im, lo, up, epsabsvar, epsrelvar, wslimit, 
                                                                      key, ws1im, &result1im, &error1im);

                                epsabsvar *= 1.1;
                                epsrelvar *= 1.1;

                                if(integral_status) {cout << setprecision(3) << scientific
                                                 << "\tProblems in integration; increasing tolerance to "
                                                 << epsabsvar << endl;}
                            }
                            gsl_set_error_handler(old_handler2);
                            gsl_integration_workspace_free(ws1im);

                            F2re.function = &Integrand2re;
                            F2re.params = reinterpret_cast<void *>(&para);
                            ws2re = gsl_integration_workspace_alloc(wslimit);
                            gsl_error_handler_t *old_handler3 = gsl_set_error_handler_off();
                            integral_status = 1;
                            epsabsvar = epsabs;
                            epsrelvar = epsrel;
                            while(integral_status)
                            {
                                integral_status = gsl_integration_qag(&F2re, lo, up, epsabsvar, epsrelvar, wslimit, 
                                                                      key, ws2re, &result2re, &error2re);

                                epsabsvar *= 1.1;
                                epsrelvar *= 1.1;

                                if(integral_status) {cout << setprecision(3) << scientific
                                                 << "\tProblems in integration; increasing tolerance to "
                                                 << epsabsvar << endl;}
                            }
                            gsl_set_error_handler(old_handler3);
                            gsl_integration_workspace_free(ws2re);

                            F2im.function = &Integrand2im;
                            F2im.params = reinterpret_cast<void *>(&para);
                            ws2im = gsl_integration_workspace_alloc(wslimit);
                            gsl_error_handler_t *old_handler4 = gsl_set_error_handler_off();
                            integral_status = 1;
                            epsabsvar = epsabs;
                            epsrelvar = epsrel;
                            while(integral_status)
                            {
                                integral_status = gsl_integration_qag(&F2im, lo, up, epsabsvar, epsrelvar, wslimit, 
                                                                      key, ws2im, &result2im, &error2im);

                                epsabsvar *= 1.1;
                                epsrelvar *= 1.1;

                                if(integral_status) {cout << setprecision(3) << scientific
                                                 << "\tProblems in integration; increasing tolerance to "
                                                 << epsabsvar << endl;}
                            }
                            gsl_set_error_handler(old_handler4);
                            gsl_integration_workspace_free(ws2im);

                            lo = 0.0;
                            up = Time(ntstep, tgridtype, dt, ii);

                            F3re.function = &Integrand3re;
                            F3re.params = reinterpret_cast<void *>(&para);
                            ws3re = gsl_integration_workspace_alloc(wslimit);
                            gsl_error_handler_t *old_handler5 = gsl_set_error_handler_off();
                            integral_status = 1;
                            epsabsvar = epsabs;
                            epsrelvar = epsrel;
                            while(integral_status)
                            {
                                integral_status = gsl_integration_qag(&F3re, lo, up, epsabsvar, epsrelvar, wslimit, 
                                                                      key, ws3re, &result3re, &error3re);

                                epsabsvar *= 1.1;
                                epsrelvar *= 1.1;

                                if(integral_status) {cout << setprecision(3) << scientific
                                                 << "\tProblems in integration; increasing tolerance to "
                                                 << epsabsvar << endl;}
                            }
                            gsl_set_error_handler(old_handler5);
                            gsl_integration_workspace_free(ws3re);

                            F3im.function = &Integrand3im;
                            F3im.params = reinterpret_cast<void *>(&para);
                            ws3im = gsl_integration_workspace_alloc(wslimit);
                            gsl_error_handler_t *old_handler6 = gsl_set_error_handler_off();
                            integral_status = 1;
                            epsabsvar = epsabs;
                            epsrelvar = epsrel;
                            while(integral_status)
                            {
                                integral_status = gsl_integration_qag(&F3im, lo, up, epsabsvar, epsrelvar, wslimit, 
                                                                      key, ws3im, &result3im, &error3im);

                                epsabsvar *= 1.1;
                                epsrelvar *= 1.1;

                                if(integral_status) {cout << setprecision(3) << scientific
                                                 << "\tProblems in integration; increasing tolerance to "
                                                 << epsabsvar << endl;}
                            }
                            gsl_set_error_handler(old_handler6);
                            gsl_integration_workspace_free(ws3im);

                            // This finally evaluates the Ggtr,Glss (sum over j,k,be)
                            temp += (1.0/(2.0*datum::pi))*psi_ueff_r.col(j)*trans(psi_ueff_r.col(k))
                                   *gamma_basis.slice(be)(j,k)*invover_uu_lr(j,j)*invover_uu_rl(k,k)
                                   *exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))
                                   *exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, ii))
                                   *(
                                      (I/(conj(eps_ueff_l(k)) - eps_ueff_l(j)))
                                     *(Psi(conj(eps_ueff_l(k))-mu,beta) - Psi(-(eps_ueff_l(j)-mu),beta))

                                     +(datum::pi/(conj(eps_ueff_l(k)) - eps_ueff_l(j)))
                                      *(exp(I*(eps_ueff_l(j)-conj(eps_ueff_l(k)))*Time(ntstep, tgridtype, dt, l))
                                      *(Delta(l,ii)+Theta(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)))
                                      +exp(I*(eps_ueff_l(j)-conj(eps_ueff_l(k)))*Time(ntstep, tgridtype, dt, ii))
                                      *Theta(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)))

                                      -((result2re + I*result2im) - (result3re + I*result3im))

                                      -(datum::pi/beta)*(result1re + I*result1im)
                                    );

                            temp2 += (1.0/(2.0*datum::pi))*psi_ueff_r.col(j)*trans(psi_ueff_r.col(k))
                                   *gamma_basis.slice(be)(j,k)*invover_uu_lr(j,j)*invover_uu_rl(k,k)
                                   *exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))
                                   *exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, ii))
                                   *(
                                      (I/(conj(eps_ueff_l(k)) - eps_ueff_l(j)))
                                     *(Psi(conj(eps_ueff_l(k))-mu,beta) - Psi(-(eps_ueff_l(j)-mu),beta))

                                     -(datum::pi/(conj(eps_ueff_l(k)) - eps_ueff_l(j)))
                                      *(exp(I*(eps_ueff_l(j)-conj(eps_ueff_l(k)))*Time(ntstep, tgridtype, dt, l))
                                      *(Delta(l,ii)+Theta(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)))
                                      +exp(I*(eps_ueff_l(j)-conj(eps_ueff_l(k)))*Time(ntstep, tgridtype, dt, ii))
                                      *Theta(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)))

                                      -((result2re + I*result2im) - (result3re + I*result3im))

                                      -(datum::pi/beta)*(result1re + I*result1im)
                                    );

                        }   // be<nlead
                    }   // k<nsite
                }   // j<nsite
            }
            Ggtr(i,ii) = temp;
            temp.zeros();
            Glss(i,ii) = temp2;
            temp2.zeros();

        } // nonparallel time loop
    } // parallel time loop

    tmpdble2 = MPI_Wtime();
    if(rank == 0) {StopWatch(tmpdble1, tmpdble2);}

    if(rank == 0) // For testing
    {
        ofstream glssout("output/td_nop_from_glss.out", ios::out);
        for(unsigned int j=0; j<ntstep/size; j++)
        {
            glssout << setprecision(5) << scientific << Time(ntstep, tgridtype, dt, j)
                    << "\t" << real(-2.0*cx_double(0.0,1.0)*Glss(j,j)(0,0)) << endl;
        }
        glssout.close();
    }
}
