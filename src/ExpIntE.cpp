#include "Main.hpp"
cx_double ExpIntE(cx_double z)
{
    cx_double I(0.0,1.0);

    double X, Y, A0, kk;
    cx_double CE1;
    X = real(z);
    Y = imag(z);
    A0 = abs(z);

    if(A0 == 0.0) CE1 = cx_double(1.0e300,0.0);             // Diverges at zero
    else if(A0 <= 10.0 || X < 0.0 && A0 < 20.0)
    {
        CE1 = cx_double(1.0,0.0);
        cx_double CR(1.0,0.0);
        for(int k=1; k<=150; k++)                           // Power series
        {
            kk = static_cast<double>(k);
            CR *= -kk*z/pow((kk+1.0),2);
            CE1 += CR;
            if(abs(CR) <= abs(CE1)*1.0e-15) break;          // Convergence test
        }
//        CE1 = cx_double(-datum::euler - log(z) + z*CE1);
        CE1 = cx_double(exp(z)*(-datum::euler - log(z) + z*CE1));  // Return exp(z)*E1(z) instead (See Sec. 7 in the notes.pdf)
    }
    else
    {
        cx_double CT0(0.0,0.0);
        for(int k=120; k>=1; k--)                           // Continued fraction (''bottom up'')
        {
            kk = static_cast<double>(k);
            CT0 = kk/(1.0+kk/(z+CT0));
        }
        cx_double CT(1.0/(z+CT0));

//        CE1 = cx_double(exp(-z)*CT);
        CE1 = cx_double(CT);                                // Return exp(z)*E1(z) instead (See Sec. 7 in the notes.pdf)
        if(X <= 0.0 && Y == 0.0) CE1 = CE1-I*datum::pi;    // Branch cut on the negative real axis
    }

    return CE1;
}
