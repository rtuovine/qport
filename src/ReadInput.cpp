#include "Main.hpp"
void ReadInput(int &nsite, int &systype, int &biastype, int &nlead, int &ntstep, int &tgridtype, 
               int &nwstep, int &layers, int &snaps, int &pert, int &sc, int &finitet, int &besselorder, int &NstochMax,
               vector<int> &lcontact, vector<int> &rcontact, vector<int> &lcontact2, vector<int> &rcontact2, 
               vector<int> &bridgeone, vector<int> &bridgetwo, vector<int> &bridgethree, 
               double &lambda_strength, double &gamma_strength, double &mol_hop, double &lead_hop,
               int &lead_row, int &bridge_sites, double &dt, double &bias_strength, double &cutoff,
               cx_double &sc_delta, double &beta, vector<double> &constpart, vector<double> &amplitude, 
               vector<double> &frequency, vector<double> &phase, int &freq_mult1, int &freq_mult2, double &ampl_rat,
               int &padeorder, int &subs, double &acc, int &rule, int &partpara, vector<int> &calc,
               vector<vector<double> > &coords, double &scw_t, double &scw_Vz, double &scw_aso, double &scw_Delta,
               double &scw_mu, double &scw_gate, double &Dfluct, double &wcorr)
{

// Read parameters from files "*.in" into stream 'input' and further into corresponding variables
    string temp;
    int tempint;
    double tempdble;
    ifstream input, inputtmp;
    input.open("input/sysparm.in");
    input >> systype; getline(input,temp);
    input >> pert; getline(input,temp);
    input >> sc; getline(input,temp);
    input >> sc_delta; getline(input,temp);
    input >> finitet; getline(input,temp);
    input >> beta; getline(input,temp);
    input >> layers; getline(input,temp);
    input >> nlead; getline(input,temp);
    input >> lead_hop; getline(input,temp);
    getline(input,temp);
    if(systype == 10)
    {
        inputtmp.open("input/misc.in");
        inputtmp >> nsite; getline(input,temp);
        inputtmp >> mol_hop; getline(input,temp);
        inputtmp >> lead_row; getline(input,temp);
        inputtmp >> bridge_sites; getline(input,temp);
        inputtmp.close();
    }
    else
    {
        input >> nsite; getline(input,temp);
        input >> mol_hop; getline(input,temp);
        input >> lead_row; getline(input,temp);
        input >> bridge_sites; getline(input,temp);
    }
    input.close();

    input.open("input/contparm.in");
    input >> lambda_strength; getline(input,temp);
    input >> gamma_strength; getline(input,temp);
    input >> biastype; getline(input,temp);
    input >> bias_strength; getline(input,temp);
    getline(input,temp);
    if(systype == 10)
    {
        if(nlead == 2) // Two-lead model
        {
            inputtmp.open("input/lcouple.in");
            for(unsigned int j=0; j<lead_row; j++)
            {
                inputtmp >> tempint; getline(input,temp);
                lcontact.push_back(tempint);
            }
            inputtmp.close();

            inputtmp.open("input/rcouple.in");
            for(unsigned int j=0; j<lead_row; j++)
            {
                inputtmp >> tempint; getline(input,temp);
                rcontact.push_back(tempint);
            }
            inputtmp.close();
        }
        if(nlead == 3) // Three-lead model
        {
            inputtmp.open("input/lcouple.in");
            for(unsigned int j=0; j<lead_row; j++)
            {
                inputtmp >> tempint; getline(input,temp);
                lcontact.push_back(tempint);
            }
            inputtmp.close();

            inputtmp.open("input/lcouple2.in");
            for(unsigned int j=0; j<lead_row; j++)
            {
                inputtmp >> tempint; getline(input,temp);
                lcontact2.push_back(tempint);
            }
            inputtmp.close();            

            inputtmp.open("input/rcouple.in");
            for(unsigned int j=0; j<lead_row; j++)
            {
                inputtmp >> tempint; getline(input,temp);
                rcontact.push_back(tempint);
            }
            inputtmp.close();
        }
        if(nlead == 4) // Four-lead model
        {
            inputtmp.open("input/lcouple.in");
            for(unsigned int j=0; j<lead_row; j++)
            {
                inputtmp >> tempint; getline(input,temp);
                lcontact.push_back(tempint);
            }
            inputtmp.close();

            inputtmp.open("input/lcouple2.in");
            for(unsigned int j=0; j<lead_row; j++)
            {
                inputtmp >> tempint; getline(input,temp);
                lcontact2.push_back(tempint);
            }
            inputtmp.close();            

            inputtmp.open("input/rcouple.in");
            for(unsigned int j=0; j<lead_row; j++)
            {
                inputtmp >> tempint; getline(input,temp);
                rcontact.push_back(tempint);
            }
            inputtmp.close();

            inputtmp.open("input/rcouple2.in");
            for(unsigned int j=0; j<lead_row; j++)
            {
                inputtmp >> tempint; getline(input,temp);
                rcontact2.push_back(tempint);
            }
            inputtmp.close();
        }

        inputtmp.open("input/bridgeone.in");
        for(unsigned int j=0; j<bridge_sites; j++)
        {
            inputtmp >> tempint; getline(input,temp);
            bridgeone.push_back(tempint);
        }
        inputtmp.close();

        inputtmp.open("input/bridgetwo.in");
        for(unsigned int j=0; j<bridge_sites; j++)
        {
            inputtmp >> tempint; getline(input,temp);
            bridgetwo.push_back(tempint);
        }
        inputtmp.close();

        inputtmp.open("input/bridgethree.in");
        for(unsigned int j=0; j<bridge_sites; j++)
        {
            inputtmp >> tempint; getline(input,temp);
            bridgethree.push_back(tempint);
        }
        inputtmp.close();
    }
    else
    {
        for(unsigned int j=0; j<lead_row; j++)
        {
            input >> tempint; getline(input,temp);
            lcontact.push_back(tempint);
        }
        for(unsigned int j=0; j<lead_row; j++)
        {
            input >> tempint; getline(input,temp);
            rcontact.push_back(tempint);
        }
        for(unsigned int j=0; j<bridge_sites; j++)
        {
            input >> tempint; getline(input,temp);
            bridgeone.push_back(tempint);
        }
        for(unsigned int j=0; j<bridge_sites; j++)
        {
            input >> tempint; getline(input,temp);
            bridgetwo.push_back(tempint);
        }
        for(unsigned int j=0; j<bridge_sites; j++)
        {
            input >> tempint; getline(input,temp);
            bridgethree.push_back(tempint);
        }
    }
    input.close();
    
    input.open("input/tdparm.in");
    input >> ntstep; getline(input,temp);
    input >> tgridtype; getline(input,temp);
    input >> nwstep; getline(input,temp);
    input >> dt; getline(input,temp);
    input >> snaps; getline(input,temp);
    input >> cutoff; getline(input,temp);
    input.close();

    // Input parameters for the stochastic bias
    input.open("input/stochparm.in");
    input >> Dfluct; getline(input,temp);
    input >> wcorr; getline(input,temp);
    input.close();

    // Input parameters for the superconducting wire
    input.open("input/scwparm.in");
    input >> scw_t; getline(input,temp);
    input >> scw_Vz; getline(input,temp);
    input >> scw_aso; getline(input,temp);
    input >> scw_Delta; getline(input,temp);
    input >> scw_mu; getline(input,temp);
    input >> scw_gate; getline(input,temp);
    input.close();
    
// TD bias parameters
// (adding a small number as 'zero' in case some special functions would not converge)
    input.open("input/biasparm.in");
    for(unsigned int j=0; j<nlead; j++)
    {
        getline(input,temp);
        input >> tempdble; getline(input,temp);
        constpart.push_back(tempdble+1.0e-15);
        input >> tempdble; getline(input,temp);
        amplitude.push_back(tempdble+1.0e-15);
        input >> tempdble; getline(input,temp);
        frequency.push_back(tempdble+1.0e-15);
        input >> tempdble; getline(input,temp);
        phase.push_back(tempdble+1.0e-15);
    }
    getline(input,temp);
    input >> freq_mult1; getline(input,temp);
    input >> freq_mult2; getline(input,temp);
    input >> tempdble; getline(input,temp);
    ampl_rat = tempdble+1.0e-15;
    input.close();

    input.open("input/numparm.in");
    input >> besselorder; getline(input,temp);
    input >> NstochMax; getline(input,temp);
    input >> subs; getline(input,temp);
    input >> acc; getline(input,temp);
    input >> rule; getline(input,temp);
    input >> padeorder; getline(input,temp);
    input >> partpara; getline(input,temp);
    input >> tempint; getline(input,temp);
    calc.push_back(tempint);
    input >> tempint; getline(input,temp);
    calc.push_back(tempint);
    input >> tempint; getline(input,temp);
    calc.push_back(tempint);
    input >> tempint; getline(input,temp);
    calc.push_back(tempint);
    input >> tempint; getline(input,temp);
    calc.push_back(tempint);
    input >> tempint; getline(input,temp);
    calc.push_back(tempint);
    input >> tempint; getline(input,temp);
    calc.push_back(tempint);
    input >> tempint; getline(input,temp);
    calc.push_back(tempint);
    input >> tempint; getline(input,temp);
    calc.push_back(tempint);
    input >> tempint; getline(input,temp);
    calc.push_back(tempint);
    input >> tempint; getline(input,temp);
    calc.push_back(tempint);
    input.close();

    if(systype == 10)
    {
		input.open("input/coords.in");
		for(unsigned int j=0; j<nsite; j++)
		{
			 vector<double> row;
			 coords.push_back(row);
		}
		for(unsigned int j=0; j<nsite; j++)
		{
			input >> tempint;
			input >> tempdble;
			coords[tempint].push_back(tempdble);
			input >> tempdble;
			coords[tempint].push_back(tempdble);
			input >> tempdble; getline(input,temp);
			coords[tempint].push_back(tempdble);
		}
		input.close();
	}

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if(rank == 0) 
    {
    // Print the values of the variables
        cout << endl;
        cout << " _______________________________________________________"                           << endl;
        cout << "|                                                       |"                          << endl;
        cout << "|" << "\t" << "W  E  L  C  O  M  E    T  O    Q  P  O  R  T" << "\t" << "|"         << endl;        
        cout << "|_______________________________________________________|"                          << endl;
        cout << "|                                                       |"                          << endl;
        cout << "|" << "\t" << "Central region:" << "\t\t\t\t\t" << "|"                              << endl;
        cout << "|" << "\t" << "--------------- " << "\t\t\t\t" << "|"                               << endl;
        cout << "|" << "\t";
        if(systype == 1) cout << "TB chain\t\t\t" << nsite << " sites" << "\t\t" << "|"              << endl;
        else if(systype == 2) cout << "TB ring\t\t\t\t" << nsite << " sites" << "\t\t" << "|"        << endl;
        else if(systype == 3) cout << "FB chain\t\t\t" << nsite << " sites" << "\t\t" << "|"         << endl;
        else if(systype == 4) cout << "FB ring\t\t\t\t" << nsite << " sites" << "\t\t" << "|"        << endl;
        else if(systype == 5) cout << "2-level molecule" << "\t\t\t\t" << "|"                        << endl;
        else if(systype == 6) cout << "Degenerate 2-level molecule" << "\t\t\t" << "|"               << endl;
        else if(systype == 7) cout << "1-level molecule" << "\t\t\t\t" << "|"                        << endl;
        else if(systype == 8) cout << "Quadruplet" << "\t\t\t\t" << "|"                              << endl;
        else if(systype == 9) cout << "Kagome lattice\t\t\t" << nsite << " sites" << "\t\t" << "|"   << endl;
        else if(systype == 10){
                             cout << "From file (matrix.in)\t\t" << nsite << " sites" << "\t" << "|" << endl;
                              }
        else if(systype == 12) cout << "Majorana wire\t\t\t" << nsite/4 << " sites" << "\t" << "|" << endl;
        else{
                 cout << "Unknown central region, " << nsite << " sites \t\t" << "|"                 << endl;
             }
        cout << "|" << "\t" << setprecision(3) << fixed << "Hopping:" 
             << "\t\t\t" << mol_hop << "\t\t" << "|"                                                 << endl;
        cout << "|" << "\t" << setprecision(3) << fixed << "Coupling strength to the leads:"
             << "\t" << lambda_strength << "\t\t" << "|"                                             << endl;
        if(pert == 1) cout << "|" << "\t" << "Perturbation?" << "\t\t\t" << "Yes" << "\t\t" << "|"   << endl;
        if(pert == 0) cout << "|" << "\t" << "Perturbation?" << "\t\t\t" << "No" << "\t\t" << "|"    << endl;
        if(sc == 1) cout << "|" << "\t" << "Superconducting?" << "\t\t" << "Yes" << "\t\t" << "|"    << endl;
        if(sc == 0) cout << "|" << "\t" << "Superconducting?" << "\t\t" << "No" << "\t\t" << "|"     << endl;
        if(finitet == 1) cout << "|" << "\t" << "Inverse temperature:" << "\t\t" << beta << "\t\t" << "|"    << endl;
        if(finitet == 0) cout << "|" << "\t" << "Inverse temperature:" << "\t\t" << "Inf" << "\t\t" << "|"     << endl;
        cout << "|                                                       |"                          << endl;
        cout << "|" << "\t" << "Leads:" << "\t\t\t\t\t\t" << "|"                                     << endl;
        cout << "|" << "\t" << "------" << "\t\t\t\t\t\t" << "|"                                     << endl;
        cout << "|" << "\t" << "Number of leads:" << "\t\t" << nlead << "\t\t" << "|"                << endl;
        cout << "|" << "\t" << "Number of rows in the leads:" << "\t" << lead_row << "\t\t" << "|"   << endl;
        cout << "|" << "\t" << setprecision(3) << fixed << "Hopping:" 
             << "\t\t\t" << lead_hop << "\t\t" << "|"                                                << endl;
        cout << "|" << "\t" << setprecision(3) << fixed << "Broadening (Gamma):"
             << "\t\t" << gamma_strength << "\t\t" << "|"                                            << endl;
        cout << "|" << "\t" << "Bias type:" << "\t\t\t";
        if(biastype == 1){
                         cout << "Symmetric, " "\t" << "|"                                           << endl;
                         cout << "|                                       V_L = -V_R" << "\t" << "|" << endl;
                         }
        else cout << "One-sided" << "\t" << "|"                                                      << endl;
        cout << "|" << "\t" << setprecision(2) << "Bias strength:" 
             << "\t\t\t" << bias_strength << "\t\t" << "|"                                           << endl;
        cout << "|                                                       |"                          << endl;
        cout << "|" << "\t" << "Time propagation:" << "\t\t\t\t" << "|"                              << endl;
        cout << "|" << "\t" << "-----------------" << "\t\t\t\t" << "|"                              << endl;
        cout << "|" << "\t" << "Number of time steps:" << "\t\t" << ntstep << "\t\t" << "|"          << endl;
        if(tgridtype == 1)
        {
            cout << "|" << "\t" << "Timestepping:" << "\t\t\t" << "linear" << "\t\t" << "|"          << endl;
            cout << "|" << "\t" << setprecision(1) << fixed << "Propagation time:" << "\t\t"
                 << Time(ntstep, tgridtype, dt, ntstep) << "\t\t" << "|"                             << endl;
        }
        else if(tgridtype == 2)
        {
            cout << "|" << "\t" << "Timestepping:" << "\t\t\t" << "power" << "\t\t" << "|"           << endl;
            cout << "|" << "\t" << setprecision(1) << fixed << "Propagation time:" << "\t\t"
                 << Time(ntstep, tgridtype, dt, ntstep) << "\t\t" << "|"                             << endl;
        }
        cout << "|" << "\t" << "Number of Fourier points:" << "\t" << nwstep << "\t\t" << "|"        << endl;
        cout << "|" << "\t" << "Cut-off energy:" << "\t\t\t" << setprecision(2) << cutoff
             << "\t\t" << "|"              << endl;
        cout << "|                                                       |"                          << endl;
        cout << "|" << "\t" << "Output:" << "\t\t\t\t\t\t" << "|"                                    << endl;
        cout << "|" << "\t" << "-------" << "\t\t\t\t\t\t" << "|"                                    << endl;
        if(snaps == 1) cout << "|" << "\t" << "Density-matrix snapshots?"
                            << "\t" << "Yes" << "\t\t" << "|"                                        << endl;
        if(snaps == 0) cout << "|" << "\t" << "Density-matrix snapshots?" 
                            << "\t" << "No" << "\t\t" << "|"                                         << endl;
        cout << "|_______________________________________________________|"                          << endl;
        cout << endl;
    }
}
