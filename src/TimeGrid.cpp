#include "Main.hpp"
double Time(int ntstep, int tgridtype, double dt, int j)
{
    double delta = 1.0e-15;

    if(tgridtype == 1)          // Uniform time stepping
    {
        return (j+delta)*dt;
    }
    else if(tgridtype == 2)     // Non-uniform time stepping; highly experimental!
    {
        double cut1 = 0.2;
        double cut2 = 0.8;
        double alpha = 1.5;
        double beta = 3.5;
        if(static_cast<double>(j)/static_cast<double>(ntstep) < cut1)
        {
            return (j+delta)*dt/2.0;
        }
        else if(static_cast<double>(j)/static_cast<double>(ntstep) < cut2)
        {
            return (j+delta)*dt/2.0+pow((j+delta-cut1*static_cast<double>(ntstep))*dt,alpha);
        }
        else
        {
            return (j+delta)*dt/2.0+pow((j+delta-cut1*static_cast<double>(ntstep))*dt,alpha)+pow((j+delta-cut2*static_cast<double>(ntstep))*dt,beta);
        }
	}
    else{ cout << "Erroneous time grid type in TimeGrid.cpp, exiting ..." << endl; exit(1);}
}
