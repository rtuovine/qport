#include "Main.hpp"
void SCHamiltonian(int nsite, cx_mat Hu, cx_mat Hp, cx_mat &Hu_sc, cx_mat &Hp_sc, cx_double sc_delta)
{
    Hu_sc.zeros();
    Hp_sc.zeros();

    cx_double eta(1.0e-15,1.0e-15); // When Delta->0 some accidental degeneracies might be lifted; this is a numerical fix
    double phase2 = 0.0;
    double phase4 = 0.0;//datum::pi;
    cx_double I(0.0,1.0);

// Localized delta within the central region
    const int localdelta = 0;

    ifstream input("input/local.out");
    if(!input){ cout << "Erroneous input file in SCHamiltonian.cpp, exiting ..." << endl; exit(1);}
    int a, b, temp;
    vec locdel(nsite);
    string line;

    while(getline(input, line))
    {
        stringstream ss(line);
        if (ss >> a >> b >> temp)
        {
            locdel(a) = b;
        }
    }

    for(unsigned int j=0; j<2*nsite; j++)
    {
        for(unsigned int k=0; k<2*nsite; k++)
        {
            if(j < nsite && k < nsite)
            {
                Hu_sc(j,k) = Hu(j,k);
                Hp_sc(j,k) = Hp(j,k);
            }

            if(j >= nsite && k >= nsite)
            {
                Hu_sc(j,k) = -Hu(j-nsite,k-nsite);
                Hp_sc(j,k) = -Hp(j-nsite,k-nsite);
                // CHECK IF THIS MAKES A DIFFERENCE
                // Hp_sc(j,k) = -Hu(j-nsite,k-nsite) + (Hp(j-nsite,k-nsite) - Hu(j-nsite,k-nsite));
                // Here the down-down block for perturbed central region is not -Hp since the 
                // gate still acts as a raising potential: -Hu + gate
            }

            if(j >= nsite && k <= nsite)
            {
                if(localdelta == 0)
                {
                    if(j >= k + nsite && j <= k + nsite)
                    {
                        Hu_sc(j,k) = conj(sc_delta+eta);    // See the meaning of eta above
                        Hp_sc(j,k) = conj(sc_delta+eta);
                    }
                }
                
                else if(localdelta == 1)
                {
                    if(j >= k + nsite && j <= k + nsite)
                    {
                        if(locdel(j-nsite) == 1)
                        {
                            Hu_sc(j,k) = conj(sc_delta*exp(I*phase2));
                            Hp_sc(j,k) = conj(sc_delta*exp(I*phase2));
                            //Hu_sc(j,k) = 0.0;
                            //Hp_sc(j,k) = 0.0;
                        }
                        else if(locdel(j-nsite) == 2)
                        {
                            Hu_sc(j,k) = conj(sc_delta*exp(I*phase4));
                            Hp_sc(j,k) = conj(sc_delta*exp(I*phase4));
                            //Hu_sc(j,k) = 0.0;
                            //Hp_sc(j,k) = 0.0;
                        }
                        else
                        {
                            Hu_sc(j,k) = eta;
                            Hp_sc(j,k) = eta;
                            //Hu_sc(j,k) = conj(sc_delta*exp(I*phase4));
                            //Hp_sc(j,k) = conj(sc_delta*exp(I*phase4));
                        }
                    }
                }

                else{ cout << "Erroneous local delta in SCHamiltonian.cpp, exiting ..." << endl; exit(1);}
            }
            else if(j <= nsite && k >= nsite)
            {
                if(localdelta == 0)
                {
                    if(k >= j + nsite && k <= j + nsite)
                    {
                        Hu_sc(j,k) = sc_delta+eta;
                        Hp_sc(j,k) = sc_delta+eta;
                    }
                }
                else if(localdelta == 1)
                {
                    if(k >= j + nsite && k <= j + nsite)
                    {
                        if(locdel(j) == 1)
                        {
                            Hu_sc(j,k) = sc_delta*exp(I*phase2);
                            Hp_sc(j,k) = sc_delta*exp(I*phase2);
                        }
                        else if(locdel(j) == 2)
                        {
                            Hu_sc(j,k) = sc_delta*exp(I*phase4);
                            Hp_sc(j,k) = sc_delta*exp(I*phase4);
                        }
                        else
                        {
                            Hu_sc(j,k) = eta;
                            Hp_sc(j,k) = eta;
                        }
                    }
                }
                else{ cout << "Erroneous local delta in SCHamiltonian.cpp, exiting ..." << endl; exit(1);}
            }
        }
    }

// For small enough central systems, print the real part of the Hamiltonian matrix
    mat Hreal = real(Hu_sc);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if(rank == 0) 
    {
        cout << "Given Hamiltonian:" << endl;
        if(nsite <= 8) Hreal.print(cout, "H =");
        else cout << "(not shown)" << endl;
    }
}
