#include "Main.hpp"
void FourierTransform(int nsite, int nlead, int ntstep, int tgridtype, int nwstep, int sc, double dt, cx_double bondcurrent_one_ss, 
                      cx_vec bondcurrent_one, cx_vec &bondcurrent_one_ft, cx_double bondcurrent_two_ss, 
                      cx_vec bondcurrent_two, cx_vec &bondcurrent_two_ft, cx_double bondcurrent_three_ss, 
                      cx_vec bondcurrent_three, cx_vec &bondcurrent_three_ft, cx_double supercurrent_one_ss, 
                      cx_vec supercurrent_one, cx_vec &supercurrent_one_ft, cx_double supercurrent_two_ss, 
                      cx_vec supercurrent_two, cx_vec &supercurrent_two_ft, cx_double supercurrent_three_ss, 
                      cx_vec supercurrent_three, cx_vec &supercurrent_three_ft, cx_mat pairdensity, cx_mat &pairdensity_ft,
                      cx_mat supermatrix, cx_mat &supermatrix_ft, cx_mat TDCurrent, cx_mat &TDCurrent_ft,
                      cx_vec NC, cx_vec &NC_ft, vector<int> calc, cx_vec dipmom, cx_vec &dipmom_ft)
{
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    if(rank == 0) 
    {
        cout << endl;
        cout << "Fourier transforming... " << endl;
    }

    cx_double dw((2.0*datum::pi)/(nwstep*Time(ntstep, tgridtype, dt, ntstep)/ntstep),0.0); // Frequency step
    cx_double w(0.0,0.0);

	cx_double I(0.0,1.0);

    bondcurrent_one_ft.zeros();
    bondcurrent_two_ft.zeros();
    bondcurrent_three_ft.zeros();

    TDCurrent_ft.zeros();
    NC_ft.zeros();
    dipmom_ft.zeros();

    supercurrent_one_ft.zeros();
    supercurrent_two_ft.zeros();
    supercurrent_three_ft.zeros();

    cx_double sum_one, sum_two, sum_three, sum_current, sum_current2, 
              sum_N, sum_dip, sum_super_one, sum_super_two, sum_super_three;

    pairdensity_ft.zeros();
    cx_double sum_pair;
    cx_vec pairdensity_ss(nsite);
    if(rank == size-1) // Obtain the last element of the pairing density (as the steady-state value)
    {
        for(unsigned int j=0; j<nsite; j++)
        {
            pairdensity_ss(j) = pairdensity(j,ntstep/size-1);
        }
    }

	cx_double current_ss(real(TDCurrent(0,ntstep/size-1)));
	cx_double current2_ss(real(TDCurrent(1,ntstep/size-1)));
	cx_double dipmom_ss(dipmom(ntstep/size-1)/3.33564e-30);

    supermatrix_ft.zeros();
    cx_double sum_supermatrix;
    cx_vec supermatrix_ss(nsite);
    if(rank == size-1) // Obtain the last element of the super current matrix (as the steady-state value)
    {
        for(unsigned int j=0; j<nsite; j++)
        {
            supermatrix_ss(j) = supermatrix(j,ntstep/size-1);
        }
    }

    unsigned int elems = ntstep/size;   // How many elements on each process
    // Gather the bondcurrents from each process into root and do the Fourier transform
    double sendbuff_one[elems], *recvbuff_one, *sendbuf2_one, recvbuf2_one[nwstep/size], 
           sendbuff_two[elems], *recvbuff_two, *sendbuf2_two, recvbuf2_two[nwstep/size], 
           sendbuff_three[elems], *recvbuff_three, *sendbuf2_three, recvbuf2_three[nwstep/size],
           sendbuff_current[elems], *recvbuff_current, *sendbuf2_current, recvbuf2_current[nwstep/size],
           sendbuff_current2[elems], *recvbuff_current2, *sendbuf2_current2, recvbuf2_current2[nwstep/size],
           sendbuff_N[elems], *recvbuff_N, *sendbuf2_N, recvbuf2_N[nwstep/size],
           sendbuff_dip[elems], *recvbuff_dip, *sendbuf2_dip, recvbuf2_dip[nwstep/size],
           sendbuff_super_one[elems], *recvbuff_super_one, *sendbuf2_super_one, recvbuf2_super_one[nwstep/size], 
           sendbuff_super_two[elems], *recvbuff_super_two, *sendbuf2_super_two, recvbuf2_super_two[nwstep/size], 
           sendbuff_super_three[elems], *recvbuff_super_three, *sendbuf2_super_three, recvbuf2_super_three[nwstep/size];
    MPI_Datatype recvtype_one, recvtype2_one, recvtype_two, recvtype2_two, 
                 recvtype_three, recvtype2_three, recvtype_current, recvtype2_current, 
                 recvtype_N, recvtype2_N, recvtype_current2, recvtype2_current2,
                 recvtype_dip, recvtype2_dip,
                 recvtype_super_one, recvtype2_super_one, recvtype_super_two, recvtype2_super_two, 
                 recvtype_super_three, recvtype2_super_three;

    for(unsigned int i=0; i<elems; i++)
    {
        sendbuff_one[i] = real(bondcurrent_one(i)); // Each process has its own sendbuff
        sendbuff_two[i] = real(bondcurrent_two(i));
        sendbuff_three[i] = real(bondcurrent_two(i));

        sendbuff_current[i] = real(TDCurrent(0,i));
        sendbuff_current2[i] = real(TDCurrent(1,i));
        sendbuff_N[i] = real(NC(i));
        sendbuff_dip[i] = real(dipmom(i))/3.33564e-30;

        if(sc == 1)
        {
            sendbuff_super_one[i] = real(supercurrent_one(i));
            sendbuff_super_two[i] = real(supercurrent_two(i));
            sendbuff_super_three[i] = real(supercurrent_two(i));
        }
    }

    recvbuff_one = (double*)malloc(ntstep*sizeof(double));
    sendbuf2_one = (double*)malloc(nwstep*sizeof(double));
    MPI_Type_contiguous(elems, MPI_DOUBLE, &recvtype_one);
    MPI_Type_commit(&recvtype_one);
    MPI_Type_contiguous(nwstep/size, MPI_DOUBLE, &recvtype2_one);
    MPI_Type_commit(&recvtype2_one);

    recvbuff_two = (double*)malloc(ntstep*sizeof(double));
    sendbuf2_two = (double*)malloc(nwstep*sizeof(double));
    MPI_Type_contiguous(elems, MPI_DOUBLE, &recvtype_two);
    MPI_Type_commit(&recvtype_two);
    MPI_Type_contiguous(nwstep/size, MPI_DOUBLE, &recvtype2_two);
    MPI_Type_commit(&recvtype2_two);

    recvbuff_three = (double*)malloc(ntstep*sizeof(double));
    sendbuf2_three = (double*)malloc(nwstep*sizeof(double));
    MPI_Type_contiguous(elems, MPI_DOUBLE, &recvtype_three);
    MPI_Type_commit(&recvtype_three);
    MPI_Type_contiguous(nwstep/size, MPI_DOUBLE, &recvtype2_three);
    MPI_Type_commit(&recvtype2_three);

    recvbuff_current = (double*)malloc(ntstep*sizeof(double));
    sendbuf2_current = (double*)malloc(nwstep*sizeof(double));
    MPI_Type_contiguous(elems, MPI_DOUBLE, &recvtype_current);
    MPI_Type_commit(&recvtype_current);
    MPI_Type_contiguous(nwstep/size, MPI_DOUBLE, &recvtype2_current);
    MPI_Type_commit(&recvtype2_current);

    recvbuff_current2 = (double*)malloc(ntstep*sizeof(double));
    sendbuf2_current2 = (double*)malloc(nwstep*sizeof(double));
    MPI_Type_contiguous(elems, MPI_DOUBLE, &recvtype_current2);
    MPI_Type_commit(&recvtype_current2);
    MPI_Type_contiguous(nwstep/size, MPI_DOUBLE, &recvtype2_current2);
    MPI_Type_commit(&recvtype2_current2);

    recvbuff_N = (double*)malloc(ntstep*sizeof(double));
    sendbuf2_N = (double*)malloc(nwstep*sizeof(double));
    MPI_Type_contiguous(elems, MPI_DOUBLE, &recvtype_N);
    MPI_Type_commit(&recvtype_N);
    MPI_Type_contiguous(nwstep/size, MPI_DOUBLE, &recvtype2_N);
    MPI_Type_commit(&recvtype2_N);

    recvbuff_dip = (double*)malloc(ntstep*sizeof(double));
    sendbuf2_dip = (double*)malloc(nwstep*sizeof(double));
    MPI_Type_contiguous(elems, MPI_DOUBLE, &recvtype_dip);
    MPI_Type_commit(&recvtype_dip);
    MPI_Type_contiguous(nwstep/size, MPI_DOUBLE, &recvtype2_dip);
    MPI_Type_commit(&recvtype2_dip);

    recvbuff_super_one = (double*)malloc(ntstep*sizeof(double));
    sendbuf2_super_one = (double*)malloc(nwstep*sizeof(double));
    MPI_Type_contiguous(elems, MPI_DOUBLE, &recvtype_super_one);
    MPI_Type_commit(&recvtype_super_one);
    MPI_Type_contiguous(nwstep/size, MPI_DOUBLE, &recvtype2_super_one);
    MPI_Type_commit(&recvtype2_super_one);

    recvbuff_super_two = (double*)malloc(ntstep*sizeof(double));
    sendbuf2_super_two = (double*)malloc(nwstep*sizeof(double));
    MPI_Type_contiguous(elems, MPI_DOUBLE, &recvtype_super_two);
    MPI_Type_commit(&recvtype_super_two);
    MPI_Type_contiguous(nwstep/size, MPI_DOUBLE, &recvtype2_super_two);
    MPI_Type_commit(&recvtype2_super_two);

    recvbuff_super_three = (double*)malloc(ntstep*sizeof(double));
    sendbuf2_super_three = (double*)malloc(nwstep*sizeof(double));
    MPI_Type_contiguous(elems, MPI_DOUBLE, &recvtype_super_three);
    MPI_Type_commit(&recvtype_super_three);
    MPI_Type_contiguous(nwstep/size, MPI_DOUBLE, &recvtype2_super_three);
    MPI_Type_commit(&recvtype2_super_three);

    // The TD bondcurrent is gathered to recvbuff in rank=0 (root) process
    MPI_Gather(sendbuff_one, elems, MPI_DOUBLE, recvbuff_one, 1, recvtype_one, 0, MPI_COMM_WORLD);
    MPI_Gather(sendbuff_two, elems, MPI_DOUBLE, recvbuff_two, 1, recvtype_two, 0, MPI_COMM_WORLD);
    MPI_Gather(sendbuff_three, elems, MPI_DOUBLE, recvbuff_three, 1, recvtype_three, 0, MPI_COMM_WORLD);

    MPI_Gather(sendbuff_current, elems, MPI_DOUBLE, recvbuff_current, 1, recvtype_current, 0, MPI_COMM_WORLD);
    MPI_Gather(sendbuff_current2, elems, MPI_DOUBLE, recvbuff_current2, 1, recvtype_current2, 0, MPI_COMM_WORLD);
    MPI_Gather(sendbuff_N, elems, MPI_DOUBLE, recvbuff_N, 1, recvtype_N, 0, MPI_COMM_WORLD);
    MPI_Gather(sendbuff_dip, elems, MPI_DOUBLE, recvbuff_dip, 1, recvtype_dip, 0, MPI_COMM_WORLD);

    if(sc == 1)
    {
        MPI_Gather(sendbuff_super_one, elems, MPI_DOUBLE, recvbuff_super_one, 1, recvtype_super_one, 0, MPI_COMM_WORLD);
        MPI_Gather(sendbuff_super_two, elems, MPI_DOUBLE, recvbuff_super_two, 1, recvtype_super_two, 0, MPI_COMM_WORLD);
        MPI_Gather(sendbuff_super_three, elems, MPI_DOUBLE, recvbuff_super_three, 1, recvtype_super_three, 0, MPI_COMM_WORLD);
    }

    const int wtype = 1;  // Windowtype: 1=Blackman, 2=Nuttall, 3=Blackman-Nuttall, 4=Blackman-Harris, 5=flat top

    cx_vec armadummy_one(ntstep), armadummy_two(ntstep), armadummy_three(ntstep),
           armadummy_super_one(ntstep), armadummy_super_two(ntstep), armadummy_super_three(ntstep),
           armadummy_dip(ntstep);

// TEST

    double average_one, average_two, average_three, average_current, average_current2, average_N, average_dip;
    double average_super_one, average_super_two, average_super_three;
    double tmpav_one = 0.0;
    double tmpav_two = 0.0;
    double tmpav_three = 0.0;
    double tmpav_current = 0.0;
    double tmpav_current2 = 0.0;
    double tmpav_N = 0.0;
    double tmpav_dip = 0.0;
    double tmpav_super_one = 0.0;
    double tmpav_super_two = 0.0;
    double tmpav_super_three = 0.0;
    for(unsigned int j=0; j<ntstep; j++)
    {
        tmpav_one += recvbuff_one[j];
        tmpav_two += recvbuff_two[j];
        tmpav_three += recvbuff_three[j];
        tmpav_current += recvbuff_current[j];
        tmpav_current2 += recvbuff_current2[j];
        tmpav_N += recvbuff_N[j];
        tmpav_dip += recvbuff_dip[j];
        if(sc == 1)
        {
            tmpav_super_one += recvbuff_super_one[j];
            tmpav_super_two += recvbuff_super_two[j];
            tmpav_super_three += recvbuff_super_three[j];
        }
    }
    average_one = tmpav_one/ntstep;
    average_two = tmpav_two/ntstep;
    average_three = tmpav_three/ntstep;
    average_current = tmpav_current/ntstep;
    average_current2 = tmpav_current2/ntstep;
    average_N = tmpav_N/ntstep;
    average_dip = tmpav_dip/ntstep;
    if(sc == 1)
    {
        average_super_one = tmpav_super_one/ntstep;
        average_super_two = tmpav_super_two/ntstep;
        average_super_three = tmpav_super_three/ntstep;
    }

// TEST

    for(unsigned int j=0; j<ntstep; j++)
    {
      //        armadummy_one(j) = (recvbuff_one[j] - bondcurrent_one_ss)*WindowFunc(j,ntstep,wtype);
      //        armadummy_two(j) = (recvbuff_two[j] - bondcurrent_two_ss)*WindowFunc(j,ntstep,wtype);
      //        armadummy_three(j) = (recvbuff_three[j] - bondcurrent_three_ss)*WindowFunc(j,ntstep,wtype);

        armadummy_super_one(j) = (recvbuff_super_one[j] - supercurrent_one_ss)*WindowFunc(j,ntstep,wtype);
        armadummy_super_two(j) = (recvbuff_super_two[j] - supercurrent_two_ss)*WindowFunc(j,ntstep,wtype);
        armadummy_super_three(j) = (recvbuff_super_three[j] - supercurrent_three_ss)*WindowFunc(j,ntstep,wtype);

		//armadummy_dip(j) = (recvbuff_dip[j] - average_dip)*WindowFunc(j,ntstep,wtype);
		armadummy_dip(j) = (recvbuff_dip[j] - dipmom_ss)*WindowFunc(j,ntstep,wtype);

        armadummy_one(j) = (recvbuff_one[j] - average_one)*WindowFunc(j,ntstep,wtype);
        armadummy_two(j) = (recvbuff_two[j] - average_two)*WindowFunc(j,ntstep,wtype);
        armadummy_three(j) = (recvbuff_three[j] - average_three)*WindowFunc(j,ntstep,wtype);
    }


    if(rank == 0)   // Fourier transform is done only in root
    {

        for(unsigned int k=0; k<nwstep; k++)
        {
            sum_one = cx_double(0.0,0.0);
            sum_two = cx_double(0.0,0.0);
            sum_three = cx_double(0.0,0.0);

            sum_current = cx_double(0.0,0.0);
            sum_current2 = cx_double(0.0,0.0);
            sum_N = cx_double(0.0,0.0);
            sum_dip = cx_double(0.0,0.0);

            if(sc == 1)
            {
                sum_super_one = cx_double(0.0,0.0);
                sum_super_two = cx_double(0.0,0.0);
                sum_super_three = cx_double(0.0,0.0);
            }
            for(unsigned int l=1; l<ntstep; l++)
            {
                // For each frequency calculate integral
                // over t with trapezoidal rule
                // (Steady-state value is taken into account)
                if(calc[0] == 1)
                {
                    sum_one += (Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, l-1))
                              *((recvbuff_one[l] - bondcurrent_one_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l))*WindowFunc(l,ntstep,wtype)
                               +(recvbuff_one[l-1] - bondcurrent_one_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l-1))*WindowFunc(l-1,ntstep,wtype));
                    sum_two += (Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, l-1))
                              *((recvbuff_two[l] - bondcurrent_two_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l))*WindowFunc(l,ntstep,wtype)
                               +(recvbuff_two[l-1] - bondcurrent_two_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l-1))*WindowFunc(l-1,ntstep,wtype));
                    sum_three += (Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, l-1))
                              *((recvbuff_three[l] - bondcurrent_three_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l))*WindowFunc(l,ntstep,wtype)
                               +(recvbuff_three[l-1] - bondcurrent_three_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l-1))*WindowFunc(l-1,ntstep,wtype));

                    sum_dip += (Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, l-1))
                              *((recvbuff_dip[l]-dipmom_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l))*WindowFunc(l,ntstep,wtype)
                               +(recvbuff_dip[l-1]-dipmom_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l-1))*WindowFunc(l-1,ntstep,wtype));

                    if(sc == 1)
                    {
                        sum_super_one += (Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, l-1))
                                        *((recvbuff_super_one[l] - supercurrent_one_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l))*WindowFunc(l,ntstep,wtype)
                                         +(recvbuff_super_one[l-1] - supercurrent_one_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l-1))*WindowFunc(l-1,ntstep,wtype));
                        sum_super_two += (Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, l-1))
                                        *((recvbuff_super_two[l] - supercurrent_two_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l))*WindowFunc(l,ntstep,wtype)
                                         +(recvbuff_super_two[l-1] - supercurrent_two_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l-1))*WindowFunc(l-1,ntstep,wtype));
                        sum_super_three += (Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, l-1))
                                        *((recvbuff_super_three[l] - supercurrent_three_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l))*WindowFunc(l,ntstep,wtype)
                                         +(recvbuff_super_three[l-1] - supercurrent_three_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l-1))*WindowFunc(l-1,ntstep,wtype));
                     }
                }

                if(calc[1] == 1)
                {
                    sum_current += (Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, l-1))
                                  *((recvbuff_current[l]-current_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l))*WindowFunc(l,ntstep,wtype)
                                   +(recvbuff_current[l-1]-current_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l-1))*WindowFunc(l-1,ntstep,wtype));

                    sum_current2 += (Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, l-1))
                                  *((recvbuff_current2[l]-current2_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l))*WindowFunc(l,ntstep,wtype)
                                   +(recvbuff_current2[l-1]-current2_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l-1))*WindowFunc(l-1,ntstep,wtype));
                }

                if(calc[2] == 1)
                {
                    sum_N += (Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, l-1))
                            *((recvbuff_N[l]-average_N)*exp(-I*w*Time(ntstep, tgridtype, dt, l))*WindowFunc(l,ntstep,wtype)
                             +(recvbuff_N[l-1]-average_N)*exp(-I*w*Time(ntstep, tgridtype, dt, l-1))*WindowFunc(l-1,ntstep,wtype));
                }

                if(calc[9] == 1 && calc[0] != 1)
                {
                    sum_dip += (Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, l-1))
                              *((recvbuff_dip[l]-dipmom_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l))*WindowFunc(l,ntstep,wtype)
                               +(recvbuff_dip[l-1]-dipmom_ss)*exp(-I*w*Time(ntstep, tgridtype, dt, l-1))*WindowFunc(l-1,ntstep,wtype));
                }
            }

            // The result from the Fourier transform is at once stored into sendbuf2
            // which will be scattered along the the different processes
            sendbuf2_one[k] = abs(0.5*sum_one);  // Trapezoidal rule
            sendbuf2_two[k] = abs(0.5*sum_two);
            sendbuf2_three[k] = abs(0.5*sum_three);

            sendbuf2_current[k] = abs(0.5*sum_current);
            sendbuf2_current2[k] = abs(0.5*sum_current2);
            sendbuf2_N[k] = abs(0.5*sum_N);
            sendbuf2_dip[k] = abs(0.5*sum_dip);

            if(sc == 1)
            {
                sendbuf2_super_one[k] = abs(0.5*sum_super_one);
                sendbuf2_super_two[k] = abs(0.5*sum_super_two);
                sendbuf2_super_three[k] = abs(0.5*sum_super_three);
            }

            w += dw;
        }

// TESTING ARMADILLO FFT

        cx_vec armafft_one = fft(armadummy_one);
        cx_vec armafft_two = fft(armadummy_two);
        cx_vec armafft_three = fft(armadummy_three);

		cx_vec armafft_dip = fft(armadummy_dip);

        cx_vec armafft_super_one = fft(armadummy_super_one);
        cx_vec armafft_super_two = fft(armadummy_super_two);
        cx_vec armafft_super_three = fft(armadummy_super_three);

        ofstream outfft("output/armafft.out", ios::out);
        outfft <<"#w" << "\t\t" << "|F{I1(t)}(w)|" << "\t" << "|F{I2(t)}(w)|" << "\t" << "|F{I3(t)}(w)|" << endl;

        double armadw((2.0*datum::pi)/(ntstep*Time(ntstep, tgridtype, dt, ntstep)/ntstep));
        double armaw(0.0);

        for(unsigned int j=0; j<ntstep; j++)
        {
            outfft << scientific << setprecision(5) << armaw << "\t" << abs(armafft_one(j)*dt) << "\t" 
                   << abs(armafft_two(j)*dt) << "\t" << abs(armafft_three(j)*dt) << endl;
            armaw += armadw;
        }
        outfft.close();

        ofstream outfft_dip("output/dipmom_ft.arma", ios::out);
        outfft_dip <<"#w" << "\t\t" << "|d(w)|" << endl;

        double armadw_dip((2.0*datum::pi)/(ntstep*Time(ntstep, tgridtype, dt, ntstep)/ntstep));
        double armaw_dip(0.0);

        for(unsigned int j=0; j<ntstep; j++)
        {
            outfft_dip << scientific << setprecision(5) << armaw_dip << "\t" << abs(armafft_dip(j)*dt) << endl;
            armaw_dip += armadw_dip;
        }
        outfft_dip.close();

        if(sc == 1)
        {
            ofstream outfft2("output/armafft2.out", ios::out);
            outfft <<"#w" << "\t\t" << "|F{I_{sc,1}(t)}(w)|" << "\t" << "|F{I_{sc,2}(t)}(w)|" << "\t" << "|F{I_{sc,3}(t)}(w)|" << endl;

            double armadw2((2.0*datum::pi)/(ntstep*Time(ntstep, tgridtype, dt, ntstep)/ntstep));
            double armaw2(0.0);

            for(unsigned int j=0; j<ntstep; j++)
            {
                outfft2 << scientific << setprecision(5) << armaw2 << "\t" << abs(armafft_super_one(j)*dt) << "\t" 
                        << abs(armafft_super_two(j)*dt) << "\t" << abs(armafft_super_three(j)*dt) << endl;
                armaw2 += armadw2;
            }
            outfft2.close();
        }
    }

    // Fourier transformed bondcurrent is scattered back to all processes
    MPI_Scatter(sendbuf2_one, nwstep/size, MPI_DOUBLE, recvbuf2_one, 1, recvtype2_one, 0, MPI_COMM_WORLD);
    MPI_Scatter(sendbuf2_two, nwstep/size, MPI_DOUBLE, recvbuf2_two, 1, recvtype2_two, 0, MPI_COMM_WORLD);
    MPI_Scatter(sendbuf2_three, nwstep/size, MPI_DOUBLE, recvbuf2_three, 1, recvtype2_three, 0, MPI_COMM_WORLD);

    MPI_Scatter(sendbuf2_current, nwstep/size, MPI_DOUBLE, recvbuf2_current, 1, recvtype2_current, 0, MPI_COMM_WORLD);
    MPI_Scatter(sendbuf2_current2, nwstep/size, MPI_DOUBLE, recvbuf2_current2, 1, recvtype2_current2, 0, MPI_COMM_WORLD);
    MPI_Scatter(sendbuf2_N, nwstep/size, MPI_DOUBLE, recvbuf2_N, 1, recvtype2_N, 0, MPI_COMM_WORLD);
    MPI_Scatter(sendbuf2_dip, nwstep/size, MPI_DOUBLE, recvbuf2_dip, 1, recvtype2_dip, 0, MPI_COMM_WORLD);

    if(sc == 1)
    {
        MPI_Scatter(sendbuf2_super_one, nwstep/size, MPI_DOUBLE, recvbuf2_super_one, 1, recvtype2_super_one, 0, MPI_COMM_WORLD);
        MPI_Scatter(sendbuf2_super_two, nwstep/size, MPI_DOUBLE, recvbuf2_super_two, 1, recvtype2_super_two, 0, MPI_COMM_WORLD);
        MPI_Scatter(sendbuf2_super_three, nwstep/size, MPI_DOUBLE, recvbuf2_super_three, 1, recvtype2_super_three, 0, MPI_COMM_WORLD);
    }

    // Each process has its own part of the scattered bondcurrent; this is then passed to WriteResults()
    for(unsigned int j=0; j<nwstep/size; j++)
    {
        bondcurrent_one_ft(j) = recvbuf2_one[j];
        bondcurrent_two_ft(j) = recvbuf2_two[j];
        bondcurrent_three_ft(j) = recvbuf2_three[j];

        TDCurrent_ft(0,j) = recvbuf2_current[j];
        TDCurrent_ft(1,j) = recvbuf2_current2[j];
        NC_ft(j) = recvbuf2_N[j];
        dipmom_ft(j) = recvbuf2_dip[j];

        if(sc == 1)
        {
            supercurrent_one_ft(j) = recvbuf2_super_one[j];
            supercurrent_two_ft(j) = recvbuf2_super_two[j];
            supercurrent_three_ft(j) = recvbuf2_super_three[j];
        }
    }

    if(sc == 1)
    {
        // Same for the pairing densities
        double sendbuff_pair[elems], *recvbuff_pair, *sendbuf2_pair, recvbuf2_pair[nwstep/size];
        MPI_Datatype recvtype_pair, recvtype2_pair;

        for(unsigned int j=0; j<nsite; j++)
        {
            for(unsigned int i=0; i<elems; i++)
            {
                sendbuff_pair[i] = real(pairdensity(j,i)); // Each process has its own sendbuff
            }

            recvbuff_pair = (double*)malloc(ntstep*sizeof(double));
            sendbuf2_pair = (double*)malloc(nwstep*sizeof(double));
            MPI_Type_contiguous(elems, MPI_DOUBLE, &recvtype_pair);
            MPI_Type_commit(&recvtype_pair);
            MPI_Type_contiguous(nwstep/size, MPI_DOUBLE, &recvtype2_pair);
            MPI_Type_commit(&recvtype2_pair);

            // The TD pairing density is gathered to recvbuff in rank=size-1 (last) process
            MPI_Gather(sendbuff_pair, elems, MPI_DOUBLE, recvbuff_pair, 1, recvtype_pair, size-1, MPI_COMM_WORLD);

            if(rank == size-1)   // Fourier transform is done only in rank=size-1
            {
                for(unsigned int k=0; k<nwstep; k++)
                {
                    sum_pair = cx_double(0.0,0.0);
                    for(unsigned int l=1; l<ntstep; l++)
                    {
                        // For each frequency calculate integral
                        // over t with trapezoidal rule
                        // (Steady-state value is taken into account)
                        if(calc[0] == 1)
                        {
                            sum_pair += (Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, l-1))
                                      *((recvbuff_pair[l]-pairdensity_ss(j))*exp(-I*w*Time(ntstep, tgridtype, dt, l))*WindowFunc(l,ntstep,wtype)
                                       +(recvbuff_pair[l-1]-pairdensity_ss(j))*exp(-I*w*Time(ntstep, tgridtype, dt, l-1))*WindowFunc(l-1,ntstep,wtype));
                        }
                    }

                    // The result from the Fourier transform is at once stored into sendbuf2
                    // which will be scattered along the the different processes
                    sendbuf2_pair[k] = abs(0.5*sum_pair);  // Trapezoidal rule
                    w += dw;
                }
            }

            // Fourier transformed pairdensity is scattered back to all processes
            MPI_Scatter(sendbuf2_pair, nwstep/size, MPI_DOUBLE, recvbuf2_pair, 1, recvtype2_pair, 0, MPI_COMM_WORLD);

            // Each process has its own part of the scattered pairdensity; this is then passed to WriteResults()
            for(unsigned int jj=0; jj<nwstep/size; jj++)
            {
                pairdensity_ft(j,jj) = recvbuf2_pair[jj];
            }
        }

        // Same for the super current matrix
        double sendbuff_supermatrix[elems], *recvbuff_supermatrix, *sendbuf2_supermatrix, recvbuf2_supermatrix[nwstep/size];
        MPI_Datatype recvtype_supermatrix, recvtype2_supermatrix;

        for(unsigned int j=0; j<nsite; j++)
        {
            for(unsigned int i=0; i<elems; i++)
            {
                sendbuff_supermatrix[i] = real(supermatrix(j,i)); // Each process has its own sendbuff
            }

            recvbuff_supermatrix = (double*)malloc(ntstep*sizeof(double));
            sendbuf2_supermatrix = (double*)malloc(nwstep*sizeof(double));
            MPI_Type_contiguous(elems, MPI_DOUBLE, &recvtype_supermatrix);
            MPI_Type_commit(&recvtype_supermatrix);
            MPI_Type_contiguous(nwstep/size, MPI_DOUBLE, &recvtype2_supermatrix);
            MPI_Type_commit(&recvtype2_supermatrix);

            // The TD super current matrix is gathered to recvbuff in rank=size-1 (last) process
            MPI_Gather(sendbuff_supermatrix, elems, MPI_DOUBLE, recvbuff_supermatrix, 1, recvtype_supermatrix, size-1, MPI_COMM_WORLD);

            if(rank == size-1)   // Fourier transform is done only in rank=size-1
            {
                for(unsigned int k=0; k<nwstep; k++)
                {
                    sum_supermatrix = cx_double(0.0,0.0);
                    for(unsigned int l=1; l<ntstep; l++)
                    {
                        // For each frequency calculate integral
                        // over t with trapezoidal rule
                        // (Steady-state value is taken into account)
                        if(calc[0] == 1)
                        {
                            sum_supermatrix += (Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, l-1))
                                      *((recvbuff_supermatrix[l]-supermatrix_ss(j))*exp(-I*w*Time(ntstep, tgridtype, dt, l))*WindowFunc(l,ntstep,wtype)
                                       +(recvbuff_supermatrix[l-1]-supermatrix_ss(j))*exp(-I*w*Time(ntstep, tgridtype, dt, l-1))*WindowFunc(l-1,ntstep,wtype));
                        }
                    }

                    // The result from the Fourier transform is at once stored into sendbuf2
                    // which will be scattered along the the different processes
                    sendbuf2_supermatrix[k] = abs(0.5*sum_supermatrix);  // Trapezoidal rule
                    w += dw;
                }
            }

            // Fourier transformed supercurrent matrix is scattered back to all processes
            MPI_Scatter(sendbuf2_supermatrix, nwstep/size, MPI_DOUBLE, recvbuf2_supermatrix, 1, recvtype2_supermatrix, 0, MPI_COMM_WORLD);

            // Each process has its own part of the scattered supercurrent matrix; this is then passed to WriteResults()
            for(unsigned int jj=0; jj<nwstep/size; jj++)
            {
                supermatrix_ft(j,jj) = recvbuf2_supermatrix[jj];
            }
        }

    }

    if(rank == 0)
    {
        cout << "Ready!" << endl;
    }
}
