#include "Main.hpp"
void ConstructHamiltonian(int nsite, int systype, double mol_hop, cx_mat &Hu, int layers, cx_mat &Hp, int pert, int sc,
			  double scw_t, double scw_Vz, double scw_aso, double scw_Delta, double scw_mu, double scw_gate)
{
    Hu.zeros();
    Hp.zeros();

// Tight-binding chain
    if(systype == 1)
    {
        for(unsigned int j=0; j<nsite; j++)
        {
           
            /* if(j==0)
            {
            Hu(j,j)=-0.3;
            }
            
            if(j==1)
            {
            Hu(j,j)=0.5;
            }
	  
	   */
	    Hu(j,j)=0.0;
	     
           
           
           
            if(j==0)
            {
                Hu(j,j+1) = mol_hop;
            }
            else if(j<nsite-1 && j>0)
            {
                Hu(j,j+1) = mol_hop;
                Hu(j,j-1) = mol_hop;
            }
            else
            {
                Hu(j,j-1) = mol_hop;
            }
        }
    }

// Tight-binding ring
    else if(systype == 2)
    {
        for(unsigned int j=0; j<nsite; j++)
        {
            Hu(j,j) = -2.0*mol_hop;
            if(j==0)
            {
                Hu(j,j+1) = mol_hop;
                Hu(j,nsite-1) = mol_hop;
            }
            else if(j<nsite-1 && j>0)
            {
                Hu(j,j+1) = mol_hop;
                Hu(j,j-1) = mol_hop;
            }
            else
            {
                Hu(j,0) = mol_hop;
                Hu(j,j-1) = mol_hop;
            }
        }

/*
// TB ring with random hopping

        for(unsigned int j=0; j<nsite; j++)
        {
            Seed();
            if(j%2==0){Hu(j,j+1) = UniformRandom()*mol_hop; Hu(j+1,j) = UniformRandom()*mol_hop;}
            if(j<nsite-1 && j%2!=0){Hu(j,j+1) = UniformRandom()*mol_hop; Hu(j+1,j) = UniformRandom()*mol_hop;}
            if(j>=nsite-1){Hu(0,j) = UniformRandom()*mol_hop; Hu(j,0) = UniformRandom()*mol_hop;}
        }
*/
    }

// Flat-band chain
    else if(systype == 3)
    {
        for(unsigned int j=0; j<nsite; j++)
        {
            Hu(j,j) = -4.0*mol_hop;
            for(unsigned int k=0; k<nsite; k++)
            {
                if(j==0 && k==0) {Hu(j,k+1) = mol_hop; Hu(j+1,k) = mol_hop;}
                if( (j>=0 && k>=0) && (j<=nsite-2 && k<=nsite-2) )
                {
                    Hu(j,j+1) = mol_hop*datum::sqrt2; Hu(j+1,j) = mol_hop*datum::sqrt2;
                    if(j%2==0 && k%2==0 && j<nsite-2) {Hu(j,j+2) = mol_hop; Hu(j+2,j) = mol_hop;}
                }
            }
        }
    }

// Flat-band ring
    else if(systype == 4)
    {
        Hu(0,nsite-1) = mol_hop*datum::sqrt2; Hu(nsite-1,0) = mol_hop*datum::sqrt2;
        Hu(0,nsite-2) = mol_hop; Hu(nsite-2,0) = mol_hop;
        for(unsigned int j=0; j<nsite; j++)
        {
            Hu(j,j) = -4.0*mol_hop;
            for(unsigned int k=0; k<nsite; k++)
            {
                if(j==0 && k==0) {Hu(j,k+1) = mol_hop; Hu(j+1,k) = mol_hop;}
                if( (j>=0 && k>=0) && (j<=nsite-2 && k<=nsite-2) )
                {
                    Hu(j,j+1) = mol_hop*datum::sqrt2; Hu(j+1,j) = mol_hop*datum::sqrt2;
                    if(j%2==0 && k%2==0 && j<nsite-3) {Hu(j,j+2) = mol_hop; Hu(j+2,j) = mol_hop;}
                }
            }
        }
    }

// Two-site molecule
    else if(systype == 5)
    {
        Hu(0,0) = (1.0-0.04)/2.0; Hu(1,1) = (1.0+0.04)/2.0;
        //Hu(0,0) = 2.0; Hu(1,1) = 5.0;
        //Hu(0,0) = 1.0; Hu(1,1) = 1.5;
    }

// Two doublets
    else if(systype == 6)
    {
        Hu(0,0) = (1.0+0.04)/2.0; Hu(1,1) = (1.0-0.04)/2.0;
        Hu(2,2) = (1.0+0.04)/2.0; Hu(3,3) = (1.0-0.04)/2.0;
    }

// One single level
    else if(systype == 7)
    {
        Hu(0,0) = 1.0;
    }

// Quadruplet
    else if(systype == 8)
    {
        Hu(0,0) = 0.0;
        Hu(1,1) = 0.0;
        Hu(2,2) = 0.0;
        Hu(3,3) = 0.0;
    }

// Kagome lattice
    else if(systype == 9)
    {
        for(unsigned int j=0; j<nsite; j++)
        {
            if(j%4!=0 && j<nsite-2){Hu(j,j+2) = mol_hop; Hu(j+2,j) = mol_hop;}
            if(j<1){Hu(j,j+1) = mol_hop; Hu(j+1,j) = mol_hop;
                    Hu(j,j+2) = mol_hop; Hu(j+2,j) = mol_hop;}
            if(j>nsite-2){Hu(j,j-1) = mol_hop; Hu(j-1,j) = mol_hop;
                          Hu(j,j-2) = mol_hop; Hu(j-2,j) = mol_hop;}
            if(j%5==0 && j%2!=0){Hu(j,j+1) = mol_hop; Hu(j+1,j) = mol_hop;
                                 Hu(j,j-1) = mol_hop; Hu(j-1,j) = mol_hop;
                                 Hu(j-1,j+2) = mol_hop; Hu(j+2,j-1) = mol_hop;
                                 Hu(j-2,j+1) = mol_hop; Hu(j+1,j-2) = mol_hop;}
        }
    }

// Read the Hamiltonian matrix from file
    else if(systype == 10)
    {
        ifstream input("input/matrix_unpert.in");
        if(!input){ cout << "Erroneous input file in CentralHamiltonian.cpp, exiting ..." << endl; exit(1);}

        unsigned int a, b;
        double c, d;
        cx_double I(0.0,1.0);
        string line;
        while(getline(input, line))
        {
            stringstream ss(line);
            if (ss >> a >> b >> c >> d)
            {
                Hu(a,b) = c + I*d;
            }
        }

        ifstream inputp("input/matrix_pert.in");
        if(!inputp){ cout << "Erroneous input file in CentralHamiltonian.cpp, exiting ..." << endl; exit(1);}

        unsigned int aa, bb;
        double cc, dd;
        string linep;
        while(getline(inputp, linep))
        {
            stringstream ssp(linep);
            if (ssp >> aa >> bb >> cc >> dd)
            {
                Hp(aa,bb) = cc + I*dd;
            }
        }
    }

//  Rectangular grid
    else if(systype == 11)
    {
        for(unsigned int j=0; j<nsite; j++)
        {
            double jj = static_cast<double>(j);
            double ii;
            if(j==0)
            {
                Hu(j,j+1) = mol_hop;
            }
            else if(j<nsite-1 && j>0)
            {
                Hu(j,j+1) = mol_hop;
                Hu(j,j-1) = mol_hop;
            }
            else
            {
                Hu(j,j-1) = mol_hop;
            }

            for(unsigned int k=0; k<layers; k++)
            {
                double kk = static_cast<double>(k);
                double ld = static_cast<double>(layers);
                if( j>=static_cast<int>(Round(kk*nsite/(ld+1.0))) 
                 && j<static_cast<int>(Round((kk+1.0)*nsite/(ld+1.0))-1.0) )
                {
                    ii = static_cast<int>(Round((kk+2.0)*nsite/(ld+1.0))-1.0)
                       + static_cast<int>(Round((kk+1.0)*nsite/(ld+1.0))-(nsite/(ld+1.0)))
                       - j;
                    Hu(j,static_cast<int>(ii)) = mol_hop;
                    Hu(static_cast<int>(ii),j) = mol_hop;
                }
            }
        }
    }

//	Majorana wire
	else if(systype == 12)
	{
		double Delta;
		cx_double eta(0.0,0.0);//(1.0e-10,1.0e-10);
		double gate, x, L;
		L = nsite/4.0;
		cx_mat pert(nsite,nsite);
		pert.zeros();
		ofstream profile("output/profile.out", ios::out);

		for(int j=0; j<nsite; j++)
		{
			for(int k=0; k<nsite; k++)
			{
				if(j%4==0 && k%4==0 && abs(j-k)<=4)
				{
					x = j/4.0;

					Delta=scw_Delta;

					// Remove pair potential from the edges
					//      		      if(j==0 || j==nsite-4) Delta=0.0;
					//		      else Delta=scw_Delta;

					Hu(j,j) = -(scw_mu-scw_t)+scw_Vz; Hu(j,j+1) = -(Delta+eta);
					Hu(j+1,j) = -(Delta+eta); Hu(j+1,j+1) = (scw_mu-scw_t)+scw_Vz;
					Hu(j+2,j+2) = -(scw_mu-scw_t)-scw_Vz; Hu(j+2,j+3) = (Delta+eta);
					Hu(j+3,j+2) = (Delta+eta); Hu(j+3,j+3) = (scw_mu-scw_t)-scw_Vz;

					if(x < L/3.0) gate = scw_gate;
					else if(x < 2.0*L/3.0) gate = scw_gate*cos((x-L/3.0)*(3.0*datum::pi/L));
					else gate = -scw_gate;
					profile << x << "\t" << gate << endl;

					pert(j,j) = gate;
					pert(j+1,j+1) = -gate;
					pert(j+2,j+2) = gate;
					pert(j+3,j+3) = -gate;

					if(j<nsite-7)
					{
						Hu(j,j+4) = -scw_t/2.0; Hu(j,j+6) = -(scw_aso+eta)/2.0;
						Hu(j+1,j+5) = scw_t/2.0; Hu(j+1,j+7) = -(scw_aso+eta)/2.0;
						Hu(j+2,j+4) = (scw_aso+eta)/2.0; Hu(j+2,j+6) = -scw_t/2.0;
						Hu(j+3,j+5) = (scw_aso+eta)/2.0; Hu(j+3,j+7) = scw_t/2.0;

						Hu(j+4,j) = -scw_t/2.0; Hu(j+6,j) = -(scw_aso+eta)/2.0;
						Hu(j+5,j+1) = scw_t/2.0; Hu(j+7,j+1) = -(scw_aso+eta)/2.0;
						Hu(j+4,j+2) = (scw_aso+eta)/2.0; Hu(j+6,j+2) = -scw_t/2.0;
						Hu(j+5,j+3) = (scw_aso+eta)/2.0; Hu(j+7,j+3) = scw_t/2.0;
					}
				}
			}
		}
		Hp = Hu + pert;
		//	  (real(Hu)).print();
		//	  cout << endl;
		//	  Hp.print();
		//	  abort();
		profile.close();
	}

    else{ cout << "Erroneous system type in CentralHamiltonian.cpp, exiting ..." << endl; exit(1);}

// For small enough central systems, print the real part of the Hamiltonian matrix
    mat Hreal = real(Hu);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if(rank == 0 && sc == 0) 
    {
        cout << "Given Hamiltonian:" << endl;
        if(nsite <= 16) Hreal.print(cout, "H =");
        else cout << "(not shown)" << endl;
    }
}
