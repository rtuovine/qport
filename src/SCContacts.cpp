#include "Main.hpp"
void SCContacts(int nsite, vector<int> lcontact, vector<int> rcontact, vector<int> lcontact2, vector<int> rcontact2, 
                vector<int> bridgeone, vector<int> bridgetwo, vector<int> bridgethree, 
                vector<int> &lcontact_sc, vector<int> &rcontact_sc, vector<int> &lcontact2_sc, 
                vector<int> &rcontact2_sc, vector<int> &bridgeone_sc, vector<int> &bridgetwo_sc, 
                vector<int> &bridgethree_sc)
{
    // Couplings to the leads are doubled (up-up and down-down blocks)
    for(unsigned int j=0; j<lcontact.size(); j++)
    {
        lcontact_sc.push_back(lcontact[j]);
        lcontact_sc.push_back(lcontact[j]+nsite);
    }
    for(unsigned int j=0; j<rcontact.size(); j++)
    {
        rcontact_sc.push_back(rcontact[j]);
        rcontact_sc.push_back(rcontact[j]+nsite);
    }
    for(unsigned int j=0; j<lcontact2.size(); j++)
    {
        lcontact2_sc.push_back(lcontact2[j]);
        lcontact2_sc.push_back(lcontact2[j]+nsite);
    }
    for(unsigned int j=0; j<rcontact2.size(); j++)
    {
        rcontact2_sc.push_back(rcontact2[j]);
        rcontact2_sc.push_back(rcontact2[j]+nsite);
    }

    // Bridge site indices are doubled (up-up and down-down blocks)
    for(unsigned int j=0; j<bridgeone.size()-1; j++)
    {
        if(j%2==0)
        {
            bridgeone_sc.push_back(bridgeone[j]);
            bridgeone_sc.push_back(bridgeone[j+1]);
            bridgeone_sc.push_back(bridgeone[j]+nsite);
            bridgeone_sc.push_back(bridgeone[j+1]+nsite);
        }
    }
    for(unsigned int j=0; j<bridgetwo.size()-1; j++)
    {
        if(j%2==0)
        {
            bridgetwo_sc.push_back(bridgetwo[j]);
            bridgetwo_sc.push_back(bridgetwo[j+1]);
            bridgetwo_sc.push_back(bridgetwo[j]+nsite);
            bridgetwo_sc.push_back(bridgetwo[j+1]+nsite);
        }
    }
    for(unsigned int j=0; j<bridgethree.size()-1; j++)
    {
        if(j%2==0)
        {
            bridgethree_sc.push_back(bridgethree[j]);
            bridgethree_sc.push_back(bridgethree[j+1]);
            bridgethree_sc.push_back(bridgethree[j]+nsite);
            bridgethree_sc.push_back(bridgethree[j+1]+nsite);
        }
    }
}
