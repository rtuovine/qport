#include "Main.hpp"
double WindowFunc(unsigned int l, int ntstep, int windowtype)
{
    // See http://en.wikipedia.org/wiki/Window_function

	if(windowtype == 1)         // Blackman
	{
	    return (7938.0/18608.0) - (9240.0/18608.0)*cos((2.0*datum::pi*l)/(ntstep-1.0))
	         + (1430.0/18608.0)*cos((4.0*datum::pi*l)/(ntstep-1.0));
    }
	else if(windowtype == 2)    // Nuttall
	{
	    return (0.355768) - (0.487396)*cos((2.0*datum::pi*l)/(ntstep-1.0))
             + (0.144232)*cos((4.0*datum::pi*l)/(ntstep-1.0))
             - (0.012604)*cos((6.0*datum::pi*l)/(ntstep-1.0));
    }
	else if(windowtype == 3)    // Blackman-Nuttall
	{
	    return (0.3635819) - (0.4891775)*cos((2.0*datum::pi*l)/(ntstep-1.0))
             + (0.1365995)*cos((4.0*datum::pi*l)/(ntstep-1.0))
             - (0.0106411)*cos((6.0*datum::pi*l)/(ntstep-1.0));
    }
	else if(windowtype == 4)    // Blackman-Harris
	{
	    return (0.35875) - (0.48829)*cos((2.0*datum::pi*l)/(ntstep-1.0))
             + (0.14128)*cos((4.0*datum::pi*l)/(ntstep-1.0))
             - (0.01168)*cos((6.0*datum::pi*l)/(ntstep-1.0));
    }
	else if(windowtype == 5)    // Flat top
	{
	    return (1.0) - (1.93)*cos((2.0*datum::pi*l)/(ntstep-1.0))
             + (1.29)*cos((4.0*datum::pi*l)/(ntstep-1.0))
             - (0.388)*cos((6.0*datum::pi*l)/(ntstep-1.0))
             + (0.028)*cos((8.0*datum::pi*l)/(ntstep-1.0));
    }
	else                        // No window
	{
	    return 1.0;
    }
}
