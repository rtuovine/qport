#include "Main.hpp"
#define SIGN(a) (((a) < 0) ? (-1) : (1))
#include "ComplexFunctions.hpp"
#include "Hypgeo2F1.hpp"

cx_double F(cx_double z)
{
    if( real(z) <= 0.0 && imag(z) < 0.0 )
    {
        cx_double I(0.0,1.0);
        return exp(z)*2.0*datum::pi*I - ExpIntE(z);
    }
    else
    {
        return -ExpIntE(z);
    }
}

cx_double Fhyp(cx_double z, double t, double beta)
{
    cx_double F;
    F = hyp_2F1(1.0, 0.5 + z/(2.0*datum::pi), 1.5 + z/(2.0*datum::pi), exp(-2.0*datum::pi*t/beta));
    return (cx_double(1.0,0.0)/(z + datum::pi))*F;
}

cx_double Psi(cx_double z, double beta)
{
	cx_double I(0.0,1.0);
	int status;
	double x, y;
	gsl_sf_result result_re, result_im;
	x = real(0.5 + beta*z/(2.0*datum::pi*I));
	y = imag(0.5 + beta*z/(2.0*datum::pi*I));
	status = gsl_sf_complex_psi_e(x, y, &result_re, &result_im);
	return result_re.val + I*result_im.val;
}

double BesselJ(int n, double x)
{
	/*
	 * The Bessel function is evaluated using GSL. Sometimes, when
	 * the order is a large number or the argument is a small number,
	 * the GSL function may return an ''underflow error''. This issue
	 * is handled by using an asymptotic expression which is used if
	 * the GSL function failes (status!=0).
	 */
	int status = 1;
    double temp;
    gsl_sf_result result;
    gsl_error_handler_t *old_handler = gsl_set_error_handler_off();
    while(status)
    {
        status = gsl_sf_bessel_Jn_e(n, x, &result);
        if(status)
        {
            if(n>0)
            {
                temp = (1.0/(sqrt(2.0*datum::pi*n)))*pow(exp(1.0)*x/(2.0*n),n);
            }
            else // Use J_{-n}(x) = (-1)^n J_n(x) for negative orders
            {
                temp = pow(1.0,-n)*(1.0/(sqrt(2.0*datum::pi*(-n))))*pow(exp(1.0)*x/(2.0*(-n)),-n);
            }
            gsl_set_error_handler(old_handler);
            return temp;
        }
    }
    return result.val;
}

cx_double Phibar(cx_double z, double t, double beta)
{
	cx_double I(0.0,1.0);
	return 2.0*datum::pi*exp(-datum::pi*t/beta)*Fhyp(-I*beta*z,t,beta);
}

cx_double PiCalculate(double t, void *param)
{
    cx_double I(0.0,1.0);

    ParamsP &funcpara= *reinterpret_cast<ParamsP *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    unsigned int m = funcpara.m;
    unsigned int n = funcpara.n;
    unsigned int alpha = funcpara.alpha;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;
    cx_vec eps_peff_l = funcpara.eps_peff_l;
    double mu = funcpara.mu;
    int biastype = funcpara.biastype;
    int pert = funcpara.pert;
    double bias_strength = funcpara.bias_strength;
    int finitet = funcpara.finitet;
    double beta = funcpara.beta;

    if(pert == 0 && finitet == 0)
    {
        return ( exp(-I*(eps_ueff_l(j)-mu-V(alpha,biastype,bias_strength))*t)
                /(
                  2.0*datum::pi
                  *(conj(eps_ueff_l(k))-eps_ueff_l(j))
                  *(conj(eps_ueff_l(k))-eps_ueff_l(j)-V(alpha,biastype,bias_strength))
                 )
               )
               *(
                  F(I*(conj(eps_ueff_l(k))-mu-V(alpha,biastype,bias_strength))*t)

                 +(
                   (conj(eps_ueff_l(k))-eps_ueff_l(j)-V(alpha,biastype,bias_strength))
                   /V(alpha,biastype,bias_strength)
                  )*F(I*(eps_ueff_l(j)-mu-V(alpha,biastype,bias_strength))*t)

                 -(
                   (conj(eps_ueff_l(k))-eps_ueff_l(j))
                   /V(alpha,biastype,bias_strength)
                  )*F(I*(eps_ueff_l(j)-mu)*t)
                );
    }

    if(pert == 0 && finitet == 1)
    {
        return ( I/(
                    (conj(eps_ueff_l(k))-eps_ueff_l(j))
                   *(conj(eps_ueff_l(k))-eps_ueff_l(j)-V(alpha,biastype,bias_strength))
                   )
               )
              *( exp(I*(conj(eps_ueff_l(k))-eps_ueff_l(j))*t)
                 *Fermi(beta,conj(eps_ueff_l(k))-mu-V(alpha,biastype,bias_strength))
                 
                +I*exp(-datum::pi*t/beta)*exp(-I*(eps_ueff_l(j)-mu-V(alpha,biastype,bias_strength))*t)
                *(
                  Fhyp(I*beta*(conj(eps_ueff_l(k))-mu-V(alpha,biastype,bias_strength)),t,beta)

                 +(
                   (conj(eps_ueff_l(k))-eps_ueff_l(j)-V(alpha,biastype,bias_strength))
                   /V(alpha,biastype,bias_strength)
                  )*Fhyp(I*beta*(eps_ueff_l(j)-mu-V(alpha,biastype,bias_strength)),t,beta)

                 -(
                   (conj(eps_ueff_l(k))-eps_ueff_l(j))
                   /V(alpha,biastype,bias_strength)
                  )*Fhyp(I*beta*(eps_ueff_l(j)-mu),t,beta)
                 )
               );
    }

    if(pert == 1 && finitet == 0)
    {
        return ( exp(-I*(eps_peff_l(j)-mu-V(alpha,biastype,bias_strength))*t)
                /(
                  2.0*datum::pi
                  *(conj(eps_peff_l(k))-eps_peff_l(n))
                  *(conj(eps_peff_l(k))-eps_ueff_l(m)-V(alpha,biastype,bias_strength))
                 )
               )
               *(
                  F(I*(conj(eps_peff_l(k))-mu-V(alpha,biastype,bias_strength))*t)

                 -(
                   (conj(eps_peff_l(k))-eps_ueff_l(m)-V(alpha,biastype,bias_strength))
                   /(eps_peff_l(n)-eps_ueff_l(m)-V(alpha,biastype,bias_strength))
                  )*F(I*(eps_peff_l(n)-mu-V(alpha,biastype,bias_strength))*t)

                 +(
                   (conj(eps_peff_l(k))-eps_peff_l(n))
                   /(eps_peff_l(n)-eps_ueff_l(m)-V(alpha,biastype,bias_strength))
                  )*F(I*(eps_ueff_l(m)-mu)*t)
                );
    }

    if(pert == 1 && finitet == 1)
    {
        return ( I/(
                    (conj(eps_peff_l(k))-eps_peff_l(n))
                   *(conj(eps_peff_l(k))-eps_ueff_l(m)-V(alpha,biastype,bias_strength))
                   )
               )
              *( exp(I*(conj(eps_peff_l(k))-eps_peff_l(j))*t)
                 *Fermi(beta,conj(eps_peff_l(k))-mu-V(alpha,biastype,bias_strength))
                 
                +I*exp(-datum::pi*t/beta)*exp(-I*(eps_peff_l(j)-mu-V(alpha,biastype,bias_strength))*t)
                *(
                  Fhyp(I*beta*(conj(eps_peff_l(k))-mu-V(alpha,biastype,bias_strength)),t,beta)

                 -(
                   (conj(eps_peff_l(k))-eps_ueff_l(m)-V(alpha,biastype,bias_strength))
                   /(eps_peff_l(n) - eps_ueff_l(m) - V(alpha,biastype,bias_strength))
                  )*Fhyp(I*beta*(eps_peff_l(n)-mu-V(alpha,biastype,bias_strength)),t,beta)

                 +(
                   (conj(eps_peff_l(k))-eps_peff_l(n))
                   /(eps_peff_l(n) - eps_ueff_l(m) - V(alpha,biastype,bias_strength))
                  )*Fhyp(I*beta*(eps_ueff_l(m)-mu),t,beta)
                 )
               );
    }
}

// Mike's model
// The following terms are for the current calculation (with sinusoidal TD bias)
cx_double FirstTerm(double t, void *param)
{
    cx_double I(0.0,1.0);

    ParamsTDCurr &funcpara= *reinterpret_cast<ParamsTDCurr *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int s = funcpara.s;
    unsigned int alpha = funcpara.alpha;
    unsigned int be = funcpara.be;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;

    return I*exp(I*(amplitude[alpha]/frequency[alpha])*sin(frequency[alpha]*t + phase[alpha]))
            *BesselJ(r,amplitude[alpha]/frequency[alpha])*exp(-I*phase[alpha]*r)
            *(exp(-I*r*frequency[alpha]*t)*Psi(-(eps_ueff_l(j)-mu-constpart[alpha]-r*frequency[alpha]),beta)
             +exp(-I*(eps_ueff_l(j)-mu-constpart[alpha])*t)
             *(Phibar(-(eps_ueff_l(j)-mu-constpart[alpha]-r*frequency[alpha]),t,beta)-Phibar(-(eps_ueff_l(j)-mu),t,beta))
             );
}

cx_double SecondTerm(double t, void *param)
{
    cx_double I(0.0,1.0);

    ParamsTDCurr &funcpara= *reinterpret_cast<ParamsTDCurr *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int s = funcpara.s;
    unsigned int alpha = funcpara.alpha;
    unsigned int be = funcpara.be;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;

    return exp(-I*(eps_ueff_l(j)-conj(eps_ueff_l(k)))*t)
          *(Psi(-(eps_ueff_l(j)-mu),beta) - Psi(conj(eps_ueff_l(k))-mu,beta))
          /(eps_ueff_l(j) - conj(eps_ueff_l(k)));
}

cx_double ThirdTerm(double t, void *param)
{
    cx_double I(0.0,1.0);

    ParamsTDCurr &funcpara= *reinterpret_cast<ParamsTDCurr *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int s = funcpara.s;
    unsigned int alpha = funcpara.alpha;
    unsigned int be = funcpara.be;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;

    return exp(-I*(eps_ueff_l(j)-conj(eps_ueff_l(k)))*t)
          *(exp(-I*(amplitude[be]/frequency[be])*sin(phase[be]))
          *(BesselJ(r,amplitude[be]/frequency[be])*exp(I*r*phase[be])/(conj(eps_ueff_l(k))-eps_ueff_l(j)-mu-constpart[be]-r*frequency[be]))
          *(Psi(-(eps_ueff_l(j)-mu),beta) - Psi(-(conj(eps_ueff_l(k))-mu-constpart[be]-r*frequency[be]),beta)
           +exp(-I*(conj(eps_ueff_l(k))-mu-constpart[be]-r*frequency[be])*t)
            *(Phibar(-(eps_ueff_l(j)-mu),t,beta)-Phibar(-(conj(eps_ueff_l(k))-mu-constpart[be]-r*frequency[be]),t,beta))
            )
          +conj(exp(-I*(amplitude[be]/frequency[be])*sin(phase[be]))
          *(BesselJ(r,amplitude[be]/frequency[be])*exp(I*r*phase[be])/(conj(eps_ueff_l(j))-eps_ueff_l(k)-mu-constpart[be]-r*frequency[be]))
          *(Psi(-(eps_ueff_l(k)-mu),beta) - Psi(-(conj(eps_ueff_l(j))-mu-constpart[be]-r*frequency[be]),beta)
           +exp(-I*(conj(eps_ueff_l(j))-mu-constpart[be]-r*frequency[be])*t)
            *(Phibar(-(eps_ueff_l(k)-mu),t,beta)-Phibar(-(conj(eps_ueff_l(j))-mu-constpart[be]-r*frequency[be]),t,beta))
            ))
           );
}

cx_double FourthTerm(double t, void *param)
{
    cx_double I(0.0,1.0);

    ParamsTDCurr &funcpara= *reinterpret_cast<ParamsTDCurr *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int s = funcpara.s;
    unsigned int alpha = funcpara.alpha;
    unsigned int be = funcpara.be;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;

    return exp(-I*(eps_ueff_l(j)-conj(eps_ueff_l(k)))*t)
          *((BesselJ(r,amplitude[be]/frequency[be])*BesselJ(s,amplitude[be]/frequency[be])*exp(-I*(r-s)*phase[be]))/(eps_ueff_l(j)-conj(eps_ueff_l(k))-(r-s)*frequency[be]))
          *((exp(I*(eps_ueff_l(j)-conj(eps_ueff_l(k))-(r-s)*frequency[be])*t)-cx_double(1.0,0.0))
            *(Psi(-(eps_ueff_l(j)-mu-constpart[be]-r*frequency[be]),beta) - Psi(conj(eps_ueff_l(k))-mu-constpart[be]-s*frequency[be],beta))
           -(Psi(conj(eps_ueff_l(k))-mu-constpart[be]-s*frequency[be],beta)-Psi(eps_ueff_l(j)-mu-constpart[be]-r*frequency[be],beta)
           +exp(I*(eps_ueff_l(j)-mu-constpart[be]-r*frequency[be])*t)
            *(Phibar(conj(eps_ueff_l(k))-mu-constpart[be]-s*frequency[be],t,beta)
             -Phibar(eps_ueff_l(j)-mu-constpart[be]-r*frequency[be],t,beta))
           +exp(-I*(conj(eps_ueff_l(k))-mu-constpart[be]-s*frequency[be])*t)
            *(-Phibar(-(eps_ueff_l(j)-mu-constpart[be]-r*frequency[be]),t,beta)
              +Phibar(-(conj(eps_ueff_l(k))-mu-constpart[be]-s*frequency[be]),t,beta))
           +(-Psi(-(eps_ueff_l(j)-mu-constpart[be]-r*frequency[be]),beta)+Psi(-(conj(eps_ueff_l(k))-mu-constpart[be]-s*frequency[be]),beta)))
           );
}

// New TD current
cx_double SecondTermNew(double t, void *param)
{
    cx_double I(0.0,1.0);

    ParamsTDCurrNew &funcpara= *reinterpret_cast<ParamsTDCurrNew *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int rp = funcpara.rp;
    int s = funcpara.s;
    int sp = funcpara.sp;
    unsigned int alpha = funcpara.alpha;
    unsigned int ga = funcpara.ga;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    int freq_mult1 = funcpara.freq_mult1;
    int freq_mult2 = funcpara.freq_mult2;
	double ampl_rat = funcpara.ampl_rat;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;

    return I*exp(I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*t + phase[alpha]) - sin(phase[alpha]))
            *exp(I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*t))
          *BesselJ(r,amplitude[alpha]/(freq_mult1*frequency[alpha]))*BesselJ(s,(ampl_rat*amplitude[alpha])/(freq_mult2*frequency[alpha]))*exp(I*r*phase[alpha])
          *(exp(-I*(eps_ueff_l(j)-mu-constpart[alpha])*t)
           *(Phibar(-(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),t,beta)
            -Phibar(-(eps_ueff_l(j)-mu),t,beta))
           +exp(-I*frequency[alpha]*(freq_mult1*r+freq_mult2*s)*t)*Psi(-(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),beta)
           );
}

cx_double ThirdTermNew(double t, void *param)
{
    cx_double I(0.0,1.0);

    ParamsTDCurrNew &funcpara= *reinterpret_cast<ParamsTDCurrNew *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int rp = funcpara.rp;
    int s = funcpara.s;
    int sp = funcpara.sp;
    unsigned int alpha = funcpara.alpha;
    unsigned int ga = funcpara.ga;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    int freq_mult1 = funcpara.freq_mult1;
    int freq_mult2 = funcpara.freq_mult2;
	double ampl_rat = funcpara.ampl_rat;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;

    return ((exp(-I*(eps_ueff_l(j)-conj(eps_ueff_l(k)))*t))/(conj(eps_ueff_l(k))-eps_ueff_l(j)))
          *(Psi(conj(eps_ueff_l(k))-mu,beta) - Psi(-(eps_ueff_l(j)-mu),beta));
}

cx_double FourthTermNew(double t, void *param)
{
    cx_double I(0.0,1.0);

    ParamsTDCurrNew &funcpara= *reinterpret_cast<ParamsTDCurrNew *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int rp = funcpara.rp;
    int s = funcpara.s;
    int sp = funcpara.sp;
    unsigned int alpha = funcpara.alpha;
    unsigned int ga = funcpara.ga;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    int freq_mult1 = funcpara.freq_mult1;
    int freq_mult2 = funcpara.freq_mult2;
	double ampl_rat = funcpara.ampl_rat;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;

    return BesselJ(r,amplitude[ga]/(freq_mult1*frequency[ga]))*BesselJ(s,ampl_rat*amplitude[ga]/(freq_mult2*frequency[ga]))
          *(((exp(-I*r*phase[ga])*exp(I*(amplitude[ga]/(freq_mult1*frequency[ga]))*sin(phase[ga])))
            /(eps_ueff_l(j)-conj(eps_ueff_l(k))-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)))
           *(exp(-I*(eps_ueff_l(j)-conj(eps_ueff_l(k)))*t)*(Psi(conj(eps_ueff_l(k))-mu,beta)-Psi(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),beta))
            +exp(I*(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s))*t)
             *(Phibar(conj(eps_ueff_l(k))-mu,t,beta)
              -Phibar(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),t,beta)))

			+((exp(I*r*phase[ga])*exp(-I*(amplitude[ga]/(freq_mult1*frequency[ga]))*sin(phase[ga])))
            /(conj(eps_ueff_l(k))-eps_ueff_l(j)-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)))
           *(exp(-I*(eps_ueff_l(j)-conj(eps_ueff_l(k)))*t)*(Psi(-(eps_ueff_l(j)-mu),beta)-Psi(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),beta))
            +exp(-I*(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s))*t)
             *(Phibar(-(eps_ueff_l(j)-mu),t,beta)
              -Phibar(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),t,beta)))
           );
}

cx_double FifthTermNew(double t, void *param)
{
    cx_double I(0.0,1.0);

    ParamsTDCurrNew &funcpara= *reinterpret_cast<ParamsTDCurrNew *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int rp = funcpara.rp;
    int s = funcpara.s;
    int sp = funcpara.sp;
    unsigned int alpha = funcpara.alpha;
    unsigned int ga = funcpara.ga;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    int freq_mult1 = funcpara.freq_mult1;
    int freq_mult2 = funcpara.freq_mult2;
	double ampl_rat = funcpara.ampl_rat;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;

    return BesselJ(r,amplitude[ga]/(freq_mult1*frequency[ga]))*BesselJ(rp,amplitude[ga]/(freq_mult1*frequency[ga]))
          *BesselJ(s,ampl_rat*amplitude[ga]/(freq_mult2*frequency[ga]))*BesselJ(sp,ampl_rat*amplitude[ga]/(freq_mult2*frequency[ga]))
          *(exp(-I*(r-rp)*phase[ga])/(eps_ueff_l(j)-conj(eps_ueff_l(k))-frequency[ga]*(freq_mult1*(r-rp)+freq_mult2*(s-sp))))
          *(exp(-I*frequency[ga]*(freq_mult1*(r-rp)+freq_mult2*(s-sp))*t)
           *(Psi(-(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),beta) 
            -Psi(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp),beta))
           +exp(-I*(eps_ueff_l(j)-conj(eps_ueff_l(k)))*t)
           *(Psi(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),beta)
            -Psi(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp)),beta))
           +exp(I*(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s))*t)
           *(Phibar(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),t,beta)
            -Phibar(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp),t,beta))
           +exp(-I*(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp))*t)
           *(Phibar(-(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),t,beta)
            -Phibar(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp)),t,beta))
           );
}

// The following terms are for the N_C(t) calculation (with sinusoidal TD bias)
cx_double FirstTermN(double t, void *param)
{
    cx_double I(0.0,1.0);

    ParamsTDCurr &funcpara= *reinterpret_cast<ParamsTDCurr *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int s = funcpara.s;
    unsigned int alpha = funcpara.alpha;
    unsigned int be = funcpara.be;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;

    return I*datum::pi/(conj(eps_ueff_l(k)) - eps_ueff_l(j))
          +(exp(-I*(eps_ueff_l(j) - conj(eps_ueff_l(k)))*t)
             *(Psi(-(eps_ueff_l(j)-mu),beta) - Psi(conj(eps_ueff_l(k))-mu,beta))
             /(eps_ueff_l(j) - conj(eps_ueff_l(k))));
}

cx_double SecondTermN(double t, void *param)
{
    cx_double I(0.0,1.0);

    ParamsTDCurr &funcpara= *reinterpret_cast<ParamsTDCurr *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int s = funcpara.s;
    unsigned int alpha = funcpara.alpha;
    unsigned int be = funcpara.be;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;

    return exp(-I*(eps_ueff_l(j) - conj(eps_ueff_l(k)))*t)
          *(exp(-I*(amplitude[be]/frequency[be])*sin(phase[be]))
          *(BesselJ(r,amplitude[be]/frequency[be])*exp(I*r*phase[be])/(conj(eps_ueff_l(k))-eps_ueff_l(j)-mu-constpart[be]-r*frequency[be]))
          *(Psi(-(eps_ueff_l(j)-mu),beta) - Psi(-(conj(eps_ueff_l(k))-mu-constpart[be]-r*frequency[be]),beta)
           +exp(-I*(conj(eps_ueff_l(k))-mu-constpart[be]-r*frequency[be])*t)
            *(Phibar(-(eps_ueff_l(j)-mu),t,beta)
             -Phibar(-(conj(eps_ueff_l(k))-mu-constpart[be]-r*frequency[be]),t,beta))
            )
          +conj(exp(-I*(amplitude[be]/frequency[be])*sin(phase[be]))
          *(BesselJ(r,amplitude[be]/frequency[be])*exp(I*r*phase[be])/(conj(eps_ueff_l(j))-eps_ueff_l(k)-mu-constpart[be]-r*frequency[be]))
          *(Psi(-(eps_ueff_l(k)-mu),beta) - Psi(-(conj(eps_ueff_l(j))-mu-constpart[be]-r*frequency[be]),beta)
           +exp(-I*(conj(eps_ueff_l(j))-mu-constpart[be]-r*frequency[be])*t)
            *(Phibar(-(eps_ueff_l(k)-mu),t,beta)
             -Phibar(-(conj(eps_ueff_l(j))-mu-constpart[be]-r*frequency[be]),t,beta))
            )));
}

cx_double ThirdTermN(double t, void *param)
{
    cx_double I(0.0,1.0);

    ParamsTDCurr &funcpara= *reinterpret_cast<ParamsTDCurr *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int s = funcpara.s;
    unsigned int alpha = funcpara.alpha;
    unsigned int be = funcpara.be;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;

    return exp(-I*(eps_ueff_l(j)-conj(eps_ueff_l(k)))*t)
          *((BesselJ(r,amplitude[be]/frequency[be])*BesselJ(s,amplitude[be]/frequency[be])*exp(-I*(r-s)*phase[be]))/(eps_ueff_l(j)-conj(eps_ueff_l(k))-(r-s)*frequency[be]))
          *((exp(I*(eps_ueff_l(j)-conj(eps_ueff_l(k))-(r-s)*frequency[be])*t)-cx_double(1.0,0.0))
            *(Psi(-(eps_ueff_l(j)-mu-constpart[be]-r*frequency[be]),beta) - Psi(conj(eps_ueff_l(k))-mu-constpart[be]-s*frequency[be],beta))
           -(Psi(conj(eps_ueff_l(k))-mu-constpart[be]-s*frequency[be],beta)-Psi(eps_ueff_l(j)-mu-constpart[be]-r*frequency[be],beta)
           +exp(I*(eps_ueff_l(j)-mu-constpart[be]-r*frequency[be])*t)
            *(Phibar(conj(eps_ueff_l(k))-mu-constpart[be]-s*frequency[be],t,beta)
             -Phibar(eps_ueff_l(j)-mu-constpart[be]-r*frequency[be],t,beta))
           +exp(-I*(conj(eps_ueff_l(k))-mu-constpart[be]-s*frequency[be])*t)
            *(-Phibar(-(eps_ueff_l(j)-mu-constpart[be]-r*frequency[be]),t,beta)
              +Phibar(-(conj(eps_ueff_l(k))-mu-constpart[be]-s*frequency[be]),t,beta))
           +(-Psi(-(eps_ueff_l(j)-mu-constpart[be]-r*frequency[be]),beta)+Psi(-(conj(eps_ueff_l(k))-mu-constpart[be]-s*frequency[be]),beta)))
           );
}

// This is for the steady-state pump current calculation (with sinusoidal bias)
cx_double PumpTerm1(void *param)
{
    cx_double I(0.0,1.0);

    ParamsPump &funcpara= *reinterpret_cast<ParamsPump *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int rp = funcpara.rp;
    int s = funcpara.s;
    int sp = funcpara.sp;
    unsigned int alpha = funcpara.alpha;
    unsigned int be = funcpara.be;
    unsigned int ga = funcpara.ga;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    int freq_mult1 = funcpara.freq_mult1;
    int freq_mult2 = funcpara.freq_mult2;
    double ampl_rat = funcpara.ampl_rat;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;

    return (exp(-I*(r-rp)*phase[alpha])
           *BesselJ(r,amplitude[alpha]/(freq_mult1*frequency[alpha]))*BesselJ(rp,amplitude[alpha]/(freq_mult1*frequency[alpha]))
           *BesselJ(s,ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*BesselJ(sp,ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))
           *(Psi(-(eps_ueff_l(j)-mu-constpart[alpha]-(freq_mult1*r+freq_mult2*s)*frequency[alpha]),beta)
            -Psi(conj(eps_ueff_l(k))-mu-constpart[alpha]-(freq_mult1*rp+freq_mult2*sp)*frequency[alpha],beta))
           -exp(-I*(r-rp)*phase[ga])
           *BesselJ(r,amplitude[ga]/(freq_mult1*frequency[ga]))*BesselJ(rp,amplitude[ga]/(freq_mult1*frequency[ga]))
           *BesselJ(s,ampl_rat*amplitude[ga]/(freq_mult2*frequency[ga]))*BesselJ(sp,ampl_rat*amplitude[ga]/(freq_mult2*frequency[ga]))
           *(Psi(-(eps_ueff_l(j)-mu-constpart[ga]-(freq_mult1*r+freq_mult2*s)*frequency[alpha]),beta)
            -Psi(conj(eps_ueff_l(k))-mu-constpart[ga]-(freq_mult1*rp+freq_mult2*sp)*frequency[alpha],beta))
            );
}

cx_double PumpTerm2(void *param)
{
    cx_double I(0.0,1.0);

    ParamsPump &funcpara= *reinterpret_cast<ParamsPump *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int rp = funcpara.rp;
    int s = funcpara.s;
    int sp = funcpara.sp;
    unsigned int alpha = funcpara.alpha;
    unsigned int be = funcpara.be;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    double ga = funcpara.ga;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    int freq_mult1 = funcpara.freq_mult1;
    int freq_mult2 = funcpara.freq_mult2;
    double ampl_rat = funcpara.ampl_rat;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;

    return (exp(-I*(r-rp)*phase[be])
           *BesselJ(r,amplitude[be]/(freq_mult1*frequency[be]))*BesselJ(rp,amplitude[be]/(freq_mult1*frequency[be]))
           *BesselJ(s,ampl_rat*amplitude[be]/(freq_mult2*frequency[be]))*BesselJ(sp,ampl_rat*amplitude[be]/(freq_mult2*frequency[be]))
           *(Psi(-(eps_ueff_l(j)-mu-constpart[be]-(freq_mult1*r+freq_mult2*s)*frequency[alpha]),beta)
            -Psi(conj(eps_ueff_l(k))-mu-constpart[be]-(freq_mult1*rp+freq_mult2*sp)*frequency[alpha],beta))
           -exp(-I*(r-rp)*phase[ga])
           *BesselJ(r,amplitude[ga]/(freq_mult1*frequency[ga]))*BesselJ(rp,amplitude[ga]/(freq_mult1*frequency[ga]))
           *BesselJ(s,ampl_rat*amplitude[ga]/(freq_mult2*frequency[ga]))*BesselJ(sp,ampl_rat*amplitude[ga]/(freq_mult2*frequency[ga]))
           *(Psi(-(eps_ueff_l(j)-mu-constpart[ga]-(freq_mult1*r+freq_mult2*s)*frequency[alpha]),beta)
            -Psi(conj(eps_ueff_l(k))-mu-constpart[ga]-(freq_mult1*rp+freq_mult2*sp)*frequency[alpha],beta))
            );
}

cx_double PumpLR(void *param)
{
    cx_double I(0.0,1.0);

    ParamsPumpLR &funcpara= *reinterpret_cast<ParamsPumpLR *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int rp = funcpara.rp;
    int s = funcpara.s;
    int sp = funcpara.sp;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    int freq_mult1 = funcpara.freq_mult1;
    int freq_mult2 = funcpara.freq_mult2;
    double ampl_rat = funcpara.ampl_rat;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;

    return (exp(-I*(r-rp)*phase[0])
           *BesselJ(r,amplitude[0]/(freq_mult1*frequency[0]))*BesselJ(rp,amplitude[0]/(freq_mult1*frequency[0]))
           *BesselJ(s,ampl_rat*amplitude[0]/(freq_mult2*frequency[0]))*BesselJ(sp,ampl_rat*amplitude[0]/(freq_mult2*frequency[0]))
           *(Psi(-(eps_ueff_l(j)-mu-constpart[0]-(freq_mult1*r+freq_mult2*s)*frequency[0]),beta)
            -Psi(conj(eps_ueff_l(k))-mu-constpart[0]-(freq_mult1*rp+freq_mult2*sp)*frequency[0],beta))
           -exp(-I*(r-rp)*phase[1])
           *BesselJ(r,amplitude[1]/(freq_mult1*frequency[1]))*BesselJ(rp,amplitude[1]/(freq_mult1*frequency[1]))
           *BesselJ(s,ampl_rat*amplitude[1]/(freq_mult2*frequency[1]))*BesselJ(sp,ampl_rat*amplitude[1]/(freq_mult2*frequency[1]))
           *(Psi(-(eps_ueff_l(j)-mu-constpart[1]-(freq_mult1*r+freq_mult2*s)*frequency[0]),beta)
            -Psi(conj(eps_ueff_l(k))-mu-constpart[1]-(freq_mult1*rp+freq_mult2*sp)*frequency[0],beta))
            );
}

cx_double PumpLRstoch(void *param)
{
    cx_double I(0.0,1.0);

    ParamsPumpLRstoch &funcpara= *reinterpret_cast<ParamsPumpLRstoch *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int r = funcpara.r;
    int rp = funcpara.rp;
    int s = funcpara.s;
    int sp = funcpara.sp;
    int n = funcpara.n;
    int m = funcpara.m;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    int freq_mult1 = funcpara.freq_mult1;
    int freq_mult2 = funcpara.freq_mult2;
    double ampl_rat = funcpara.ampl_rat;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;
    double Dfluct = funcpara.Dfluct;
    double wcorr = funcpara.wcorr;

    return (exp(-I*(r-rp)*phase[0])
           *BesselJ(r,amplitude[0]/(freq_mult1*frequency[0]))*BesselJ(rp,amplitude[0]/(freq_mult1*frequency[0]))
           *BesselJ(s,ampl_rat*amplitude[0]/(freq_mult2*frequency[0]))*BesselJ(sp,ampl_rat*amplitude[0]/(freq_mult2*frequency[0]))
           *(Psi(-(eps_ueff_l(j)-mu-constpart[0]-(freq_mult1*r+freq_mult2*s)*frequency[0]-I*(Dfluct+m*wcorr)),beta)
            -Psi(conj(eps_ueff_l(k))-mu-constpart[0]-(freq_mult1*rp+freq_mult2*sp)*frequency[0]+I*(Dfluct+m*wcorr),beta))
           -exp(-I*(r-rp)*phase[1])
           *BesselJ(r,amplitude[1]/(freq_mult1*frequency[1]))*BesselJ(rp,amplitude[1]/(freq_mult1*frequency[1]))
           *BesselJ(s,ampl_rat*amplitude[1]/(freq_mult2*frequency[1]))*BesselJ(sp,ampl_rat*amplitude[1]/(freq_mult2*frequency[1]))
           *(Psi(-(eps_ueff_l(j)-mu-constpart[1]-(freq_mult1*r+freq_mult2*s)*frequency[0]-I*(Dfluct+m*wcorr)),beta)
            -Psi(conj(eps_ueff_l(k))-mu-constpart[1]-(freq_mult1*rp+freq_mult2*sp)*frequency[0]+I*(Dfluct+m*wcorr),beta))
            );
}


cx_double PumpLRstochAdiabatic(void *param)
{
    cx_double I(0.0,1.0);

    ParamsPumpLRstochAdiabatic &funcpara= *reinterpret_cast<ParamsPumpLRstochAdiabatic *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    int n = funcpara.n;
    int m = funcpara.m;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;
    double Dfluct = funcpara.Dfluct;
    double wcorr = funcpara.wcorr;

    return (Psi(-(eps_ueff_l(j)-mu-constpart[0]-I*(Dfluct+m*wcorr)),beta)
           -Psi(conj(eps_ueff_l(k))-mu-constpart[0]+I*(Dfluct+m*wcorr),beta)
           -Psi(-(eps_ueff_l(j)-mu-constpart[1]-I*(Dfluct+m*wcorr)),beta)
           +Psi(conj(eps_ueff_l(k))-mu-constpart[1]+I*(Dfluct+m*wcorr),beta)
            );
}


double Integrand2re(double tau, void *param)
{
/*
This is gsl integrand (real part) for the single (tau) integral. This 
is called from the evaluation of Glss,Ggtr.
*/
    ParamsTimeIntegral &funcpara= *reinterpret_cast<ParamsTimeIntegral *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    unsigned int be = funcpara.be;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;
    int subs = funcpara.subs;
    double acc = funcpara.acc;
    int rule = funcpara.rule;

    cx_double I(0.0,1.0);

/* 
This integrand depends on the phase psi which again is a time integral
of the bias profile over the range [0,tau].
*/
    gsl_function F111re;
    gsl_integration_workspace *ws111re;
    double result111re, error111re;

    const double epsabs = acc;
    const double epsrel = acc;
    const size_t wslimit = subs;
    const int key = rule;
    int integral_status;
    double epsabsvar, epsrelvar;

    ParamsTimeIntegral para11;
    para11.constpart = constpart;
    para11.amplitude = amplitude;
    para11.frequency = frequency;
    para11.phase = phase;

    double lo = 0.0;
    double up = tau;

    F111re.function = &Integrand111re;
    F111re.params = reinterpret_cast<void *>(&para11);
    ws111re = gsl_integration_workspace_alloc(wslimit);
    gsl_error_handler_t *old_handler111 = gsl_set_error_handler_off();
    integral_status = 1;
    epsabsvar = epsabs;
    epsrelvar = epsrel;
    while(integral_status)
    {
        integral_status = gsl_integration_qag(&F111re, lo, up, epsabsvar, epsrelvar, wslimit, 
                                              key, ws111re, &result111re, &error111re);

        epsabsvar *= 1.1;
        epsrelvar *= 1.1;

        if(integral_status) {cout << setprecision(3) << scientific
                         << "\tProblems in integration; increasing tolerance to "
                         << epsabsvar << endl;}
    }
    gsl_set_error_handler(old_handler111);
    gsl_integration_workspace_free(ws111re);

    return real(exp(I*(eps_ueff_l(j)-mu)*tau)*exp(-I*result111re)*Phibar(conj(eps_ueff_l(k))-mu,tau,beta));
}

double Integrand2im(double tau, void *param)
{
/*
This is gsl integrand (imaginary part) for the single (tau) integral. This 
is called from the evaluation of Glss,Ggtr.
*/
    ParamsTimeIntegral &funcpara= *reinterpret_cast<ParamsTimeIntegral *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    unsigned int be = funcpara.be;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;
    int subs = funcpara.subs;
    double acc = funcpara.acc;
    int rule = funcpara.rule;

    cx_double I(0.0,1.0);

/* 
This integrand depends on the phase psi which again is a time integral
of the bias profile over the range [0,tau].
*/
    gsl_function F111re;
    gsl_integration_workspace *ws111re;
    double result111re, error111re;

    const double epsabs = acc;
    const double epsrel = acc;
    const size_t wslimit = subs;
    const int key = rule;
    int integral_status;
    double epsabsvar, epsrelvar;

    ParamsTimeIntegral para11;
    para11.constpart = constpart;
    para11.amplitude = amplitude;
    para11.frequency = frequency;
    para11.phase = phase;

    double lo = 0.0;
    double up = tau;

    F111re.function = &Integrand111re;
    F111re.params = reinterpret_cast<void *>(&para11);
    ws111re = gsl_integration_workspace_alloc(wslimit);
    gsl_error_handler_t *old_handler111 = gsl_set_error_handler_off();
    integral_status = 1;
    epsabsvar = epsabs;
    epsrelvar = epsrel;
    while(integral_status)
    {
        integral_status = gsl_integration_qag(&F111re, lo, up, epsabsvar, epsrelvar, wslimit, 
                                              key, ws111re, &result111re, &error111re);

        epsabsvar *= 1.1;
        epsrelvar *= 1.1;

        if(integral_status) {cout << setprecision(3) << scientific
                         << "\tProblems in integration; increasing tolerance to "
                         << epsabsvar << endl;}
    }
    gsl_set_error_handler(old_handler111);
    gsl_integration_workspace_free(ws111re);

    return imag(exp(I*(eps_ueff_l(j)-mu)*tau)*exp(-I*result111re)*Phibar(conj(eps_ueff_l(k))-mu,tau,beta));
}

double Integrand3re(double tau, void *param)
{
/*
This is gsl integrand (real part) for the single (tau) integral. This 
is called from the evaluation of Glss,Ggtr.
*/
    ParamsTimeIntegral &funcpara= *reinterpret_cast<ParamsTimeIntegral *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    unsigned int be = funcpara.be;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;
    int subs = funcpara.subs;
    double acc = funcpara.acc;
    int rule = funcpara.rule;

    cx_double I(0.0,1.0);

/* 
This integrand depends on the phase psi which again is a time integral
of the bias profile over the range [0,tau].
*/
    gsl_function F111re;
    gsl_integration_workspace *ws111re;
    double result111re, error111re;

    const double epsabs = acc;
    const double epsrel = acc;
    const size_t wslimit = subs;
    const int key = rule;
    int integral_status;
    double epsabsvar, epsrelvar;

    ParamsTimeIntegral para11;
    para11.be = be;
    para11.constpart = constpart;
    para11.amplitude = amplitude;
    para11.frequency = frequency;
    para11.phase = phase;

    double lo = 0.0;
    double up = tau;

    F111re.function = &Integrand111re;
    F111re.params = reinterpret_cast<void *>(&para11);
    ws111re = gsl_integration_workspace_alloc(wslimit);
    gsl_error_handler_t *old_handler111 = gsl_set_error_handler_off();
    integral_status = 1;
    epsabsvar = epsabs;
    epsrelvar = epsrel;
    while(integral_status)
    {
        integral_status = gsl_integration_qag(&F111re, lo, up, epsabsvar, epsrelvar, wslimit, 
                                              key, ws111re, &result111re, &error111re);

        epsabsvar *= 1.1;
        epsrelvar *= 1.1;

        if(integral_status) {cout << setprecision(3) << scientific
                         << "\tProblems in integration; increasing tolerance to "
                         << epsabsvar << endl;}
    }
    gsl_set_error_handler(old_handler111);
    gsl_integration_workspace_free(ws111re);

    return real(exp(-I*(conj(eps_ueff_l(k))-mu)*tau)*exp(I*result111re)*Phibar(-((eps_ueff_l(j))-mu),tau,beta));
}

double Integrand3im(double tau, void *param)
{
/*
This is gsl integrand (imaginary part) for the single (tau) integral. This 
is called from the evaluation of Glss,Ggtr.
*/
    ParamsTimeIntegral &funcpara= *reinterpret_cast<ParamsTimeIntegral *>(param);

    unsigned int j = funcpara.j;
    unsigned int k = funcpara.k;
    unsigned int be = funcpara.be;
    double mu = funcpara.mu;
    double beta = funcpara.beta;
    vector<double> constpart = funcpara.constpart;
    vector<double> amplitude = funcpara.amplitude;
    vector<double> frequency = funcpara.frequency;
    vector<double> phase = funcpara.phase;
    cx_vec eps_ueff_l = funcpara.eps_ueff_l;
    int subs = funcpara.subs;
    double acc = funcpara.acc;
    int rule = funcpara.rule;

    cx_double I(0.0,1.0);

/* 
This integrand depends on the phase psi which again is a time integral
of the bias profile over the range [0,tau].
*/
    gsl_function F111re;
    gsl_integration_workspace *ws111re;
    double result111re, error111re;

    const double epsabs = acc;
    const double epsrel = acc;
    const size_t wslimit = subs;
    const int key = rule;
    int integral_status;
    double epsabsvar, epsrelvar;

    ParamsTimeIntegral para11;
    para11.be = be;
    para11.constpart = constpart;
    para11.amplitude = amplitude;
    para11.frequency = frequency;
    para11.phase = phase;

    double lo = 0.0;
    double up = tau;

    F111re.function = &Integrand111re;
    F111re.params = reinterpret_cast<void *>(&para11);
    ws111re = gsl_integration_workspace_alloc(wslimit);
    gsl_error_handler_t *old_handler111 = gsl_set_error_handler_off();
    integral_status = 1;
    epsabsvar = epsabs;
    epsrelvar = epsrel;
    while(integral_status)
    {
        integral_status = gsl_integration_qag(&F111re, lo, up, epsabsvar, epsrelvar, wslimit, 
                                              key, ws111re, &result111re, &error111re);

        epsabsvar *= 1.1;
        epsrelvar *= 1.1;

        if(integral_status) {cout << setprecision(3) << scientific
                         << "\tProblems in integration; increasing tolerance to "
                         << epsabsvar << endl;}
    }
    gsl_set_error_handler(old_handler111);
    gsl_integration_workspace_free(ws111re);

    return imag(exp(-I*(conj(eps_ueff_l(k))-mu)*tau)*exp(I*result111re)*Phibar(-((eps_ueff_l(j))-mu),tau,beta));
}
