#include "Main.hpp"
void WriteResults(int nsite, int nlead, int ntstep, int tgridtype, int nwstep, int snaps, int sc, double dt, cx_cube rho, cx_mat rho_ss, 
                  cx_vec bondcurrent_one, cx_vec bondcurrent_one_ft, cx_vec bondcurrent_two, cx_vec bondcurrent_two_ft, 
                  cx_vec bondcurrent_three, cx_vec bondcurrent_three_ft, cx_vec supercurrent_one, cx_vec supercurrent_one_ft, 
                  cx_vec supercurrent_two, cx_vec supercurrent_two_ft, cx_vec supercurrent_three, cx_vec supercurrent_three_ft, 
                  cx_mat pairdensity, cx_mat pairdensity_ft, cx_mat supermatrix, cx_mat supermatrix_ft, cx_mat TDCurrent, cx_mat TDCurrentNew,
                  cx_mat TDBias, cx_mat TDCurrent_ft, cx_vec NC, cx_vec NC_ft, cx_mat PumpCurrent, cx_double PumpCurrentLR, cx_double PumpCurrentLRstoch, 
                  cx_double PumpCurrentLRstochAdiabatic, field<cx_mat> Glss, field<cx_mat> Ggtr, vector<int> calc, field<cx_mat> Cab, field<cx_mat> GLssNew, 
                  field<cx_mat> GGtrNew, cx_vec dipmom, cx_vec dipmom_ft)
{
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

//  TD densities (diagonal elements of the density matrix)

    MPI_File file;
    MPI_Status status;
    MPI_Datatype num_as_string;         // Declare new datatype for storing numbers on a string
    const int nrows = ntstep;           // Total number of rows to be written
    int ncols;
    if(sc == 0) ncols = nsite+1;        // Total number of columns to be written
    if(sc == 1) ncols = 2*nsite+1;
    char *const fmt = "%15.8e ";        // Writing format within a row
    char *const endfmt = "%15.8e\n";    // Writing format for the end of row
    int startrow, endrow, locnrows;

    const int charspernum = 16;         // Each number is represented by this many chars

    locnrows = nrows/size;              // Local number of rows
    startrow = rank*locnrows;           // Each process starts from this row
    endrow = startrow+locnrows-1;       // Each process writes its own part
    if(rank == size-1)
    {
        endrow = nrows-1;               // The last process finalizes
        locnrows = endrow-startrow+1;
    }

    // Each number takes charspernum chars
    MPI_Type_contiguous(charspernum, MPI_CHAR, &num_as_string);
    MPI_Type_commit(&num_as_string);

    // Collect data into char buffer
    char *data_as_txt = (char*)malloc(locnrows*ncols*charspernum*sizeof(char));
    int count = 0;
    for(int i=0; i<locnrows; i++)
    {
        sprintf(&data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, locnrows*rank+i));
        count++;
        for(int j=0; j<ncols-2; j++)
        {
            sprintf(&data_as_txt[count*charspernum], fmt, real(rho(j,j,i)));
            count++;
        }
        sprintf(&data_as_txt[count*charspernum], endfmt, real(rho(ncols-2,ncols-2,i)));
        count++;
    }

    // Open the file, set the view, write, close and free datatypes
    MPI_File_open(MPI_COMM_WORLD, "output/td_density.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &file);
    int disp = locnrows*rank*ncols*charspernum*sizeof(char);
    MPI_File_set_view(file, disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
    MPI_File_write_all(file, data_as_txt, locnrows*ncols, num_as_string, &status);
    MPI_File_close(&file);
    MPI_Type_free(&num_as_string);

//  SS Densities
//  (Only one row)

    if(rank == 0) 
    {
        ofstream ss_density("output/ss_density.out", ios::out);
        for(unsigned int j=0; j<nsite; j++)
        {
            ss_density << setprecision(5) << scientific << real(rho_ss(j,j)) << "\t";
        }
        ss_density << endl;
        ss_density.close();

        ofstream ss1rdm("output/ss1rdm.out", ios::out);
        for(unsigned int j=0; j<nsite; j++)
        {
            for(unsigned int k=0; k<nsite; k++)
            {
                ss1rdm << setprecision(10) << scientific << real(rho_ss(j,k)) << "  " << imag(rho_ss(j,k)) << "  ";
            }
        }
        ss1rdm << endl;
        ss1rdm.close();
    }

// Bond currents (from the off-diagonal elements of the density matrix
// (Same idea as in "output/td_density.out")
    if(calc[0] == 1)
    {
        MPI_File bc_one_file;
        MPI_Status bc_one_status;
        MPI_Datatype bc_one_num_as_string;
        const int bc_one_ncols = 2;
        int bc_one_startrow, bc_one_endrow, bc_one_locnrows;

        bc_one_locnrows = nrows/size;
        bc_one_startrow = rank*bc_one_locnrows;
        bc_one_endrow = bc_one_startrow+bc_one_locnrows-1;
        if(rank == size-1)
        {
            bc_one_endrow = nrows-1;
            bc_one_locnrows = bc_one_endrow-bc_one_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &bc_one_num_as_string);
        MPI_Type_commit(&bc_one_num_as_string);

        char *bc_one_data_as_txt = (char*)malloc(bc_one_locnrows*bc_one_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<bc_one_locnrows; i++)
        {
            sprintf(&bc_one_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, bc_one_locnrows*rank+i));
            count++;
            sprintf(&bc_one_data_as_txt[count*charspernum], endfmt, real(bondcurrent_one(i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/bond_current_one.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &bc_one_file);
        int bc_one_disp = bc_one_locnrows*rank*bc_one_ncols*charspernum*sizeof(char);
        MPI_File_set_view(bc_one_file, bc_one_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(bc_one_file, bc_one_data_as_txt, bc_one_locnrows*bc_one_ncols, bc_one_num_as_string, &bc_one_status);
        MPI_File_close(&bc_one_file);
        MPI_Type_free(&bc_one_num_as_string);

        MPI_File bc_two_file;
        MPI_Status bc_two_status;
        MPI_Datatype bc_two_num_as_string;
        const int bc_two_ncols = 2;
        int bc_two_startrow, bc_two_endrow, bc_two_locnrows;

        bc_two_locnrows = nrows/size;
        bc_two_startrow = rank*bc_two_locnrows;
        bc_two_endrow = bc_two_startrow+bc_two_locnrows-1;
        if(rank == size-1)
        {
            bc_two_endrow = nrows-1;
            bc_two_locnrows = bc_two_endrow-bc_two_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &bc_two_num_as_string);
        MPI_Type_commit(&bc_two_num_as_string);

        char *bc_two_data_as_txt = (char*)malloc(bc_two_locnrows*bc_two_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<bc_two_locnrows; i++)
        {
            sprintf(&bc_two_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, bc_two_locnrows*rank+i));
            count++;
            sprintf(&bc_two_data_as_txt[count*charspernum], endfmt, real(bondcurrent_two(i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/bond_current_two.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &bc_two_file);
        int bc_two_disp = bc_two_locnrows*rank*bc_two_ncols*charspernum*sizeof(char);
        MPI_File_set_view(bc_two_file, bc_two_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(bc_two_file, bc_two_data_as_txt, bc_two_locnrows*bc_two_ncols, bc_two_num_as_string, &bc_two_status);
        MPI_File_close(&bc_two_file);
        MPI_Type_free(&bc_two_num_as_string);

        MPI_File bc_three_file;
        MPI_Status bc_three_status;
        MPI_Datatype bc_three_num_as_string;
        const int bc_three_ncols = 2;
        int bc_three_startrow, bc_three_endrow, bc_three_locnrows;

        bc_three_locnrows = nrows/size;
        bc_three_startrow = rank*bc_three_locnrows;
        bc_three_endrow = bc_three_startrow+bc_three_locnrows-1;
        if(rank == size-1)
        {
            bc_three_endrow = nrows-1;
            bc_three_locnrows = bc_three_endrow-bc_three_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &bc_three_num_as_string);
        MPI_Type_commit(&bc_three_num_as_string);

        char *bc_three_data_as_txt = (char*)malloc(bc_three_locnrows*bc_three_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<bc_three_locnrows; i++)
        {
            sprintf(&bc_three_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, bc_three_locnrows*rank+i));
            count++;
            sprintf(&bc_three_data_as_txt[count*charspernum], endfmt, real(bondcurrent_three(i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/bond_current_three.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &bc_three_file);
        int bc_three_disp = bc_three_locnrows*rank*bc_three_ncols*charspernum*sizeof(char);
        MPI_File_set_view(bc_three_file, bc_three_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(bc_three_file, bc_three_data_as_txt, bc_three_locnrows*bc_three_ncols, bc_three_num_as_string, &bc_three_status);
        MPI_File_close(&bc_three_file);
        MPI_Type_free(&bc_three_num_as_string);

    // Fourier transformed bond currents
    //  (Same idea as above)

        MPI_File ft_one_file;
        MPI_Status ft_one_status;
        MPI_Datatype ft_one_num_as_string;
        const int ft_one_nrows = nwstep;
        const int ft_one_ncols = 2;
        int ft_one_startrow, ft_one_endrow, ft_one_locnrows;

        ft_one_locnrows = ft_one_nrows/size;
        ft_one_startrow = rank*ft_one_locnrows;
        ft_one_endrow = ft_one_startrow+ft_one_locnrows-1;
        if(rank == size-1)
        {
            ft_one_endrow = ft_one_nrows-1;
            ft_one_locnrows = ft_one_endrow-ft_one_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &ft_one_num_as_string);
        MPI_Type_commit(&ft_one_num_as_string);

        char *ft_one_data_as_txt = (char*)malloc(ft_one_locnrows*ft_one_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<ft_one_locnrows; i++)
        {
            sprintf(&ft_one_data_as_txt[count*charspernum], fmt, (ft_one_locnrows*rank+i)*(2.0*datum::pi/(nwstep*dt)));
            count++;
            sprintf(&ft_one_data_as_txt[count*charspernum], endfmt, abs(bondcurrent_one_ft(i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/bond_current_one_ft.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ft_one_file);
        int ft_one_disp = ft_one_locnrows*rank*ft_one_ncols*charspernum*sizeof(char);
        MPI_File_set_view(ft_one_file, ft_one_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(ft_one_file, ft_one_data_as_txt, ft_one_locnrows*ft_one_ncols, ft_one_num_as_string, &ft_one_status);
        MPI_File_close(&ft_one_file);
        MPI_Type_free(&ft_one_num_as_string);

        MPI_File ft_two_file;
        MPI_Status ft_two_status;
        MPI_Datatype ft_two_num_as_string;
        const int ft_two_nrows = nwstep;
        const int ft_two_ncols = 2;
        int ft_two_startrow, ft_two_endrow, ft_two_locnrows;

        ft_two_locnrows = ft_two_nrows/size;
        ft_two_startrow = rank*ft_two_locnrows;
        ft_two_endrow = ft_two_startrow+ft_two_locnrows-1;
        if(rank == size-1)
        {
            ft_two_endrow = ft_two_nrows-1;
            ft_two_locnrows = ft_two_endrow-ft_two_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &ft_two_num_as_string);
        MPI_Type_commit(&ft_two_num_as_string);

        char *ft_two_data_as_txt = (char*)malloc(ft_two_locnrows*ft_two_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<ft_two_locnrows; i++)
        {
            sprintf(&ft_two_data_as_txt[count*charspernum], fmt, (ft_two_locnrows*rank+i)*(2.0*datum::pi/(nwstep*dt)));
            count++;
            sprintf(&ft_two_data_as_txt[count*charspernum], endfmt, abs(bondcurrent_two_ft(i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/bond_current_two_ft.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ft_two_file);
        int ft_two_disp = ft_two_locnrows*rank*ft_two_ncols*charspernum*sizeof(char);
        MPI_File_set_view(ft_two_file, ft_two_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(ft_two_file, ft_two_data_as_txt, ft_two_locnrows*ft_two_ncols, ft_two_num_as_string, &ft_two_status);
        MPI_File_close(&ft_two_file);
        MPI_Type_free(&ft_two_num_as_string);

        MPI_File ft_three_file;
        MPI_Status ft_three_status;
        MPI_Datatype ft_three_num_as_string;
        const int ft_three_nrows = nwstep;
        const int ft_three_ncols = 2;
        int ft_three_startrow, ft_three_endrow, ft_three_locnrows;

        ft_three_locnrows = ft_three_nrows/size;
        ft_three_startrow = rank*ft_three_locnrows;
        ft_three_endrow = ft_three_startrow+ft_three_locnrows-1;
        if(rank == size-1)
        {
            ft_three_endrow = ft_three_nrows-1;
            ft_three_locnrows = ft_three_endrow-ft_three_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &ft_three_num_as_string);
        MPI_Type_commit(&ft_three_num_as_string);

        char *ft_three_data_as_txt = (char*)malloc(ft_three_locnrows*ft_three_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<ft_three_locnrows; i++)
        {
            sprintf(&ft_three_data_as_txt[count*charspernum], fmt, (ft_three_locnrows*rank+i)*(2.0*datum::pi/(nwstep*dt)));
            count++;
            sprintf(&ft_three_data_as_txt[count*charspernum], endfmt, abs(bondcurrent_three_ft(i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/bond_current_three_ft.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ft_three_file);
        int ft_three_disp = ft_three_locnrows*rank*ft_three_ncols*charspernum*sizeof(char);
        MPI_File_set_view(ft_three_file, ft_three_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(ft_three_file, ft_three_data_as_txt, ft_three_locnrows*ft_three_ncols, ft_three_num_as_string, &ft_three_status);
        MPI_File_close(&ft_three_file);
        MPI_Type_free(&ft_three_num_as_string);

	    if(calc[6] == 1) // Full TD1RDM (with anomalous Green's function)
        {
            MPI_File nf_file;
            MPI_Status nf_status;
            MPI_Datatype nf_num_as_string;
            const int nf_nrows = ntstep;
            int nf_ncols = 2*pow(nsite,2)+1;
            int nf_startrow, nf_endrow, nf_locnrows;

            nf_locnrows = nf_nrows/size;
            nf_startrow = rank*nf_locnrows;
            nf_endrow = nf_startrow+nf_locnrows-1;
            if(rank == size-1)
              {
            nf_endrow = nf_nrows-1;
            nf_locnrows = nf_endrow-nf_startrow+1;
              }

            MPI_Type_contiguous(charspernum, MPI_CHAR, &nf_num_as_string);
            MPI_Type_commit(&nf_num_as_string);

            char *nf_data_as_txt = (char*)malloc(nf_locnrows*nf_ncols*charspernum*sizeof(char));
            int nf_count = 0;
            for(int i=0; i<nf_locnrows; i++)
              {
            sprintf(&nf_data_as_txt[nf_count*charspernum], fmt, (nf_locnrows*rank+i)*dt);
            nf_count++;
            for(int j=0; j<nsite; j++)
              {
                if(j<nsite-1)
                  {
	            for(int k=0; k<nsite; k++)
	              {
	                sprintf(&nf_data_as_txt[nf_count*charspernum], fmt, real(rho(j,k,i)));
	                nf_count++;
	                sprintf(&nf_data_as_txt[nf_count*charspernum], fmt, imag(rho(j,k,i)));
	                nf_count++;
	              }
                  }
                else
                  {
	            for(int k=0; k<nsite-1; k++)
	              {
	                sprintf(&nf_data_as_txt[nf_count*charspernum], fmt, real(rho(j,k,i)));
	                nf_count++;
	                sprintf(&nf_data_as_txt[nf_count*charspernum], fmt, imag(rho(j,k,i)));
	                nf_count++;
	              }
	            sprintf(&nf_data_as_txt[nf_count*charspernum], fmt, real(rho(nsite-1,nsite-1,i)));
	            nf_count++;
	            sprintf(&nf_data_as_txt[nf_count*charspernum], endfmt, imag(rho(nsite-1,nsite-1,i)));
	            nf_count++;
                  }
              }
              }

            MPI_File_open(MPI_COMM_WORLD, "output/td1rdm.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &nf_file);
            int nf_disp = nf_locnrows*rank*nf_ncols*charspernum*sizeof(char);
            MPI_File_set_view(nf_file, nf_disp, MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
            MPI_File_write_all(nf_file, nf_data_as_txt, nf_locnrows*nf_ncols, nf_num_as_string, &nf_status);
            MPI_File_close(&nf_file);
            MPI_Type_free(&nf_num_as_string);

        }

        if(sc == 1)
        {
        // Super currents

            MPI_File bc_super_one_file;
            MPI_Status bc_super_one_status;
            MPI_Datatype bc_super_one_num_as_string;
            const int bc_super_one_ncols = 2;
            int bc_super_one_startrow, bc_super_one_endrow, bc_super_one_locnrows;

            bc_super_one_locnrows = nrows/size;
            bc_super_one_startrow = rank*bc_super_one_locnrows;
            bc_super_one_endrow = bc_super_one_startrow+bc_super_one_locnrows-1;
            if(rank == size-1)
            {
                bc_super_one_endrow = nrows-1;
                bc_super_one_locnrows = bc_super_one_endrow-bc_super_one_startrow+1;
            }

            MPI_Type_contiguous(charspernum, MPI_CHAR, &bc_super_one_num_as_string);
            MPI_Type_commit(&bc_super_one_num_as_string);

            char *bc_super_one_data_as_txt = (char*)malloc(bc_super_one_locnrows*bc_super_one_ncols*charspernum*sizeof(char));
            count = 0;
            for(int i=0; i<bc_super_one_locnrows; i++)
            {
                sprintf(&bc_super_one_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, bc_super_one_locnrows*rank+i));
                count++;
                sprintf(&bc_super_one_data_as_txt[count*charspernum], endfmt, real(supercurrent_one(i)));
                count++;
            }

            MPI_File_open(MPI_COMM_WORLD, "output/super_current_one.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &bc_super_one_file);
            int bc_super_one_disp = bc_super_one_locnrows*rank*bc_super_one_ncols*charspernum*sizeof(char);
            MPI_File_set_view(bc_super_one_file, bc_super_one_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
            MPI_File_write_all(bc_super_one_file, bc_super_one_data_as_txt, bc_super_one_locnrows*bc_super_one_ncols, bc_super_one_num_as_string, &bc_super_one_status);
            MPI_File_close(&bc_super_one_file);
            MPI_Type_free(&bc_super_one_num_as_string);

            MPI_File bc_super_two_file;
            MPI_Status bc_super_two_status;
            MPI_Datatype bc_super_two_num_as_string;
            const int bc_super_two_ncols = 2;
            int bc_super_two_startrow, bc_super_two_endrow, bc_super_two_locnrows;

            bc_super_two_locnrows = nrows/size;
            bc_super_two_startrow = rank*bc_super_two_locnrows;
            bc_super_two_endrow = bc_super_two_startrow+bc_super_two_locnrows-1;
            if(rank == size-1)
            {
                bc_super_two_endrow = nrows-1;
                bc_super_two_locnrows = bc_super_two_endrow-bc_super_two_startrow+1;
            }

            MPI_Type_contiguous(charspernum, MPI_CHAR, &bc_super_two_num_as_string);
            MPI_Type_commit(&bc_super_two_num_as_string);

            char *bc_super_two_data_as_txt = (char*)malloc(bc_super_two_locnrows*bc_super_two_ncols*charspernum*sizeof(char));
            count = 0;
            for(int i=0; i<bc_super_two_locnrows; i++)
            {
                sprintf(&bc_super_two_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, bc_super_two_locnrows*rank+i));
                count++;
                sprintf(&bc_super_two_data_as_txt[count*charspernum], endfmt, real(supercurrent_two(i)));
                count++;
            }

            MPI_File_open(MPI_COMM_WORLD, "output/super_current_two.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &bc_super_two_file);
            int bc_super_two_disp = bc_super_two_locnrows*rank*bc_super_two_ncols*charspernum*sizeof(char);
            MPI_File_set_view(bc_super_two_file, bc_super_two_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
            MPI_File_write_all(bc_super_two_file, bc_super_two_data_as_txt, bc_super_two_locnrows*bc_super_two_ncols, bc_super_two_num_as_string, &bc_super_two_status);
            MPI_File_close(&bc_super_two_file);
            MPI_Type_free(&bc_super_two_num_as_string);

            MPI_File bc_super_three_file;
            MPI_Status bc_super_three_status;
            MPI_Datatype bc_super_three_num_as_string;
            const int bc_super_three_ncols = 2;
            int bc_super_three_startrow, bc_super_three_endrow, bc_super_three_locnrows;

            bc_super_three_locnrows = nrows/size;
            bc_super_three_startrow = rank*bc_super_three_locnrows;
            bc_super_three_endrow = bc_super_three_startrow+bc_super_three_locnrows-1;
            if(rank == size-1)
            {
                bc_super_three_endrow = nrows-1;
                bc_super_three_locnrows = bc_super_three_endrow-bc_super_three_startrow+1;
            }

            MPI_Type_contiguous(charspernum, MPI_CHAR, &bc_super_three_num_as_string);
            MPI_Type_commit(&bc_super_three_num_as_string);

            char *bc_super_three_data_as_txt = (char*)malloc(bc_super_three_locnrows*bc_super_three_ncols*charspernum*sizeof(char));
            count = 0;
            for(int i=0; i<bc_super_three_locnrows; i++)
            {
                sprintf(&bc_super_three_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, bc_super_three_locnrows*rank+i));
                count++;
                sprintf(&bc_super_three_data_as_txt[count*charspernum], endfmt, real(supercurrent_three(i)));
                count++;
            }

            MPI_File_open(MPI_COMM_WORLD, "output/super_current_three.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &bc_super_three_file);
            int bc_super_three_disp = bc_super_three_locnrows*rank*bc_super_three_ncols*charspernum*sizeof(char);
            MPI_File_set_view(bc_super_three_file, bc_super_three_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
            MPI_File_write_all(bc_super_three_file, bc_super_three_data_as_txt, bc_super_three_locnrows*bc_super_three_ncols, bc_super_three_num_as_string, &bc_super_three_status);
            MPI_File_close(&bc_super_three_file);
            MPI_Type_free(&bc_super_three_num_as_string);

            // Fourier transformed super currents

            MPI_File ft_super_one_file;
            MPI_Status ft_super_one_status;
            MPI_Datatype ft_super_one_num_as_string;
            const int ft_super_one_nrows = nwstep;
            const int ft_super_one_ncols = 2;
            int ft_super_one_startrow, ft_super_one_endrow, ft_super_one_locnrows;

            ft_super_one_locnrows = ft_super_one_nrows/size;
            ft_super_one_startrow = rank*ft_super_one_locnrows;
            ft_super_one_endrow = ft_super_one_startrow+ft_super_one_locnrows-1;
            if(rank == size-1)
            {
                ft_super_one_endrow = ft_super_one_nrows-1;
                ft_super_one_locnrows = ft_super_one_endrow-ft_super_one_startrow+1;
            }

            MPI_Type_contiguous(charspernum, MPI_CHAR, &ft_super_one_num_as_string);
            MPI_Type_commit(&ft_super_one_num_as_string);

            char *ft_super_one_data_as_txt = (char*)malloc(ft_super_one_locnrows*ft_super_one_ncols*charspernum*sizeof(char));
            count = 0;
            for(int i=0; i<ft_super_one_locnrows; i++)
            {
                sprintf(&ft_super_one_data_as_txt[count*charspernum], fmt, (ft_super_one_locnrows*rank+i)*(2.0*datum::pi/(nwstep*dt)));
                count++;
                sprintf(&ft_super_one_data_as_txt[count*charspernum], endfmt, abs(supercurrent_one_ft(i)));
                count++;
            }

            MPI_File_open(MPI_COMM_WORLD, "output/super_current_one_ft.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ft_super_one_file);
            int ft_super_one_disp = ft_super_one_locnrows*rank*ft_super_one_ncols*charspernum*sizeof(char);
            MPI_File_set_view(ft_super_one_file, ft_super_one_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
            MPI_File_write_all(ft_super_one_file, ft_super_one_data_as_txt, ft_super_one_locnrows*ft_super_one_ncols, ft_super_one_num_as_string, &ft_super_one_status);
            MPI_File_close(&ft_super_one_file);
            MPI_Type_free(&ft_super_one_num_as_string);

            MPI_File ft_super_two_file;
            MPI_Status ft_super_two_status;
            MPI_Datatype ft_super_two_num_as_string;
            const int ft_super_two_nrows = nwstep;
            const int ft_super_two_ncols = 2;
            int ft_super_two_startrow, ft_super_two_endrow, ft_super_two_locnrows;

            ft_super_two_locnrows = ft_super_two_nrows/size;
            ft_super_two_startrow = rank*ft_super_two_locnrows;
            ft_super_two_endrow = ft_super_two_startrow+ft_super_two_locnrows-1;
            if(rank == size-1)
            {
                ft_super_two_endrow = ft_super_two_nrows-1;
                ft_super_two_locnrows = ft_super_two_endrow-ft_super_two_startrow+1;
            }

            MPI_Type_contiguous(charspernum, MPI_CHAR, &ft_super_two_num_as_string);
            MPI_Type_commit(&ft_super_two_num_as_string);

            char *ft_super_two_data_as_txt = (char*)malloc(ft_super_two_locnrows*ft_super_two_ncols*charspernum*sizeof(char));
            count = 0;
            for(int i=0; i<ft_super_two_locnrows; i++)
            {
                sprintf(&ft_super_two_data_as_txt[count*charspernum], fmt, (ft_super_two_locnrows*rank+i)*(2.0*datum::pi/(nwstep*dt)));
                count++;
                sprintf(&ft_super_two_data_as_txt[count*charspernum], endfmt, abs(supercurrent_two_ft(i)));
                count++;
            }

            MPI_File_open(MPI_COMM_WORLD, "output/super_current_two_ft.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ft_super_two_file);
            int ft_super_two_disp = ft_super_two_locnrows*rank*ft_super_two_ncols*charspernum*sizeof(char);
            MPI_File_set_view(ft_super_two_file, ft_super_two_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
            MPI_File_write_all(ft_super_two_file, ft_super_two_data_as_txt, ft_super_two_locnrows*ft_super_two_ncols, ft_super_two_num_as_string, &ft_super_two_status);
            MPI_File_close(&ft_super_two_file);
            MPI_Type_free(&ft_super_two_num_as_string);

            MPI_File ft_super_three_file;
            MPI_Status ft_super_three_status;
            MPI_Datatype ft_super_three_num_as_string;
            const int ft_super_three_nrows = nwstep;
            const int ft_super_three_ncols = 2;
            int ft_super_three_startrow, ft_super_three_endrow, ft_super_three_locnrows;

            ft_super_three_locnrows = ft_super_three_nrows/size;
            ft_super_three_startrow = rank*ft_super_three_locnrows;
            ft_super_three_endrow = ft_super_three_startrow+ft_super_three_locnrows-1;
            if(rank == size-1)
            {
                ft_super_three_endrow = ft_super_three_nrows-1;
                ft_super_three_locnrows = ft_super_three_endrow-ft_super_three_startrow+1;
            }

            MPI_Type_contiguous(charspernum, MPI_CHAR, &ft_super_three_num_as_string);
            MPI_Type_commit(&ft_super_three_num_as_string);

            char *ft_super_three_data_as_txt = (char*)malloc(ft_super_three_locnrows*ft_super_three_ncols*charspernum*sizeof(char));
            count = 0;
            for(int i=0; i<ft_super_three_locnrows; i++)
            {
                sprintf(&ft_super_three_data_as_txt[count*charspernum], fmt, (ft_super_three_locnrows*rank+i)*(2.0*datum::pi/(nwstep*dt)));
                count++;
                sprintf(&ft_super_three_data_as_txt[count*charspernum], endfmt, abs(supercurrent_three_ft(i)));
                count++;
            }

            MPI_File_open(MPI_COMM_WORLD, "output/super_current_three_ft.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ft_super_three_file);
            int ft_super_three_disp = ft_super_three_locnrows*rank*ft_super_three_ncols*charspernum*sizeof(char);
            MPI_File_set_view(ft_super_three_file, ft_super_three_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
            MPI_File_write_all(ft_super_three_file, ft_super_three_data_as_txt, ft_super_three_locnrows*ft_super_three_ncols, ft_super_three_num_as_string, &ft_super_three_status);
            MPI_File_close(&ft_super_three_file);
            MPI_Type_free(&ft_super_three_num_as_string);

            // Same for the pairing densities

            MPI_File pair_file;
            MPI_Status pair_status;
            MPI_Datatype pair_num_as_string;
            const int pair_ncols = nsite + 1;
            int pair_startrow, pair_endrow, pair_locnrows;

            pair_locnrows = nrows/size;
            pair_startrow = rank*pair_locnrows;
            pair_endrow = pair_startrow+pair_locnrows-1;
            if(rank == size-1)
            {
                pair_endrow = nrows-1;
                pair_locnrows = pair_endrow-pair_startrow+1;
            }

            MPI_Type_contiguous(charspernum, MPI_CHAR, &pair_num_as_string);
            MPI_Type_commit(&pair_num_as_string);

            char *pair_data_as_txt = (char*)malloc(pair_locnrows*pair_ncols*charspernum*sizeof(char));
            count = 0;
            for(int i=0; i<pair_locnrows; i++)
            {
                sprintf(&pair_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, pair_locnrows*rank+i));
                count++;
                for(int j=0; j<pair_ncols-2; j++)
                {
    //                sprintf(&pair_data_as_txt[count*charspernum], fmt, real(pairdensity(j,i)));
    //                count++;
    //                sprintf(&pair_data_as_txt[count*charspernum], fmt, imag(pairdensity(j,i)));
    //                count++;
                    sprintf(&pair_data_as_txt[count*charspernum], fmt, abs(pairdensity(j,i)));
                    count++;
                }
    //            sprintf(&pair_data_as_txt[count*charspernum], fmt, real(pairdensity(pair_ncols-2,i)));
    //            count++;
    //            sprintf(&pair_data_as_txt[count*charspernum], fmt, imag(pairdensity(pair_ncols-2,i)));
    //            count++;
                sprintf(&pair_data_as_txt[count*charspernum], endfmt, abs(pairdensity(pair_ncols-2,i)));
                count++;
            }

            MPI_File_open(MPI_COMM_WORLD, "output/pairdensity.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &pair_file);
            int pair_disp = pair_locnrows*rank*pair_ncols*charspernum*sizeof(char);
            MPI_File_set_view(pair_file, pair_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
            MPI_File_write_all(pair_file, pair_data_as_txt, pair_locnrows*pair_ncols, pair_num_as_string, &pair_status);
            MPI_File_close(&pair_file);
            MPI_Type_free(&pair_num_as_string);

            MPI_File ft_pair_file;
            MPI_Status ft_pair_status;
            MPI_Datatype ft_pair_num_as_string;
            const int ft_pair_nrows = nwstep;
            const int ft_pair_ncols = nsite + 1;
            int ft_pair_startrow, ft_pair_endrow, ft_pair_locnrows;

            ft_pair_locnrows = ft_pair_nrows/size;
            ft_pair_startrow = rank*ft_pair_locnrows;
            ft_pair_endrow = ft_pair_startrow+ft_pair_locnrows-1;
            if(rank == size-1)
            {
                ft_pair_endrow = ft_pair_nrows-1;
                ft_pair_locnrows = ft_pair_endrow-ft_pair_startrow+1;
            }

            MPI_Type_contiguous(charspernum, MPI_CHAR, &ft_pair_num_as_string);
            MPI_Type_commit(&ft_pair_num_as_string);

            char *ft_pair_data_as_txt = (char*)malloc(ft_pair_locnrows*ft_pair_ncols*charspernum*sizeof(char));
            count = 0;
            for(int i=0; i<ft_pair_locnrows; i++)
            {
                sprintf(&ft_pair_data_as_txt[count*charspernum], fmt, (ft_pair_locnrows*rank+i)*(2.0*datum::pi/(nwstep*dt)));
                count++;
                for(int j=0; j<ft_pair_ncols-2; j++)
                {
                    sprintf(&ft_pair_data_as_txt[count*charspernum], fmt, abs(pairdensity_ft(j,i)));
                    count++;
                }
                sprintf(&ft_pair_data_as_txt[count*charspernum], endfmt, abs(pairdensity_ft(ft_pair_ncols-2,i)));
                count++;
            }

            MPI_File_open(MPI_COMM_WORLD, "output/pairdensity_ft.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ft_pair_file);
            int ft_pair_disp = ft_pair_locnrows*rank*ft_pair_ncols*charspernum*sizeof(char);
            MPI_File_set_view(ft_pair_file, ft_pair_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
            MPI_File_write_all(ft_pair_file, ft_pair_data_as_txt, ft_pair_locnrows*ft_pair_ncols, ft_pair_num_as_string, &ft_pair_status);
            MPI_File_close(&ft_pair_file);
            MPI_Type_free(&ft_pair_num_as_string);

            // Same for the supercurrent matrix

            MPI_File supermatrix_file;
            MPI_Status supermatrix_status;
            MPI_Datatype supermatrix_num_as_string;
            const int supermatrix_ncols = nsite + 1;
            int supermatrix_startrow, supermatrix_endrow, supermatrix_locnrows;

            supermatrix_locnrows = nrows/size;
            supermatrix_startrow = rank*supermatrix_locnrows;
            supermatrix_endrow = supermatrix_startrow+supermatrix_locnrows-1;
            if(rank == size-1)
            {
                supermatrix_endrow = nrows-1;
                supermatrix_locnrows = supermatrix_endrow-supermatrix_startrow+1;
            }

            MPI_Type_contiguous(charspernum, MPI_CHAR, &supermatrix_num_as_string);
            MPI_Type_commit(&supermatrix_num_as_string);

            char *supermatrix_data_as_txt = (char*)malloc(supermatrix_locnrows*supermatrix_ncols*charspernum*sizeof(char));
            count = 0;
            for(int i=0; i<supermatrix_locnrows; i++)
            {
                sprintf(&supermatrix_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, supermatrix_locnrows*rank+i));
                count++;
                for(int j=0; j<supermatrix_ncols-2; j++)
                {
                    sprintf(&supermatrix_data_as_txt[count*charspernum], fmt, real(supermatrix(j,i)));
                    count++;
                }
                sprintf(&supermatrix_data_as_txt[count*charspernum], endfmt, real(supermatrix(supermatrix_ncols-2,i)));
                count++;
            }

            MPI_File_open(MPI_COMM_WORLD, "output/supermatrix.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &supermatrix_file);
            int supermatrix_disp = supermatrix_locnrows*rank*supermatrix_ncols*charspernum*sizeof(char);
            MPI_File_set_view(supermatrix_file, supermatrix_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
            MPI_File_write_all(supermatrix_file, supermatrix_data_as_txt, supermatrix_locnrows*supermatrix_ncols, supermatrix_num_as_string, &supermatrix_status);
            MPI_File_close(&supermatrix_file);
            MPI_Type_free(&supermatrix_num_as_string);

            MPI_File ft_supermatrix_file;
            MPI_Status ft_supermatrix_status;
            MPI_Datatype ft_supermatrix_num_as_string;
            const int ft_supermatrix_nrows = nwstep;
            const int ft_supermatrix_ncols = nsite + 1;
            int ft_supermatrix_startrow, ft_supermatrix_endrow, ft_supermatrix_locnrows;

            ft_supermatrix_locnrows = ft_supermatrix_nrows/size;
            ft_supermatrix_startrow = rank*ft_supermatrix_locnrows;
            ft_supermatrix_endrow = ft_supermatrix_startrow+ft_supermatrix_locnrows-1;
            if(rank == size-1)
            {
                ft_supermatrix_endrow = ft_supermatrix_nrows-1;
                ft_supermatrix_locnrows = ft_supermatrix_endrow-ft_supermatrix_startrow+1;
            }

            MPI_Type_contiguous(charspernum, MPI_CHAR, &ft_supermatrix_num_as_string);
            MPI_Type_commit(&ft_supermatrix_num_as_string);

            char *ft_supermatrix_data_as_txt = (char*)malloc(ft_supermatrix_locnrows*ft_supermatrix_ncols*charspernum*sizeof(char));
            count = 0;
            for(int i=0; i<ft_supermatrix_locnrows; i++)
            {
                sprintf(&ft_supermatrix_data_as_txt[count*charspernum], fmt, (ft_supermatrix_locnrows*rank+i)*(2.0*datum::pi/(nwstep*dt)));
                count++;
                for(int j=0; j<ft_supermatrix_ncols-2; j++)
                {
                    sprintf(&ft_supermatrix_data_as_txt[count*charspernum], fmt, abs(supermatrix_ft(j,i)));
                    count++;
                }
                sprintf(&ft_supermatrix_data_as_txt[count*charspernum], endfmt, abs(supermatrix_ft(ft_supermatrix_ncols-2,i)));
                count++;
            }

            MPI_File_open(MPI_COMM_WORLD, "output/supermatrix_ft.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ft_supermatrix_file);
            int ft_supermatrix_disp = ft_supermatrix_locnrows*rank*ft_supermatrix_ncols*charspernum*sizeof(char);
            MPI_File_set_view(ft_supermatrix_file, ft_supermatrix_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
            MPI_File_write_all(ft_supermatrix_file, ft_supermatrix_data_as_txt, ft_supermatrix_locnrows*ft_supermatrix_ncols, ft_supermatrix_num_as_string, &ft_supermatrix_status);
            MPI_File_close(&ft_supermatrix_file);
            MPI_Type_free(&ft_supermatrix_num_as_string);


            if(calc[6] == 1) // Full TD1RDM (with anomalous Green's function)
            {
                MPI_File f_file;
                MPI_Status f_status;
                MPI_Datatype f_num_as_string;
                const int f_nrows = ntstep;
                int f_ncols = 2*pow(2*nsite,2)+1;
                int f_startrow, f_endrow, f_locnrows;

                f_locnrows = f_nrows/size;
                f_startrow = rank*f_locnrows;
                f_endrow = f_startrow+f_locnrows-1;
                if(rank == size-1)
                {
                    f_endrow = f_nrows-1;
                    f_locnrows = f_endrow-f_startrow+1;
                }

                MPI_Type_contiguous(charspernum, MPI_CHAR, &f_num_as_string);
                MPI_Type_commit(&f_num_as_string);

                char *f_data_as_txt = (char*)malloc(f_locnrows*f_ncols*charspernum*sizeof(char));
                int f_count = 0;
                for(int i=0; i<f_locnrows; i++)
                {
                    sprintf(&f_data_as_txt[f_count*charspernum], fmt, (f_locnrows*rank+i)*dt);
                    f_count++;
                    for(int j=0; j<2*nsite; j++)
                    {
                        if(j<2*nsite-1)
                        {
                            for(int k=0; k<2*nsite; k++)
                            {
                                sprintf(&f_data_as_txt[f_count*charspernum], fmt, real(rho(j,k,i)));
                                f_count++;
                                sprintf(&f_data_as_txt[f_count*charspernum], fmt, imag(rho(j,k,i)));
                                f_count++;
                            }
                        }
                        else
                        {
                            for(int k=0; k<2*nsite-1; k++)
                            {
                                sprintf(&f_data_as_txt[f_count*charspernum], fmt, real(rho(j,k,i)));
                                f_count++;
                                sprintf(&f_data_as_txt[f_count*charspernum], fmt, imag(rho(j,k,i)));
                                f_count++;
                            }
                            sprintf(&f_data_as_txt[f_count*charspernum], fmt, real(rho(nsite-1,nsite-1,i)));
                            f_count++;
                            sprintf(&f_data_as_txt[f_count*charspernum], endfmt, imag(rho(nsite-1,nsite-1,i)));
                            f_count++;
                        }
                    }
                }

                MPI_File_open(MPI_COMM_WORLD, "output/td1rdm.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &f_file);
                int f_disp = f_locnrows*rank*f_ncols*charspernum*sizeof(char);
                MPI_File_set_view(f_file, f_disp, MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
                MPI_File_write_all(f_file, f_data_as_txt, f_locnrows*f_ncols, f_num_as_string, &f_status);
                MPI_File_close(&f_file);
                MPI_Type_free(&f_num_as_string);
            }
	    
        }

    // Snapshots of the full density matrix (from different processes)
        if(snaps == 1)
        {
            string tmpstr, tmpstr_hole;
            stringstream tmpss;
            unsigned int elems = ntstep/size;
            for(unsigned int j=0; j<size; j++)
            {
                if(j == rank)   // Different processes write locally
                {
                    if(elems <= 10)     // If there is more than 1 cpu per 10 tsteps
                    {
                        if(j%(10/elems) == 0)   // Write every 10th timestep
                        {
                            tmpss << int(j/(10/elems));
                            tmpstr = "output/snapshot_re" + tmpss.str() + ".out";  // Filename contains an index
                            ofstream snapshot_re(tmpstr.c_str(), ios::out);
                            snapshot_re << setprecision(5) << scientific;

                            // For sc == 1
                            tmpstr_hole = "output/snapshot_hole_re" + tmpss.str() + ".out";
                            ofstream snapshot_hole_re(tmpstr_hole.c_str(), ios::out);
                            snapshot_hole_re << setprecision(5) << scientific;

                            if(sc == 0)
                            {
                                for(unsigned int k=0; k<nsite; k++)
                                {
                                    for(unsigned int l=0; l<nsite; l++)
                                    {
                                        snapshot_re << real(rho(k,l,0)) << "\t";   // First timestep of the local rho
                                    }
                                    snapshot_re << endl;
                                }
                            }
                            if(sc == 1) // TODO
                            {
                                for(unsigned int k=0; k<nsite; k++)
                                {
                                    for(unsigned int l=0; l<nsite; l++)
                                    {
                                        snapshot_re << real(rho(k,l,0)) << "\t";   // First timestep of the local rho
                                        snapshot_hole_re << real(rho(k+nsite,l+nsite,0)) << "\t";
                                    }
                                    snapshot_re << endl;
                                    snapshot_hole_re << endl;
                                }
                            }
                            snapshot_re.close();
                            snapshot_hole_re.close();
                            tmpss.str("");

                            tmpss << int(j/(10/elems));
                            tmpstr = "output/snapshot_im" + tmpss.str() + ".out";  // Filename contains an index
                            ofstream snapshot_im(tmpstr.c_str(), ios::out);
                            snapshot_im << setprecision(5) << scientific;

                            // For sc == 1
                            tmpstr_hole = "output/snapshot_hole_im" + tmpss.str() + ".out";
                            ofstream snapshot_hole_im(tmpstr_hole.c_str(), ios::out);
                            snapshot_hole_im << setprecision(5) << scientific;

                            if(sc == 0)
                            {
                                for(unsigned int k=0; k<nsite; k++)
                                {
                                    for(unsigned int l=0; l<nsite; l++)
                                    {
                                        snapshot_im << imag(rho(k,l,0)) << "\t";   // First timestep of the local rho
                                    }
                                    snapshot_im << endl;
                                }
                            }
                            if(sc == 1) // TODO
                            {
                                for(unsigned int k=0; k<nsite; k++)
                                {
                                    for(unsigned int l=0; l<nsite; l++)
                                    {
                                        snapshot_im << imag(rho(k,l,0)) << "\t";   // First timestep of the local rho
                                        snapshot_hole_im << imag(rho(k+nsite,l+nsite,0)) << "\t";
                                    }
                                    snapshot_im << endl;
                                    snapshot_hole_im << endl;
                                }
                            }
                            snapshot_im.close();
                            snapshot_hole_im.close();
                            tmpss.str("");
                        }
                    }
                    else     // If there is less than 1 cpu per 10 tsteps
                    {
                        for(unsigned int k=0; k<elems; k++)
                        {
                            unsigned int l = elems*rank + k;    // l accounts for individual steps
                            if(l%10 == 0)   // Write every 10th timestep
                            {
                                tmpss << int(l/10);
                                tmpstr = "output/snapshot_re" + tmpss.str() + ".out";  // Filename contains an index
                                ofstream snapshot_re(tmpstr.c_str(), ios::out);
                                snapshot_re << setprecision(5) << scientific;

                                // For sc == 1
                                tmpstr_hole = "output/snapshot_hole_re" + tmpss.str() + ".out";
                                ofstream snapshot_hole_re(tmpstr_hole.c_str(), ios::out);
                                snapshot_hole_re << setprecision(5) << scientific;

                                if(sc == 0)
                                {
                                    for(unsigned int m=0; m<nsite; m++)
                                    {
                                        for(unsigned int n=0; n<nsite; n++)
                                        {
                                            snapshot_re << real(rho(m,n,k)) << "\t";   // k:th step of the local rho
                                        }
                                        snapshot_re << endl;
                                    }
                                }
                                if(sc == 1) // TODO
                                {
                                    for(unsigned int m=0; m<nsite; m++)
                                    {
                                        for(unsigned int n=0; n<nsite; n++)
                                        {
                                            snapshot_re << real(rho(m,n,k)) << "\t";   // k:th step of the local rho
                                            snapshot_hole_re << real(rho(m+nsite,n+nsite,k)) << "\t";
                                        }
                                        snapshot_re << endl;
                                        snapshot_hole_re << endl;
                                    }
                                }
                                snapshot_re.close();
                                snapshot_hole_re.close();
                                tmpss.str("");

                                tmpss << int(l/10);
                                tmpstr = "output/snapshot_im" + tmpss.str() + ".out";  // Filename contains an index
                                ofstream snapshot_im(tmpstr.c_str(), ios::out);
                                snapshot_im << setprecision(5) << scientific;

                                // For sc == 1
                                tmpstr_hole = "output/snapshot_hole_im" + tmpss.str() + ".out";
                                ofstream snapshot_hole_im(tmpstr_hole.c_str(), ios::out);
                                snapshot_hole_im << setprecision(5) << scientific;
                                if(sc == 0)
                                {
                                    for(unsigned int m=0; m<nsite; m++)
                                    {
                                        for(unsigned int n=0; n<nsite; n++)
                                        {
                                            snapshot_im << imag(rho(m,n,k)) << "\t";   // k:th step of the local rho
                                        }
                                        snapshot_im << endl;
                                    }
                                }
                                if(sc == 1) // TODO
                                {
                                    for(unsigned int m=0; m<nsite; m++)
                                    {
                                        for(unsigned int n=0; n<nsite; n++)
                                        {
                                            snapshot_im << imag(rho(m,n,k)) << "\t";   // k:th step of the local rho
                                            snapshot_hole_im << imag(rho(m+nsite,n+nsite,k)) << "\t";
                                        }
                                        snapshot_im << endl;
                                        snapshot_hole_im << endl;
                                    }
                                }
                                snapshot_im.close();
                                snapshot_hole_im.close();
                                tmpss.str("");
                            }
                        }
                    }
                }
            }
        }

        MPI_File td_dip_file;
        MPI_Status td_dip_status;
        MPI_Datatype td_dip_num_as_string;
        const int td_dip_ncols = 3;
        int td_dip_startrow, td_dip_endrow, td_dip_locnrows;

        td_dip_locnrows = nrows/size;
        td_dip_startrow = rank*td_dip_locnrows;
        td_dip_endrow = td_dip_startrow+td_dip_locnrows-1;
        if(rank == size-1)
        {
            td_dip_endrow = nrows-1;
            td_dip_locnrows = td_dip_endrow-td_dip_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &td_dip_num_as_string);
        MPI_Type_commit(&td_dip_num_as_string);

        char *td_dip_data_as_txt = (char*)malloc(td_dip_locnrows*td_dip_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<td_dip_locnrows; i++)
        {
            sprintf(&td_dip_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, td_dip_locnrows*rank+i));
            count++;
            sprintf(&td_dip_data_as_txt[count*charspernum], fmt, real(dipmom(i)));
            count++;
            sprintf(&td_dip_data_as_txt[count*charspernum], endfmt, real(dipmom(i)/3.33564e-30)); // Coulomb-metres in Debyes
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/dipmom.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &td_dip_file);
        int td_dip_disp = td_dip_locnrows*rank*td_dip_ncols*charspernum*sizeof(char);
        MPI_File_set_view(td_dip_file, td_dip_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(td_dip_file, td_dip_data_as_txt, td_dip_locnrows*td_dip_ncols, td_dip_num_as_string, &td_dip_status);
        MPI_File_close(&td_dip_file);
        MPI_Type_free(&td_dip_num_as_string);

        MPI_File ft_dip_file;
        MPI_Status ft_dip_status;
        MPI_Datatype ft_dip_num_as_string;
        const int ft_dip_nrows = nwstep;
        const int ft_dip_ncols = 2;
        int ft_dip_startrow, ft_dip_endrow, ft_dip_locnrows;

        ft_dip_locnrows = ft_dip_nrows/size;
        ft_dip_startrow = rank*ft_dip_locnrows;
        ft_dip_endrow = ft_dip_startrow+ft_dip_locnrows-1;
        if(rank == size-1)
        {
            ft_dip_endrow = ft_dip_nrows-1;
            ft_dip_locnrows = ft_dip_endrow-ft_dip_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &ft_dip_num_as_string);
        MPI_Type_commit(&ft_dip_num_as_string);

        char *ft_dip_data_as_txt = (char*)malloc(ft_dip_locnrows*ft_dip_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<ft_dip_locnrows; i++)
        {
            sprintf(&ft_dip_data_as_txt[count*charspernum], fmt, (ft_dip_locnrows*rank+i)*(2.0*datum::pi/(nwstep*dt)));
            count++;
            sprintf(&ft_dip_data_as_txt[count*charspernum], endfmt, abs(dipmom_ft(i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/dipmom_ft.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ft_dip_file);
        int ft_dip_disp = ft_dip_locnrows*rank*ft_dip_ncols*charspernum*sizeof(char);
        MPI_File_set_view(ft_dip_file, ft_dip_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(ft_dip_file, ft_dip_data_as_txt, ft_dip_locnrows*ft_dip_ncols, ft_dip_num_as_string, &ft_dip_status);
        MPI_File_close(&ft_dip_file);
        MPI_Type_free(&ft_dip_num_as_string);

    } // if calc 1rdm

// TD bias current
    if(calc[1] == 1)
    {
        MPI_File td_curr_file;
        MPI_Status td_curr_status;
        MPI_Datatype td_curr_num_as_string;
        const int td_curr_ncols = nlead + 1;
        int td_curr_startrow, td_curr_endrow, td_curr_locnrows;

        td_curr_locnrows = nrows/size;
        td_curr_startrow = rank*td_curr_locnrows;
        td_curr_endrow = td_curr_startrow+td_curr_locnrows-1;
        if(rank == size-1)
        {
            td_curr_endrow = nrows-1;
            td_curr_locnrows = td_curr_endrow-td_curr_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &td_curr_num_as_string);
        MPI_Type_commit(&td_curr_num_as_string);

        char *td_curr_data_as_txt = (char*)malloc(td_curr_locnrows*td_curr_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<td_curr_locnrows; i++)
        {
            sprintf(&td_curr_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, td_curr_locnrows*rank+i));
            count++;
            for(int j=0; j<td_curr_ncols-2; j++)
            {
                sprintf(&td_curr_data_as_txt[count*charspernum], fmt, real(TDCurrent(j,i)));
                count++;
            }
            sprintf(&td_curr_data_as_txt[count*charspernum], endfmt, real(TDCurrent(td_curr_ncols-2,i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/td_current.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &td_curr_file);
        int td_curr_disp = td_curr_locnrows*rank*td_curr_ncols*charspernum*sizeof(char);
        MPI_File_set_view(td_curr_file, td_curr_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(td_curr_file, td_curr_data_as_txt, td_curr_locnrows*td_curr_ncols, td_curr_num_as_string, &td_curr_status);
        MPI_File_close(&td_curr_file);
        MPI_Type_free(&td_curr_num_as_string);

    // TD bias current
        MPI_File td_bias_file;
        MPI_Status td_bias_status;
        MPI_Datatype td_bias_num_as_string;
        const int td_bias_ncols = nlead + 1;
        int td_bias_startrow, td_bias_endrow, td_bias_locnrows;

        td_bias_locnrows = nrows/size;
        td_bias_startrow = rank*td_bias_locnrows;
        td_bias_endrow = td_bias_startrow+td_bias_locnrows-1;
        if(rank == size-1)
        {
            td_bias_endrow = nrows-1;
            td_bias_locnrows = td_bias_endrow-td_bias_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &td_bias_num_as_string);
        MPI_Type_commit(&td_bias_num_as_string);

        char *td_bias_data_as_txt = (char*)malloc(td_bias_locnrows*td_bias_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<td_bias_locnrows; i++)
        {
            sprintf(&td_bias_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, td_bias_locnrows*rank+i));
            count++;
            for(int j=0; j<td_bias_ncols-2; j++)
            {
                sprintf(&td_bias_data_as_txt[count*charspernum], fmt, real(TDBias(j,i)));
                count++;
            }
            sprintf(&td_bias_data_as_txt[count*charspernum], endfmt, real(TDBias(td_bias_ncols-2,i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/td_bias.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &td_bias_file);
        int td_bias_disp = td_bias_locnrows*rank*td_bias_ncols*charspernum*sizeof(char);
        MPI_File_set_view(td_bias_file, td_bias_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(td_bias_file, td_bias_data_as_txt, td_bias_locnrows*td_bias_ncols, td_bias_num_as_string, &td_bias_status);
        MPI_File_close(&td_bias_file);
        MPI_Type_free(&td_bias_num_as_string);

    // Fourier transform

        MPI_File ft_bias_file;
        MPI_Status ft_bias_status;
        MPI_Datatype ft_bias_num_as_string;
        const int ft_bias_nrows = nwstep;
        const int ft_bias_ncols = nlead + 1;
        int ft_bias_startrow, ft_bias_endrow, ft_bias_locnrows;

        ft_bias_locnrows = ft_bias_nrows/size;
        ft_bias_startrow = rank*ft_bias_locnrows;
        ft_bias_endrow = ft_bias_startrow+ft_bias_locnrows-1;
        if(rank == size-1)
        {
            ft_bias_endrow = ft_bias_nrows-1;
            ft_bias_locnrows = ft_bias_endrow-ft_bias_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &ft_bias_num_as_string);
        MPI_Type_commit(&ft_bias_num_as_string);

        char *ft_bias_data_as_txt = (char*)malloc(ft_bias_locnrows*ft_bias_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<ft_bias_locnrows; i++)
        {
            sprintf(&ft_bias_data_as_txt[count*charspernum], fmt, (ft_bias_locnrows*rank+i)*(2.0*datum::pi/(nwstep*dt)));
            count++;
            for(int j=0; j<ft_bias_ncols-2; j++)
            {
                sprintf(&ft_bias_data_as_txt[count*charspernum], fmt, real(TDCurrent_ft(j,i)));
                count++;
            }
            sprintf(&ft_bias_data_as_txt[count*charspernum], endfmt, real(TDCurrent_ft(ft_bias_ncols-2,i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/td_current_ft.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ft_bias_file);
        int ft_bias_disp = ft_bias_locnrows*rank*ft_bias_ncols*charspernum*sizeof(char);
        MPI_File_set_view(ft_bias_file, ft_bias_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(ft_bias_file, ft_bias_data_as_txt, ft_bias_locnrows*ft_bias_ncols, ft_bias_num_as_string, &ft_bias_status);
        MPI_File_close(&ft_bias_file);
        MPI_Type_free(&ft_bias_num_as_string);
    } // if calc td current

// New TD current
    if(calc[9] == 1)
    {
        MPI_File td_currnew_file;
        MPI_Status td_currnew_status;
        MPI_Datatype td_currnew_num_as_string;
        const int td_currnew_ncols = nlead + 1;
        int td_currnew_startrow, td_currnew_endrow, td_currnew_locnrows;

        td_currnew_locnrows = nrows/size;
        td_currnew_startrow = rank*td_currnew_locnrows;
        td_currnew_endrow = td_currnew_startrow+td_currnew_locnrows-1;
        if(rank == size-1)
        {
            td_currnew_endrow = nrows-1;
            td_currnew_locnrows = td_currnew_endrow-td_currnew_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &td_currnew_num_as_string);
        MPI_Type_commit(&td_currnew_num_as_string);

        char *td_currnew_data_as_txt = (char*)malloc(td_currnew_locnrows*td_currnew_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<td_currnew_locnrows; i++)
        {
            sprintf(&td_currnew_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, td_currnew_locnrows*rank+i));
            count++;
            for(int j=0; j<td_currnew_ncols-2; j++)
            {
                sprintf(&td_currnew_data_as_txt[count*charspernum], fmt, real(TDCurrentNew(j,i)));
                count++;
            }
            sprintf(&td_currnew_data_as_txt[count*charspernum], endfmt, real(TDCurrentNew(td_currnew_ncols-2,i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/td_current_new.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &td_currnew_file);
        int td_currnew_disp = td_currnew_locnrows*rank*td_currnew_ncols*charspernum*sizeof(char);
        MPI_File_set_view(td_currnew_file, td_currnew_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(td_currnew_file, td_currnew_data_as_txt, td_currnew_locnrows*td_currnew_ncols, td_currnew_num_as_string, &td_currnew_status);
        MPI_File_close(&td_currnew_file);
        MPI_Type_free(&td_currnew_num_as_string);
    } // if calc new td current

// TD bias particle number
    if(calc[2] == 1)
    {
        MPI_File td_N_file;
        MPI_Status td_N_status;
        MPI_Datatype td_N_num_as_string;
        const int td_N_ncols = 2;
        int td_N_startrow, td_N_endrow, td_N_locnrows;

        td_N_locnrows = nrows/size;
        td_N_startrow = rank*td_N_locnrows;
        td_N_endrow = td_N_startrow+td_N_locnrows-1;
        if(rank == size-1)
        {
            td_N_endrow = nrows-1;
            td_N_locnrows = td_N_endrow-td_N_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &td_N_num_as_string);
        MPI_Type_commit(&td_N_num_as_string);

        char *td_N_data_as_txt = (char*)malloc(td_N_locnrows*td_N_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<td_N_locnrows; i++)
        {
            sprintf(&td_N_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, td_N_locnrows*rank+i));
            count++;
            sprintf(&td_N_data_as_txt[count*charspernum], endfmt, real(NC(i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/td_nop.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &td_N_file);
        int td_N_disp = td_N_locnrows*rank*td_N_ncols*charspernum*sizeof(char);
        MPI_File_set_view(td_N_file, td_N_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(td_N_file, td_N_data_as_txt, td_N_locnrows*td_N_ncols, td_N_num_as_string, &td_N_status);
        MPI_File_close(&td_N_file);
        MPI_Type_free(&td_N_num_as_string);

    // Fourier transform

        MPI_File ft_N_file;
        MPI_Status ft_N_status;
        MPI_Datatype ft_N_num_as_string;
        const int ft_N_nrows = nwstep;
        const int ft_N_ncols = 2;
        int ft_N_startrow, ft_N_endrow, ft_N_locnrows;

        ft_N_locnrows = ft_N_nrows/size;
        ft_N_startrow = rank*ft_N_locnrows;
        ft_N_endrow = ft_N_startrow+ft_N_locnrows-1;
        if(rank == size-1)
        {
            ft_N_endrow = ft_N_nrows-1;
            ft_N_locnrows = ft_N_endrow-ft_N_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &ft_N_num_as_string);
        MPI_Type_commit(&ft_N_num_as_string);

        char *ft_N_data_as_txt = (char*)malloc(ft_N_locnrows*ft_N_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<ft_N_locnrows; i++)
        {
            sprintf(&ft_N_data_as_txt[count*charspernum], fmt, (ft_N_locnrows*rank+i)*(2.0*datum::pi/(nwstep*dt)));
            count++;
            sprintf(&ft_N_data_as_txt[count*charspernum], endfmt, abs(NC_ft(i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/td_nop_ft.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ft_N_file);
        int ft_N_disp = ft_N_locnrows*rank*ft_N_ncols*charspernum*sizeof(char);
        MPI_File_set_view(ft_N_file, ft_N_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(ft_N_file, ft_N_data_as_txt, ft_N_locnrows*ft_N_ncols, ft_N_num_as_string, &ft_N_status);
        MPI_File_close(&ft_N_file);
        MPI_Type_free(&ft_N_num_as_string);
    } // if calc N

    // Pump current (all leads)
    if(calc[3] == 1)
    {
        if(rank == 0) 
        {
            ofstream pumpout("output/pump_current.out", ios::out);
            for(unsigned int alpha=0; alpha<nlead; alpha++)
            {
                for(unsigned int be=0; be<nlead; be++)
                {
                    pumpout << setprecision(5) << scientific << alpha << "\t" 
                            << be << "\t" << real(PumpCurrent(alpha,be)) << endl;
                }
            }
            pumpout.close();
        }
    } // if calc pump

    // Pump current (LR)
    if(calc[7] == 1)
    {
        if(rank == 0) 
        {
            ofstream pumplr("output/pump_current_lr.out", ios::out);
			pumplr << setprecision(5) << scientific << real(PumpCurrentLR) << endl;
            pumplr.close();
        }
    } // if calc pump

    // Pump current (stochastic)
    if(calc[8] == 1)
    {
        if(rank == 0) 
        {
            ofstream pumplrstoch("output/pump_current_lr_stoch.out", ios::out);
			pumplrstoch << setprecision(5) << scientific << real(PumpCurrentLRstoch) << endl;
            pumplrstoch.close();
             
             ofstream pumplrstochadiabatic("output/pump_current_lr_stochadiabatic.out", ios::out);
			pumplrstochadiabatic << setprecision(5) << scientific << real(PumpCurrentLRstochAdiabatic) << endl;
            pumplrstochadiabatic.close();
        }
    } // if calc pump

//  Glss and Ggtr
    if(calc[4] == 1)
    {
        MPI_File glss_re_file;
        MPI_Status glss_re_status;
        MPI_Datatype glss_re_num_as_string;
        const int glss_re_ncols = nsite + 2;
        int glss_re_startrow, glss_re_endrow, glss_re_locnrows;

        glss_re_locnrows = nrows/size;
        glss_re_startrow = rank*glss_re_locnrows;
        glss_re_endrow = glss_re_startrow+glss_re_locnrows-1;
        if(rank == size-1)
        {
            glss_re_endrow = nrows-1;
            glss_re_locnrows = glss_re_endrow-glss_re_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &glss_re_num_as_string);
        MPI_Type_commit(&glss_re_num_as_string);

        char *glss_re_data_as_txt = (char*)malloc(glss_re_locnrows*ntstep*glss_re_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<glss_re_locnrows; i++)
        {
            for(unsigned int ii=0; ii<ntstep; ii++)
            {
                sprintf(&glss_re_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, glss_re_locnrows*rank+i));
                count++;
                sprintf(&glss_re_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ii));
                count++;
                for(int j=0; j<glss_re_ncols-3; j++)
                {
                    sprintf(&glss_re_data_as_txt[count*charspernum], fmt, real(Glss(i,ii)(j,j)));
                    count++;
                }
                sprintf(&glss_re_data_as_txt[count*charspernum], endfmt, real(Glss(i,ii)(glss_re_ncols-3,glss_re_ncols-3)));
                count++;
            }
        }

        MPI_File_open(MPI_COMM_WORLD, "output/glss_re.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &glss_re_file);
        int glss_re_disp = glss_re_locnrows*rank*ntstep*glss_re_ncols*charspernum*sizeof(char);
        MPI_File_set_view(glss_re_file, glss_re_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(glss_re_file, glss_re_data_as_txt, glss_re_locnrows*ntstep*glss_re_ncols, glss_re_num_as_string, &glss_re_status);
        MPI_File_close(&glss_re_file);
        MPI_Type_free(&glss_re_num_as_string);

    //

        MPI_File glss_im_file;
        MPI_Status glss_im_status;
        MPI_Datatype glss_im_num_as_string;
        const int glss_im_ncols = nsite + 2;
        int glss_im_startrow, glss_im_endrow, glss_im_locnrows;

        glss_im_locnrows = nrows/size;
        glss_im_startrow = rank*glss_im_locnrows;
        glss_im_endrow = glss_im_startrow+glss_im_locnrows-1;
        if(rank == size-1)
        {
            glss_im_endrow = nrows-1;
            glss_im_locnrows = glss_im_endrow-glss_im_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &glss_im_num_as_string);
        MPI_Type_commit(&glss_im_num_as_string);

        char *glss_im_data_as_txt = (char*)malloc(glss_im_locnrows*ntstep*glss_im_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<glss_im_locnrows; i++)
        {
            for(unsigned int ii=0; ii<ntstep; ii++)
            {
                sprintf(&glss_im_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, glss_im_locnrows*rank+i));
                count++;
                sprintf(&glss_im_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ii));
                count++;
                for(int j=0; j<glss_im_ncols-3; j++)
                {
                    sprintf(&glss_im_data_as_txt[count*charspernum], fmt, imag(Glss(i,ii)(j,j)));
                    count++;
                }
                sprintf(&glss_im_data_as_txt[count*charspernum], endfmt, imag(Glss(i,ii)(glss_im_ncols-3,glss_im_ncols-3)));
                count++;
            }
        }

        MPI_File_open(MPI_COMM_WORLD, "output/glss_im.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &glss_im_file);
        int glss_im_disp = glss_im_locnrows*rank*ntstep*glss_im_ncols*charspernum*sizeof(char);
        MPI_File_set_view(glss_im_file, glss_im_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(glss_im_file, glss_im_data_as_txt, glss_im_locnrows*ntstep*glss_im_ncols, glss_im_num_as_string, &glss_im_status);
        MPI_File_close(&glss_im_file);
        MPI_Type_free(&glss_im_num_as_string);

    //

        MPI_File ggtr_re_file;
        MPI_Status ggtr_re_status;
        MPI_Datatype ggtr_re_num_as_string;
        const int ggtr_re_ncols = nsite + 2;
        int ggtr_re_startrow, ggtr_re_endrow, ggtr_re_locnrows;

        ggtr_re_locnrows = nrows/size;
        ggtr_re_startrow = rank*ggtr_re_locnrows;
        ggtr_re_endrow = ggtr_re_startrow+ggtr_re_locnrows-1;
        if(rank == size-1)
        {
            ggtr_re_endrow = nrows-1;
            ggtr_re_locnrows = ggtr_re_endrow-ggtr_re_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &ggtr_re_num_as_string);
        MPI_Type_commit(&ggtr_re_num_as_string);

        char *ggtr_re_data_as_txt = (char*)malloc(ggtr_re_locnrows*ntstep*ggtr_re_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<ggtr_re_locnrows; i++)
        {
            for(unsigned int ii=0; ii<ntstep; ii++)
            {
                sprintf(&ggtr_re_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ggtr_re_locnrows*rank+i));
                count++;
                sprintf(&ggtr_re_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ii));
                count++;
                for(int j=0; j<ggtr_re_ncols-3; j++)
                {
                    sprintf(&ggtr_re_data_as_txt[count*charspernum], fmt, real(Ggtr(i,ii)(j,j)));
                    count++;
                }
                sprintf(&ggtr_re_data_as_txt[count*charspernum], endfmt, real(Ggtr(i,ii)(ggtr_re_ncols-3,ggtr_re_ncols-3)));
                count++;
            }
        }

        MPI_File_open(MPI_COMM_WORLD, "output/ggtr_re.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ggtr_re_file);
        int ggtr_re_disp = ggtr_re_locnrows*rank*ntstep*ggtr_re_ncols*charspernum*sizeof(char);
        MPI_File_set_view(ggtr_re_file, ggtr_re_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(ggtr_re_file, ggtr_re_data_as_txt, ggtr_re_locnrows*ntstep*ggtr_re_ncols, ggtr_re_num_as_string, &ggtr_re_status);
        MPI_File_close(&ggtr_re_file);
        MPI_Type_free(&ggtr_re_num_as_string);

    //

        MPI_File ggtr_im_file;
        MPI_Status ggtr_im_status;
        MPI_Datatype ggtr_im_num_as_string;
        const int ggtr_im_ncols = nsite + 2;
        int ggtr_im_startrow, ggtr_im_endrow, ggtr_im_locnrows;

        ggtr_im_locnrows = nrows/size;
        ggtr_im_startrow = rank*ggtr_im_locnrows;
        ggtr_im_endrow = ggtr_im_startrow+ggtr_im_locnrows-1;
        if(rank == size-1)
        {
            ggtr_im_endrow = nrows-1;
            ggtr_im_locnrows = ggtr_im_endrow-ggtr_im_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &ggtr_im_num_as_string);
        MPI_Type_commit(&ggtr_im_num_as_string);

        char *ggtr_im_data_as_txt = (char*)malloc(ggtr_im_locnrows*ntstep*ggtr_im_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<ggtr_im_locnrows; i++)
        {
            for(unsigned int ii=0; ii<ntstep; ii++)
            {
                sprintf(&ggtr_im_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ggtr_im_locnrows*rank+i));
                count++;
                sprintf(&ggtr_im_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ii));
                count++;
                for(int j=0; j<ggtr_im_ncols-3; j++)
                {
                    sprintf(&ggtr_im_data_as_txt[count*charspernum], fmt, imag(Ggtr(i,ii)(j,j)));
                    count++;
                }
                sprintf(&ggtr_im_data_as_txt[count*charspernum], endfmt, imag(Ggtr(i,ii)(ggtr_im_ncols-3,ggtr_im_ncols-3)));
                count++;
            }
        }

        MPI_File_open(MPI_COMM_WORLD, "output/ggtr_im.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ggtr_im_file);
        int ggtr_im_disp = ggtr_im_locnrows*rank*ntstep*ggtr_im_ncols*charspernum*sizeof(char);
        MPI_File_set_view(ggtr_im_file, ggtr_im_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(ggtr_im_file, ggtr_im_data_as_txt, ggtr_im_locnrows*ntstep*ggtr_im_ncols, ggtr_im_num_as_string, &ggtr_im_status);
        MPI_File_close(&ggtr_im_file);
        MPI_Type_free(&ggtr_im_num_as_string);
    } // if calc two-time Green's function

//  Current correlation function
    if(calc[10] == 1)
    {
        MPI_File curr_corr_file;
        MPI_Status curr_corr_status;
        MPI_Datatype curr_corr_num_as_string;
        const int curr_corr_ncols = 2*pow(nlead,2) + 2;
        int curr_corr_startrow, curr_corr_endrow, curr_corr_locnrows;

        curr_corr_locnrows = nrows/size;
        curr_corr_startrow = rank*curr_corr_locnrows;
        curr_corr_endrow = curr_corr_startrow+curr_corr_locnrows-1;
        if(rank == size-1)
        {
            curr_corr_endrow = nrows-1;
            curr_corr_locnrows = curr_corr_endrow-curr_corr_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &curr_corr_num_as_string);
        MPI_Type_commit(&curr_corr_num_as_string);

        char *curr_corr_data_as_txt = (char*)malloc(curr_corr_locnrows*ntstep*curr_corr_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<curr_corr_locnrows; i++)
        {
            for(unsigned int ii=0; ii<ntstep; ii++)
            {
                sprintf(&curr_corr_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, curr_corr_locnrows*rank+i));
                count++;
                sprintf(&curr_corr_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ii));
                count++;
                for(unsigned int j=0; j<nlead; j++)
                {
                    for(unsigned int k=0; k<nlead; k++)
                    {
						if(j!=nlead-1 || k!=nlead-1)
						{
							sprintf(&curr_corr_data_as_txt[count*charspernum], fmt, real((Cab(i,ii))(j,k)));
							count++;
							sprintf(&curr_corr_data_as_txt[count*charspernum], fmt, imag((Cab(i,ii))(j,k)));
							count++;
						}
						else
						{
							sprintf(&curr_corr_data_as_txt[count*charspernum], fmt, real((Cab(i,ii))(j,k)));
							count++;
							sprintf(&curr_corr_data_as_txt[count*charspernum], endfmt, imag((Cab(i,ii))(j,k)));
							count++;
						}
                    }
                }
            }
        }

        MPI_File_open(MPI_COMM_WORLD, "output/curr_corr.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &curr_corr_file);
        int curr_corr_disp = curr_corr_locnrows*ntstep*rank*curr_corr_ncols*charspernum*sizeof(char);
        MPI_File_set_view(curr_corr_file, curr_corr_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(curr_corr_file, curr_corr_data_as_txt, curr_corr_locnrows*ntstep*curr_corr_ncols, curr_corr_num_as_string, &curr_corr_status);
        MPI_File_close(&curr_corr_file);
        MPI_Type_free(&curr_corr_num_as_string);

	//

        MPI_File auto_corr_file;
        MPI_Status auto_corr_status;
        MPI_Datatype auto_corr_num_as_string;
        const int auto_corr_ncols = 2 + 2;
        int auto_corr_startrow, auto_corr_endrow, auto_corr_locnrows;

        auto_corr_locnrows = nrows/size;
        auto_corr_startrow = rank*auto_corr_locnrows;
        auto_corr_endrow = auto_corr_startrow+auto_corr_locnrows-1;
        if(rank == size-1)
        {
            auto_corr_endrow = nrows-1;
            auto_corr_locnrows = auto_corr_endrow-auto_corr_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &auto_corr_num_as_string);
        MPI_Type_commit(&auto_corr_num_as_string);

        char *auto_corr_data_as_txt = (char*)malloc(auto_corr_locnrows*ntstep*auto_corr_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<auto_corr_locnrows; i++)
        {
            for(unsigned int ii=0; ii<ntstep; ii++)
            {
                sprintf(&auto_corr_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, auto_corr_locnrows*rank+i));
                count++;
                sprintf(&auto_corr_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ii));
                count++;
				sprintf(&auto_corr_data_as_txt[count*charspernum], fmt, real(0.5*((Cab(i,ii))(0,0) + (Cab(i,ii))(1,1))));
				count++;
				sprintf(&auto_corr_data_as_txt[count*charspernum], endfmt, imag(0.5*((Cab(i,ii))(0,0) + (Cab(i,ii))(1,1))));
				count++;
            }
        }

        MPI_File_open(MPI_COMM_WORLD, "output/auto_corr.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &auto_corr_file);
        int auto_corr_disp = auto_corr_locnrows*ntstep*rank*auto_corr_ncols*charspernum*sizeof(char);
        MPI_File_set_view(auto_corr_file, auto_corr_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(auto_corr_file, auto_corr_data_as_txt, auto_corr_locnrows*ntstep*auto_corr_ncols, auto_corr_num_as_string, &auto_corr_status);
        MPI_File_close(&auto_corr_file);
        MPI_Type_free(&auto_corr_num_as_string);

	//

        MPI_File cross_corr_file;
        MPI_Status cross_corr_status;
        MPI_Datatype cross_corr_num_as_string;
        const int cross_corr_ncols = 2 + 2;
        int cross_corr_startrow, cross_corr_endrow, cross_corr_locnrows;

        cross_corr_locnrows = nrows/size;
        cross_corr_startrow = rank*cross_corr_locnrows;
        cross_corr_endrow = cross_corr_startrow+cross_corr_locnrows-1;
        if(rank == size-1)
        {
            cross_corr_endrow = nrows-1;
            cross_corr_locnrows = cross_corr_endrow-cross_corr_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &cross_corr_num_as_string);
        MPI_Type_commit(&cross_corr_num_as_string);

        char *cross_corr_data_as_txt = (char*)malloc(cross_corr_locnrows*ntstep*cross_corr_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<cross_corr_locnrows; i++)
        {
            for(unsigned int ii=0; ii<ntstep; ii++)
            {
				sprintf(&cross_corr_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, cross_corr_locnrows*rank+i));
				count++;
				sprintf(&cross_corr_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ii));
				count++;
				sprintf(&cross_corr_data_as_txt[count*charspernum], fmt, real(0.5*((Cab(i,ii))(0,1) + (Cab(i,ii))(1,0))));
				count++;
				sprintf(&cross_corr_data_as_txt[count*charspernum], endfmt, imag(0.5*((Cab(i,ii))(0,1) + (Cab(i,ii))(1,0))));
				count++;
            }
        }

        MPI_File_open(MPI_COMM_WORLD, "output/cross_corr.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &cross_corr_file);
        int cross_corr_disp = cross_corr_locnrows*ntstep*rank*cross_corr_ncols*charspernum*sizeof(char);
        MPI_File_set_view(cross_corr_file, cross_corr_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(cross_corr_file, cross_corr_data_as_txt, cross_corr_locnrows*ntstep*cross_corr_ncols, cross_corr_num_as_string, &cross_corr_status);
        MPI_File_close(&cross_corr_file);
        MPI_Type_free(&cross_corr_num_as_string);

    // G^{<,>}

        MPI_File glss_new_re_file;
        MPI_Status glss_new_re_status;
        MPI_Datatype glss_new_re_num_as_string;
        const int glss_new_re_ncols = nsite + 2;
        int glss_new_re_startrow, glss_new_re_endrow, glss_new_re_locnrows;

        glss_new_re_locnrows = nrows/size;
        glss_new_re_startrow = rank*glss_new_re_locnrows;
        glss_new_re_endrow = glss_new_re_startrow+glss_new_re_locnrows-1;
        if(rank == size-1)
        {
            glss_new_re_endrow = nrows-1;
            glss_new_re_locnrows = glss_new_re_endrow-glss_new_re_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &glss_new_re_num_as_string);
        MPI_Type_commit(&glss_new_re_num_as_string);

        char *glss_new_re_data_as_txt = (char*)malloc(glss_new_re_locnrows*ntstep*glss_new_re_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<glss_new_re_locnrows; i++)
        {
            for(unsigned int ii=0; ii<ntstep; ii++)
            {
                sprintf(&glss_new_re_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, glss_new_re_locnrows*rank+i));
                count++;
                sprintf(&glss_new_re_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ii));
                count++;
                for(int j=0; j<glss_new_re_ncols-3; j++)
                {
                    sprintf(&glss_new_re_data_as_txt[count*charspernum], fmt, real(GLssNew(ii,i)(j,j)));
                    count++;
                }
                sprintf(&glss_new_re_data_as_txt[count*charspernum], endfmt, real((GLssNew(ii,i))(glss_new_re_ncols-3,glss_new_re_ncols-3)));
                count++;
            }
        }

        MPI_File_open(MPI_COMM_WORLD, "output/glss_new_re.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &glss_new_re_file);
        int glss_new_re_disp = glss_new_re_locnrows*rank*ntstep*glss_new_re_ncols*charspernum*sizeof(char);
        MPI_File_set_view(glss_new_re_file, glss_new_re_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(glss_new_re_file, glss_new_re_data_as_txt, glss_new_re_locnrows*ntstep*glss_new_re_ncols, glss_new_re_num_as_string, &glss_new_re_status);
        MPI_File_close(&glss_new_re_file);
        MPI_Type_free(&glss_new_re_num_as_string);

    //

        MPI_File glss_new_im_file;
        MPI_Status glss_new_im_status;
        MPI_Datatype glss_new_im_num_as_string;
        const int glss_new_im_ncols = nsite + 2;
        int glss_new_im_startrow, glss_new_im_endrow, glss_new_im_locnrows;

        glss_new_im_locnrows = nrows/size;
        glss_new_im_startrow = rank*glss_new_im_locnrows;
        glss_new_im_endrow = glss_new_im_startrow+glss_new_im_locnrows-1;
        if(rank == size-1)
        {
            glss_new_im_endrow = nrows-1;
            glss_new_im_locnrows = glss_new_im_endrow-glss_new_im_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &glss_new_im_num_as_string);
        MPI_Type_commit(&glss_new_im_num_as_string);

        char *glss_new_im_data_as_txt = (char*)malloc(glss_new_im_locnrows*ntstep*glss_new_im_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<glss_new_im_locnrows; i++)
        {
            for(unsigned int ii=0; ii<ntstep; ii++)
            {
                sprintf(&glss_new_im_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, glss_new_im_locnrows*rank+i));
                count++;
                sprintf(&glss_new_im_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ii));
                count++;
                for(int j=0; j<glss_new_im_ncols-3; j++)
                {
                    sprintf(&glss_new_im_data_as_txt[count*charspernum], fmt, imag(GLssNew(ii,i)(j,j)));
                    count++;
                }
                sprintf(&glss_new_im_data_as_txt[count*charspernum], endfmt, imag(GLssNew(ii,i)(glss_new_im_ncols-3,glss_new_im_ncols-3)));
                count++;
            }
        }

        MPI_File_open(MPI_COMM_WORLD, "output/glss_new_im.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &glss_new_im_file);
        int glss_new_im_disp = glss_new_im_locnrows*rank*ntstep*glss_new_im_ncols*charspernum*sizeof(char);
        MPI_File_set_view(glss_new_im_file, glss_new_im_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(glss_new_im_file, glss_new_im_data_as_txt, glss_new_im_locnrows*ntstep*glss_new_im_ncols, glss_new_im_num_as_string, &glss_new_im_status);
        MPI_File_close(&glss_new_im_file);
        MPI_Type_free(&glss_new_im_num_as_string);

    //

        MPI_File ggtr_new_re_file;
        MPI_Status ggtr_new_re_status;
        MPI_Datatype ggtr_new_re_num_as_string;
        const int ggtr_new_re_ncols = nsite + 2;
        int ggtr_new_re_startrow, ggtr_new_re_endrow, ggtr_new_re_locnrows;

        ggtr_new_re_locnrows = nrows/size;
        ggtr_new_re_startrow = rank*ggtr_new_re_locnrows;
        ggtr_new_re_endrow = ggtr_new_re_startrow+ggtr_new_re_locnrows-1;
        if(rank == size-1)
        {
            ggtr_new_re_endrow = nrows-1;
            ggtr_new_re_locnrows = ggtr_new_re_endrow-ggtr_new_re_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &ggtr_new_re_num_as_string);
        MPI_Type_commit(&ggtr_new_re_num_as_string);

        char *ggtr_new_re_data_as_txt = (char*)malloc(ggtr_new_re_locnrows*ntstep*ggtr_new_re_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<ggtr_new_re_locnrows; i++)
        {
            for(unsigned int ii=0; ii<ntstep; ii++)
            {
                sprintf(&ggtr_new_re_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ggtr_new_re_locnrows*rank+i));
                count++;
                sprintf(&ggtr_new_re_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ii));
                count++;
                for(int j=0; j<ggtr_new_re_ncols-3; j++)
                {
                    sprintf(&ggtr_new_re_data_as_txt[count*charspernum], fmt, real(GGtrNew(i,ii)(j,j)));
                    count++;
                }
                sprintf(&ggtr_new_re_data_as_txt[count*charspernum], endfmt, real(GGtrNew(i,ii)(ggtr_new_re_ncols-3,ggtr_new_re_ncols-3)));
                count++;
            }
        }

        MPI_File_open(MPI_COMM_WORLD, "output/ggtr_new_re.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ggtr_new_re_file);
        int ggtr_new_re_disp = ggtr_new_re_locnrows*rank*ntstep*ggtr_new_re_ncols*charspernum*sizeof(char);
        MPI_File_set_view(ggtr_new_re_file, ggtr_new_re_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(ggtr_new_re_file, ggtr_new_re_data_as_txt, ggtr_new_re_locnrows*ntstep*ggtr_new_re_ncols, ggtr_new_re_num_as_string, &ggtr_new_re_status);
        MPI_File_close(&ggtr_new_re_file);
        MPI_Type_free(&ggtr_new_re_num_as_string);

    //

        MPI_File ggtr_new_im_file;
        MPI_Status ggtr_new_im_status;
        MPI_Datatype ggtr_new_im_num_as_string;
        const int ggtr_new_im_ncols = nsite + 2;
        int ggtr_new_im_startrow, ggtr_new_im_endrow, ggtr_new_im_locnrows;

        ggtr_new_im_locnrows = nrows/size;
        ggtr_new_im_startrow = rank*ggtr_new_im_locnrows;
        ggtr_new_im_endrow = ggtr_new_im_startrow+ggtr_new_im_locnrows-1;
        if(rank == size-1)
        {
            ggtr_new_im_endrow = nrows-1;
            ggtr_new_im_locnrows = ggtr_new_im_endrow-ggtr_new_im_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &ggtr_new_im_num_as_string);
        MPI_Type_commit(&ggtr_new_im_num_as_string);

        char *ggtr_new_im_data_as_txt = (char*)malloc(ggtr_new_im_locnrows*ntstep*ggtr_new_im_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<ggtr_new_im_locnrows; i++)
        {
            for(unsigned int ii=0; ii<ntstep; ii++)
            {
                sprintf(&ggtr_new_im_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ggtr_new_im_locnrows*rank+i));
                count++;
                sprintf(&ggtr_new_im_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, ii));
                count++;
                for(int j=0; j<ggtr_new_im_ncols-3; j++)
                {
                    sprintf(&ggtr_new_im_data_as_txt[count*charspernum], fmt, imag(GGtrNew(i,ii)(j,j)));
                    count++;
                }
                sprintf(&ggtr_new_im_data_as_txt[count*charspernum], endfmt, imag(GGtrNew(i,ii)(ggtr_new_im_ncols-3,ggtr_new_im_ncols-3)));
                count++;
            }
        }

        MPI_File_open(MPI_COMM_WORLD, "output/ggtr_new_im.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ggtr_new_im_file);
        int ggtr_new_im_disp = ggtr_new_im_locnrows*rank*ntstep*ggtr_new_im_ncols*charspernum*sizeof(char);
        MPI_File_set_view(ggtr_new_im_file, ggtr_new_im_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(ggtr_new_im_file, ggtr_new_im_data_as_txt, ggtr_new_im_locnrows*ntstep*ggtr_new_im_ncols, ggtr_new_im_num_as_string, &ggtr_new_im_status);
        MPI_File_close(&ggtr_new_im_file);
        MPI_Type_free(&ggtr_new_im_num_as_string);

		if(calc[0]!=1)
		{
			MPI_File td_dip2_file;
			MPI_Status td_dip2_status;
			MPI_Datatype td_dip2_num_as_string;
			const int td_dip2_ncols = 3;
			int td_dip2_startrow, td_dip2_endrow, td_dip2_locnrows;

			td_dip2_locnrows = nrows/size;
			td_dip2_startrow = rank*td_dip2_locnrows;
			td_dip2_endrow = td_dip2_startrow+td_dip2_locnrows-1;
			if(rank == size-1)
			{
				td_dip2_endrow = nrows-1;
				td_dip2_locnrows = td_dip2_endrow-td_dip2_startrow+1;
			}

			MPI_Type_contiguous(charspernum, MPI_CHAR, &td_dip2_num_as_string);
			MPI_Type_commit(&td_dip2_num_as_string);

			char *td_dip2_data_as_txt = (char*)malloc(td_dip2_locnrows*td_dip2_ncols*charspernum*sizeof(char));
			count = 0;
			for(int i=0; i<td_dip2_locnrows; i++)
			{
				sprintf(&td_dip2_data_as_txt[count*charspernum], fmt, Time(ntstep, tgridtype, dt, td_dip2_locnrows*rank+i));
				count++;
				sprintf(&td_dip2_data_as_txt[count*charspernum], fmt, real(dipmom(i)));
				count++;
				sprintf(&td_dip2_data_as_txt[count*charspernum], endfmt, real(dipmom(i)/3.33564e-30)); // Coulomb-metres in Debyes
				count++;
			}

			MPI_File_open(MPI_COMM_WORLD, "output/dipmom.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &td_dip2_file);
			int td_dip2_disp = td_dip2_locnrows*rank*td_dip2_ncols*charspernum*sizeof(char);
			MPI_File_set_view(td_dip2_file, td_dip2_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
			MPI_File_write_all(td_dip2_file, td_dip2_data_as_txt, td_dip2_locnrows*td_dip2_ncols, td_dip2_num_as_string, &td_dip2_status);
			MPI_File_close(&td_dip2_file);
			MPI_Type_free(&td_dip2_num_as_string);

			MPI_File ft_dip_file;
			MPI_Status ft_dip_status;
			MPI_Datatype ft_dip_num_as_string;
			const int ft_dip_nrows = nwstep;
			const int ft_dip_ncols = 2;
			int ft_dip_startrow, ft_dip_endrow, ft_dip_locnrows;

			ft_dip_locnrows = ft_dip_nrows/size;
			ft_dip_startrow = rank*ft_dip_locnrows;
			ft_dip_endrow = ft_dip_startrow+ft_dip_locnrows-1;
			if(rank == size-1)
			{
				ft_dip_endrow = ft_dip_nrows-1;
				ft_dip_locnrows = ft_dip_endrow-ft_dip_startrow+1;
			}

			MPI_Type_contiguous(charspernum, MPI_CHAR, &ft_dip_num_as_string);
			MPI_Type_commit(&ft_dip_num_as_string);

			char *ft_dip_data_as_txt = (char*)malloc(ft_dip_locnrows*ft_dip_ncols*charspernum*sizeof(char));
			count = 0;
			for(int i=0; i<ft_dip_locnrows; i++)
			{
				sprintf(&ft_dip_data_as_txt[count*charspernum], fmt, (ft_dip_locnrows*rank+i)*(2.0*datum::pi/(nwstep*dt)));
				count++;
				sprintf(&ft_dip_data_as_txt[count*charspernum], endfmt, abs(dipmom_ft(i)));
				count++;
			}

			MPI_File_open(MPI_COMM_WORLD, "output/dipmom_ft.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &ft_dip_file);
			int ft_dip_disp = ft_dip_locnrows*rank*ft_dip_ncols*charspernum*sizeof(char);
			MPI_File_set_view(ft_dip_file, ft_dip_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
			MPI_File_write_all(ft_dip_file, ft_dip_data_as_txt, ft_dip_locnrows*ft_dip_ncols, ft_dip_num_as_string, &ft_dip_status);
			MPI_File_close(&ft_dip_file);
			MPI_Type_free(&ft_dip_num_as_string);
		}
    } // if calc correlation function
}
