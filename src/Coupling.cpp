#include "Main.hpp"
cx_cube Gamma(int nsite, int nlead, vector<int> lcontact, vector<int> rcontact, 
              vector<int> lcontact2, vector<int> rcontact2, int lead_row, double lambda_strength, 
              double gamma_strength, double lead_hop, cx_mat gs_eigvec, int systype, int sc)
{
    cx_cube Gam(nsite,nsite,nlead);
    Gam.zeros();
    
    if(systype == 5)
    {
        Gam.fill(gamma_strength);
/*
        Gam.slice(0)(0,0) = 0.01;        Gam.slice(0)(0,1) = 0.02;
        Gam.slice(0)(1,0) = 0.02;        Gam.slice(0)(1,1) = 0.04;

        Gam.slice(1)(0,0) = 0.09;        Gam.slice(1)(0,1) = 0.12;
        Gam.slice(1)(1,0) = 0.12;        Gam.slice(1)(1,1) = 0.16;
*/
    }
    else if(systype == 6)
    {
        Gam.slice(0)(0,0) = gamma_strength;        Gam.slice(0)(0,1) = gamma_strength;        Gam.slice(0)(0,2) = gamma_strength;        Gam.slice(0)(0,3) = gamma_strength;
        Gam.slice(0)(1,0) = gamma_strength;        Gam.slice(0)(1,1) = gamma_strength;        Gam.slice(0)(1,2) = gamma_strength;        Gam.slice(0)(1,3) = gamma_strength;
        Gam.slice(0)(2,0) = gamma_strength;        Gam.slice(0)(2,1) = gamma_strength;        Gam.slice(0)(2,2) = gamma_strength;        Gam.slice(0)(2,3) = gamma_strength;
        Gam.slice(0)(3,0) = gamma_strength;        Gam.slice(0)(3,1) = gamma_strength;        Gam.slice(0)(3,2) = gamma_strength;        Gam.slice(0)(3,3) = gamma_strength;

        Gam.slice(1)(0,0) = gamma_strength;        Gam.slice(1)(0,1) = -gamma_strength;       Gam.slice(1)(0,2) = gamma_strength;        Gam.slice(1)(0,3) = -gamma_strength;
        Gam.slice(1)(1,0) = -gamma_strength;       Gam.slice(1)(1,1) = gamma_strength;        Gam.slice(1)(1,2) = -gamma_strength;       Gam.slice(1)(1,3) = gamma_strength;
        Gam.slice(1)(2,0) = gamma_strength;        Gam.slice(1)(2,1) = -gamma_strength;       Gam.slice(1)(2,2) = gamma_strength;        Gam.slice(1)(2,3) = -gamma_strength;
        Gam.slice(1)(3,0) = -gamma_strength;       Gam.slice(1)(3,1) = gamma_strength;        Gam.slice(1)(3,2) = -gamma_strength;       Gam.slice(1)(3,3) = gamma_strength;
    }
    else if(systype == 7 || systype == 8)
    {
        Gam.fill(gamma_strength);
    }
    else if(systype == 12)
    {
		Gam.slice(0)(0,0) = gamma_strength;
		Gam.slice(0)(1,1) = gamma_strength;
		Gam.slice(0)(2,2) = gamma_strength;
		Gam.slice(0)(3,3) = gamma_strength;

		Gam.slice(1)(nsite-4,nsite-4) = gamma_strength;
		Gam.slice(1)(nsite-3,nsite-3) = gamma_strength;
		Gam.slice(1)(nsite-2,nsite-2) = gamma_strength;
		Gam.slice(1)(nsite-1,nsite-1) = gamma_strength;
    }
    else
    {
        // Coupling in site basis
        cube TT(nsite,nlead,lead_row);
        TT.zeros();
        for(unsigned int j=0; j<nsite; j++)
        {
            for(unsigned int k=0; k<nlead; k++)
            {
                for(unsigned int l=0; l<lead_row; l++)
                {
                    if(nlead == 2) // Two-lead model
                    {
                        if(sc == 0)
                        {
                            if(j==lcontact[l] && k==0) TT(j,k,l) = lambda_strength;     // Coupling to the left
                            if(j==rcontact[l] && k==1) TT(j,k,l) = lambda_strength;     // Coupling to the right
                        }
                        if(sc == 1) // This should be tested
                        {
                            if(j==lcontact[2*l] && k==0) TT(j,k,l) = lambda_strength;     // Coupling to the left
                            if(j==lcontact[2*l+1] && k==0) TT(j,k,l) = -lambda_strength;  // Coupling to the left
                            if(j==rcontact[2*l] && k==1) TT(j,k,l) = lambda_strength;     // Coupling to the right
                            if(j==rcontact[2*l+1] && k==1) TT(j,k,l) = -lambda_strength;  // Coupling to the right
                        }
                    }
                    if(nlead == 3) // Three-lead model
                    {
                        if(sc == 0)
                        {
                            if(j==lcontact[l] && k==0) TT(j,k,l) = lambda_strength;     // Coupling to the left1
                            if(j==lcontact2[l] && k==2) TT(j,k,l) = lambda_strength;    // Coupling to the left2
                            if(j==rcontact[l] && k==1) TT(j,k,l) = lambda_strength;     // Coupling to the right
                        }
                        if(sc == 1) // This should be tested
                        {
                            if(j==lcontact[2*l] && k==0) TT(j,k,l) = lambda_strength;     // Coupling to the left1
                            if(j==lcontact[2*l+1] && k==0) TT(j,k,l) = -lambda_strength;  // Coupling to the left1
                            if(j==lcontact2[2*l] && k==2) TT(j,k,l) = lambda_strength;    // Coupling to the left2
                            if(j==lcontact2[2*l+1] && k==2) TT(j,k,l) = -lambda_strength; // Coupling to the left2
                            if(j==rcontact[2*l] && k==1) TT(j,k,l) = lambda_strength;     // Coupling to the right
                            if(j==rcontact[2*l+1] && k==1) TT(j,k,l) = -lambda_strength;  // Coupling to the right
                        }
                    }
                    if(nlead == 4) // Four-lead model
                    {
                        if(sc == 0)
                        {
                            if(j==lcontact[l] && k==0) TT(j,k,l) = lambda_strength;     // Coupling to the left1
                            if(j==lcontact2[l] && k==2) TT(j,k,l) = lambda_strength;    // Coupling to the left2
                            if(j==rcontact[l] && k==1) TT(j,k,l) = lambda_strength;     // Coupling to the right1
                            if(j==rcontact2[l] && k==3) TT(j,k,l) = lambda_strength;    // Coupling to the right2
                        }
                        if(sc == 1) // This should be tested
                        {
                            if(j==lcontact[2*l] && k==0) TT(j,k,l) = lambda_strength;     // Coupling to the left1
                            if(j==lcontact[2*l+1] && k==0) TT(j,k,l) = -lambda_strength;  // Coupling to the left1
                            if(j==lcontact2[2*l] && k==2) TT(j,k,l) = lambda_strength;    // Coupling to the left2
                            if(j==lcontact2[2*l+1] && k==2) TT(j,k,l) = -lambda_strength; // Coupling to the left2
                            if(j==rcontact[2*l] && k==1) TT(j,k,l) = lambda_strength;     // Coupling to the right1
                            if(j==rcontact[2*l+1] && k==1) TT(j,k,l) = -lambda_strength;  // Coupling to the right1
                            if(j==rcontact2[2*l] && k==3) TT(j,k,l) = lambda_strength;    // Coupling to the right2
                            if(j==rcontact2[2*l+1] && k==3) TT(j,k,l) = -lambda_strength; // Coupling to the right2
                        }
                    }
                }
            }
        }

/*
        // Coupling in eigenbasis
        mat TTT(nsite,nlead);
        TTT.zeros();
        double tempsum;
        for(unsigned int l=0; l<nsite; l++)
        {
            for(unsigned int k=0; k<nlead; k++)
            {
                tempsum = 0.0;
                for(unsigned int m=0; m<nsite; m++)
                {
                    tempsum += TT(m,k)*real(gs_eigvec(m,l));
                }
                TTT(l,k) = tempsum;
            }
        }
*/

        // Level-width function (in site basis)
        double tempsum;
        for(unsigned int j=0; j<nlead; j++)
        {
            for(unsigned int k=0; k<nsite; k++)
            {
                for(unsigned int l=0; l<nsite; l++)
                {
                    tempsum = 0.0;
                    if(sc == 0)
                    {
                        for(unsigned int m=0; m<lead_row; m++)
                        {
                            tempsum += TT(k,j,m)*TT(l,j,m)*2.0/abs(lead_hop);
                        }
                    }
                    if(sc == 1)
                    {
                        if((k < nsite/2 && l < nsite/2) || (k >= nsite/2 && l >= nsite/2))
                        {
                            for(unsigned int m=0; m<lead_row; m++)
                            {
                                tempsum += TT(k,j,m)*TT(l,j,m)*2.0/abs(lead_hop);
                            }
                        }
                    }
                    Gam.slice(j)(k,l) = tempsum;
                }
            }
        }
    }

//    (real(Gam)).print();abort();
	return Gam;
}
