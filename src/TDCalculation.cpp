
#include "Main.hpp"
void CalculateQuantities(int nsite, int nlead, int biastype, vector<int> lcontact, vector<int> rcontact, 
                         vector<int> lcontact2, vector<int> rcontact2, vector<int> bridgeone, 
                         vector<int> bridgetwo, vector<int> bridgethree, int lead_row, int bridge_sites, 
                         double bias_strength, int ntstep, int tgridtype, int sc, int finitet, int besselorder, int NstochMax, double dt, 
                         double lambda_strength, double gamma_strength, double lead_hop, double mu, double beta, cx_mat Hu, 
                         cx_mat Hp, int pert, cx_cube &rho, cx_mat &rho_ss, vec gs_eigval, cx_mat gs_eigvec, 
                         int systype, cx_double &bondcurrent_one_ss, cx_vec &bondcurrent_one, 
                         cx_double &bondcurrent_two_ss, cx_vec &bondcurrent_two, cx_double &bondcurrent_three_ss, 
                         cx_vec &bondcurrent_three, cx_double &supercurrent_one_ss, cx_vec &supercurrent_one, 
                         cx_double &supercurrent_two_ss, cx_vec &supercurrent_two, cx_double &supercurrent_three_ss, 
                         cx_vec &supercurrent_three, cx_mat &pairdensity, cx_mat &supermatrix, double mol_hop, double cutoff,
                         cx_mat &TDCurrent, cx_vec &I_ss, cx_mat &TDCurrentNew, cx_mat &TDBias, vector<double> constpart, vector<double> amplitude, 
                         vector<double> frequency, vector<double> phase, cx_vec &NC, int freq_mult1, int freq_mult2,
                         double ampl_rat, cx_mat &PumpCurrent, cx_double &PumpCurrentLR, cx_double &PumpCurrentLRstoch, cx_double &PumpCurrentLRstochAdiabatic,
                         int partpara, vector<int> calc, cx_vec &dipmom, vector<vector<double> > coords, double Dfluct, double wcorr)
{
	// MPI variables
	int mpirank, mpisize;

	//  Get the number of processes and the individual process ID
    MPI_Comm_size(MPI_COMM_WORLD, &mpisize);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);

    if(mpirank == 0)
    {
        cout << endl;
        cout << "Evaluating LB formulas..." << endl;
    }

// Allocate arrays

    cx_double I(0.0,1.0);

    cx_vec eps_ueff_r(nsite), eps_ueff_l(nsite), eps_peff_r(nsite), eps_peff_l(nsite), rho_bar(nsite),
           rho_cutoff(nsite);

    cx_mat hu_eff(nsite,nsite), hp_eff(nsite,nsite), temp_mat(nsite,nsite), 
           psi_ueff_r(nsite,nsite), psi_ueff_l(nsite,nsite), psi_peff_r(nsite,nsite), 
           psi_peff_l(nsite,nsite), rho_basis_ss(nsite,nsite);

    cx_cube gamma_basis(nsite,nsite,nlead), gamma_basis_rr(nsite,nsite,nlead), gamma_basis_lr(nsite,nsite,nlead), 
            lambda(nsite,nsite,nlead), omega(nsite,nsite,nlead), 
            rho_basis(nsite,nsite,ntstep/mpisize), Vtilde(nsite,nsite,nlead), 
            uVtildep(nsite,nsite,nlead), pVtildeu(nsite,nsite,nlead);

    hu_eff.zeros();
    hp_eff.zeros();
    temp_mat.zeros();
    eps_ueff_l.zeros();
    eps_ueff_r.zeros();
    psi_ueff_l.zeros();
    psi_ueff_r.zeros();
    eps_peff_l.zeros();
    eps_peff_r.zeros();
    psi_peff_l.zeros();
    psi_peff_r.zeros();
    gamma_basis.zeros();
    gamma_basis_rr.zeros();
    gamma_basis_lr.zeros();
    lambda.zeros();
    omega.zeros();
    rho_basis.zeros();
    Vtilde.zeros();
    uVtildep.zeros();
    pVtildeu.zeros();
    rho_basis_ss.zeros();
    rho_bar.zeros();
    rho_cutoff.zeros();

    bondcurrent_one.zeros();
    bondcurrent_two.zeros();
    bondcurrent_three.zeros();
    supercurrent_one.zeros();
    supercurrent_two.zeros();
    supercurrent_three.zeros();
    pairdensity.zeros();
    supermatrix.zeros();
    TDCurrent.zeros();
    I_ss.zeros();
    TDCurrentNew.zeros();
    TDBias.zeros();
    NC.zeros();
    PumpCurrent.zeros();
    PumpCurrentLR = cx_double(0.0,0.0);
    PumpCurrentLRstoch = cx_double(0.0,0.0);
    PumpCurrentLRstochAdiabatic = cx_double(0.0,0.0);
    dipmom.zeros();

    double tmpdble1, tmpdble2;

// Construct h_eff

    for(unsigned int alpha=0; alpha<nlead; alpha++)
    {
        temp_mat += Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,lead_row,lambda_strength,
                          gamma_strength,lead_hop,gs_eigvec,systype,sc).slice(alpha);
        Vtilde.slice(alpha) = V(alpha,biastype,bias_strength)*eye<cx_mat>(nsite,nsite) - (Hp-Hu);
    }

    hu_eff = Hu - 0.5*I*temp_mat;

    if(pert == 0)
    {
        hp_eff = Hu - 0.5*I*temp_mat;
    }
    else if(pert == 1)
    {
        hp_eff = Hp - 0.5*I*temp_mat;
    }
    else{ cout << "Perturbation is not defined in TDCalculation.cpp, exiting ..." << endl; exit(1);}

    if(mpirank == 0) {cout << endl; cout << "Diagonalization" << endl;}
    tmpdble1 = MPI_Wtime();

    eig_gen(eps_ueff_r, psi_ueff_r, hu_eff, 'r');  // h_eff |psi_eff_r> = eps_eff_r |psi_eff_r>
    eig_gen(eps_ueff_l, psi_ueff_l, hu_eff, 'l');  // <psi_eff_l| h_eff = eps_eff_l <psi_eff_l|
    eig_gen(eps_peff_r, psi_peff_r, hp_eff, 'r');  // h_eff |psi_eff_r> = eps_eff_r |psi_eff_r>
    eig_gen(eps_peff_l, psi_peff_l, hp_eff, 'l');  // <psi_eff_l| h_eff = eps_eff_l <psi_eff_l|

    /*
    eig_gen(eps_ueff_l, psi_ueff_l, strans(hu_eff));
    eig_gen(eps_peff_l, psi_peff_l, strans(hp_eff));
    eig_gen(eps_ueff_r, psi_ueff_r, hu_eff);
    eig_gen(eps_peff_r, psi_peff_r, hp_eff);
    */
    tmpdble2 = MPI_Wtime();
    if(mpirank == 0) {StopWatch(tmpdble1, tmpdble2);}

//  eig_gen returns left/right eigenvectors as column vectors of the matrix psi_eff.
//  Eigenvectors are biorthogonal (do not form an orthonormal basis).
//
//  L^dagger R = S => S^-1 L^dagger R = 1 => R^-1 = S^-1 L^dagger
//
//  Inserting a complete set of left/right eigenvectors then assumes dealing with the overlap S!
//  Completeness assumes orthoNORMAL set and left/right eigenvectors are only bi-orthoGONAL.

    cx_mat overlap_uu_lr(nsite,nsite), invover_uu_lr(nsite,nsite), overlap_uu_rl(nsite,nsite), 
           invover_uu_rl(nsite,nsite), overlap_uu_rr(nsite,nsite), overlap_uu_ll(nsite,nsite), 
           overlap_pp_lr(nsite,nsite), invover_pp_lr(nsite,nsite), overlap_pp_rl(nsite,nsite), 
           invover_pp_rl(nsite,nsite), overlap_up_rl(nsite,nsite), overlap_pu_lr(nsite,nsite),
           invpsi_ueff_r(nsite,nsite);

    invpsi_ueff_r = inv(psi_ueff_r);

// Only these overlaps are needed (other cross terms etc do not appear in the basis transformations)

    overlap_uu_lr = trans(psi_ueff_l)*psi_ueff_r;
    overlap_uu_rl = trans(psi_ueff_r)*psi_ueff_l;
    overlap_uu_rr = trans(psi_ueff_r)*psi_ueff_r;
    overlap_uu_ll = trans(psi_ueff_l)*psi_ueff_l;
    overlap_pp_lr = trans(psi_peff_l)*psi_peff_r;
    overlap_pp_rl = trans(psi_peff_r)*psi_peff_l;
    overlap_up_rl = trans(psi_ueff_r)*psi_peff_l;
    overlap_pu_lr = trans(psi_peff_l)*psi_ueff_r;

    invover_uu_lr = inv(overlap_uu_lr);
    invover_uu_rl = inv(overlap_uu_rl);
    invover_pp_lr = inv(overlap_pp_lr);
    invover_pp_rl = inv(overlap_pp_rl);

/*
//  TEST THE LEFT/RIGHT EIGENVECTOR BUSINESS
    cout<<endl;
    cx_mat complete(nsite,nsite);
    complete = inv(overlap_uu_lr)*trans(psi_ueff_l)*psi_ueff_r;
    complete.print();cout<<endl;    // This should always be a unit matrix!
    
    cx_mat testheff(nsite,nsite);
    testheff.zeros();
    cx_mat testheff2(nsite,nsite);
    testheff2.zeros();
    cx_mat testdiag(nsite,nsite);
    testdiag.zeros();
    cx_mat testdiag2(nsite,nsite);
    testdiag2.zeros();
    for(unsigned int j=0; j<nsite; j++)
    {
        testdiag(j,j) = eps_ueff_r(j);
        testdiag2(j,j) = conj(eps_ueff_l(j));
    }

    testheff = psi_ueff_r*testdiag*inv(psi_ueff_r);   // Eigen decomposition
    testheff2 = psi_ueff_l*testdiag2*inv(psi_ueff_l); // Eigen decomposition

    // Decomposition should give back the original h_eff!
    cx_mat moretest(nsite,nsite);
    moretest = hu_eff - testheff;
    moretest.print();cout<<endl;          // Should be zero
    cx_mat moretest2(nsite,nsite);
    moretest2 = trans(hu_eff) - testheff2;
    moretest2.print();cout<<endl;         // Should be zero
*/

//  If there are states in the central region that are eigenfunctions of Gamma with eigenvalue 0,
//  we need to add the contribution from these states to the static density matrix. Later, in
//  the dynamic part, we take these into account (by not calculating anything), see notes.

    // Gamma in the uncontacted basis
    cx_mat gamma_uncontacted(nsite,nsite);
    gamma_uncontacted.zeros();
    for(unsigned int alpha=0; alpha<nlead; alpha++)
    {
        gamma_uncontacted += trans(gs_eigvec)
                            *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,lead_row,lambda_strength,
                                   gamma_strength,lead_hop,gs_eigvec,systype,sc).slice(alpha)
                            *gs_eigvec;
    }

    // Check when Gamma|psi> = 0 and take the unoccupied states
    // Also, specify the cutoff states
    for(unsigned int j=0; j<nsite; j++)
    {
        for(unsigned int k=0; k<nsite; k++)
        {
            // Eigenstates of Gamma|psi> = 0
            if(abs(gamma_uncontacted(k,k)) < 1.0e-14 && gs_eigval(k) < mu)
            {
                if(finitet == 0)
                {
                    rho_bar(j) += gs_eigvec(j,k)*conj(gs_eigvec(j,k));
                }
                else if(finitet == 1)
                {
                    rho_bar(j) += Fermi(beta, gs_eigval(k)-mu)*gs_eigvec(j,k)*conj(gs_eigvec(j,k));
                }
                else{ cout << "Erroneous temperature parameter in TDCalculation.cpp, exiting ..." << endl; exit(1);}
            }

            // Cut-off states
            if(abs(gs_eigval(j)) >= cutoff && abs(gs_eigval(k)) >= cutoff)
            {
                if(gs_eigval(k) < mu)
                {
                    if(finitet == 0)
                    {
                        rho_cutoff(j) += gs_eigvec(j,k)*conj(gs_eigvec(j,k));
                    }
                    else if(finitet == 1)
                    {
                        rho_cutoff(j) += Fermi(beta, gs_eigval(k)-mu)*gs_eigvec(j,k)*conj(gs_eigvec(j,k));
                    }
                    else{ cout << "Erroneous temperature parameter in TDCalculation.cpp, exiting ..." << endl; exit(1);}
                }
            }
        }
    }

//  Spectral functions etc.
	if(mpirank == 0 && calc[5] == 1)
	{
        cout << "Spectral function" << endl;
        tmpdble1 = MPI_Wtime();
        // Write the eigenvalues into file "eff_eval.out" and the spectral function into file "spectral.out"
        double wmaxu(0.0);
        ofstream eff_ueval("output/eff_ueval.out", ios::out);
        eff_ueval <<"#k" << "\t" << "u_epsilon_k" << endl;
        for(unsigned int k=0; k<nsite; k++)
        {
            eff_ueval << setprecision(5) << scientific << k << "\t" << eps_ueff_l(k) << endl;
            // Determine maximum omega for the spectral function already in this loop
            if(real(eps_ueff_l(k)) > wmaxu) wmaxu = real(eps_ueff_l(k));
        }
        wmaxu*=1.5;
        eff_ueval.close();
        double wmaxp(0.0);
        ofstream eff_peval("output/eff_peval.out", ios::out);
        eff_peval <<"#k" << "\t" << "p_epsilon_k" << endl;
        for(unsigned int k=0; k<nsite; k++)
        {
            eff_peval << setprecision(5) << scientific << k << "\t" << eps_peff_l(k) << endl;
            // Determine maximum omega for the spectral function already in this loop
            if(real(eps_peff_l(k)) > wmaxp) wmaxp = real(eps_peff_l(k));
        }
        wmaxp*=1.5;
        eff_peval.close();

    // Find the states close to Fermi energy and write into files
    // "eff_wf_u.out", "eff_norm_u.out", "eff_wf_p.out" and "eff_norm_p.out"
    // (These states correspond to, e.g., the edge state in zGNRs)
        ofstream eff_wf_u("output/eff_wf_u.out", ios::out);
        eff_wf_u <<"#k" << "\t" << "Re[psi_j(k)]" << "\t" << "Im[psi_j(k)]" << endl;
        ofstream eff_norm_u("output/eff_norm_u.out", ios::out);
        eff_norm_u <<"#k" << "\t" << "|psi_j(k)|^2" << endl;
        ofstream eff_wf_p("output/eff_wf_p.out", ios::out);
        eff_wf_p <<"#k" << "\t" << "Re[psi_j(k)]" << "\t" << "Im[psi_j(k)]" << endl;
        ofstream eff_norm_p("output/eff_norm_p.out", ios::out);
        eff_norm_p <<"#k" << "\t" << "|psi_j(k)|^2" << endl;
        for(unsigned int j=0; j<nsite; j++)
        {
            if(j == nsite/2-1 && abs(real(eps_ueff_l(j))) < 1.0e-4) // Around Fermi level
            {
                for(unsigned int k=0; k<nsite; k++)
                {
                    eff_wf_u << setprecision(5) << scientific << k << "\t" 
                             << real(psi_ueff_l.col(j-1)(k)) << "\t" << imag(psi_ueff_l.col(j-1)(k)) << "\t"
                             << real(psi_ueff_l.col(j)(k)) << "\t" << imag(psi_ueff_l.col(j)(k)) << "\t"
                             << real(psi_ueff_l.col(j+1)(k)) << "\t" << imag(psi_ueff_l.col(j+1)(k)) << "\t"
                             << real(psi_ueff_l.col(j+2)(k)) << "\t" << imag(psi_ueff_l.col(j+2)(k)) << "\t"
                             << real(psi_ueff_l.col(j)(k)+psi_ueff_l.col(j+1)(k)) << "\t"
                             << imag(psi_ueff_l.col(j)(k)+psi_ueff_l.col(j+1)(k))
                             << endl;

                    eff_norm_u << setprecision(5) << scientific << k << "\t"
                               << real(psi_ueff_l.col(j-1)(k)*conj(psi_ueff_l.col(j-1)(k))) << "\t"
                               << real(psi_ueff_l.col(j)(k)*conj(psi_ueff_l.col(j)(k))) << "\t"
                               << real(psi_ueff_l.col(j+1)(k)*conj(psi_ueff_l.col(j+1)(k))) << "\t"
                               << real(psi_ueff_l.col(j+2)(k)*conj(psi_ueff_l.col(j+2)(k))) << "\t"
                               << real((psi_ueff_l.col(j)(k)+psi_ueff_l.col(j+1)(k))
                                  *conj(psi_ueff_l.col(j)(k)+psi_ueff_l.col(j+1)(k)))
                               << endl;
                }
            }
            if(j == nsite/2-1 && abs(real(eps_peff_l(j))) < 1.0e-4) // Around Fermi level
            {
                for(unsigned int k=0; k<nsite; k++)
                {
                    eff_wf_p << setprecision(5) << scientific << k << "\t" 
                             << real(psi_peff_l.col(j-1)(k)) << "\t" << imag(psi_peff_l.col(j-1)(k)) << "\t"
                             << real(psi_peff_l.col(j)(k)) << "\t" << imag(psi_peff_l.col(j)(k)) << "\t"
                             << real(psi_peff_l.col(j+1)(k)) << "\t" << imag(psi_peff_l.col(j+1)(k)) << "\t"
                             << real(psi_peff_l.col(j+2)(k)) << "\t" << imag(psi_peff_l.col(j+2)(k)) << "\t"
                             << real(psi_peff_l.col(j)(k)+psi_peff_l.col(j+1)(k)) << "\t"
                             << imag(psi_peff_l.col(j)(k)+psi_peff_l.col(j+1)(k))
                             << endl;

                    eff_norm_p << setprecision(5) << scientific << k << "\t"
                               << real(psi_peff_l.col(j-1)(k)*conj(psi_peff_l.col(j-1)(k))) << "\t"
                               << real(psi_peff_l.col(j)(k)*conj(psi_peff_l.col(j)(k))) << "\t"
                               << real(psi_peff_l.col(j+1)(k)*conj(psi_peff_l.col(j+1)(k))) << "\t"
                               << real(psi_peff_l.col(j+2)(k)*conj(psi_peff_l.col(j+2)(k))) << "\t"
                               << real((psi_peff_l.col(j)(k)+psi_peff_l.col(j+1)(k))
                                  *conj(psi_peff_l.col(j)(k)+psi_peff_l.col(j+1)(k)))
                               << endl;
                }
            }
        }
        eff_wf_u.close();
        eff_norm_u.close();
        eff_wf_p.close();
        eff_norm_p.close();

		if(systype==12)
		{
			cx_mat majostate(nsite,2), tempstate(nsite,2);
			majostate.zeros();
			tempstate.zeros();
			int count=0;
			for(unsigned int j=0; j<nsite; j++)
			{
				if(abs(real(eps_peff_l(j))) < 1.0e-5)
				{
					cout << "found majostate " << j << endl;
					majostate.col(count) = psi_peff_l.col(j);
					count++;
				}
			}

			tempstate = majostate;
			majostate.zeros();
			majostate.col(0) = (tempstate.col(0)+tempstate.col(1))/sqrt(2.0);
			majostate.col(1) = (tempstate.col(0)-tempstate.col(1))/sqrt(2.0);

			ofstream majo("output/majostate.out", ios::out);
			majo <<"#k" << "\t" << "|psi(k)|^2" << endl;
			for(unsigned int k=0; k<nsite; k++)
			{
				majo << setprecision(10) << scientific << k << "\t"
					 << real(majostate.col(0)(k)*conj(majostate.col(0)(k))) << "\t"
					 << real(majostate.col(1)(k)*conj(majostate.col(1)(k))) << "\t"
					 << real(tempstate.col(0)(k)*conj(tempstate.col(0)(k))) << "\t"
					 << real(tempstate.col(1)(k)*conj(tempstate.col(1)(k))) << endl;
			}
			majo.close();

			double exponent=0.0;
			exponent = norm(trans(majostate.col(0))*temp_mat*majostate.col(0))
					 + norm(trans(majostate.col(1))*temp_mat*majostate.col(1));
			cout << scientific << setprecision(10) << "Majorana exponent = " << exponent/2.0 << endl;
		}

        int spacing; // Omega grid density for the spectral function (adaptive to the time steps)
        if(ntstep < 100) spacing = 10000;
        else if(ntstep < 1000) spacing = 1000;
        else spacing = 100;
        cx_double one(1.0,0.0);

        cx_double wu, dwu;
        if(gamma_strength < 0.01)
        {
            wu = cx_double(-wmaxu-double(spacing)*gamma_strength,0.0);
            dwu = cx_double((2.0*(wmaxu+double(spacing)*gamma_strength)/ntstep)/double(spacing),0.0);
        }
        else if(gamma_strength < 0.1)
        {
            wu = cx_double(-wmaxu-double(spacing)*gamma_strength/10.0,0.0);
            dwu = cx_double((2.0*(wmaxu+double(spacing)*gamma_strength/10.0)/ntstep)/double(spacing),0.0);
        }        
        else
        {
            wu = cx_double(-wmaxu-double(spacing)*gamma_strength/100.0,0.0);
            dwu = cx_double((2.0*(wmaxu+double(spacing)*gamma_strength/100.0)/ntstep)/double(spacing),0.0);
        }

        cx_double tmpcdbl1(0.0,0.0);
        double tmpsum1(0.0);
        ofstream spectral1("output/spectral_eq.out", ios::out);
        spectral1 <<"#w" << "\t\t" << "A_eq(w)" << endl;
        ofstream spectral_spat("output/spectral_spat.out", ios::out);
        spectral_spat <<"#w" << "\t\t" << "A_spat(w)" << endl;

        for(unsigned int i=0; i<spacing*ntstep; i++)
        {
            spectral_spat << setprecision(5) << scientific << real(wu);
            for(unsigned int j=0; j<nsite; j++)
            {
                tmpcdbl1 += (one/(wu-eps_ueff_l(j)))*cdot(psi_ueff_l.col(j),psi_ueff_l.col(j));
                spectral_spat << "\t"
                              << (-1.0/datum::pi)
                                *imag((one/(wu-eps_ueff_l(j)))*cdot(psi_ueff_l.col(j),psi_ueff_l.col(j)));
            }
            spectral_spat << endl;
            spectral1 << setprecision(5) << scientific << real(wu) << "\t"
                      << (-1.0/datum::pi)*imag(tmpcdbl1) << endl;
            tmpsum1 += (-1.0/datum::pi)*imag(tmpcdbl1)*real(dwu);
            tmpcdbl1 = cx_double(0.0,0.0);
            wu += dwu;
        }
        spectral1.close();
        spectral_spat.close();
        cout << "Integral A_eq(w) dw = " << tmpsum1 << " (Normalized: "
             << tmpsum1/nsite << ")" << endl;

        cx_double wp, dwp;
        if(gamma_strength < 0.01)
        {
            wp = cx_double(-wmaxp-double(spacing)*gamma_strength,0.0);
            dwp = cx_double((2.0*(wmaxp+double(spacing)*gamma_strength)/ntstep)/double(spacing),0.0);
        }
        else if(gamma_strength < 0.1)
        {
            wp = cx_double(-wmaxp-double(spacing)*gamma_strength/10.0,0.0);
            dwp = cx_double((2.0*(wmaxp+double(spacing)*gamma_strength/10.0)/ntstep)/double(spacing),0.0);
        }
        else
        {
            wp = cx_double(-wmaxp-double(spacing)*gamma_strength/100.0,0.0);
            dwp = cx_double((2.0*(wmaxp+double(spacing)*gamma_strength/100.0)/ntstep)/double(spacing),0.0);
        }

        cx_double tmpcdbl2(0.0,0.0);
        double tmpsum2(0.0);
        ofstream spectral2("output/spectral_neq.out", ios::out);
        spectral2 <<"#w" << "\t\t" << "A_neq(w)" << endl;
        for(unsigned int i=0; i<spacing*ntstep; i++)
        {
            for(unsigned int j=0; j<nsite; j++)
            {
                tmpcdbl2 += (one/(wp-eps_peff_l(j)))*cdot(psi_peff_l.col(j),psi_peff_l.col(j));
            }
            spectral2 << setprecision(5) << scientific << real(wp) << "\t"
                      << (-1.0/datum::pi)*imag(tmpcdbl2) << endl;
            tmpsum2 += (-1.0/datum::pi)*imag(tmpcdbl2)*real(dwp);
            tmpcdbl2 = cx_double(0.0,0.0);
            wp += dwp;
        }
        spectral2.close();
        cout << "Integral A_neq(w) dw = " << tmpsum2 << " (Normalized: "
             << tmpsum2/nsite << ")" << endl;
	/*
// Transmission
        cx_double wt, dwt;
        if(sc == 0)
        {
            if(gamma_strength < 0.01)
            {
                wt = cx_double(-wmaxu-double(spacing)*gamma_strength,0.0);
                dwt = cx_double((2.0*(wmaxu+double(spacing)*gamma_strength)/ntstep)/double(spacing),0.0);
            }
            else if(gamma_strength < 0.1)
            {
                wt = cx_double(-wmaxu-double(spacing)*gamma_strength/10.0,0.0);
                dwt = cx_double((2.0*(wmaxu+double(spacing)*gamma_strength/10.0)/ntstep)/double(spacing),0.0);
            }        
            else
            {
                wt = cx_double(-wmaxu-double(spacing)*gamma_strength/100.0,0.0);
                dwt = cx_double((2.0*(wmaxu+double(spacing)*gamma_strength/100.0)/ntstep)/double(spacing),0.0);
            }
            cx_mat tempcxmat(nsite,nsite);
            tempcxmat.zeros();
            ofstream transmission("output/transmission.out", ios::out);
            transmission <<"#w" << "\t\t" << "T_{alpha beta}(w)" << endl;
            for(unsigned int i=0; i<spacing*ntstep; i++)
            {
                transmission << setprecision(5) << scientific << real(wt) << "\t";
                for(unsigned int alpha=0; alpha<nlead; alpha++)
                {
                    for(unsigned int bet=0; bet<nlead; bet++)
                    {
                        if(bet != alpha)
                        {
    // Eigen decomposition for the transmission function (less expensive computationally)
                            for(unsigned int j=0; j<nsite; j++)
                            {
                                tempcxmat(j,j) = cx_double(1.0,0.0)/(wt - eps_ueff_r(j));
                            }
                            transmission << real(trace(
                                            Gamma(nsite,nlead,lcontact,
                                                  rcontact,lcontact2,
                                                  rcontact2,lead_row,
                                                  lambda_strength,
                                                  gamma_strength,
                                                  lead_hop,gs_eigvec,
                                                  systype,sc).slice(alpha)
                                           *psi_ueff_r*diagmat(tempcxmat)*invpsi_ueff_r
                                           *Gamma(nsite,nlead,lcontact,
                                                  rcontact,lcontact2,
                                                  rcontact2,lead_row,
                                                  lambda_strength,
                                                  gamma_strength,
                                                  lead_hop,gs_eigvec,
                                                  systype,sc).slice(bet)
                                           *trans(psi_ueff_r*diagmat(tempcxmat)*invpsi_ueff_r)
                                             )) << "\t";

                        }
                    }
                }
                transmission << endl;
                wt += dwt;
            }
            transmission.close();
        }

        if(sc == 1)
        {
            if(gamma_strength < 0.01)
            {
                wt = cx_double(-wmaxu-double(spacing)*gamma_strength,0.0);
                dwt = cx_double((2.0*(wmaxu+double(spacing)*gamma_strength)/ntstep)/double(spacing),0.0);
            }
            else if(gamma_strength < 0.1)
            {
                wt = cx_double(-wmaxu-double(spacing)*gamma_strength/10.0,0.0);
                dwt = cx_double((2.0*(wmaxu+double(spacing)*gamma_strength/10.0)/ntstep)/double(spacing),0.0);
            }        
            else
            {
                wt = cx_double(-wmaxu-double(spacing)*gamma_strength/100.0,0.0);
                dwt = cx_double((2.0*(wmaxu+double(spacing)*gamma_strength/100.0)/ntstep)/double(spacing),0.0);
            }
            cx_mat tempcxmat(nsite,nsite), GR(nsite,nsite), GA(nsite,nsite);
            tempcxmat.zeros(); GR.zeros(); GA.zeros();
            ofstream normtrans("output/normal_transmission.out", ios::out);
            normtrans <<"#w" << "\t\t" << "T_{alpha beta}(w)" << endl;
            ofstream andrrefl("output/andreev_reflection.out", ios::out);
            andrrefl <<"#w" << "\t\t" << "T_{alpha beta}(w)" << endl;
            for(unsigned int i=0; i<spacing*ntstep; i++)
            {
                normtrans << setprecision(5) << scientific << real(wt) << "\t";
                andrrefl << setprecision(5) << scientific << real(wt) << "\t";
                for(unsigned int alpha=0; alpha<nlead; alpha++)
                {
                    for(unsigned int bet=0; bet<nlead; bet++)
                    {
                        //if(bet != alpha)
                        {
                            for(unsigned int j=0; j<nsite; j++)
                            {
                                tempcxmat(j,j) = cx_double(1.0,0.0)/(wt - eps_ueff_r(j));
                            }
                            GR = psi_ueff_r*diagmat(tempcxmat)*invpsi_ueff_r;
                            GA = trans((psi_ueff_r*diagmat(tempcxmat)*invpsi_ueff_r));
                            normtrans << real(trace(
                                         Gamma(nsite,nlead,lcontact,
                                               rcontact,lcontact2,
                                               rcontact2,lead_row,
                                               lambda_strength,
                                               gamma_strength,
                                               lead_hop,gs_eigvec,
                                               systype,sc).slice(alpha).submat(0,0,nsite/2-1,nsite/2-1)
                                        *GR.submat(0,0,nsite/2-1,nsite/2-1)
                                        *Gamma(nsite,nlead,lcontact,
                                               rcontact,lcontact2,
                                               rcontact2,lead_row,
                                               lambda_strength,
                                               gamma_strength,
                                               lead_hop,gs_eigvec,
                                               systype,sc).slice(bet).submat(0,0,nsite/2-1,nsite/2-1)
                                        *GA.submat(0,0,nsite/2-1,nsite/2-1)
                                          )) << "\t";

                            andrrefl << real(trace(
                                        Gamma(nsite,nlead,lcontact,
                                              rcontact,lcontact2,
                                              rcontact2,lead_row,
                                              lambda_strength,
                                              gamma_strength,
                                              lead_hop,gs_eigvec,
                                              systype,sc).slice(alpha).submat(0,0,nsite/2-1,nsite/2-1)
                                       *GR.submat(0,nsite/2,nsite/2-1,nsite-1)
                                       *Gamma(nsite,nlead,lcontact,
                                              rcontact,lcontact2,
                                              rcontact2,lead_row,
                                              lambda_strength,
                                              gamma_strength,
                                              lead_hop,gs_eigvec,
                                              systype,sc).slice(bet).submat(nsite/2,nsite/2,nsite-1,nsite-1)
                                       *GA.submat(nsite/2,0,nsite-1,nsite/2-1)
                                         )) << "\t";
                        }
                    }
                }
                normtrans << endl;
                andrrefl << endl;
                wt += dwt;
            }
            normtrans.close();
            andrrefl.close();
        }
	*/
        tmpdble2 = MPI_Wtime();
        StopWatch(tmpdble1, tmpdble2);
	}

    // Calculation of the actual time-dependent density matrix starts here
    // (although first for the time-independent terms)

    cx_double tmpcdble(0.0,0.0);
    cx_double tmpcdble2(0.0,0.0);
    cx_double tmpcdble3(0.0,0.0);
    cx_double tmpcdble4(0.0,0.0);
    cx_double tmpcdble5(0.0,0.0);
    cx_double tmpcdble51(0.0,0.0);
    cx_double tmpcdble6(0.0,0.0);
    cx_double tmpcdble7(0.0,0.0);
    cx_double tmpcdble8(0.0,0.0);
    cx_double tmpcdble9(0.0,0.0);
    cx_double tmpcdbleA(0.0,0.0);
    cx_double tmpcdbleB(0.0,0.0);
    cx_double tmpcdbleC(0.0,0.0);

    cx_mat eye_nambu(nsite,nsite);
    eye_nambu.zeros();
    /*
    for(int j=0; j<nsite; j++)
      {
	for(int k=0; k<nsite; k++)
	  {
	    if(j%4==0 && k%4==0 && abs(j-k)<=4)
	      {
		eye_nambu(j,j) = 1.0;
		eye_nambu(j+1,j+1) = -1.0;
		eye_nambu(j+2,j+2) = 1.0;
		eye_nambu(j+3,j+3) = -1.0;
	      }
	  }
      }
    */
    eye_nambu = eye<cx_mat>(nsite,nsite);
    if(mpirank == 0) {cout << endl; cout << "Time-independent terms" << endl;}
    tmpdble1 = MPI_Wtime();
    for(unsigned int alpha=0; alpha<nlead; alpha++)
    {
        if(pert == 0)
        {
            gamma_basis.slice(alpha) = trans(psi_ueff_l)*eye_nambu
                                      *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,lead_row,lambda_strength,
                                             gamma_strength,lead_hop,gs_eigvec,systype,sc).slice(alpha)
                                      *psi_ueff_l;

            gamma_basis_rr.slice(alpha) = trans(psi_ueff_r)
                                         *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,lead_row,lambda_strength,
                                                gamma_strength,lead_hop,gs_eigvec,systype,sc).slice(alpha)
                                         *psi_ueff_r;

            gamma_basis_lr.slice(alpha) = trans(psi_ueff_l)
                                         *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,lead_row,lambda_strength,
                                                gamma_strength,lead_hop,gs_eigvec,systype,sc).slice(alpha)
                                         *psi_ueff_r;
        }
        if(pert == 1)
        {
            gamma_basis.slice(alpha) = trans(psi_peff_l)
                                      *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,lead_row,lambda_strength,
                                             gamma_strength,lead_hop,gs_eigvec,systype,sc).slice(alpha)
                                      *psi_peff_l;
            uVtildep.slice(alpha) = trans(psi_ueff_l)*Vtilde.slice(alpha)*psi_peff_r;
            pVtildeu.slice(alpha) = trans(psi_peff_r)*trans(Vtilde.slice(alpha))*psi_ueff_l;
        }

        if(calc[0] == 1)    // Calculate TD 1RDM?
        {
            for(unsigned int j=0; j<nsite; j++)
            {
                for(unsigned int k=0; k<=j; k++)    // Density matrix is hermitian, so due to
                {                                   // symmetry the inner loop may be optimized
                    if( (abs(real(eps_ueff_l(j))) < cutoff && abs(real(eps_ueff_l(k))) < cutoff) ||
                        (abs(real(eps_ueff_l(j))) >= cutoff && abs(real(eps_ueff_l(k))) < cutoff) ||
                        (abs(real(eps_ueff_l(j))) < cutoff && abs(real(eps_ueff_l(k))) >= cutoff) )
                    {
                        if(pert == 0)
                        {
                            ParamsL paral;
                            paral.j = j;
                            paral.k = k;
                            paral.alpha = alpha;
                            paral.eps_ueff_l = eps_ueff_l;
                            paral.eps_peff_l = eps_peff_l;
                            paral.mu = mu;
                            paral.biastype = biastype;
                            paral.pert = pert;
                            paral.bias_strength = bias_strength;
                            paral.finitet = finitet;
                            paral.beta = beta;

                            ParamsO parao;
                            parao.j = j;
                            parao.k = k;
                            parao.m = 0;
                            parao.n = 0;
                            parao.p = 0;
                            parao.q = 0;
                            parao.alpha = alpha;
                            parao.eps_ueff_l = eps_ueff_l;
                            parao.eps_peff_l = eps_peff_l;
                            parao.mu = mu;
                            parao.biastype = biastype;
                            parao.pert = pert;
                            parao.bias_strength = bias_strength;
                            parao.finitet = finitet;
                            parao.beta = beta;

                            if(abs(gamma_basis.slice(alpha)(j,k)) > 1.0e-15)    // See notes.pdf
                            {
                                tmpcdble = LambdaCalculate(reinterpret_cast<void *>(&paral));
                                tmpcdble2 = OmegaCalculate(reinterpret_cast<void *>(&parao));
                            }
                            else
                            {
                                tmpcdble = cx_double(0.0,0.0);
                                tmpcdble2 = cx_double(0.0,0.0);
                            }
                            if(isnan(tmpcdble) == true || isinf(tmpcdble) == true)  // Check divergences
                            {
                                if(mpirank == 0)
                                {
                                    cout << "Error in Lambda!" << endl;
                                }
                            }

                            if(isnan(tmpcdble2) == true || isinf(tmpcdble2) == true)  // Check divergences
                            {
                                if(mpirank == 0)
                                {
                                    cout << "Error in Omega!" << endl;
                                }
                            }
                            
                            lambda(j,k,alpha) = tmpcdble;
                            lambda(k,j,alpha) = conj(tmpcdble);
                            tmpcdble = cx_double(0.0,0.0);

                            omega(j,k,alpha) = tmpcdble2;
                            omega(k,j,alpha) = conj(tmpcdble2);
                            tmpcdble2 = cx_double(0.0,0.0);
                        }   // if pert==0

                        if(pert == 1)
                        {
                            ParamsL paral;
                            paral.j = j;
                            paral.k = k;
                            paral.alpha = alpha;
                            paral.eps_ueff_l = eps_ueff_l;
                            paral.eps_peff_l = eps_peff_l;
                            paral.mu = mu;
                            paral.biastype = biastype;
                            paral.pert = pert;
                            paral.bias_strength = bias_strength;
                            paral.finitet = finitet;
                            paral.beta = beta;

                            if(abs(gamma_basis.slice(alpha)(j,k)) > 1.0e-15)    // See notes.pdf
                            {
                                tmpcdble = LambdaCalculate(reinterpret_cast<void *>(&paral));
                            }
                            else
                            {
                                tmpcdble = cx_double(0.0,0.0);
                            }
                            if(isnan(tmpcdble) == true || isinf(tmpcdble) == true)  // Check divergences
                            {
                                if(mpirank == 0)
                                {
                                    cout << "Error in Lambda!" << endl;
                                }
                            }

                            lambda(j,k,alpha) = tmpcdble;
                            lambda(k,j,alpha) = conj(tmpcdble);
                            tmpcdble = cx_double(0.0,0.0);
                        }   // if pert==1
                    }   // cutoffs
                }   // k<=j
            }   // j<nsite
        }   // if calc 1rdm
    }   // alpha<nlead

    tmpdble2 = MPI_Wtime();
    if(mpirank == 0) {StopWatch(tmpdble1, tmpdble2); cout << endl;}

    // Time-dependent terms are calculated now
    
    cx_double temp1, temp2;         // Temp variables for parallel calculation

/*
 * This optimization trick includes only the zeroth mode Bessel 
 * functions when the amplitude of the second harmonic is zero.
 */
    int smin, smax;
    if(ampl_rat == 0.0)
    {
        smin = 0;
        smax = 0;
    }
    else
    {
        smin = -besselorder;
        smax = besselorder;
    }

    // Divide the steps according to the number of processes
    unsigned int elems = ntstep/mpisize;

    if(mpirank == 0) {cout << endl; cout << "Time-dependent terms" << endl;}
    tmpdble1 = MPI_Wtime();
	for(unsigned int i=0; i<elems; i++)                 // Time loop
    {
        unsigned int l = elems*mpirank + i;                // The for-loop (i) runs up to divided steps,
                                                        // index l accounts for individual steps
        if(calc[0] == 1)
        {
            for(unsigned int j=0; j<nsite; j++)
            {
                for(unsigned int k=0; k<=j; k++)    // Symmetry
                {
                    if( (abs(real(eps_ueff_l(j))) < cutoff && abs(real(eps_ueff_l(k))) < cutoff) ||
                        (abs(real(eps_ueff_l(j))) >= cutoff && abs(real(eps_ueff_l(k))) < cutoff) ||
                        (abs(real(eps_ueff_l(j))) < cutoff && abs(real(eps_ueff_l(k))) >= cutoff) )
                    {
                        temp1 = cx_double(0.0,0.0);
                        temp2 = cx_double(0.0,0.0);

                        for(unsigned int alpha=0; alpha<nlead; alpha++)
                        {
                            if(pert == 0)
                            {
                                ParamsP parap1;
                                parap1.j = j;
                                parap1.k = k;
                                parap1.m = 0;
                                parap1.n = 0;
                                parap1.alpha = alpha;
                                parap1.eps_ueff_l = eps_ueff_l;
                                parap1.eps_peff_l = eps_peff_l;
                                parap1.mu = mu;
                                parap1.biastype = biastype;
                                parap1.pert = pert;
                                parap1.bias_strength = bias_strength;
                                parap1.finitet = finitet;
                                parap1.beta = beta;

                                ParamsP parap2;
                                parap2.j = k;
                                parap2.k = j;
                                parap2.m = 0;
                                parap2.n = 0;
                                parap2.alpha = alpha;
                                parap2.eps_ueff_l = eps_ueff_l;
                                parap2.eps_peff_l = eps_peff_l;
                                parap2.mu = mu;
                                parap2.biastype = biastype;
                                parap2.pert = pert;
                                parap2.bias_strength = bias_strength;
                                parap2.finitet = finitet;
                                parap2.beta = beta;

                                if(abs(gamma_basis.slice(alpha)(j,k)) > 1.0e-15)    // See notes.pdf
                                {
                                    tmpcdble = PiCalculate(Time(ntstep, tgridtype, dt, l),reinterpret_cast<void *>(&parap1));
                                    tmpcdble2 = PiCalculate(Time(ntstep, tgridtype, dt, l),reinterpret_cast<void *>(&parap2));
                                }
                                else
                                {
                                    tmpcdble = cx_double(0.0,0.0);
                                    tmpcdble2 = cx_double(0.0,0.0);
                                }
                                if(isnan(tmpcdble) == true || isinf(tmpcdble) == true)  // Check divergences
                                {
                                    if(mpirank == 0)
                                    {
                                        cout << "Error in Pi!" << endl;
                                    }
                                }

                                temp1 += gamma_basis(j,k,alpha)*lambda(j,k,alpha)
									   + V(alpha,biastype,bias_strength)*gamma_basis(j,k,alpha)*tmpcdble
									   + V(alpha,biastype,bias_strength)*gamma_basis(j,k,alpha)*conj(tmpcdble2)
									   + pow(V(alpha,biastype,bias_strength),2.0)*gamma_basis(j,k,alpha)
									    *exp(-I*(eps_ueff_l(j)-conj(eps_ueff_l(k)))*Time(ntstep, tgridtype, dt, l))*omega(j,k,alpha);

                                if(l == 0)
                                {
									temp2 += gamma_basis(j,k,alpha)*lambda(j,k,alpha);
                                }
                            }   // if pert==0

                            if(pert == 1)
                            {
                                for(unsigned int m=0; m<nsite; m++)
                                {
                                    for(unsigned int n=0; n<nsite; n++)
                                    {
// Cut-off hack
										if( (abs(real(eps_ueff_l(m))) < cutoff && abs(real(eps_ueff_l(n))) < cutoff) ||
											(abs(real(eps_ueff_l(m))) >= cutoff && abs(real(eps_ueff_l(n))) < cutoff) ||
											(abs(real(eps_ueff_l(m))) < cutoff && abs(real(eps_ueff_l(n))) >= cutoff) )
										{
											ParamsP parap1;
											parap1.j = j;
											parap1.k = k;
											parap1.m = m;
											parap1.n = n;
											parap1.alpha = alpha;
											parap1.eps_ueff_l = eps_ueff_l;
											parap1.eps_peff_l = eps_peff_l;
											parap1.mu = mu;
											parap1.biastype = biastype;
											parap1.pert = pert;
											parap1.bias_strength = bias_strength;
											parap1.finitet = finitet;
											parap1.beta = beta;

											ParamsP parap2;
											parap2.j = k;
											parap2.k = j;
											parap2.m = m;
											parap2.n = n;
											parap2.alpha = alpha;
											parap2.eps_ueff_l = eps_ueff_l;
											parap2.eps_peff_l = eps_peff_l;
											parap2.mu = mu;
											parap2.biastype = biastype;
											parap2.pert = pert;
											parap2.bias_strength = bias_strength;
											parap2.finitet = finitet;
											parap2.beta = beta;

											if(abs(gamma_basis.slice(alpha)(n,k)) > 1.0e-15)    // See notes.pdf
											{
												tmpcdble += overlap_pu_lr(j,m)*invover_uu_lr(m,m)
														   *uVtildep.slice(alpha)(m,n)*invover_pp_lr(n,n)
														   *gamma_basis.slice(alpha)(n,k)
														   *PiCalculate(Time(ntstep, tgridtype, dt, l),reinterpret_cast<void *>(&parap1));

												tmpcdble2 += overlap_pu_lr(k,m)*invover_uu_lr(m,m)
															*uVtildep.slice(alpha)(m,n)*invover_pp_lr(n,n)
															*gamma_basis.slice(alpha)(n,j)
															*PiCalculate(Time(ntstep, tgridtype, dt, l),reinterpret_cast<void *>(&parap2));
											}
											else
											{
												tmpcdble += cx_double(0.0,0.0);
												tmpcdble2 += cx_double(0.0,0.0);
											}

											for(unsigned int p=0; p<nsite; p++)
											{
												for(unsigned int q=0; q<nsite; q++)
												{
// Cut-off hack
													if( (abs(real(eps_ueff_l(p))) < cutoff && abs(real(eps_ueff_l(q))) < cutoff) ||
														(abs(real(eps_ueff_l(p))) >= cutoff && abs(real(eps_ueff_l(q))) < cutoff) ||
														(abs(real(eps_ueff_l(p))) < cutoff && abs(real(eps_ueff_l(q))) >= cutoff) )
													{
														ParamsO parao1;
														parao1.j = j;
														parao1.k = k;
														parao1.m = m;
														parao1.n = n;
														parao1.p = p;
														parao1.q = q;
														parao1.alpha = alpha;
														parao1.eps_ueff_l = eps_ueff_l;
														parao1.eps_peff_l = eps_peff_l;
														parao1.mu = mu;
														parao1.biastype = biastype;
														parao1.pert = pert;
														parao1.bias_strength = bias_strength;
														parao1.finitet = finitet;
														parao1.beta = beta;

														ParamsO parao2;
														parao2.j = k;
														parao2.k = j;
														parao2.m = m;
														parao2.n = n;
														parao2.p = p;
														parao2.q = q;
														parao2.alpha = alpha;
														parao2.eps_ueff_l = eps_ueff_l;
														parao2.eps_peff_l = eps_peff_l;
														parao2.mu = mu;
														parao2.biastype = biastype;
														parao2.pert = pert;
														parao2.bias_strength = bias_strength;
														parao2.finitet = finitet;
														parao2.beta = beta;

														if(abs(gamma_basis.slice(alpha)(n,p)) > 1.0e-15)    // See notes.pdf
														{
															tmpcdble3 += overlap_pu_lr(j,m)*invover_uu_lr(m,m)
																		*uVtildep.slice(alpha)(m,n)*invover_pp_lr(n,n)
																		*gamma_basis.slice(alpha)(n,p)
																		*pVtildeu.slice(alpha)(p,q)*invover_pp_rl(p,p)
																		*overlap_up_rl(q,k)*invover_uu_rl(q,q)
																		*exp(-I*(eps_peff_l(j)-conj(eps_peff_l(k)))*Time(ntstep, tgridtype, dt, l))
																		*OmegaCalculate(reinterpret_cast<void *>(&parao1));

															tmpcdble4 += overlap_pu_lr(k,m)*invover_uu_lr(m,m)
																		*uVtildep.slice(alpha)(m,n)*invover_pp_lr(n,n)
																		*gamma_basis.slice(alpha)(n,p)
																		*pVtildeu.slice(alpha)(p,q)*invover_pp_rl(p,p)
																		*overlap_up_rl(q,j)*invover_uu_rl(q,q)
																		*exp(-I*(eps_peff_l(k)-conj(eps_peff_l(j)))*Time(ntstep, tgridtype, dt, l))
																		*OmegaCalculate(reinterpret_cast<void *>(&parao2));
														}
														else
														{
															tmpcdble3 += cx_double(0.0,0.0);
															tmpcdble4 += cx_double(0.0,0.0);
														}
													}
												} // q<nsite
											} // p<nsite
										}
                                    } // n<nsite
                                } // m<nsite

                                if( isnan(tmpcdble) == true || isinf(tmpcdble) == true
                                   || isnan(tmpcdble2) == true || isinf(tmpcdble2) == true )  // Check divergences
                                {
                                    if(mpirank == 0)
                                    {
                                        cout << "Error in Pi!" << endl;
                                    }
                                }

                                if( isnan(tmpcdble3) == true || isinf(tmpcdble3) == true
                                   || isnan(tmpcdble4) == true || isinf(tmpcdble4) == true )  // Check divergences
                                {
									if(mpirank == 0)
                                    {
                                        cout << "Error in Omega!" << endl;
                                    }
                                }

                                temp1 += gamma_basis(j,k,alpha)*lambda(j,k,alpha)
                                       + tmpcdble + conj(tmpcdble2)
                                       + 0.5*(tmpcdble3 + conj(tmpcdble4));

                                if(l == 0)
                                {
                                    temp2 += gamma_basis(j,k,alpha)*lambda(j,k,alpha);
                                }
                            }   // if pert==1

                            tmpcdble = cx_double(0.0,0.0);
                            tmpcdble2 = cx_double(0.0,0.0);
                            tmpcdble3 = cx_double(0.0,0.0);
                            tmpcdble4 = cx_double(0.0,0.0);
                        }

                        if(isnan(temp1) == true || isinf(temp1) == true)    // Check divergences
                        {
                            if(mpirank == 0)
                            {
                                cout << "Error in rho!" << endl;
                            }
                        }

                        if(isnan(temp2) == true || isinf(temp2) == true)    // Check divergences
                        {
                            if(mpirank == 0)
                            {
                                cout << "Error in rho_ss!" << endl;
                            }
                        }

                        rho_basis(j,k,i) = temp1;
                        rho_basis(k,j,i) = conj(temp1);
                        
                        if(l == 0)
                        {
                            rho_basis_ss(j,k) = temp2;
                            rho_basis_ss(k,j) = conj(temp2);
                        }

                    }   // cutoffs
                }   // k<=j
            }   // j<nsite
        }   // if calc 1rdm

// CURRENT CALCULATION (inside the time loop)
        if(calc[1] == 1)
        {
            // Loop over the lead alpha to calculate I_alpha(t)
            for(unsigned int alpha=0; alpha<nlead; alpha++)
            {
                // Internal sum over lead 'be' (symbol 'beta' is reserved for the inverse temperature)
                for(unsigned int be=0; be<nlead; be++)
                {
                    // Internal sum over the central region index j
                    for(unsigned int j=0; j<nsite; j++)
                    {
                        // Internal sum over the central region index k
                        for(unsigned int k=0; k<nsite; k++)
                        {
                            // Internal sum over the Bessel function index r
                            for(int r=-besselorder; r<=besselorder; r++)
                            {
                                // Internal sum over the Bessel function index s
                                for(int s=-besselorder; s<=besselorder; s++)
                                {
                                    // Parameters defined in one bunch (to be sent to the evaluating function)
                                    ParamsTDCurr paratdcurr;
                                    paratdcurr.j = j;
                                    paratdcurr.k = k;
                                    paratdcurr.r = r;
                                    paratdcurr.s = s;
                                    paratdcurr.alpha = alpha;
                                    paratdcurr.be = be;
                                    paratdcurr.mu = mu;
                                    paratdcurr.beta = beta;
                                    paratdcurr.constpart = constpart;
                                    paratdcurr.amplitude = amplitude;
                                    paratdcurr.frequency = frequency;
                                    paratdcurr.phase = phase;
                                    paratdcurr.eps_ueff_l = eps_ueff_l;

                                    // The first term excludes sums over k, be and s
                                    if(k == 0 && be == 0 && s == -besselorder)
                                    {
										tmpcdble5 += (2.0/datum::pi)
                                                    *real(gamma_basis_lr.slice(alpha)(j,j)
                                                    *invover_uu_lr(j,j)
                                                    *FirstTerm(Time(ntstep, tgridtype, dt, l),
                                                               reinterpret_cast<void *>(&paratdcurr)));
                                    } // if k=0, be=0, s=-besselorder

                                    // The second term excludes sums over r and s
                                    if(r == -besselorder && s == -besselorder)
                                    {
                                        tmpcdble6 += (-1.0/datum::pi)
                                                    *(gamma_basis_rr.slice(alpha)(k,j)
                                                    *gamma_basis.slice(be)(j,k)
                                                    *invover_uu_lr(j,j)*invover_uu_rl(k,k)
                                                    *SecondTerm(Time(ntstep, tgridtype, dt, l),
                                                                reinterpret_cast<void *>(&paratdcurr)));
                                    } // if r=-besselorder, s=-besselorder

                                    // The third term excludes the sum over s
                                    if(s == -besselorder)
                                    {
                                        tmpcdble7 += (-1.0/datum::pi)
                                                    *(gamma_basis_rr.slice(alpha)(k,j)
                                                    *gamma_basis.slice(be)(j,k)
                                                    *invover_uu_lr(j,j)*invover_uu_rl(k,k)
                                                    *(ThirdTerm(Time(ntstep, tgridtype, dt, l),
                                                                reinterpret_cast<void *>(&paratdcurr))));
                                    } // if s=-besselorder

                                    // The fourth term includes all the sums
                                    tmpcdble8 += (-1.0/datum::pi)
                                                *(gamma_basis_rr.slice(alpha)(k,j)
                                                *gamma_basis.slice(be)(j,k)
                                                *invover_uu_lr(j,j)*invover_uu_rl(k,k)
                                                *(FourthTerm(Time(ntstep, tgridtype, dt, l),
                                                             reinterpret_cast<void *>(&paratdcurr))));

                                } // for -besselorder <= s <= besselorder
                            } // for -besselorder <= r <= besselorder
                        } // for 0 < k < nsite
                    } // for 0 < j < nsite
                } // for 0 < be < nlead

                TDCurrent(alpha,i) = tmpcdble5 + tmpcdble6 + tmpcdble7 + tmpcdble8;

                TDBias(alpha,i) = constpart[alpha] 
                                  + amplitude[alpha]
                                   *cos(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, l) 
                                      + phase[alpha])
                                  + ampl_rat*amplitude[alpha]
                                   *cos(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, l));

                if(l == 0)
                {
                    I_ss(alpha) = tmpcdble5;
                }

                tmpcdble5 = cx_double(0.0,0.0);
                tmpcdble6 = cx_double(0.0,0.0);
                tmpcdble7 = cx_double(0.0,0.0);
                tmpcdble8 = cx_double(0.0,0.0);
            } // for 0 < alpha < nlead
        } // if calc current

// New TD current
        if(calc[9] == 1)
        {
            // Loop over the lead alpha to calculate I_alpha(t)
            for(unsigned int alpha=0; alpha<nlead; alpha++)
            {
                // Internal sum over lead index 'ga'
                for(unsigned int ga=0; ga<nlead; ga++)
                {
                    // Internal sum over the central region index j
                    for(unsigned int j=0; j<nsite; j++)
                    {
                        // Internal sum over the central region index k
                        for(unsigned int k=0; k<nsite; k++)
                        {
                            // Internal sum over the Bessel function index r
                            for(int r=-besselorder; r<=besselorder; r++)
                            {
                                // Internal sum over the Bessel function index r'
                                for(int rp=-besselorder; rp<=besselorder; rp++)
                                {
									// Internal sum over the Bessel function index s
									for(int s=smin; s<=smax; s++)
									{
										// Internal sum over the Bessel function index s'
										for(int sp=smin; sp<=smax; sp++)
										{
											// Parameters defined in one bunch (to be sent to the evaluating function)
											ParamsTDCurrNew paratdcurrnew;
											paratdcurrnew.j = j;
											paratdcurrnew.k = k;
											paratdcurrnew.r = r;
											paratdcurrnew.s = s;
											paratdcurrnew.rp = rp;
											paratdcurrnew.sp = sp;
											paratdcurrnew.alpha = alpha;
											paratdcurrnew.ga = ga;
											paratdcurrnew.mu = mu;
											paratdcurrnew.beta = beta;
											paratdcurrnew.constpart = constpart;
											paratdcurrnew.amplitude = amplitude;
											paratdcurrnew.frequency = frequency;
											paratdcurrnew.phase = phase;
											paratdcurrnew.freq_mult1 = freq_mult1;
											paratdcurrnew.freq_mult2 = freq_mult2;
											paratdcurrnew.ampl_rat = ampl_rat;
											paratdcurrnew.eps_ueff_l = eps_ueff_l;

											// The second term includes sums over j, r, s
											if(k == 0 && ga == 0 && rp == -besselorder && sp == smin)
											{
												tmpcdble6 += (2.0/datum::pi)
												            *real(gamma_basis_lr.slice(alpha)(j,j)
												            *invover_uu_lr(j,j)
            												*SecondTermNew(Time(ntstep, tgridtype, dt, l),
																	       reinterpret_cast<void *>(&paratdcurrnew)));
											}

											// The third term includes sums over j, k, ga
											if(r == -besselorder && s == smin && rp == -besselorder && sp == smin)
											{
												tmpcdble7 += (-1.0/datum::pi)*gamma_basis_rr.slice(alpha)(k,j)
												            *(gamma_basis.slice(ga)(j,k)
												            *invover_uu_lr(j,j)
												            *invover_uu_rl(k,k)
												            *ThirdTermNew(Time(ntstep, tgridtype, dt, l),
																	      reinterpret_cast<void *>(&paratdcurrnew)));
											}

											// The fourth term includes sums over j, k, ga, r, s
											if(rp == -besselorder && sp == smin)
											{
												tmpcdble8 += (-1.0/datum::pi)
               												*static_cast<double>(partpara)
				            								*gamma_basis_rr.slice(alpha)(k,j)
												            *(gamma_basis.slice(ga)(j,k)
												            *invover_uu_lr(j,j)
												            *invover_uu_rl(k,k)
												            *FourthTermNew(Time(ntstep, tgridtype, dt, l),
																	       reinterpret_cast<void *>(&paratdcurrnew)));
											}

											// The fifth term includes sums over j, k, ga, r, s, rp, sp
											{
												tmpcdble9 += (-1.0/datum::pi)*gamma_basis_rr.slice(alpha)(k,j)
												            *(gamma_basis.slice(ga)(j,k)
												            *invover_uu_lr(j,j)
												            *invover_uu_rl(k,k)
												            *FifthTermNew(Time(ntstep, tgridtype, dt, l),
																          reinterpret_cast<void *>(&paratdcurrnew)));
											}

										} // for -besselorder <= sp <= besselorder
									} // for -besselorder <= s <= besselorder
                                } // for -besselorder <= rp <= besselorder
                            } // for -besselorder <= r <= besselorder
                        } // for 0 < k < nsite
                    } // for 0 < j < nsite
                } // for 0 < ga < nlead

                TDCurrentNew(alpha,i) = tmpcdble6 + tmpcdble7 + tmpcdble8 + tmpcdble9;

				if(calc[1] == 0)
				{
					TDBias(alpha,i) = constpart[alpha] 
									  + amplitude[alpha]
									   *cos(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, l) 
										  + phase[alpha])
									  + ampl_rat*amplitude[alpha]
									   *cos(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, l));
				}

                tmpcdble5 = cx_double(0.0,0.0);
                tmpcdble6 = cx_double(0.0,0.0);
                tmpcdble7 = cx_double(0.0,0.0);
                tmpcdble8 = cx_double(0.0,0.0);
                tmpcdble9 = cx_double(0.0,0.0);
            } // for 0 < alpha < nlead
        } // if calc current

// NC calculation (inside the time loop)
        if(calc[2] == 1)
        {
            // Internal sum over lead 'be' (symbol 'beta' is reserved for the inverse temperature)
            for(unsigned int be=0; be<nlead; be++)
            {
                // Internal sum over the central region index j
                for(unsigned int j=0; j<nsite; j++)
                {
                    // Internal sum over the central region index k
                    for(unsigned int k=0; k<nsite; k++)
                    {
                        // Internal sum over the Bessel function index r
                        for(int r=-besselorder; r<=besselorder; r++)
                        {
                            // Internal sum over the Bessel function index s
                            for(int s=-besselorder; s<=besselorder; s++)
                            {
                                // Parameters defined in one bunch (to be sent to the evaluating function)
                                ParamsTDCurr paratdcurr;
                                paratdcurr.j = j;
                                paratdcurr.k = k;
                                paratdcurr.r = r;
                                paratdcurr.s = s;
                                paratdcurr.alpha = 0;
                                paratdcurr.be = be;
                                paratdcurr.mu = mu;
                                paratdcurr.beta = beta;
                                paratdcurr.constpart = constpart;
                                paratdcurr.amplitude = amplitude;
                                paratdcurr.frequency = frequency;
                                paratdcurr.phase = phase;
                                paratdcurr.eps_ueff_l = eps_ueff_l;

                                // The first term excludes sums over r and s
                                if(r == 0 && s == 0)
                                {
                                    tmpcdbleA += (1.0/datum::pi)
                                                *(overlap_uu_rr(k,j)
                                                *gamma_basis.slice(be)(j,k)
                                                *invover_uu_lr(j,j)
                                                *invover_uu_rl(k,k)
                                                *FirstTermN(Time(ntstep, tgridtype, dt, l),
                                                            reinterpret_cast<void *>(&paratdcurr)));
                                } // if r=0, s=0

                                // The second term excludes sum over s
                                if(s == 0)
                                {
                                    tmpcdbleB += (1.0/datum::pi)
                                                *(overlap_uu_rr(k,j)
                                                *gamma_basis.slice(be)(j,k)
                                                *invover_uu_lr(j,j)
                                                *invover_uu_rl(k,k)
                                                *SecondTermN(Time(ntstep, tgridtype, dt, l),
                                                             reinterpret_cast<void *>(&paratdcurr)));
                                } // if s=0

                                // The third term includes all the sums
                                tmpcdbleC += (1.0/datum::pi)
                                            *(overlap_uu_rr(k,j)
                                            *gamma_basis.slice(be)(j,k)
                                            *invover_uu_lr(j,j)
                                            *invover_uu_rl(k,k)
                                            *ThirdTermN(Time(ntstep, tgridtype, dt, l),
                                                        reinterpret_cast<void *>(&paratdcurr)));

                            } // for -besselorder <= s <= besselorder
                        } // for -besselorder <= r <= besselorder
                    } // for 0 < k < nsite
                } // for 0 < j < nsite
            } // for 0 < be < nlead
        } // if calc N

        NC(i) = tmpcdbleA + tmpcdbleB + tmpcdbleC;
        tmpcdbleA = cx_double(0.0,0.0);
        tmpcdbleB = cx_double(0.0,0.0);
        tmpcdbleC = cx_double(0.0,0.0);

    }   // i<elems
    tmpdble2 = MPI_Wtime();
    if(mpirank == 0) {StopWatch(tmpdble1, tmpdble2); cout << endl;}

// Pump current (outside the time loop => only done in root)
	if(mpirank == 0) {cout << endl; cout << "Pump currents" << endl;}
	tmpdble1 = MPI_Wtime();
    if(calc[3] == 1 && mpirank == 0)
    {
        tmpcdble5 = cx_double(0.0,0.0);
        for(unsigned int alpha=0; alpha<nlead; alpha++)
        {
            for(unsigned int be=0; be<nlead; be++)
            {
				for(unsigned int ga=0; ga<nlead; ga++)
				{
					for(unsigned int j=0; j<nsite; j++)
					{
						for(unsigned int k=0; k<nsite; k++)
						{
							for(int r=-besselorder; r<=besselorder; r++)
							{
								for(int rp=-besselorder; rp<=besselorder; rp++)
								{
									for(int s=smin; s<=smax; s++)
									{
										for(int sp=smin; sp<=smax; sp++)
										{
											ParamsPump parapump;
											parapump.j = j;
											parapump.k = k;
											parapump.r = r;
											parapump.rp = rp;
											parapump.s = s;
											parapump.sp = sp;
											parapump.alpha = alpha;
											parapump.be = be;
											parapump.ga = ga;
											parapump.mu = mu;
											parapump.beta = beta;
											parapump.constpart = constpart;
											parapump.amplitude = amplitude;
											parapump.frequency = frequency;
											parapump.phase = phase;
											parapump.freq_mult1 = freq_mult1;
											parapump.freq_mult2 = freq_mult2;
											parapump.ampl_rat = ampl_rat;
											parapump.eps_ueff_l = eps_ueff_l;
											if(freq_mult1*(r-rp) + freq_mult2*(s-sp) == 0)
											{
												tmpcdble5 += (1.0/(datum::pi))
														*(
														  gamma_basis_rr.slice(alpha)(k,j)
														 *gamma_basis.slice(ga)(j,k)
														 *invover_uu_lr(j,j)
														 *invover_uu_rl(k,k)
														 *PumpTerm1(reinterpret_cast<void *>(&parapump))
														 /(eps_ueff_l(j)-conj(eps_ueff_l(k)))

														- gamma_basis_rr.slice(be)(k,j)
														 *gamma_basis.slice(ga)(j,k)
														 *invover_uu_lr(j,j)
														 *invover_uu_rl(k,k)
														 *PumpTerm2(reinterpret_cast<void *>(&parapump))
														 /(eps_ueff_l(j)-conj(eps_ueff_l(k)))
														 );
											} // if
										} // -besselorder <= sp <= besselorder
									} // -besselorder <= s <= besselorder
								} // -besselorder <= rp <= besselorder
							} // -besselorder <= r <= besselorder
						} // 0 < k < nsite
					} // 0 < j < nsite
				} // 0 < ga < nlead
                PumpCurrent(alpha,be) = tmpcdble5;
                tmpcdble5 = cx_double(0.0,0.0);
            } // 0 < be < nlead
        } // 0 < alpha < nlead
    } // if calc pump

	// LR pump
    if(calc[7] == 1 && mpirank == 0)
    {
        tmpcdble5 = cx_double(0.0,0.0);
		for(unsigned int j=0; j<nsite; j++)
		{
			for(unsigned int k=0; k<nsite; k++)
			{
				for(int r=-besselorder; r<=besselorder; r++)
				{
					for(int rp=-besselorder; rp<=besselorder; rp++)
					{
						for(int s=smin; s<=smax; s++)
						{
							for(int sp=smin; sp<=smax; sp++)
							{
								ParamsPumpLR parapump;
								parapump.j = j;
								parapump.k = k;
								parapump.r = r;
								parapump.rp = rp;
								parapump.s = s;
								parapump.sp = sp;
								parapump.mu = mu;
								parapump.beta = beta;
								parapump.constpart = constpart;
								parapump.amplitude = amplitude;
								parapump.frequency = frequency;
								parapump.phase = phase;
								parapump.freq_mult1 = freq_mult1;
								parapump.freq_mult2 = freq_mult2;
								parapump.ampl_rat = ampl_rat;
								parapump.eps_ueff_l = eps_ueff_l;
								if(freq_mult1*(r-rp) + freq_mult2*(s-sp) == 0)
								{
									tmpcdble5 += (1.0/(datum::pi))
											*((gamma_basis_rr.slice(0)(k,j)
											  *gamma_basis.slice(1)(j,k) 
											  +gamma_basis_rr.slice(1)(k,j)
											  *gamma_basis.slice(0)(j,k))
											 *invover_uu_lr(j,j)
											 *invover_uu_rl(k,k)
											 *PumpLR(reinterpret_cast<void *>(&parapump))
											 /(eps_ueff_l(j)-conj(eps_ueff_l(k))));
								} // if
							} // -besselorder <= sp <= besselorder
						} // -besselorder <= s <= besselorder
					} // -besselorder <= rp <= besselorder
				} // -besselorder <= r <= besselorder
			} // 0 < k < nsite
		} // 0 < j < nsite
		PumpCurrentLR = tmpcdble5;
		tmpcdble5 = cx_double(0.0,0.0);
    } // if calc pump

	// Stochastic LR pump
    if(calc[8] == 1 && mpirank == 0)
    {
        tmpcdble5 = cx_double(0.0,0.0);
		for(unsigned int j=0; j<nsite; j++)
		{
			for(unsigned int k=0; k<nsite; k++)
			{
                for(int n=0; n<NstochMax; n++)
                {
                    for(int m=0; m<=n; m++)
                    {
                        for(int r=-besselorder; r<=besselorder; r++)
                        {
                            for(int rp=-besselorder; rp<=besselorder; rp++)
                            {
                                for(int s=smin; s<=smax; s++)
                                {
                                    for(int sp=smin; sp<=smax; sp++)
                                    {
                                        ParamsPumpLRstoch parapumpstoch;
                                        parapumpstoch.j = j;
                                        parapumpstoch.k = k;
                                        parapumpstoch.r = r;
                                        parapumpstoch.rp = rp;
                                        parapumpstoch.s = s;
                                        parapumpstoch.sp = sp;
                                        parapumpstoch.m = m;
                                        parapumpstoch.n = n;
                                        parapumpstoch.mu = mu;
                                        parapumpstoch.beta = beta;
                                        parapumpstoch.constpart = constpart;
                                        parapumpstoch.amplitude = amplitude;
                                        parapumpstoch.frequency = frequency;
                                        parapumpstoch.phase = phase;
                                        parapumpstoch.freq_mult1 = freq_mult1;
                                        parapumpstoch.freq_mult2 = freq_mult2;
                                        parapumpstoch.ampl_rat = ampl_rat;
                                        parapumpstoch.eps_ueff_l = eps_ueff_l;
                                        parapumpstoch.Dfluct = Dfluct;
                                        parapumpstoch.wcorr = wcorr;
                                        if(freq_mult1*(r-rp) + freq_mult2*(s-sp) == 0)
                                        {
                                            tmpcdble5 += (1.0/(datum::pi))
                                                    *((gamma_basis_rr.slice(0)(k,j)
                                                      *gamma_basis.slice(1)(j,k) 
                                                      +gamma_basis_rr.slice(1)(k,j)
                                                      *gamma_basis.slice(0)(j,k))
                                                     *invover_uu_lr(j,j)
                                                     *invover_uu_rl(k,k)
                                                     *pow(Dfluct/wcorr,n)
                                                    // *(tgamma(n+1)/(tgamma(n-m+1)*tgamma(m+1)))
                                                    *(pow(-1.0,m)/(tgamma(n-m+1)*tgamma(m+1)))
                                                     *PumpLRstoch(reinterpret_cast<void *>(&parapumpstoch))
                                                     /(eps_ueff_l(j)-conj(eps_ueff_l(k))));
                                        } // if
                                    } // -besselorder <= sp <= besselorder
                                } // -besselorder <= s <= besselorder
                            } // -besselorder <= rp <= besselorder
                        } // -besselorder <= r <= besselorder
                    } // 0 <= m <= n
                } // 0 <= n < NstochMax
			} // 0 < k < nsite
		} // 0 < j < nsite
		PumpCurrentLRstoch = tmpcdble5;
		tmpcdble5 = cx_double(0.0,0.0);
		
		 tmpcdble51 = cx_double(0.0,0.0);
		for(unsigned int j=0; j<nsite; j++)
		{
			for(unsigned int k=0; k<nsite; k++)
			{
                for(int n=0; n<NstochMax; n++)
                {
                    for(int m=0; m<=n; m++)
                    {
                        for(int r=-besselorder; r<=besselorder; r++)
                        {
                            for(int rp=-besselorder; rp<=besselorder; rp++)
                            {
                                for(int s=smin; s<=smax; s++)
                                {
                                    for(int sp=smin; sp<=smax; sp++)
                                    {
                                        ParamsPumpLRstochAdiabatic parapumpstochadiabatic;
                                        parapumpstochadiabatic.j = j;
                                        parapumpstochadiabatic.k = k;
                                        parapumpstochadiabatic.m = m;
                                        parapumpstochadiabatic.n = n;
                                        parapumpstochadiabatic.mu = mu;
                                        parapumpstochadiabatic.beta = beta;
                                        parapumpstochadiabatic.constpart = constpart;
                                        parapumpstochadiabatic.eps_ueff_l = eps_ueff_l;
                                        parapumpstochadiabatic.Dfluct = Dfluct;
                                        parapumpstochadiabatic.wcorr = wcorr;
                                        if(freq_mult1*(r-rp) + freq_mult2*(s-sp) == 0)
                                        {
                                            tmpcdble51 += (1.0/(datum::pi))
                                                    *((gamma_basis_rr.slice(0)(k,j)
                                                      *gamma_basis.slice(1)(j,k) 
                                                      +gamma_basis_rr.slice(1)(k,j)
                                                      *gamma_basis.slice(0)(j,k))
                                                     *invover_uu_lr(j,j)
                                                     *invover_uu_rl(k,k)
                                                     *pow(Dfluct/wcorr,n)
                                                    // *(tgamma(n+1)/(tgamma(n-m+1)*tgamma(m+1)))
                                                    *(pow(-1.0,m)/(tgamma(n-m+1)*tgamma(m+1)))
                                                     *PumpLRstochAdiabatic(reinterpret_cast<void *>(&parapumpstochadiabatic))
                                                     /(eps_ueff_l(j)-conj(eps_ueff_l(k))));
                                        } // if
                                    } // -besselorder <= sp <= besselorder
                                } // -besselorder <= s <= besselorder
                            } // -besselorder <= rp <= besselorder
                        } // -besselorder <= r <= besselorder
                    } // 0 <= m <= n
                } // 0 <= n < NstochMax
			} // 0 < k < nsite
		} // 0 < j < nsite
		PumpCurrentLRstochAdiabatic = tmpcdble51;
		tmpcdble51 = cx_double(0.0,0.0);
    } // if calc pump

    tmpdble2 = MPI_Wtime();
    if(mpirank == 0) {StopWatch(tmpdble1, tmpdble2); cout << endl;}

// Basis transformation from the h_eff eigenbasis to site basis of the central system
    if(mpirank == 0) {cout << endl; cout << "Basis transformations" << endl;}
    if(calc[0] == 1)
    {
        tmpdble1 = MPI_Wtime();
        for(unsigned int i=0; i<elems; i++)
        {
            for(unsigned int m=0; m<nsite; m++)
            {
                for(unsigned int n=0; n<=m; n++)    // Symmetry
                {
                    temp1 = cx_double(0.0,0.0);
                    temp2 = cx_double(0.0,0.0);

                    if(m == n || real(hp_eff(m,n)) != 0.0 || imag(hp_eff(m,n)) != 0.0)
                    {
                        for(unsigned int j=0; j<nsite; j++)
                        {
                            for(unsigned int k=0; k<nsite; k++)
                            {
                                if( (abs(real(eps_ueff_l(j))) < cutoff && abs(real(eps_ueff_l(k))) < cutoff) ||
                                    (abs(real(eps_ueff_l(j))) >= cutoff && abs(real(eps_ueff_l(k))) < cutoff) ||
                                    (abs(real(eps_ueff_l(j))) < cutoff && abs(real(eps_ueff_l(k))) >= cutoff) )
                                {
                                    if(pert == 0)
                                    {
                                        temp1 += psi_ueff_r(m,j)*invover_uu_lr(j,j)
                                                *conj(psi_ueff_r(n,k))*invover_uu_rl(k,k)
                                                *rho_basis.slice(i)(j,k);

                                        if(elems*mpirank + i == 0)
                                        {
                                            temp2 += psi_ueff_r(m,j)*invover_uu_lr(j,j)
                                                    *conj(psi_ueff_r(n,k))*invover_uu_rl(k,k)
                                                    *rho_basis_ss(j,k);
                                        }
                                    }
                                    if(pert == 1)
                                    {
                                        temp1 += psi_peff_r(m,j)*invover_pp_lr(j,j)
                                                *conj(psi_peff_r(n,k))*invover_pp_rl(k,k)
                                                *rho_basis.slice(i)(j,k);

                                        if(elems*mpirank + i == 0)
                                        {
                                            temp2 += psi_peff_r(m,j)*invover_pp_lr(j,j)
                                                    *conj(psi_peff_r(n,k))*invover_pp_rl(k,k)
                                                    *rho_basis_ss(j,k);
                                        }
                                    }
                                }   // cutoffs
                            }   // k<nsite
                        }   // j<nsite

                        rho.slice(i)(m,n) = temp1;
                        rho.slice(i)(n,m) = conj(temp1);

                        // The contribution from eigenfunctions of Gamma with eigenvalue 0
                        // is to be added to the final density matrix.
                        // (Also the cutoff-states)
                        if(m==n)
                        {
							rho.slice(i)(m,m) += rho_bar(m) + rho_cutoff(m);
                        }

                        if(elems*mpirank + i == 0)
                        {
                            rho_ss(m,n) = temp2;
                            rho_ss(n,m) = conj(temp2);
                            if(m==n)
                            {
								rho_ss(m,m) += rho_bar(m) + rho_cutoff(m);
                            }
                        }

                    }   // if m==n etc
                }   // n<=m
            }   // m<nsite
        }   // i<elems

// Calculate dipole moment
	/*
        for(unsigned int i=0; i<elems; i++)
        {
			tmpcdble = cx_double(0.0,0.0);
            for(unsigned int j=0; j<nsite; j++)
            {
				tmpcdble += sqrt(pow(coords[j][0]*1.0e-10*rho.slice(i)(j,j)*datum::ec,2)
				                +pow(coords[j][1]*1.0e-10*rho.slice(i)(j,j)*datum::ec,2)
				                +pow(coords[j][2]*1.0e-10*rho.slice(i)(j,j)*datum::ec,2));
			}
			dipmom(i) = tmpcdble;
		}
	*/
        tmpdble2 = MPI_Wtime();
        if(mpirank == 0) {StopWatch(tmpdble1, tmpdble2); cout << endl;}
    } // if calc 1rdm

// Calculate the bond currents (and supercurrents)
    if(mpirank == 0) {cout << endl; cout << "Bond currents" << endl;}
    if(calc[0] == 1)
    {
        tmpdble1 = MPI_Wtime();
        if(sc == 0 && nsite > 1 || sc == 1 && nsite > 2)
        {
            bondcurrent_one_ss = cx_double(0.0,0.0);
            bondcurrent_two_ss = cx_double(0.0,0.0);
            bondcurrent_three_ss = cx_double(0.0,0.0);
            supercurrent_one_ss = cx_double(0.0,0.0);
            supercurrent_two_ss = cx_double(0.0,0.0);
            supercurrent_three_ss = cx_double(0.0,0.0);
            for(unsigned int j=0; j<bridge_sites-1; j++)
            {
                if(sc == 0)
                {
                    if(j%2 == 0)
                    {
                        bondcurrent_one_ss += 2.0*imag(Hu(bridgeone[j+1],bridgeone[j])
                                                      *rho_ss(bridgeone[j],bridgeone[j+1]));
                        bondcurrent_two_ss += 2.0*imag(Hu(bridgetwo[j+1],bridgetwo[j])
                                                      *rho_ss(bridgetwo[j],bridgetwo[j+1]));
                        bondcurrent_three_ss += 2.0*imag(Hu(bridgethree[j+1],bridgethree[j])
                                                        *rho_ss(bridgethree[j],bridgethree[j+1]));
                    }
                }
                if(sc == 1) 
                {
                    if(j%2 == 0)    // Normal current is calculated as before but separately for spin-up and spin-down
                    {
                        bondcurrent_one_ss += imag(Hu(bridgeone[j+1],bridgeone[j])
                                                  *rho_ss(bridgeone[j],bridgeone[j+1]));
                        bondcurrent_two_ss += imag(Hu(bridgetwo[j+1],bridgetwo[j])
                                                  *rho_ss(bridgetwo[j],bridgetwo[j+1]));
                        bondcurrent_three_ss += imag(Hu(bridgethree[j+1],bridgethree[j])
                                                    *rho_ss(bridgethree[j],bridgethree[j+1]));
                    }

                    if(j%4 == 0)    // Super current contribution
                    {
                        supercurrent_one_ss += -4.0*imag(Hu(bridgeone[j]+nsite/2,bridgeone[j])
                                                        *rho_ss(bridgeone[j],bridgeone[j]+nsite/2));
                        supercurrent_two_ss += -4.0*imag(Hu(bridgetwo[j]+nsite/2,bridgetwo[j])
                                                        *rho_ss(bridgetwo[j],bridgetwo[j]+nsite/2));
                        supercurrent_three_ss += -4.0*imag(Hu(bridgethree[j]+nsite/2,bridgethree[j])
                                                          *rho_ss(bridgethree[j],bridgethree[j]+nsite/2));
                    }
                }
            }

            if(mpirank == 0)
            {
                cout << "Steady-state values: \t(" << scientific 
                     << setprecision(10) << real(bondcurrent_one_ss) << ", "
                     << real(bondcurrent_two_ss) << ", " 
                     << real(bondcurrent_three_ss) << ")" << endl;
                ofstream ss_bondcurrent_one("output/ss_bond_current_one.out", ios::out);
                ofstream ss_bondcurrent_two("output/ss_bond_current_two.out", ios::out);
                ofstream ss_bondcurrent_three("output/ss_bond_current_three.out", ios::out);
                ss_bondcurrent_one << setprecision(10) << scientific 
                                   << real(bondcurrent_one_ss) << endl;
                ss_bondcurrent_two << setprecision(10) << scientific 
                                   << real(bondcurrent_two_ss) << endl;
                ss_bondcurrent_three << setprecision(10) << scientific 
                                     << real(bondcurrent_three_ss) << endl;
                ss_bondcurrent_one.close();
                ss_bondcurrent_two.close();
                ss_bondcurrent_three.close();
            }

            for(unsigned int i=0; i<elems; i++)
            {
                for(unsigned int j=0; j<bridge_sites-1; j++)
                {
    //              If the gs Hamiltonian includes Peierls phases etc, the bond-current formula should include it as well!
    //              (Also, see above the steady-state bond currents)
                    if(sc == 0)
                    {
                        if(j%2 == 0)
                        {
                            bondcurrent_one(i) += 2.0*imag(Hu(bridgeone[j+1],bridgeone[j])
                                                          *rho(bridgeone[j],bridgeone[j+1],i));
                            bondcurrent_two(i) += 2.0*imag(Hu(bridgetwo[j+1],bridgetwo[j])
                                                          *rho(bridgetwo[j],bridgetwo[j+1],i));
                            bondcurrent_three(i) += 2.0*imag(Hu(bridgethree[j+1],bridgethree[j])
                                                            *rho(bridgethree[j],bridgethree[j+1],i));
                        }
                    }
                    if(sc == 1) 
                    {
                        if(j%2 == 0)    // Normal current is calculated as before but separately for spin-up and spin-down
                        {
                            bondcurrent_one(i) += imag(Hu(bridgeone[j+1],bridgeone[j])
                                                      *rho(bridgeone[j],bridgeone[j+1],i));
                            bondcurrent_two(i) += imag(Hu(bridgetwo[j+1],bridgetwo[j])
                                                      *rho(bridgetwo[j],bridgetwo[j+1],i));
                            bondcurrent_three(i) += imag(Hu(bridgethree[j+1],bridgethree[j])
                                                        *rho(bridgethree[j],bridgethree[j+1],i));
                        }

                        if(j%4 == 0)    // Super current contribution
                        {
                            supercurrent_one(i) += -4.0*imag(Hu(bridgeone[j]+nsite/2,bridgeone[j])
                                                            *rho(bridgeone[j],bridgeone[j]+nsite/2,i));
                            supercurrent_two(i) += -4.0*imag(Hu(bridgetwo[j]+nsite/2,bridgetwo[j])
                                                            *rho(bridgetwo[j],bridgetwo[j]+nsite/2,i));
                            supercurrent_three(i) += -4.0*imag(Hu(bridgethree[j]+nsite/2,bridgethree[j])
                                                              *rho(bridgethree[j],bridgethree[j]+nsite/2,i));
                        }
                    }
                }
            }
			if(systype==12)
			{
				int site=1;

				ofstream ss_bondcurrent_one("output/ss_bond_current_one.out", ios::out);
				ofstream ss_source_one("output/ss_source_one.out", ios::out);

				bondcurrent_one_ss = cx_double(0.0,0.0);

				double t=-2.0*real(Hu((site+1)*4+0,site*4+0));
				double aso=-2.0*real(Hu((site+1)*4+2,site*4+0));
				double Delta=real(Hu(site*4+2,site*4+3));
				/*
				cout << "t" << endl;
				cout << -I*(-t/2.0)*(rho_ss(site*4+0,(site+1)*4+0)+rho_ss(site*4+2,(site+1)*4+2)
								   -(rho_ss((site+1)*4+0,site*4+0)+rho_ss((site+1)*4+2,site*4+2))) << endl;
				cout << -I*(-t/2.0)*(rho_ss(site*4+0,(site-1)*4+0)+rho_ss(site*4+2,(site-1)*4+2)
								   -(rho_ss((site-1)*4+0,site*4+0)+rho_ss((site-1)*4+2,site*4+2))) << endl;
				cout << endl;

				cout << "alpha" << endl;
				cout << -I*(-aso/2.0)*(rho_ss(site*4+0,(site+1)*4+2)-rho_ss(site*4+2,(site+1)*4+0)
									 -(rho_ss((site+1)*4+2,site*4+0)-rho_ss((site+1)*4+0,site*4+2))) << endl;
				cout << -I*(-aso/2.0)*(rho_ss(site*4+2,(site-1)*4+0)-rho_ss(site*4+0,(site-1)*4+2)
									 -(rho_ss((site-1)*4+0,site*4+2)-rho_ss((site-1)*4+2,site*4+0))) << endl;
				cout << endl;

				cout << "Delta" << endl;
				cout << -I*Delta*(rho_ss(site*4+1,site*4+0)) << endl;
				cout << -I*Delta*(-rho_ss(site*4+0,site*4+1)) << endl;
				cout << -I*Delta*(-rho_ss(site*4+3,site*4+2)) << endl;
				cout << -I*Delta*(rho_ss(site*4+2,site*4+3)) << endl;
				cout << -I*Delta*(rho_ss(site*4+1,site*4+0)-rho_ss(site*4+0,site*4+1)
								 -rho_ss(site*4+3,site*4+2)+rho_ss(site*4+2,site*4+3)) << endl;
				cout << endl;
				*/
				if(mpirank==0)
				{
				    cout << "Continuity equation: zero = "
						 << -I*(-t/2.0)*(rho_ss(site*4+0,(site+1)*4+0)+rho_ss(site*4+2,(site+1)*4+2)
									   -(rho_ss((site+1)*4+0,site*4+0)+rho_ss((site+1)*4+2,site*4+2)))
							-I*(-t/2.0)*(rho_ss(site*4+0,(site-1)*4+0)+rho_ss(site*4+2,(site-1)*4+2)
									   -(rho_ss((site-1)*4+0,site*4+0)+rho_ss((site-1)*4+2,site*4+2)))
							-I*(-aso/2.0)*(rho_ss(site*4+0,(site+1)*4+2)-rho_ss(site*4+2,(site+1)*4+0)
										 -(rho_ss((site+1)*4+2,site*4+0)-rho_ss((site+1)*4+0,site*4+2)))
							-I*(-aso/2.0)*(rho_ss(site*4+2,(site-1)*4+0)-rho_ss(site*4+0,(site-1)*4+2)
										 -(rho_ss((site-1)*4+0,site*4+2)-rho_ss((site-1)*4+2,site*4+0)))
							-I*Delta*(rho_ss(site*4+1,site*4+0)-rho_ss(site*4+0,site*4+1)
									 -rho_ss(site*4+3,site*4+2)+rho_ss(site*4+2,site*4+3)) << endl << endl;
				}

				cx_double testsum(0.0,0.0), testsum2(0.0,0.0), testsum3(0.0,0.0);
				testsum = -I*Delta*(rho_ss((site+1)*4+1,(site+1)*4+0)-rho_ss((site+1)*4+0,(site+1)*4+1)
								   -rho_ss((site+1)*4+3,(site+1)*4+2)+rho_ss((site+1)*4+2,(site+1)*4+3));

				for(unsigned int testidx=0; testidx<nsite/4; testidx++)
				{
					testsum2 += -I*Delta*(rho_ss(testidx*4+1,testidx*4+0)-rho_ss(testidx*4+0,testidx*4+1)
										 -rho_ss(testidx*4+3,testidx*4+2)+rho_ss(testidx*4+2,testidx*4+3));
					if(testidx>=0)
					{ 
						testsum3 += -I*Delta*(rho_ss(testidx*4+1,testidx*4+0)-rho_ss(testidx*4+0,testidx*4+1)
											 -rho_ss(testidx*4+3,testidx*4+2)+rho_ss(testidx*4+2,testidx*4+3));
					}
					if(mpirank==0 && nsite<=80)
					{
					    if(testidx<(nsite-1)/4)
					    {
							cout << "J_t(" << testidx << "," << testidx+1 << ") = "
								 << -I*(-t/2.0)*(rho_ss(testidx*4+0,(testidx+1)*4+0)+rho_ss(testidx*4+2,(testidx+1)*4+2)
											   -(rho_ss((testidx+1)*4+0,testidx*4+0)+rho_ss((testidx+1)*4+2,testidx*4+2))) << endl;
							cout << "J_a(" << testidx << "," << testidx+1 << ") = "
								 << -I*(-aso/2.0)*(rho_ss(testidx*4+0,(testidx+1)*4+2)-rho_ss(testidx*4+2,(testidx+1)*4+0)
												 -(rho_ss((testidx+1)*4+2,testidx*4+0)-rho_ss((testidx+1)*4+0,testidx*4+2))) << endl;

							cout << endl << "NORMAL CURRENT = " 
								 << -I*(-t/2.0)*(rho_ss(testidx*4+0,(testidx+1)*4+0)+rho_ss(testidx*4+2,(testidx+1)*4+2)
											   -(rho_ss((testidx+1)*4+0,testidx*4+0)+rho_ss((testidx+1)*4+2,testidx*4+2)))
									-I*(-aso/2.0)*(rho_ss(testidx*4+0,(testidx+1)*4+2)-rho_ss(testidx*4+2,(testidx+1)*4+0)
												 -(rho_ss((testidx+1)*4+2,testidx*4+0)-rho_ss((testidx+1)*4+0,testidx*4+2))) << endl;
							cout << "SUPER CURRENT = " << testsum3 << endl;
							cout << "TOTAL CURRENT = " 
								 << -I*(-t/2.0)*(rho_ss(testidx*4+0,(testidx+1)*4+0)+rho_ss(testidx*4+2,(testidx+1)*4+2)
											   -(rho_ss((testidx+1)*4+0,testidx*4+0)+rho_ss((testidx+1)*4+2,testidx*4+2)))
									-I*(-aso/2.0)*(rho_ss(testidx*4+0,(testidx+1)*4+2)-rho_ss(testidx*4+2,(testidx+1)*4+0)
												 -(rho_ss((testidx+1)*4+2,testidx*4+0)-rho_ss((testidx+1)*4+0,testidx*4+2)))+testsum3 << endl << endl;
					    }
					    cout << "S(" << testidx << ") = "
							 << -I*Delta*(rho_ss(testidx*4+1,testidx*4+0)-rho_ss(testidx*4+0,testidx*4+1)
										 -rho_ss(testidx*4+3,testidx*4+2)+rho_ss(testidx*4+2,testidx*4+3)) << endl;
					}
				}
				cout << endl;
				/*
				cout << "sum_{j=1}^" << site << " Delta = " << testsum3 << endl << endl;

				cout << "##################################################################" << endl << endl;
				cout << "Current from site " << site << " to site " << site+1  << ":"<< endl;
				cout << "Hopping contribution = " << -I*(-t/2.0)*(rho_ss(site*4+0,(site+1)*4+0)+rho_ss(site*4+2,(site+1)*4+2)
																-(rho_ss((site+1)*4+0,site*4+0)+rho_ss((site+1)*4+2,site*4+2))) << endl;
				cout << "Spin-obit contribution = " << -I*(-aso/2.0)*(rho_ss(site*4+0,(site+1)*4+2)-rho_ss(site*4+2,(site+1)*4+0)
																	-(rho_ss((site+1)*4+2,site*4+0)-rho_ss((site+1)*4+0,site*4+2))) << endl;
				cout << "SC contribution = " << testsum << endl;
				cout << "Total current = " << -I*(-t/2.0)*(rho_ss(site*4+0,(site+1)*4+0)+rho_ss(site*4+2,(site+1)*4+2)
														 -(rho_ss((site+1)*4+0,site*4+0)+rho_ss((site+1)*4+2,site*4+2)))
											  -I*(-aso/2.0)*(rho_ss(site*4+0,(site+1)*4+2)-rho_ss(site*4+2,(site+1)*4+0)
														   -(rho_ss((site+1)*4+2,site*4+0)-rho_ss((site+1)*4+0,site*4+2)))+testsum << endl << endl;
				cout << "##################################################################" << endl << endl;
				cout << "Current from site " << site << " to site " << site-1  << ":"<< endl;
				cout << "Hopping contribution = " << -I*(-t/2.0)*(rho_ss(site*4+0,(site-1)*4+0)+rho_ss(site*4+2,(site-1)*4+2)
																-(rho_ss((site-1)*4+0,site*4+0)+rho_ss((site-1)*4+2,site*4+2))) << endl;
				cout << "Spin-obit contribution = " << -I*(-aso/2.0)*(rho_ss(site*4+2,(site-1)*4+0)-rho_ss(site*4+0,(site-1)*4+2)
																	-(rho_ss((site-1)*4+0,site*4+2)-rho_ss((site-1)*4+2,site*4+0))) << endl;
				cout << "SC contribution = " << testsum << endl;
				cout << "Total current = " << -I*(-t/2.0)*(rho_ss(site*4+0,(site-1)*4+0)+rho_ss(site*4+2,(site-1)*4+2)
														 -(rho_ss((site-1)*4+0,site*4+0)+rho_ss((site-1)*4+2,site*4+2)))
											  -I*(-aso/2.0)*(rho_ss(site*4+2,(site-1)*4+0)-rho_ss(site*4+0,(site-1)*4+2)
														   -(rho_ss((site-1)*4+0,site*4+2)-rho_ss((site-1)*4+2,site*4+0)))+testsum << endl << endl;
				cout << "##################################################################" << endl << endl;

				cout << "Total Delta = " << testsum2 << endl << endl;

				cout << "Partial Delta = " << testsum3 << endl << endl;

				cout << scientific << setprecision(15) <<  "test1 = "
					 <<
				  1.0*(-t/2.0)*imag(rho_ss(site*4+0,(site+1)*4+0)
							+rho_ss(site*4+2,(site+1)*4+2)
							)

				  +1.0*(-aso/2.0)*imag(rho_ss(site*4+0,(site+1)*4+2)
							   -rho_ss(site*4+2,(site+1)*4+0)
							   )
					 << endl;

				cout << endl;
				cout << scientific << setprecision(15) <<  "test2 = "
					 << 1.0*imag(0.0
						 +Hu((site+1)*4+0,site*4+0)*rho_ss(site*4+0,(site+1)*4+0)
						 +Hu((site+1)*4+2,site*4+2)*rho_ss(site*4+2,(site+1)*4+2)

						 +Hu((site+1)*4+2,site*4+0)*rho_ss(site*4+0,(site+1)*4+2)
						 +Hu((site+1)*4+0,site*4+2)*rho_ss(site*4+2,(site+1)*4+0)

						 ) << endl;
				cout << endl;

				cout << scientific << setprecision(15) <<  "test3 = "
					 << -I*0.5*(0.0
						+(-t/2.0)*(rho_ss(site*4+0,(site+1)*4+0)
							   +rho_ss(site*4+2,(site+1)*4+2)
							   -rho_ss((site+1)*4+0,site*4+0)
							   -rho_ss((site+1)*4+2,site*4+2)
							   )

						+(-aso/2.0)*(rho_ss(site*4+0,(site+1)*4+2)
								 -rho_ss(site*4+2,(site+1)*4+0)
								 -rho_ss((site+1)*4+2,site*4+0)
								 +rho_ss((site+1)*4+0,site*4+2)

								 )
						)
					 << endl;
				cout<<endl;
				*/
				cx_double testsum4(0.0,0.0);
				cx_double testsum4eq(0.0,0.0);
				for(unsigned int testidx=0; testidx<nsite/4; testidx++)
				{
					if(testidx>=0 && testidx <= site)
					{
					    testsum4 += -I*Delta*(rho_ss(testidx*4+1,testidx*4+0)-rho_ss(testidx*4+0,testidx*4+1)
					                         -rho_ss(testidx*4+3,testidx*4+2)+rho_ss(testidx*4+2,testidx*4+3));
					    testsum4eq += -I*Delta*(rho(testidx*4+1,testidx*4+0,0)-rho(testidx*4+0,testidx*4+1,0)
					                           -rho(testidx*4+3,testidx*4+2,0)+rho(testidx*4+2,testidx*4+3,0));
					}
				}

				if(mpirank==0)
				{
				    cout << scientific << setprecision(15) <<  "Total steady-state current = "
						 << ((-I*(-t/2.0)*(rho_ss(site*4+0,(site+1)*4+0)+rho_ss(site*4+2,(site+1)*4+2)
										 -(rho_ss((site+1)*4+0,site*4+0)+rho_ss((site+1)*4+2,site*4+2)))
							  -I*(-aso/2.0)*(rho_ss(site*4+0,(site+1)*4+2)-rho_ss(site*4+2,(site+1)*4+0)
										   -(rho_ss((site+1)*4+2,site*4+0)-rho_ss((site+1)*4+0,site*4+2)))
							  +testsum4)
//							-(-I*(-t/2.0)*(rho(site*4+0,(site+1)*4+0,0)+rho(site*4+2,(site+1)*4+2,0)
//										 -(rho((site+1)*4+0,site*4+0,0)+rho((site+1)*4+2,site*4+2,0)))
//							  -I*(-aso/2.0)*(rho(site*4+0,(site+1)*4+2,0)-rho(site*4+2,(site+1)*4+0,0)
//										   -(rho((site+1)*4+2,site*4+0,0)-rho((site+1)*4+0,site*4+2,0)))
//							  +testsum4eq)
							  )
						 << endl << endl;
				}

				bondcurrent_one_ss = (-I*(-t/2.0)*(rho_ss(site*4+0,(site+1)*4+0)+rho_ss(site*4+2,(site+1)*4+2)
												 -(rho_ss((site+1)*4+0,site*4+0)+rho_ss((site+1)*4+2,site*4+2)))
				                      -I*(-aso/2.0)*(rho_ss(site*4+0,(site+1)*4+2)-rho_ss(site*4+2,(site+1)*4+0)
												   -(rho_ss((site+1)*4+2,site*4+0)-rho_ss((site+1)*4+0,site*4+2)))
									  +testsum4
//									  -(-I*(-t/2.0)*(rho(site*4+0,(site+1)*4+0,0)+rho(site*4+2,(site+1)*4+2,0)
//												   -(rho((site+1)*4+0,site*4+0,0)+rho((site+1)*4+2,site*4+2,0)))
//										-I*(-aso/2.0)*(rho(site*4+0,(site+1)*4+2,0)-rho(site*4+2,(site+1)*4+0,0)
//													 -(rho((site+1)*4+2,site*4+0,0)-rho((site+1)*4+0,site*4+2,0)))
//										+testsum4eq)
										);

				ss_bondcurrent_one << setprecision(10) << scientific << real(bondcurrent_one_ss) << endl;
				ss_bondcurrent_one.close();

				for(unsigned int i=0; i<elems; i++)
				{
					cx_double testsum4td(0.0,0.0);
					for(unsigned int testidx=0; testidx<nsite/4; testidx++)
					{
						if(testidx>=0 && testidx <= site) 
						{
							testsum4td += -I*Delta*(rho(testidx*4+1,testidx*4+0,i)-rho(testidx*4+0,testidx*4+1,i)
												   -rho(testidx*4+3,testidx*4+2,i)+rho(testidx*4+2,testidx*4+3,i));
						}
					}
					bondcurrent_one(i) = (-I*(-t/2.0)*(rho(site*4+0,(site+1)*4+0,i)+rho(site*4+2,(site+1)*4+2,i)
													 -(rho((site+1)*4+0,site*4+0,i)+rho((site+1)*4+2,site*4+2,i)))
					                      -I*(-aso/2.0)*(rho(site*4+0,(site+1)*4+2,i)-rho(site*4+2,(site+1)*4+0,i)
													   -(rho((site+1)*4+2,site*4+0,i)-rho((site+1)*4+0,site*4+2,i)))
										  +testsum4td
//										-(-I*(-t/2.0)*(rho(site*4+0,(site+1)*4+0,0)+rho(site*4+2,(site+1)*4+2,0)
//													 -(rho((site+1)*4+0,site*4+0,0)+rho((site+1)*4+2,site*4+2,0)))
//										  -I*(-aso/2.0)*(rho(site*4+0,(site+1)*4+2,0)-rho(site*4+2,(site+1)*4+0,0)
//													   -(rho((site+1)*4+2,site*4+0,0)-rho((site+1)*4+0,site*4+2,0)))
//										  +testsum4eq)
										  );
				}
			}
		}

    // Pairing density (for superconductors) is calculated from the up-down blocks
        if(sc == 1)
        {
            for(unsigned int i=0; i<elems; i++)
            {
                unsigned int l = elems*mpirank + i;
                for(unsigned int j=0; j<nsite/2; j++)
                {
                    pairdensity(j,i) = rho(j,j+nsite/2,i)
                                      *exp(2.0*I*Hu(j,j)*Time(ntstep, tgridtype, dt, l));
                    supermatrix(j,i) = -4.0*imag(Hu(j+nsite/2,j)*rho(j,j+nsite/2,i));
                }
            }
        }

        tmpdble2 = MPI_Wtime();
        if(mpirank == 0) {StopWatch(tmpdble1, tmpdble2); cout << endl;}

    } // if calc 1rdm

	if(mpirank == 0) cout << "Ready!" << endl;
}
