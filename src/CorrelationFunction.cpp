#include "Main.hpp"
double CosechNew(double tau, double taubar, double beta, int padeorder, 
                 vec pade_eta, vec pade_zeta)
{
    double tempsum = 0.0;
    for(unsigned int j=0; j<padeorder; j++)
    {
        tempsum += 2.0*pade_eta(j)*(exp(-pade_zeta(j)*(tau-taubar)/beta)*ThetaMidpoint(tau-taubar)
                                   -exp(-pade_zeta(j)*(taubar-tau)/beta)*ThetaMidpoint(taubar-tau));
    }
    return tempsum;
}

// Implementation of Hurwitz-Lerch function in terms of the Pade poles (from Mike's code)
// --------------------------------------------------------------------------------------
cx_double HLT(double t, double beta, cx_double z, int padeorder, vec pade_eta, vec pade_zeta)
{
    cx_double sum(0.0,0.0);
    cx_double I(0.0,1.0);
    for (unsigned int j=0; j<padeorder; j++)
    {
        sum += 2.0*datum::pi*I*pade_eta(j)*exp(-pade_zeta(j)*t/beta)/(I*pade_zeta(j)+beta*z);
    }
    return sum;
}
// --------------------------------------------------------------------------------------

void CorrelationFunction(int nsite, int nlead, vector<int> lcontact, vector<int> rcontact, 
                         vector<int> lcontact2, vector<int> rcontact2, int lead_row, 
                         double bias_strength, int ntstep, int tgridtype, int sc, int besselorder, 
                         double dt, double lambda_strength, double gamma_strength, double lead_hop, 
                         double mu, double beta, cx_mat Hu, int systype, vec gs_eigval, 
                         cx_mat gs_eigvec, field<cx_mat> &SigmaGtr, field<cx_mat> &SigmaLss,
                         field<cx_mat> &LambdaPlus, field<cx_mat> &LambdaMinus,
                         field<cx_mat> &LambdaPlusD, field<cx_mat> &LambdaMinusD,
                         field<cx_mat> &GLssNew, field<cx_mat> &GGtrNew, field<cx_mat> &Cab,
                         vector<double> constpart, vector<double> amplitude, vector<double> frequency, 
                         vector<double> phase, int freq_mult1, int freq_mult2, double ampl_rat, 
                         int padeorder, vec pade_eta, vec pade_zeta, int partpara, cx_vec &dipmom,
                         vector<vector<double> > coords, vector<int> calc)
{
    int HL=0;
    double shift=0.0;

	// MPI variables
	int rank, size;

	//  Get the number of processes and the individual process ID
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if(rank == 0)
    {
        cout << endl;
        cout << "Correlation functions C_{alpha beta}(t1,t2)" << endl;
    }

// Allocate arrays
    cx_double I(0.0,1.0);
    cx_vec eps_ueff_r(nsite), eps_ueff_l(nsite);
    cx_mat hu_eff(nsite,nsite), temp_mat(nsite,nsite), 
           psi_ueff_r(nsite,nsite), psi_ueff_l(nsite,nsite);
    cx_cube gamma_basis(nsite,nsite,nlead);

    hu_eff.zeros();
    temp_mat.zeros();
    eps_ueff_l.zeros();
    eps_ueff_r.zeros();
    psi_ueff_l.zeros();
    psi_ueff_r.zeros();
    gamma_basis.zeros();

	double tmpdble1, tmpdble2;
    tmpdble1 = MPI_Wtime();

    for(unsigned int alpha=0; alpha<nlead; alpha++)
    {
        temp_mat += Gamma(nsite,nlead,lcontact,rcontact,lcontact2,
                          rcontact2,lead_row,lambda_strength,
                          gamma_strength,lead_hop,gs_eigvec,systype,sc)
                          .slice(alpha);
    }

    hu_eff = Hu - 0.5*I*temp_mat;

    eig_gen(eps_ueff_r, psi_ueff_r, hu_eff, 'r');  // h_eff |psi_eff_r> = eps_eff_r |psi_eff_r>
    eig_gen(eps_ueff_l, psi_ueff_l, hu_eff, 'l');  // <psi_eff_l| h_eff = eps_eff_l <psi_eff_l|

    cx_mat overlap_uu_lr(nsite,nsite), invover_uu_lr(nsite,nsite), 
           overlap_uu_rl(nsite,nsite), invover_uu_rl(nsite,nsite);

    overlap_uu_lr = trans(psi_ueff_l)*psi_ueff_r;
    overlap_uu_rl = trans(psi_ueff_r)*psi_ueff_l;

    invover_uu_lr = inv(overlap_uu_lr);
    invover_uu_rl = inv(overlap_uu_rl);

    for(unsigned int alpha=0; alpha<nlead; alpha++)
    {
        gamma_basis.slice(alpha) = trans(psi_ueff_l)
                                  *Gamma(nsite,nlead,lcontact,rcontact,
                                         lcontact2,rcontact2,lead_row,
                                         lambda_strength,gamma_strength,
                                         lead_hop,gs_eigvec,systype,sc)
                                         .slice(alpha)*psi_ueff_l;
    } // alpha<nlead

	unsigned int elems = ntstep/size;
    cx_mat temp(nsite,nsite), temp2(nsite,nsite), temp3(nsite,nsite), temp4(nsite,nsite);

/*
 * This optimization trick includes only the zeroth mode Bessel 
 * functions when the amplitude of the second harmonic is zero.
 */
    int smin, smax;
    if(ampl_rat == 0.0)
    {
        smin = 0;
        smax = 0;
    }
    else
    {
        smin = -besselorder;
        smax = besselorder;
    }

	for(unsigned int i=0; i<elems; i++) // Outer time loop (t1) [PARALLELIZED!]
    {
        unsigned int l = elems*rank + i; // Index i runs up to divided steps,
                                         // index l accounts for individual steps
        for(unsigned int ii=0; ii<ntstep; ii++) // Inner time loop (t2) [NOT PARALLELIZED! TODO: threading perhaps?]
        {
			temp.zeros();
			temp2.zeros();
            temp3.zeros();
            temp4.zeros();
            GLssNew(ii,i).zeros(nsite,nsite);
            GGtrNew(i,ii).zeros(nsite,nsite);
            if(ii == l) // Evaluating the time-diagonal only (for testing)
            {
			    for(unsigned j=0; j<nsite; j++)
			    {
				    for(unsigned k=0; k<nsite; k++)
				    {
					    for(unsigned int ga=0; ga<nlead; ga++)
					    {
						    for(int r=-besselorder; r<=besselorder; r++)
						    {
							    for(int rp=-besselorder; rp<=besselorder; rp++)
							    {
								    for(int s=smin; s<=smax; s++)
								    {
									    for(int sp=smin; sp<=smax; sp++)
									    {
										    if(r == -besselorder && rp == -besselorder && s == smin && sp == smin)
										    {
											    temp += psi_ueff_r.col(j)*gamma_basis.slice(ga)(j,k)*trans(psi_ueff_r.col(k))
											           *invover_uu_lr(j,j)*invover_uu_rl(k,k)
											           *((datum::pi/(conj(eps_ueff_l(k))-eps_ueff_l(j)))
											             *(ThetaMidpoint(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii))
											               *exp(-I*eps_ueff_l(j)*(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift))
											              +ThetaMidpoint(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l))
											               *exp(I*conj(eps_ueff_l(k))*(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift)))
											             +((I*exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))*exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, ii)))/(conj(eps_ueff_l(k))-eps_ueff_l(j)))
											              *(Psi(conj(eps_ueff_l(k))-mu,beta) - Psi(-(eps_ueff_l(j)-mu),beta))
											              );

											    temp2 += psi_ueff_r.col(j)*gamma_basis.slice(ga)(j,k)*trans(psi_ueff_r.col(k))
											           *invover_uu_lr(j,j)*invover_uu_rl(k,k)
											           *((-datum::pi/(conj(eps_ueff_l(k))-eps_ueff_l(j)))
											             *(ThetaMidpoint(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l))
											              *exp(-I*eps_ueff_l(j)*(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift))
											              +ThetaMidpoint(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii))
											              *exp(I*conj(eps_ueff_l(k))*(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift)))
											             +((I*exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, ii))*exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, l)))/(conj(eps_ueff_l(k))-eps_ueff_l(j)))
											              *(Psi(conj(eps_ueff_l(k))-mu,beta) - Psi(-(eps_ueff_l(j)-mu),beta))
											              );
										    }

										    if(rp == -besselorder && sp == smin)
										    {
                                                if(HL==2)
                                                {
                                                    temp += psi_ueff_r.col(j)*gamma_basis.slice(ga)(j,k)*trans(psi_ueff_r.col(k))
                                                           *invover_uu_lr(j,j)*invover_uu_rl(k,k)
                                                           *static_cast<double>(partpara)*I
                                                           *BesselJ(r,amplitude[ga]/(freq_mult1*frequency[ga]))*BesselJ(s,(ampl_rat*amplitude[ga])/(freq_mult2*frequency[ga]))
                                                           *(
                                                             ((exp(-I*static_cast<double>(r)*phase[ga])*exp(I*amplitude[ga]/(freq_mult1*frequency[ga])*sin(phase[ga])))
                                                             /(eps_ueff_l(j)-conj(eps_ueff_l(k))-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)))
                                                             *(exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))*exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, ii))
                                                              *(Psi(conj(eps_ueff_l(k))-mu,beta)-Psi(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),beta))
                                                              +exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, ii))*exp(-I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, l))
                                                              *(
                                                                HLT(Time(ntstep, tgridtype, dt, l), beta, conj(eps_ueff_l(k))-mu, padeorder, pade_eta, pade_zeta)
                                                               
                                                               -HLT(Time(ntstep, tgridtype, dt, l), beta, eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s), padeorder, pade_eta, pade_zeta)
                                                               ))

                                                            +((exp(I*static_cast<double>(r)*phase[ga])*exp(-I*amplitude[ga]/(freq_mult1*frequency[ga])*sin(phase[ga])))
                                                             /(conj(eps_ueff_l(k))-eps_ueff_l(j)-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)))
                                                             *(exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))*exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, ii))
                                                              *(Psi(-(eps_ueff_l(j)-mu),beta)-Psi(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),beta))
                                                              +exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))*exp(I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, ii))
                                                              *(
                                                                HLT(Time(ntstep, tgridtype, dt, ii), beta, -(eps_ueff_l(j)-mu), padeorder, pade_eta, pade_zeta)
                                                               
                                                               -HLT(Time(ntstep, tgridtype, dt, ii), beta, -(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)), padeorder, pade_eta, pade_zeta)
                                                               ))
                                                             );
                                                }
                                                else
                                                {
                                                    temp += psi_ueff_r.col(j)*gamma_basis.slice(ga)(j,k)*trans(psi_ueff_r.col(k))
                                                           *invover_uu_lr(j,j)*invover_uu_rl(k,k)
                                                           *static_cast<double>(partpara)*I
                                                           *BesselJ(r,amplitude[ga]/(freq_mult1*frequency[ga]))*BesselJ(s,(ampl_rat*amplitude[ga])/(freq_mult2*frequency[ga]))
                                                           *(
                                                             ((exp(-I*static_cast<double>(r)*phase[ga])*exp(I*amplitude[ga]/(freq_mult1*frequency[ga])*sin(phase[ga])))
                                                             /(eps_ueff_l(j)-conj(eps_ueff_l(k))-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)))
                                                             *(exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))*exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, ii))
                                                              *(Psi(conj(eps_ueff_l(k))-mu,beta)-Psi(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),beta))
                                                              +exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, ii))*exp(-I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, l))
                                                              *(Phibar(conj(eps_ueff_l(k))-mu,Time(ntstep, tgridtype, dt, l),beta)
                                                                
                                                               -Phibar(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),Time(ntstep, tgridtype, dt, l),beta)
                                                               
                                                               ))

                                                            +((exp(I*static_cast<double>(r)*phase[ga])*exp(-I*amplitude[ga]/(freq_mult1*frequency[ga])*sin(phase[ga])))
                                                             /(conj(eps_ueff_l(k))-eps_ueff_l(j)-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)))
                                                             *(exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))*exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, ii))
                                                              *(Psi(-(eps_ueff_l(j)-mu),beta)-Psi(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),beta))
                                                              +exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))*exp(I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, ii))
                                                              *(Phibar(-(eps_ueff_l(j)-mu),Time(ntstep, tgridtype, dt, ii),beta)
                                                                
                                                               -Phibar(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),Time(ntstep, tgridtype, dt, ii),beta)
                                                               
                                                               ))
                                                             );
                                                }

                                                if(HL==2)
                                                {
                                                    temp2 += psi_ueff_r.col(j)*gamma_basis.slice(ga)(j,k)*trans(psi_ueff_r.col(k))
                                                           *invover_uu_lr(j,j)*invover_uu_rl(k,k)
                                                           *static_cast<double>(partpara)*I
                                                           *BesselJ(r,amplitude[ga]/(freq_mult1*frequency[ga]))*BesselJ(s,(ampl_rat*amplitude[ga])/(freq_mult2*frequency[ga]))
                                                           *(
                                                             ((exp(-I*static_cast<double>(r)*phase[ga])*exp(I*amplitude[ga]/(freq_mult1*frequency[ga])*sin(phase[ga])))
                                                             /(eps_ueff_l(j)-conj(eps_ueff_l(k))-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)))
                                                             *(exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, ii))*exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, l))
                                                              *(Psi(conj(eps_ueff_l(k))-mu,beta)-Psi(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),beta))
                                                              +exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, l))*exp(-I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, ii))
                                                              *(
                                                                HLT(Time(ntstep, tgridtype, dt, ii), beta, conj(eps_ueff_l(k))-mu, padeorder, pade_eta, pade_zeta)
                                                               
                                                               -HLT(Time(ntstep, tgridtype, dt, ii), beta, eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s), padeorder, pade_eta, pade_zeta)
                                                               ))

                                                            +((exp(I*static_cast<double>(r)*phase[ga])*exp(-I*amplitude[ga]/(freq_mult1*frequency[ga])*sin(phase[ga])))
                                                             /(conj(eps_ueff_l(k))-eps_ueff_l(j)-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)))
                                                             *(exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, ii))*exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, l))
                                                              *(Psi(-(eps_ueff_l(j)-mu),beta)-Psi(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),beta))
                                                              +exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, ii))*exp(I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, l))
                                                              *(
                                                                HLT(Time(ntstep, tgridtype, dt, l), beta, -(eps_ueff_l(j)-mu), padeorder, pade_eta, pade_zeta)
                                                               
                                                               -HLT(Time(ntstep, tgridtype, dt, l), beta, -(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)), padeorder, pade_eta, pade_zeta)
                                                               ))
                                                             );
                                                 }
                                                 else
                                                 {
                                                    temp2 += psi_ueff_r.col(j)*gamma_basis.slice(ga)(j,k)*trans(psi_ueff_r.col(k))
                                                           *invover_uu_lr(j,j)*invover_uu_rl(k,k)
                                                           *static_cast<double>(partpara)*I
                                                           *BesselJ(r,amplitude[ga]/(freq_mult1*frequency[ga]))*BesselJ(s,(ampl_rat*amplitude[ga])/(freq_mult2*frequency[ga]))
                                                           *(
                                                             ((exp(-I*static_cast<double>(r)*phase[ga])*exp(I*amplitude[ga]/(freq_mult1*frequency[ga])*sin(phase[ga])))
                                                             /(eps_ueff_l(j)-conj(eps_ueff_l(k))-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)))
                                                             *(exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, ii))*exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, l))
                                                              *(Psi(conj(eps_ueff_l(k))-mu,beta)-Psi(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),beta))
                                                              +exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, l))*exp(-I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, ii))
                                                              *(Phibar(conj(eps_ueff_l(k))-mu,Time(ntstep, tgridtype, dt, ii),beta)
                                                                
                                                               -Phibar(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),Time(ntstep, tgridtype, dt, ii),beta)
                                                               
                                                               ))

                                                            +((exp(I*static_cast<double>(r)*phase[ga])*exp(-I*amplitude[ga]/(freq_mult1*frequency[ga])*sin(phase[ga])))
                                                             /(conj(eps_ueff_l(k))-eps_ueff_l(j)-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)))
                                                             *(exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, ii))*exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, l))
                                                              *(Psi(-(eps_ueff_l(j)-mu),beta)-Psi(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),beta))
                                                              +exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, ii))*exp(I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, l))
                                                              *(Phibar(-(eps_ueff_l(j)-mu),Time(ntstep, tgridtype, dt, l),beta)
                                                                
                                                               -Phibar(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),Time(ntstep, tgridtype, dt, l),beta)
                                                               
                                                               ))
                                                             );
                                                 }

										    }

                                            if(HL==2)
                                            {
                                                temp += psi_ueff_r.col(j)*gamma_basis.slice(ga)(j,k)*trans(psi_ueff_r.col(k))
                                                       *invover_uu_lr(j,j)*invover_uu_rl(k,k)
                                                       *I*BesselJ(r,amplitude[ga]/(freq_mult1*frequency[ga]))*BesselJ(rp,amplitude[ga]/(freq_mult1*frequency[ga]))
                                                         *BesselJ(s,(ampl_rat*amplitude[ga])/(freq_mult2*frequency[ga]))*BesselJ(sp,(ampl_rat*amplitude[ga])/(freq_mult2*frequency[ga]))
                                                       *((exp(-I*(static_cast<double>(r)-static_cast<double>(rp))*phase[ga]))
                                                        /(eps_ueff_l(j)-conj(eps_ueff_l(k))-frequency[ga]*(freq_mult1*(r-rp)+freq_mult2*(s-sp))))
                                                       *(
                                                          exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))*exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, ii))
                                                          *(Psi(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),beta)
                                                           -Psi(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp)),beta))

                                                         +exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, ii))*exp(-I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, l))
                                                          *(
                                                            HLT(Time(ntstep, tgridtype, dt, l), beta, eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s), padeorder, pade_eta, pade_zeta)
                                                           
                                                           -HLT(Time(ntstep, tgridtype, dt, l), beta, conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp), padeorder, pade_eta, pade_zeta)
                                                           )
                                                           

                                                         +exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))*exp(I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*rp+freq_mult2*sp))*Time(ntstep, tgridtype, dt, ii))
                                                          *(
                                                            HLT(Time(ntstep, tgridtype, dt, ii), beta, -(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)), padeorder, pade_eta, pade_zeta)
                                                           
                                                           -HLT(Time(ntstep, tgridtype, dt, ii), beta, -(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp)), padeorder, pade_eta, pade_zeta)
                                                           )

                                                         +ThetaMidpoint(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii))
                                                          *(exp(-I*frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s))*Time(ntstep, tgridtype, dt, l))
                                                           *exp(I*frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp))*Time(ntstep, tgridtype, dt, ii))
                                                           *exp(-I*(mu+constpart[ga])*(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift))
                                                           *(
                                                             HLT(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift, beta, conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp)), padeorder, pade_eta, pade_zeta)
                                                            
                                                            -HLT(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift, beta, eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s)), padeorder, pade_eta, pade_zeta)
                                                            )
                                                           +exp(-I*eps_ueff_l(j)*(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift))
                                                           *exp(-I*frequency[ga]*(freq_mult1*(static_cast<double>(r)-static_cast<double>(rp))
                                                                                 +freq_mult2*(static_cast<double>(s)-static_cast<double>(sp)))*Time(ntstep, tgridtype, dt, ii))
                                                           *(Psi(-(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),beta)
                                                            -Psi(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),beta))
                                                           )

                                                         +ThetaMidpoint(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l))
                                                          *(exp(-I*frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s))*Time(ntstep, tgridtype, dt, l))
                                                           *exp(I*frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp))*Time(ntstep, tgridtype, dt, ii))
                                                           *exp(I*(mu+constpart[ga])*(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift))
                                                           *(
                                                             HLT(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift, beta, -(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp))), padeorder, pade_eta, pade_zeta)
                                                            
                                                            -HLT(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift, beta, -(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s))), padeorder, pade_eta, pade_zeta)
                                                            )
                                                           +exp(I*conj(eps_ueff_l(k))*(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift))
                                                           *exp(-I*frequency[ga]*(freq_mult1*(static_cast<double>(r)-static_cast<double>(rp))
                                                                                 +freq_mult2*(static_cast<double>(s)-static_cast<double>(sp)))*Time(ntstep, tgridtype, dt, l))
                                                           *(Psi(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp)),beta)
                                                            -Psi(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp),beta))
                                                           )
                                                        );
                                            }
                                            else
                                            {
                                                temp += psi_ueff_r.col(j)*gamma_basis.slice(ga)(j,k)*trans(psi_ueff_r.col(k))
                                                       *invover_uu_lr(j,j)*invover_uu_rl(k,k)
                                                       *I*BesselJ(r,amplitude[ga]/(freq_mult1*frequency[ga]))*BesselJ(rp,amplitude[ga]/(freq_mult1*frequency[ga]))
                                                         *BesselJ(s,(ampl_rat*amplitude[ga])/(freq_mult2*frequency[ga]))*BesselJ(sp,(ampl_rat*amplitude[ga])/(freq_mult2*frequency[ga]))
                                                       *((exp(-I*(static_cast<double>(r)-static_cast<double>(rp))*phase[ga]))
                                                        /(eps_ueff_l(j)-conj(eps_ueff_l(k))-frequency[ga]*(freq_mult1*(r-rp)+freq_mult2*(s-sp))))
                                                       *(
                                                          exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))*exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, ii))
                                                          *(Psi(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),beta)
                                                           -Psi(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp)),beta))

                                                         +exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, ii))*exp(-I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, l))
                                                          *(Phibar(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),Time(ntstep, tgridtype, dt, l),beta)
                                                            
                                                           -Phibar(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp),Time(ntstep, tgridtype, dt, l),beta)
                                                           
                                                           )
                                                           

                                                         +exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))*exp(I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*rp+freq_mult2*sp))*Time(ntstep, tgridtype, dt, ii))
                                                          *(Phibar(-(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),Time(ntstep, tgridtype, dt, ii),beta)
                                                            
                                                           -Phibar(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp)),Time(ntstep, tgridtype, dt, ii),beta)
                                                           
                                                           )

                                                         +ThetaMidpoint(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii))
                                                          *(exp(-I*frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s))*Time(ntstep, tgridtype, dt, l))
                                                           *exp(I*frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp))*Time(ntstep, tgridtype, dt, ii))
                                                           *exp(-I*(mu+constpart[ga])*(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift))
                                                           *(Phibar(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp)),
                                                                    Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift,beta)
                                                             
                                                            -Phibar(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s)),
                                                                    Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift,beta)
                                                            
                                                            )
                                                           +exp(-I*eps_ueff_l(j)*(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift))
                                                           *exp(-I*frequency[ga]*(freq_mult1*(static_cast<double>(r)-static_cast<double>(rp))
                                                                                 +freq_mult2*(static_cast<double>(s)-static_cast<double>(sp)))*Time(ntstep, tgridtype, dt, ii))
                                                           *(Psi(-(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),beta)
                                                            -Psi(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),beta))
                                                           )

                                                         +ThetaMidpoint(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l))
                                                          *(exp(-I*frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s))*Time(ntstep, tgridtype, dt, l))
                                                           *exp(I*frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp))*Time(ntstep, tgridtype, dt, ii))
                                                           *exp(I*(mu+constpart[ga])*(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift))
                                                           *(Phibar(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp))),
                                                                    Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift,beta)
                                                             
                                                            -Phibar(-(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s))),
                                                                    Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift,beta)
                                                            
                                                            )
                                                           +exp(I*conj(eps_ueff_l(k))*(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift))
                                                           *exp(-I*frequency[ga]*(freq_mult1*(static_cast<double>(r)-static_cast<double>(rp))
                                                                                 +freq_mult2*(static_cast<double>(s)-static_cast<double>(sp)))*Time(ntstep, tgridtype, dt, l))
                                                           *(Psi(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp)),beta)
                                                            -Psi(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp),beta))
                                                           )
                                                        );
                                            }

                                            if(HL==2)
                                            {
                                                temp2 += psi_ueff_r.col(j)*gamma_basis.slice(ga)(j,k)*trans(psi_ueff_r.col(k))
                                                       *invover_uu_lr(j,j)*invover_uu_rl(k,k)
                                                       *I*BesselJ(r,amplitude[ga]/(freq_mult1*frequency[ga]))*BesselJ(rp,amplitude[ga]/(freq_mult1*frequency[ga]))
                                                         *BesselJ(s,(ampl_rat*amplitude[ga])/(freq_mult2*frequency[ga]))*BesselJ(sp,(ampl_rat*amplitude[ga])/(freq_mult2*frequency[ga]))
                                                       *((exp(-I*(static_cast<double>(r)-static_cast<double>(rp))*phase[ga]))
                                                        /(eps_ueff_l(j)-conj(eps_ueff_l(k))-frequency[ga]*(freq_mult1*(r-rp)+freq_mult2*(s-sp))))
                                                       *(
                                                          exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, ii))*exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, l))
                                                          *(Psi(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),beta)
                                                           -Psi(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp)),beta))

                                                         +exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, l))*exp(-I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, ii))
                                                          *(
                                                            HLT(Time(ntstep, tgridtype, dt, ii), beta, eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s), padeorder, pade_eta, pade_zeta)
                                                           
                                                           -HLT(Time(ntstep, tgridtype, dt, ii), beta, conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp), padeorder, pade_eta, pade_zeta)
                                                           )

                                                         +exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, ii))*exp(I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*rp+freq_mult2*sp))*Time(ntstep, tgridtype, dt, l))
                                                          *(
                                                            HLT(Time(ntstep, tgridtype, dt, l), beta, -(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)), padeorder, pade_eta, pade_zeta)
                                                           
                                                           -HLT(Time(ntstep, tgridtype, dt, l), beta, -(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp)), padeorder, pade_eta, pade_zeta)
                                                           )

                                                         +ThetaMidpoint(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l))
                                                          *(exp(-I*frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s))*Time(ntstep, tgridtype, dt, ii))
                                                           *exp(I*frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp))*Time(ntstep, tgridtype, dt, l))
                                                           *exp(-I*(mu+constpart[ga])*(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift))
                                                           *(
                                                             HLT(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift, beta, conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp)), padeorder, pade_eta, pade_zeta)
                                                            
                                                            -HLT(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift, beta, eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s)), padeorder, pade_eta, pade_zeta)
                                                                    )
                                                           +exp(-I*eps_ueff_l(j)*(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift))
                                                           *exp(-I*frequency[ga]*(freq_mult1*(static_cast<double>(r)-static_cast<double>(rp))
                                                                                 +freq_mult2*(static_cast<double>(s)-static_cast<double>(sp)))*Time(ntstep, tgridtype, dt, l))
                                                           *(Psi(-(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),beta)
                                                            -Psi(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),beta))
                                                           )

                                                         +ThetaMidpoint(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii))
                                                          *(exp(-I*frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s))*Time(ntstep, tgridtype, dt, ii))
                                                           *exp(I*frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp))*Time(ntstep, tgridtype, dt, l))
                                                           *exp(I*(mu+constpart[ga])*(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift))
                                                           *(
                                                             HLT(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift, beta, -(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp))), padeorder, pade_eta, pade_zeta)
                                                            
                                                            -HLT(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift, beta, -(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s))), padeorder, pade_eta, pade_zeta)
                                                                    )
                                                           +exp(I*conj(eps_ueff_l(k))*(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift))
                                                           *exp(-I*frequency[ga]*(freq_mult1*(static_cast<double>(r)-static_cast<double>(rp))
                                                                                 +freq_mult2*(static_cast<double>(s)-static_cast<double>(sp)))*Time(ntstep, tgridtype, dt, ii))
                                                           *(Psi(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp)),beta)
                                                            -Psi(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp),beta))
                                                           )
                                                        );
                                            }
                                            else
                                            {
                                                temp2 += psi_ueff_r.col(j)*gamma_basis.slice(ga)(j,k)*trans(psi_ueff_r.col(k))
                                                       *invover_uu_lr(j,j)*invover_uu_rl(k,k)
                                                       *I*BesselJ(r,amplitude[ga]/(freq_mult1*frequency[ga]))*BesselJ(rp,amplitude[ga]/(freq_mult1*frequency[ga]))
                                                         *BesselJ(s,(ampl_rat*amplitude[ga])/(freq_mult2*frequency[ga]))*BesselJ(sp,(ampl_rat*amplitude[ga])/(freq_mult2*frequency[ga]))
                                                       *((exp(-I*(static_cast<double>(r)-static_cast<double>(rp))*phase[ga]))
                                                        /(eps_ueff_l(j)-conj(eps_ueff_l(k))-frequency[ga]*(freq_mult1*(r-rp)+freq_mult2*(s-sp))))
                                                       *(
                                                          exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, ii))*exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, l))
                                                          *(Psi(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),beta)
                                                           -Psi(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp)),beta))

                                                         +exp(I*conj(eps_ueff_l(k))*Time(ntstep, tgridtype, dt, l))*exp(-I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, ii))
                                                          *(Phibar(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),Time(ntstep, tgridtype, dt, ii),beta)
                                                            
                                                           -Phibar(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp),Time(ntstep, tgridtype, dt, ii),beta)
                                                           
                                                           )

                                                         +exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, ii))*exp(I*(mu+constpart[ga]+frequency[ga]*(freq_mult1*rp+freq_mult2*sp))*Time(ntstep, tgridtype, dt, l))
                                                          *(Phibar(-(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),Time(ntstep, tgridtype, dt, l),beta)
                                                            
                                                           -Phibar(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp)),Time(ntstep, tgridtype, dt, l),beta)
                                                           
                                                           )

                                                         +ThetaMidpoint(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l))
                                                          *(exp(-I*frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s))*Time(ntstep, tgridtype, dt, ii))
                                                           *exp(I*frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp))*Time(ntstep, tgridtype, dt, l))
                                                           *exp(-I*(mu+constpart[ga])*(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift))
                                                           *(Phibar(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp)),
                                                                    Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift,beta)
                                                             
                                                            -Phibar(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s)),
                                                                    Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift,beta)
                                                            
                                                                    )
                                                           +exp(-I*eps_ueff_l(j)*(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift))
                                                           *exp(-I*frequency[ga]*(freq_mult1*(static_cast<double>(r)-static_cast<double>(rp))
                                                                                 +freq_mult2*(static_cast<double>(s)-static_cast<double>(sp)))*Time(ntstep, tgridtype, dt, l))
                                                           *(Psi(-(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s)),beta)
                                                            -Psi(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*r+freq_mult2*s),beta))
                                                           )

                                                         +ThetaMidpoint(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii))
                                                          *(exp(-I*frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s))*Time(ntstep, tgridtype, dt, ii))
                                                           *exp(I*frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp))*Time(ntstep, tgridtype, dt, l))
                                                           *exp(I*(mu+constpart[ga])*(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift))
                                                           *(Phibar(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp))),
                                                                    Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift,beta)
                                                             
                                                            -Phibar(-(eps_ueff_l(j)-mu-constpart[ga]-frequency[ga]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s))),
                                                                    Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift,beta)
                                                            
                                                                    )
                                                           +exp(I*conj(eps_ueff_l(k))*(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift))
                                                           *exp(-I*frequency[ga]*(freq_mult1*(static_cast<double>(r)-static_cast<double>(rp))
                                                                                 +freq_mult2*(static_cast<double>(s)-static_cast<double>(sp)))*Time(ntstep, tgridtype, dt, ii))
                                                           *(Psi(-(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp)),beta)
                                                            -Psi(conj(eps_ueff_l(k))-mu-constpart[ga]-frequency[ga]*(freq_mult1*rp+freq_mult2*sp),beta))
                                                           )
                                                        );
                                            }

									    }	// -besselorder <= sp <= besselorder
								    }	// -besselorder <= rp <= besselorder
							    }	// -besselorder <= s <= besselorder
						    }	// -besselorder <= r <= besselorder
					    }	// ga < nlead
				    }	// k < nsite
			    }	// j < nsite

                GGtrNew(i,ii) = cx_double(1.0,0.0)/(2.0*datum::pi)*temp;
			    GLssNew(ii,i) = cx_double(1.0,0.0)/(2.0*datum::pi)*temp2;

			    temp.zeros();
			    temp2.zeros();
                temp3.zeros();
                temp4.zeros();

			    for(unsigned int alpha=0; alpha<nlead; alpha++)
			    {
                    LambdaPlus(ii,i,alpha).zeros(nsite,nsite);
                    LambdaMinus(i,ii,alpha).zeros(nsite,nsite);
                    LambdaPlusD(i,ii,alpha).zeros(nsite,nsite);
                    LambdaMinusD(ii,i,alpha).zeros(nsite,nsite);
				    for(unsigned j=0; j<nsite; j++)
				    {
					    for(int r=-besselorder; r<=besselorder; r++)
					    {
                            for(int s=smin; s<=smax; s++)
						    {
							    if(r == -besselorder && s == smin)
							    {
                                    if(HL==1)
                                    {
                                        temp += Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                      lead_row,lambda_strength,gamma_strength,lead_hop,
                                                      gs_eigvec,systype,sc).slice(alpha)
                                               *psi_ueff_l.col(j)*trans(psi_ueff_r.col(j))
                                               *invover_uu_rl(j,j)*(-I/(2.0*datum::pi))
                                               *exp(I*conj(eps_ueff_l(j))*Time(ntstep, tgridtype, dt, l))
                                               *exp(-I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, ii))
                                               *exp(-I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)+phase[alpha]))
                                               *exp(-I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)))
                                               *static_cast<double>(partpara)*exp(I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(phase[alpha]))
                                               *HLT(Time(ntstep, tgridtype, dt, ii), beta, conj(eps_ueff_l(j))-mu, padeorder, pade_eta, pade_zeta);
                                   }
                                   else
                                   {
                                        temp += Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                      lead_row,lambda_strength,gamma_strength,lead_hop,
                                                      gs_eigvec,systype,sc).slice(alpha)
                                               *psi_ueff_l.col(j)*trans(psi_ueff_r.col(j))
                                               *invover_uu_rl(j,j)*(-I/(2.0*datum::pi))
                                               *exp(I*conj(eps_ueff_l(j))*Time(ntstep, tgridtype, dt, l))
                                               *exp(-I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, ii))
                                               *exp(-I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)+phase[alpha]))
                                               *exp(-I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)))
                                               *static_cast<double>(partpara)*exp(I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(phase[alpha]))
                                               *Phibar(conj(eps_ueff_l(j))-mu,Time(ntstep, tgridtype, dt, ii),beta);
                                    }

                                    if(HL==1)
                                    {
                                        temp2 += Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                       lead_row,lambda_strength,gamma_strength,lead_hop,
                                                       gs_eigvec,systype,sc).slice(alpha)
                                               *psi_ueff_l.col(j)*trans(psi_ueff_r.col(j))
                                               *invover_uu_rl(j,j)*(-I/(2.0*datum::pi))
                                               *exp(I*conj(eps_ueff_l(j))*Time(ntstep, tgridtype, dt, ii))
                                               *exp(-I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, l))
                                               *exp(-I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, l)+phase[alpha]))
                                               *exp(-I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, l)))
                                               *static_cast<double>(partpara)*exp(I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(phase[alpha]))
                                               *HLT(Time(ntstep, tgridtype, dt, l), beta, conj(eps_ueff_l(j))-mu, padeorder, pade_eta, pade_zeta);
                                    }
                                    else
                                    {
                                        temp2 += Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                       lead_row,lambda_strength,gamma_strength,lead_hop,
                                                       gs_eigvec,systype,sc).slice(alpha)
                                               *psi_ueff_l.col(j)*trans(psi_ueff_r.col(j))
                                               *invover_uu_rl(j,j)*(-I/(2.0*datum::pi))
                                               *exp(I*conj(eps_ueff_l(j))*Time(ntstep, tgridtype, dt, ii))
                                               *exp(-I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, l))
                                               *exp(-I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, l)+phase[alpha]))
                                               *exp(-I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, l)))
                                               *static_cast<double>(partpara)*exp(I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(phase[alpha]))
                                               *Phibar(conj(eps_ueff_l(j))-mu,Time(ntstep, tgridtype, dt, l),beta);

                                    }

                                    if(HL==1)
                                    {
                                        temp3 += psi_ueff_r.col(j)*trans(psi_ueff_l.col(j))
                                                *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                       lead_row,lambda_strength,gamma_strength,lead_hop,
                                                       gs_eigvec,systype,sc).slice(alpha)
                                                *invover_uu_lr(j,j)*(I/(2.0*datum::pi))
                                                *exp(-I*(eps_ueff_l(j))*Time(ntstep, tgridtype, dt, ii))
                                                *exp(I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, l))
                                                *exp(I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, l)+phase[alpha]))
                                                *exp(I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, l)))
                                                *static_cast<double>(partpara)*exp(-I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(phase[alpha]))
                                                *HLT(Time(ntstep, tgridtype, dt, l), beta, -((eps_ueff_l(j))-mu), padeorder, pade_eta, pade_zeta);
                                    }
                                    else
                                    {
                                        temp3 += psi_ueff_r.col(j)*trans(psi_ueff_l.col(j))
                                                *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                       lead_row,lambda_strength,gamma_strength,lead_hop,
                                                       gs_eigvec,systype,sc).slice(alpha)
                                                *invover_uu_lr(j,j)*(I/(2.0*datum::pi))
                                                *exp(-I*(eps_ueff_l(j))*Time(ntstep, tgridtype, dt, ii))
                                                *exp(I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, l))
                                                *exp(I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, l)+phase[alpha]))
                                                *exp(I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, l)))
                                                *static_cast<double>(partpara)*exp(-I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(phase[alpha]))
                                                *Phibar(-(eps_ueff_l(j)-mu),Time(ntstep, tgridtype, dt, l),beta);
                                    }

                                    if(HL==1)
                                    {
                                        temp4 += psi_ueff_r.col(j)*trans(psi_ueff_l.col(j))
                                               *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                       lead_row,lambda_strength,gamma_strength,lead_hop,
                                                       gs_eigvec,systype,sc).slice(alpha)
                                               *invover_uu_lr(j,j)*(I/(2.0*datum::pi))
                                               *exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))
                                               *exp(I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, ii))
                                               *exp(I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)+phase[alpha]))
                                               *exp(I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)))
                                               *static_cast<double>(partpara)*exp(-I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(phase[alpha]))
                                               *HLT(Time(ntstep, tgridtype, dt, ii), beta, -((eps_ueff_l(j))-mu), padeorder, pade_eta, pade_zeta);
                                    }
                                    else
                                    {
                                        temp4 += psi_ueff_r.col(j)*trans(psi_ueff_l.col(j))
                                               *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                       lead_row,lambda_strength,gamma_strength,lead_hop,
                                                       gs_eigvec,systype,sc).slice(alpha)
                                               *invover_uu_lr(j,j)*(I/(2.0*datum::pi))
                                               *exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))
                                               *exp(I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, ii))
                                               *exp(I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)+phase[alpha]))
                                               *exp(I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)))
                                               *static_cast<double>(partpara)*exp(-I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(phase[alpha]))
                                               *Phibar(-(eps_ueff_l(j)-mu),Time(ntstep, tgridtype, dt, ii),beta);
                                    }

                                    //

								    temp += Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
								                  lead_row,lambda_strength,gamma_strength,lead_hop,
								                  gs_eigvec,systype,sc).slice(alpha)
								           *psi_ueff_l.col(j)*trans(psi_ueff_r.col(j))
									       *invover_uu_rl(j,j)
									       *(-0.5)*ThetaMidpoint(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii))
									       *exp(I*conj(eps_ueff_l(j))*(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift));

								    temp2 += Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
								                   lead_row,lambda_strength,gamma_strength,lead_hop,
								                   gs_eigvec,systype,sc).slice(alpha)
								           *psi_ueff_l.col(j)*trans(psi_ueff_r.col(j))
									       *invover_uu_rl(j,j)
									       *0.5*ThetaMidpoint(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l))
									       *exp(I*conj(eps_ueff_l(j))*(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift));

								    temp3 += psi_ueff_r.col(j)*trans(psi_ueff_l.col(j))
                                           *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
								                   lead_row,lambda_strength,gamma_strength,lead_hop,
								                   gs_eigvec,systype,sc).slice(alpha)
									       *invover_uu_lr(j,j)
									       *(-0.5)*ThetaMidpoint(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l))
									       *exp(-I*(eps_ueff_l(j))*(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift));

								    temp4 += psi_ueff_r.col(j)*trans(psi_ueff_l.col(j))
                                            *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
								                   lead_row,lambda_strength,gamma_strength,lead_hop,
								                   gs_eigvec,systype,sc).slice(alpha)
 									        *invover_uu_lr(j,j)
 									        *0.5*ThetaMidpoint(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii))
									        *exp(-I*eps_ueff_l(j)*(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift));

							    }

                                if(HL==1)
                                {
                                    temp += Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                  lead_row,lambda_strength,gamma_strength,lead_hop,
                                                  gs_eigvec,systype,sc).slice(alpha)
                                           *psi_ueff_l.col(j)*trans(psi_ueff_r.col(j))
                                           *invover_uu_rl(j,j)*(-I/(2.0*datum::pi))
                                           *exp(I*conj(eps_ueff_l(j))*Time(ntstep, tgridtype, dt, l))
                                           *exp(-I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, ii))
                                           *exp(-I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)+phase[alpha]))
                                           *exp(-I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)))
                                           *BesselJ(r,amplitude[alpha]/(freq_mult1*frequency[alpha]))*BesselJ(s,(ampl_rat*amplitude[alpha])/(freq_mult2*frequency[alpha]))
                                           *exp(I*static_cast<double>(r)*phase[alpha])
                                           *(
                                              ThetaMidpoint(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii))
                                             *exp(-I*(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s)))*Time(ntstep, tgridtype, dt, ii))
                                             *( Psi(-(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),beta)
                                               -Psi(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s),beta) )

                                             -HLT(Time(ntstep, tgridtype, dt, ii), beta, conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s), padeorder, pade_eta, pade_zeta)

                                             +exp(-I*(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, l))
                                             *(ThetaMidpoint(Time(ntstep, tgridtype, dt, l) - Time(ntstep, tgridtype, dt, ii))
                                              *HLT(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift, beta, -(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)), padeorder, pade_eta, pade_zeta)
                                              +ThetaMidpoint(Time(ntstep, tgridtype, dt, ii) - Time(ntstep, tgridtype, dt, l))
                                              *HLT(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift, beta, (conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)), padeorder, pade_eta, pade_zeta)
                                              )
                                            );
                                }
                                else
                                {
                                    temp += Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                  lead_row,lambda_strength,gamma_strength,lead_hop,
                                                  gs_eigvec,systype,sc).slice(alpha)
                                           *psi_ueff_l.col(j)*trans(psi_ueff_r.col(j))
                                           *invover_uu_rl(j,j)*(-I/(2.0*datum::pi))
                                           *exp(I*conj(eps_ueff_l(j))*Time(ntstep, tgridtype, dt, l))
                                           *exp(-I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, ii))
                                           *exp(-I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)+phase[alpha]))
                                           *exp(-I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)))
                                           *BesselJ(r,amplitude[alpha]/(freq_mult1*frequency[alpha]))*BesselJ(s,(ampl_rat*amplitude[alpha])/(freq_mult2*frequency[alpha]))
                                           *exp(I*static_cast<double>(r)*phase[alpha])
                                           *(
                                              ThetaMidpoint(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii))
                                             *exp(-I*(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s)))*Time(ntstep, tgridtype, dt, ii))
                                             *( Psi(-(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),beta)
                                               -Psi(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s),beta) )

                                             -Phibar(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s),Time(ntstep, tgridtype, dt, ii),beta)

                                             +exp(-I*(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, l))
                                             *(ThetaMidpoint(Time(ntstep, tgridtype, dt, l) - Time(ntstep, tgridtype, dt, ii))
                                              *Phibar(-(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift,beta)
                                              +ThetaMidpoint(Time(ntstep, tgridtype, dt, ii) - Time(ntstep, tgridtype, dt, l))
                                              *Phibar(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s),Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift,beta)
                                              )
                                            );
                                }

                                if(HL==1)
                                {
                                    temp2 += Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                   lead_row,lambda_strength,gamma_strength,lead_hop,
                                                   gs_eigvec,systype,sc).slice(alpha)
                                           *psi_ueff_l.col(j)*trans(psi_ueff_r.col(j))
                                           *invover_uu_rl(j,j)*(-I/(2.0*datum::pi))
                                           *exp(I*conj(eps_ueff_l(j))*Time(ntstep, tgridtype, dt, ii))
                                           *exp(-I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, l))
                                           *exp(-I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, l)+phase[alpha]))
                                           *exp(-I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, l)))
                                           *BesselJ(r,amplitude[alpha]/(freq_mult1*frequency[alpha]))*BesselJ(s,(ampl_rat*amplitude[alpha])/(freq_mult2*frequency[alpha]))
                                           *exp(I*static_cast<double>(r)*phase[alpha])
                                           *(
                                              ThetaMidpoint(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l))
                                             *exp(-I*(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s)))*Time(ntstep, tgridtype, dt, l))
                                             *( Psi(-(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),beta)
                                               -Psi(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s),beta) )

                                             -HLT(Time(ntstep, tgridtype, dt, l), beta, conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s), padeorder, pade_eta, pade_zeta)

                                             +exp(-I*(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, ii))
                                             *(ThetaMidpoint(Time(ntstep, tgridtype, dt, l) - Time(ntstep, tgridtype, dt, ii))
                                              *HLT(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift, beta, (conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)), padeorder, pade_eta, pade_zeta)
                                              +ThetaMidpoint(Time(ntstep, tgridtype, dt, ii) - Time(ntstep, tgridtype, dt, l))
                                              *HLT(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift, beta, -(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)), padeorder, pade_eta, pade_zeta)
                                              )
                                            );
                                }
                                else
                                {
                                    temp2 += Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                   lead_row,lambda_strength,gamma_strength,lead_hop,
                                                   gs_eigvec,systype,sc).slice(alpha)
                                           *psi_ueff_l.col(j)*trans(psi_ueff_r.col(j))
                                           *invover_uu_rl(j,j)*(-I/(2.0*datum::pi))
                                           *exp(I*conj(eps_ueff_l(j))*Time(ntstep, tgridtype, dt, ii))
                                           *exp(-I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, l))
                                           *exp(-I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, l)+phase[alpha]))
                                           *exp(-I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, l)))
                                           *BesselJ(r,amplitude[alpha]/(freq_mult1*frequency[alpha]))*BesselJ(s,(ampl_rat*amplitude[alpha])/(freq_mult2*frequency[alpha]))
                                           *exp(I*static_cast<double>(r)*phase[alpha])
                                           *(
                                              ThetaMidpoint(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l))
                                             *exp(-I*(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s)))*Time(ntstep, tgridtype, dt, l))
                                             *( Psi(-(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),beta)
                                               -Psi(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s),beta) )

                                             -Phibar(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s),Time(ntstep, tgridtype, dt, l),beta)

                                             +exp(-I*(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, ii))
                                             *(ThetaMidpoint(Time(ntstep, tgridtype, dt, l) - Time(ntstep, tgridtype, dt, ii))
                                              *Phibar(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s),Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift,beta)
                                              +ThetaMidpoint(Time(ntstep, tgridtype, dt, ii) - Time(ntstep, tgridtype, dt, l))
                                              *Phibar(-(conj(eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift,beta)
                                              )
                                            );
                                }

                                if(HL==1)
                                {
                                    temp3 += psi_ueff_r.col(j)*trans(psi_ueff_l.col(j))
                                           *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                   lead_row,lambda_strength,gamma_strength,lead_hop,
                                                   gs_eigvec,systype,sc).slice(alpha)
                                           *invover_uu_lr(j,j)*(I/(2.0*datum::pi))
                                           *exp(-I*(eps_ueff_l(j))*Time(ntstep, tgridtype, dt, ii))
                                           *exp(I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, l))
                                           *exp(I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, l)+phase[alpha]))
                                           *exp(I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, l)))
                                           *BesselJ(r,amplitude[alpha]/(freq_mult1*frequency[alpha]))*BesselJ(s,(ampl_rat*amplitude[alpha])/(freq_mult2*frequency[alpha]))
                                           *exp(-I*static_cast<double>(r)*phase[alpha])
                                           *(
                                              ThetaMidpoint(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l))
                                             *exp(I*(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s)))*Time(ntstep, tgridtype, dt, l))
                                             *( Psi(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s),beta)
                                               -Psi(-(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),beta) )

                                             -HLT(Time(ntstep, tgridtype, dt, l), beta, -((eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)), padeorder, pade_eta, pade_zeta)

                                             +exp(I*(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, ii))
                                             *(ThetaMidpoint(Time(ntstep, tgridtype, dt, l) - Time(ntstep, tgridtype, dt, ii))
                                              *HLT(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift, beta, -((eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)), padeorder, pade_eta, pade_zeta)
                                              +ThetaMidpoint(Time(ntstep, tgridtype, dt, ii) - Time(ntstep, tgridtype, dt, l))
                                              *HLT(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift, beta, ((eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)), padeorder, pade_eta, pade_zeta)
                                              )

                                            );
                                }
                                else
                                {
                                    temp3 += psi_ueff_r.col(j)*trans(psi_ueff_l.col(j))
                                           *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                   lead_row,lambda_strength,gamma_strength,lead_hop,
                                                   gs_eigvec,systype,sc).slice(alpha)
                                           *invover_uu_lr(j,j)*(I/(2.0*datum::pi))
                                           *exp(-I*(eps_ueff_l(j))*Time(ntstep, tgridtype, dt, ii))
                                           *exp(I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, l))
                                           *exp(I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, l)+phase[alpha]))
                                           *exp(I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, l)))
                                           *BesselJ(r,amplitude[alpha]/(freq_mult1*frequency[alpha]))*BesselJ(s,(ampl_rat*amplitude[alpha])/(freq_mult2*frequency[alpha]))
                                           *exp(-I*static_cast<double>(r)*phase[alpha])
                                           *(
                                              ThetaMidpoint(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l))
                                             *exp(I*(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s)))*Time(ntstep, tgridtype, dt, l))
                                             *( Psi(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s),beta)
                                               -Psi(-(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),beta) )

                                             -Phibar(-(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),Time(ntstep, tgridtype, dt, l),beta)

                                             +exp(I*(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, ii))
                                             *(ThetaMidpoint(Time(ntstep, tgridtype, dt, l) - Time(ntstep, tgridtype, dt, ii))
                                              *Phibar(-(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift,beta)
                                              +ThetaMidpoint(Time(ntstep, tgridtype, dt, ii) - Time(ntstep, tgridtype, dt, l))
                                              *Phibar(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s),Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift,beta)
                                              )

                                            );
                                }

                                if(HL==1)
                                {
                                    temp4 += psi_ueff_r.col(j)*trans(psi_ueff_l.col(j))
                                            *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                   lead_row,lambda_strength,gamma_strength,lead_hop,
                                                   gs_eigvec,systype,sc).slice(alpha)
                                            *invover_uu_lr(j,j)*(I/(2.0*datum::pi))
                                            *exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))
                                            *exp(I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, ii))
                                            *exp(I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)+phase[alpha]))
                                            *exp(I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)))
                                            *BesselJ(r,amplitude[alpha]/(freq_mult1*frequency[alpha]))*BesselJ(s,(ampl_rat*amplitude[alpha])/(freq_mult2*frequency[alpha]))
                                            *exp(-I*static_cast<double>(r)*phase[alpha])
                                            *(
                                               ThetaMidpoint(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii))
                                              *exp(I*(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s)))*Time(ntstep, tgridtype, dt, ii))
                                              *( Psi(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s),beta)
                                                -Psi(-(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),beta) )

                                              -HLT(Time(ntstep, tgridtype, dt, ii), beta, -((eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)), padeorder, pade_eta, pade_zeta)

                                              +exp(I*(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, l))
                                              *(ThetaMidpoint(Time(ntstep, tgridtype, dt, l) - Time(ntstep, tgridtype, dt, ii))
                                               *HLT(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift, beta, ((eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)), padeorder, pade_eta, pade_zeta)
                                               +ThetaMidpoint(Time(ntstep, tgridtype, dt, ii) - Time(ntstep, tgridtype, dt, l))
                                               *HLT(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift, beta, -((eps_ueff_l(j))-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)), padeorder, pade_eta, pade_zeta)
                                               )
                                             );
                                 }
                                 else
                                 {
                                    temp4 += psi_ueff_r.col(j)*trans(psi_ueff_l.col(j))
                                            *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                   lead_row,lambda_strength,gamma_strength,lead_hop,
                                                   gs_eigvec,systype,sc).slice(alpha)
                                            *invover_uu_lr(j,j)*(I/(2.0*datum::pi))
                                            *exp(-I*eps_ueff_l(j)*Time(ntstep, tgridtype, dt, l))
                                            *exp(I*(mu+constpart[alpha])*Time(ntstep, tgridtype, dt, ii))
                                            *exp(I*(amplitude[alpha]/(freq_mult1*frequency[alpha]))*sin(freq_mult1*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)+phase[alpha]))
                                            *exp(I*(ampl_rat*amplitude[alpha]/(freq_mult2*frequency[alpha]))*sin(freq_mult2*frequency[alpha]*Time(ntstep, tgridtype, dt, ii)))
                                            *BesselJ(r,amplitude[alpha]/(freq_mult1*frequency[alpha]))*BesselJ(s,(ampl_rat*amplitude[alpha])/(freq_mult2*frequency[alpha]))
                                            *exp(-I*static_cast<double>(r)*phase[alpha])
                                            *(
                                               ThetaMidpoint(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii))
                                              *exp(I*(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s)))*Time(ntstep, tgridtype, dt, ii))
                                              *( Psi(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s),beta)
                                                -Psi(-(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),beta) )

                                              -Phibar(-(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),Time(ntstep, tgridtype, dt, ii),beta)

                                              +exp(I*(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s))*Time(ntstep, tgridtype, dt, l))
                                              *(ThetaMidpoint(Time(ntstep, tgridtype, dt, l) - Time(ntstep, tgridtype, dt, ii))
                                               *Phibar(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s),Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift,beta)
                                               +ThetaMidpoint(Time(ntstep, tgridtype, dt, ii) - Time(ntstep, tgridtype, dt, l))
                                               *Phibar(-(eps_ueff_l(j)-mu-constpart[alpha]-frequency[alpha]*(freq_mult1*r+freq_mult2*s)),Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift,beta)
                                               )
                                             );
                                 }
						    }
					    }
				    }

				    LambdaPlus(ii,i,alpha) = temp;
                    LambdaMinus(i,ii,alpha) = temp2;
				    LambdaPlusD(i,ii,alpha) = temp3;
				    LambdaMinusD(ii,i,alpha) = temp4;

                    temp.zeros();
                    temp2.zeros();
                    temp3.zeros();
                    temp4.zeros();

                    /*

                    for(int r=-besselorder; r<=besselorder; r++)
                    {
                        for(int s=smin; s<=smax; s++)
                        {
                            for(int rp=-besselorder; rp<=besselorder; rp++)
                            {
                                for(int sp=smin; sp<=smax; sp++)
                                {
                                    if(abs(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)) < 1.0e-10)
                                    {
                                        if(r == -besselorder && s == smin && rp == -besselorder && sp == smin)
                                        {
                                            temp3 += (I/2.0)*Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
							                       lead_row,lambda_strength,gamma_strength,lead_hop,
								                   gs_eigvec,systype,sc).slice(alpha);

                                            temp4 += (-I/2.0)*Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
							                       lead_row,lambda_strength,gamma_strength,lead_hop,
								                   gs_eigvec,systype,sc).slice(alpha);
                                        }
                                    }
                                    else
                                    {
                                        temp3 -= Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                       lead_row,lambda_strength,gamma_strength,lead_hop,
                                                       gs_eigvec,systype,sc).slice(alpha)
                                                *exp(I*constpart[alpha]*(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift))
                                                *BesselJ(r,amplitude[alpha]/(freq_mult1*frequency[alpha]))*BesselJ(rp,amplitude[alpha]/(freq_mult1*frequency[alpha]))
                                                *BesselJ(s,(ampl_rat*amplitude[alpha])/(freq_mult2*frequency[alpha]))*BesselJ(sp,(ampl_rat*amplitude[alpha])/(freq_mult2*frequency[alpha]))
                                                *exp(I*(static_cast<double>(r)-static_cast<double>(rp))*phase[alpha])
                                                *exp(I*frequency[alpha]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s))*Time(ntstep, tgridtype, dt, ii))
                                                *exp(-I*frequency[alpha]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp))*Time(ntstep, tgridtype, dt, l))
                                                *exp(-I*mu*(Time(ntstep, tgridtype, dt, ii)-Time(ntstep, tgridtype, dt, l)+shift))
                                                *CosechNew(Time(ntstep, tgridtype, dt, ii), Time(ntstep, tgridtype, dt, l), beta, padeorder, pade_eta, pade_zeta);

                                        temp4 -= Gamma(nsite,nlead,lcontact,rcontact,lcontact2,rcontact2,
                                                       lead_row,lambda_strength,gamma_strength,lead_hop,
                                                       gs_eigvec,systype,sc).slice(alpha)
                                                *exp(I*constpart[alpha]*(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift))
                                                *BesselJ(r,amplitude[alpha]/(freq_mult1*frequency[alpha]))*BesselJ(rp,amplitude[alpha]/(freq_mult1*frequency[alpha]))
                                                *BesselJ(s,(ampl_rat*amplitude[alpha])/(freq_mult2*frequency[alpha]))*BesselJ(sp,(ampl_rat*amplitude[alpha])/(freq_mult2*frequency[alpha]))
                                                *exp(I*(static_cast<double>(r)-static_cast<double>(rp))*phase[alpha])
                                                *exp(I*frequency[alpha]*(freq_mult1*static_cast<double>(r)+freq_mult2*static_cast<double>(s))*Time(ntstep, tgridtype, dt, l))
                                                *exp(-I*frequency[alpha]*(freq_mult1*static_cast<double>(rp)+freq_mult2*static_cast<double>(sp))*Time(ntstep, tgridtype, dt, ii))
                                                *exp(-I*mu*(Time(ntstep, tgridtype, dt, l)-Time(ntstep, tgridtype, dt, ii)+shift))
                                                *CosechNew(Time(ntstep, tgridtype, dt, l), Time(ntstep, tgridtype, dt, ii), beta, padeorder, pade_eta, pade_zeta);
                                    }
                                }
                            }
                        }
                    }

				    SigmaLss(ii,i,alpha) = temp3;
				    SigmaGtr(i,ii,alpha) = temp4;

                    temp3.zeros();
                    temp4.zeros();

                    */

			    }   // alpha < nlead
		    }   // if time-diagonal
		}	// ii < ntstep (nonparallel time loop)
	}	// i < elems (parallel time loop)

	temp.zeros();

	for(unsigned int i=0; i<elems; i++) // Outer time loop (t1) [PARALLELIZED!]
    {
        unsigned int l = elems*rank + i; // Index i runs up to divided steps,
                                         // index l accounts for individual steps
        for(unsigned int ii=0; ii<ntstep; ii++) // Inner time loop (t2) [NOT PARALLELIZED! TODO: threading perhaps?]
        {

			Cab(i,ii).zeros(nlead,nlead);

            if(ii == l) // Evaluating the time-diagonal only (for testing)
            {
                for(unsigned int alpha=0; alpha<nlead; alpha++)
                {
					for(unsigned int be=0; be<nlead; be++)
					{
//						if(alpha == be)
//						{
//							temp += SigmaGtr(i,ii,alpha)*GLssNew(ii,i);
//							temp += GGtrNew(i,ii)*SigmaLss(ii,i,alpha);
//						}

						temp += Gamma(nsite,nlead,lcontact,rcontact,lcontact2,
								      rcontact2,lead_row,lambda_strength,
									  gamma_strength,lead_hop,gs_eigvec,systype,sc).slice(alpha)
							   *GGtrNew(i,ii)
							   *Gamma(nsite,nlead,lcontact,rcontact,lcontact2,
								      rcontact2,lead_row,lambda_strength,
									  gamma_strength,lead_hop,gs_eigvec,systype,sc).slice(be)
							   *GLssNew(ii,i);

					    temp += I*eye(nsite,nsite)*GGtrNew(i,ii)
					           *(LambdaPlus(ii,i,be)*Gamma(nsite,nlead,lcontact,rcontact,lcontact2,
								                           rcontact2,lead_row,lambda_strength,
									                       gamma_strength,lead_hop,gs_eigvec,systype,sc).slice(alpha)
								+Gamma(nsite,nlead,lcontact,rcontact,lcontact2,
								       rcontact2,lead_row,lambda_strength,
								       gamma_strength,lead_hop,gs_eigvec,systype,sc).slice(be)
								 *LambdaPlusD(i,ii,alpha));

					    temp += I*eye(nsite,nsite)
					           *(LambdaMinus(i,ii,alpha)*Gamma(nsite,nlead,lcontact,rcontact,lcontact2,
								                               rcontact2,lead_row,lambda_strength,
									                           gamma_strength,lead_hop,gs_eigvec,systype,sc).slice(be)
								+Gamma(nsite,nlead,lcontact,rcontact,lcontact2,
								       rcontact2,lead_row,lambda_strength,
								       gamma_strength,lead_hop,gs_eigvec,systype,sc).slice(alpha)
								 *LambdaMinusD(ii,i,be))
                                 *GLssNew(ii,i);

                        temp -= (LambdaPlus(ii,i,be)*LambdaMinus(i,ii,alpha)
                                +LambdaPlusD(i,ii,alpha)*LambdaMinusD(ii,i,be));

                        (Cab(i,ii))(alpha,be) = 4.0*trace(temp);

						temp.zeros();

					}   // be<nlead
                }   // alpha<nlead
            }   // if time-diagonal
        } // nonparallel time loop
    } // parallel time loop

    tmpdble2 = MPI_Wtime();
    if(rank == 0) {StopWatch(tmpdble1, tmpdble2);}

    if(rank == 0) // For testing
    {
        ofstream glssout("output/td_nop_from_glssnew.out", ios::out);
        for(unsigned int j=0; j<ntstep/size; j++)
        {
            glssout << setprecision(5) << scientific << Time(ntstep, tgridtype, dt, j)
                    << "\t" << real(-2.0*cx_double(0.0,1.0)*GLssNew(j,j)(0,0)) << endl;
        }
        glssout.close();
    }

// Calculate dipole moment (if not calculated from 1rdm earlier)
    if(calc[0]!=1)
    {
        for(unsigned int i=0; i<elems; i++)
        {
            unsigned int l = elems*rank + i;
            for(unsigned int ii=0; ii<ntstep; ii++)
            {
                if(ii == l)
                {
                    cx_double tmpcdble(0.0,0.0);
                    for(unsigned int j=0; j<nsite; j++)
                    {
                        tmpcdble += sqrt(pow(coords[j][0]*1.0e-10*cx_double(0.0,-1.0)*GLssNew(ii,i)(j,j)*datum::ec,2)
                                        +pow(coords[j][1]*1.0e-10*cx_double(0.0,-1.0)*GLssNew(ii,i)(j,j)*datum::ec,2)
                                        +pow(coords[j][2]*1.0e-10*cx_double(0.0,-1.0)*GLssNew(ii,i)(j,j)*datum::ec,2));
                    }
                    dipmom(i) = tmpcdble;
                }
            }
        }
    }
}
