// ----------------------------- //
// Include libraries and headers //
// ----------------------------- //

#include <mpi.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <complex>
#include <armadillo>
#include <ctime>
#include <limits>
#include <cstdlib>
#include <gsl/gsl_sf_psi.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>
#include <time.h>

using namespace std;
using namespace arma;

// ------------------- //
// Declare functions   //
// ------------------- //

void ReadInput(int &nsite, int &systype, int &biastype, int &nlead, int &ntstep, int &tgridtype,
               int &nwstep, int &layers, int &snaps, int &pert, int &sc, int &finitet, int &besselorder, int &NstochMax,
               vector<int> &lcontact, vector<int> &rcontact, vector<int> &lcontact2, vector<int> &rcontact2,
               vector<int> &bridgeone, vector<int> &bridgetwo, vector<int> &bridgethree, 
               double &lambda_strength, double &gamma_strength, double &mol_hop, double &lead_hop, 
               int &lead_row, int &bridge_sites, double &dt, double &bias_strength, double &cutoff,
               cx_double &sc_delta, double &beta, vector<double> &constpart, vector<double> &amplitude, 
               vector<double> &frequency, vector<double> &phase, int &freq_mult1, int &freq_mult2, double &ampl_rat,
               int &padeorder, int &subs, double &acc, int &rule, int &partpara, vector<int> &calc,
               vector<vector<double> > &coords, double &scw_t, double &scw_Vz, double &scw_aso, double &scw_Delta,
			   double &scw_mu, double &scw_gate, double &Dfluct, double &wcorr);

void SCContacts(int nsite, vector<int> lcontact, vector<int> rcontact, vector<int> lcontact2, vector<int> rcontact2, 
                vector<int> bridgeone, vector<int> bridgetwo,  vector<int> bridgethree, 
                vector<int> &lcontact_sc, vector<int> &rcontact_sc, vector<int> &lcontact2_sc, 
                vector<int> &rcontact2_sc, vector<int> &bridgeone_sc, vector<int> &bridgetwo_sc, 
                vector<int> &bridgethree_sc);

void ConstructHamiltonian(int nsite, int systype, double mol_hop, cx_mat &Hu, int layers, cx_mat &Hp, int pert, int sc,
						  double scw_t, double scw_Vz, double scw_aso, double scw_Delta, double scw_mu, double scw_gate);

void SCHamiltonian(int nsite, cx_mat Hu, cx_mat Hp, cx_mat &Hu_sc, cx_mat &Hp_sc, cx_double sc_delta);

void CalculateGS(int nsite, int ntstep, double &mu, double gamma_strength, double bias_strength,
                 cx_mat &Hu, int systype, int finitet, double beta, vec &gs_eigval, cx_mat &gs_eigvec);

void CalculateQuantities(int nsite, int nlead, int biastype, vector<int> lcontact, vector<int> rcontact, 
                         vector<int> lcontact2, vector<int> rcontact2, vector<int> bridgeone, 
                         vector<int> bridgetwo, vector<int> bridgethree, int lead_row, 
                         int bridge_sites, double bias_strength, int ntstep, int tgridtype, int sc, int finitet, 
                         int besselorder, int NstochMax, double dt, double lambda_strength, double gamma_strength, 
                         double lead_hop, double mu, double beta, cx_mat Hu, cx_mat Hp, int pert, cx_cube &rho, 
                         cx_mat &rho_ss, vec gs_eigval, cx_mat gs_eigvec, int systype, cx_double &bondcurrent_one_ss, 
                         cx_vec &bondcurrent_one, cx_double &bondcurrent_two_ss, cx_vec &bondcurrent_two, 
                         cx_double &bondcurrent_three_ss, cx_vec &bondcurrent_three, cx_double &supercurrent_one_ss, 
                         cx_vec &supercurrent_one, cx_double &supercurrent_two_ss, cx_vec &supercurrent_two, 
                         cx_double &supercurrent_three_ss, cx_vec &supercurrent_three, cx_mat &pairdensity, 
                         cx_mat &supermatrix, double mol_hop, double cutoff, cx_mat &TDCurrent, cx_vec &I_ss, cx_mat &TDCurrentNew, cx_mat &TDBias, 
                         vector<double> constpart, vector<double> amplitude, vector<double> frequency, 
                         vector<double> phase, cx_vec &NC, int freq_mult, int freq_mult2, double ampl_rat, cx_mat &PumpCurrent, 
                         cx_double &PumpCurrentLR, cx_double &PumpCurrentLRstoch, cx_double &PumpCurrentLRstochAdiabatic, int partpara, vector<int> calc, cx_vec &dipmom,
                         vector<vector<double> > coords, double Dfluct, double wcorr);

void PadeExpansion(int padeorder, vec &pade_eta_test, vec &pade_zeta_test);

void TwoTimeGreen(int nsite, int nlead, vector<int> lcontact, vector<int> rcontact, 
                  vector<int> lcontact2, vector<int> rcontact2, int lead_row, 
                  double bias_strength, int ntstep, int tgridtype, int sc, double dt, 
                  double lambda_strength, double gamma_strength, double lead_hop, 
                  double mu, double beta, cx_mat Hu, int systype, vec gs_eigval, 
                  cx_mat gs_eigvec, field<cx_mat> &Glss, field<cx_mat> &Ggtr, 
                  vector<double> constpart, vector<double> amplitude, vector<double> frequency, 
                  vector<double> phase, int padeorder, vec pade_eta, vec pade_zeta,
                  int subs, double acc, int rule);

void CorrelationFunction(int nsite, int nlead, vector<int> lcontact, vector<int> rcontact, 
                         vector<int> lcontact2, vector<int> rcontact2, int lead_row, 
                         double bias_strength, int ntstep, int tgridtype, int sc, int besselorder,
                         double dt, double lambda_strength, double gamma_strength, double lead_hop, 
                         double mu, double beta, cx_mat Hu, int systype, vec gs_eigval, 
                         cx_mat gs_eigvec, field<cx_mat> &SigmaGtr, field<cx_mat> &SigmaLss,
                         field<cx_mat> &LambdaPlus, field<cx_mat> &LambdaMinus,
                         field<cx_mat> &LambdaPlusD, field<cx_mat> &LambdaMinusD,
                         field<cx_mat> &GLssNew, field<cx_mat> &GGtrNew, field<cx_mat> &Cab,
                         vector<double> constpart, vector<double> amplitude, vector<double> frequency, 
                         vector<double> phase, int freq_mult1, int freq_mult2, double ampl_rat, 
                         int padeorder, vec pade_eta, vec pade_zeta, int partpara, cx_vec &dipmom,
                         vector<vector<double> > coords, vector<int> calc);

void FourierTransform(int nsite, int nlead, int ntstep, int tgridtype, int nwstep, int sc, double dt, cx_double bondcurrent_one_ss, 
                      cx_vec bondcurrent_one, cx_vec &bondcurrent_one_ft, cx_double bondcurrent_two_ss, 
                      cx_vec bondcurrent_two, cx_vec &bondcurrent_two_ft, cx_double bondcurrent_three_ss, 
                      cx_vec bondcurrent_three, cx_vec &bondcurrent_three_ft, cx_double supercurrent_one_ss, 
                      cx_vec supercurrent_one, cx_vec &supercurrent_one_ft, cx_double supercurrent_two_ss, 
                      cx_vec supercurrent_two, cx_vec &supercurrent_two_ft, cx_double supercurrent_three_ss, 
                      cx_vec supercurrent_three, cx_vec &supercurrent_three_ft, cx_mat pairdensity, cx_mat &pairdensity_ft,
                      cx_mat supermatrix, cx_mat &supermatrix_ft, cx_mat TDCurrent, cx_mat &TDCurrent_ft,
                      cx_vec Nc, cx_vec &NC_ft, vector<int> calc, cx_vec dipmom, cx_vec &dipmom_ft);

void WriteResults(int nsite, int nlead, int ntstep, int tgridtype, int nwstep, int snaps, int sc, double dt, cx_cube rho, cx_mat rho_ss, 
                  cx_vec bondcurrent_one, cx_vec bondcurrent_one_ft, cx_vec bondcurrent_two, cx_vec bondcurrent_two_ft, 
                  cx_vec bondcurrent_three, cx_vec bondcurrent_three_ft, cx_vec supercurrent_one, cx_vec supercurrent_one_ft, 
                  cx_vec supercurrent_two, cx_vec supercurrent_two_ft, cx_vec supercurrent_three, 
                  cx_vec supercurrent_three_ft, cx_mat pairdensity, cx_mat pairdensity_ft, cx_mat supermatrix, cx_mat supermatrix_ft,
                  cx_mat TDCurrent, cx_mat TDCurrentNew, cx_mat TDBias, cx_mat TDCurrent_ft, cx_vec NC, cx_vec NC_ft, cx_mat PumpCurrent,
                  cx_double PumpCurrentLR, cx_double PumpCurrentLRstoch, cx_double PumpCurrentLRstochAdiabatic, field<cx_mat> Glss, field<cx_mat> Ggtr, 
                  vector<int> calc, field<cx_mat> Cab, field<cx_mat> GLssNew, field<cx_mat> GGtrNew,
                  cx_vec dipmom, cx_vec dipmom_ft);

cx_cube Gamma(int nsite, int nlead, vector<int> lcontact, vector<int> rcontact, vector<int> lcontact2, 
              vector<int> rcontact2, int lead_row, double lambda_strength, double gamma_strength, 
              double lead_hop, cx_mat gs_eigvec, int systype, int sc);

double V(int nlead, int biastype, double bias_strength);
double Time(int ntstep, int tgridtype, double dt, int j);
double Round(double x);
double Sign(double x);
double Delta(unsigned int i, unsigned int j);
double Theta(double x);
double ThetaMidpoint(double x);
cx_double Fermi(double beta, cx_double z);
void Seed();
double UniformRandom();
cx_double LambdaCalculate(void *param);
cx_double OmegaCalculate(void *param);
cx_double PiCalculate(double t, void *param);
cx_double FirstTerm(double t, void *param);
cx_double SecondTerm(double t, void *param);
cx_double ThirdTerm(double t, void *param);
cx_double FourthTerm(double t, void *param);
cx_double SecondTermNew(double t, void *param);
cx_double ThirdTermNew(double t, void *param);
cx_double FourthTermNew(double t, void *param);
cx_double FifthTermNew(double t, void *param);
cx_double FirstTermN(double t, void *param);
cx_double SecondTermN(double t, void *param);
cx_double ThirdTermN(double t, void *param);
cx_double PumpTerm1(void *param);
cx_double PumpTerm2(void *param);
cx_double PumpLR(void *param);
cx_double PumpLRstoch(void *param);
cx_double PumpLRstochAdiabatic(void *param);
cx_double ExpIntE(cx_double z);
cx_double Fhyp(cx_double z, double t, double beta);
cx_double Psi(cx_double z, double beta);
double BesselJ(int n, double x);
cx_double Phibar(cx_double z, double t, double beta);
void StopWatch(double start, double stop);
double WindowFunc(unsigned int l, int ntstep, int windowtype);
double Integrand2re(double tau, void *param);
double Integrand2im(double tau, void *param);
double Integrand3re(double tau, void *param);
double Integrand3im(double tau, void *param);
double Integrand111re(double tauprime, void *param);

// --------------------	//
// Parameter structures	//
// --------------------	//

struct ParamsL  // Defines a bunch of parameters in a same 'envelope'
{
    unsigned int j, k, alpha;
    int biastype, pert, finitet;
    double mu, bias_strength, beta;
    cx_vec eps_ueff_l;
    cx_vec eps_peff_l;
};

struct ParamsP
{
    unsigned int j, k, alpha, m, n;
    int biastype, pert, finitet;
    double mu, bias_strength, beta;
    cx_vec eps_ueff_l;
    cx_vec eps_peff_l;
};

struct ParamsO
{
    unsigned int j, k, alpha, m, n, p, q;
    int biastype, pert, finitet;
    double mu, bias_strength, beta;
    cx_vec eps_ueff_l;
    cx_vec eps_peff_l;
};

struct ParamsTDCurr
{
    unsigned int j, k, alpha, be;
    int r, s;
    double mu, beta;
    vector<double> constpart, amplitude, frequency, phase;
    cx_vec eps_ueff_l;
};

struct ParamsTDCurrNew
{
    unsigned int j, k, alpha, ga;
    int r, rp, s, sp, freq_mult1, freq_mult2;
    double mu, beta, ampl_rat;
    vector<double> constpart, amplitude, frequency, phase;
    cx_vec eps_ueff_l;
};

struct ParamsPump
{
    unsigned int j, k, alpha, be, ga;
    int r, rp, s, sp, freq_mult1, freq_mult2;
    double mu, beta, ampl_rat;
    vector<double> constpart, amplitude, frequency, phase;
    cx_vec eps_ueff_l;
};

struct ParamsPumpLR
{
    unsigned int j, k;
    int r, rp, s, sp, freq_mult1, freq_mult2;
    double mu, beta, ampl_rat;
    vector<double> constpart, amplitude, frequency, phase;
    cx_vec eps_ueff_l;
};

struct ParamsPumpLRstoch
{
    unsigned int j, k;
    int r, rp, s, sp, freq_mult1, freq_mult2, m, n;
    double mu, beta, ampl_rat, Dfluct, wcorr;
    vector<double> constpart, amplitude, frequency, phase;
    cx_vec eps_ueff_l;
};


struct ParamsPumpLRstochAdiabatic
{
    unsigned int j, k;
    int m, n;
    double mu, beta, Dfluct, wcorr;
    vector<double> constpart;
    cx_vec eps_ueff_l;
};


struct ParamsTimeIntegral
{
    unsigned int j, k, be, ii;
    int tgridtype, ntstep, subs, rule, padeorder;
    double mu, beta, tau, taubar, dt, acc;
    vector<double> constpart, amplitude, frequency, phase;
    vec pade_eta, pade_zeta;
    cx_vec eps_ueff_l;
};

template<typename T>    // Test infinity
inline bool isinf(T value)
{
    return std::numeric_limits<T>::has_infinity &&
    value == std::numeric_limits<T>::infinity();
}

template<typename T>    // Test NaN
inline bool isnan(T value)
{
    return value != value;
}
