/*

This programme calculates the time-dependent densities
and bond currents for a multi-terminal transport of an
arbitrary, non-interacting lattice network employing
wide-band limit approximation for the terminals.

Last update: May 25th 2023, Riku Tuovinen <riku.m.s.tuovinen@jyu.fi>

*/

#include "Main.hpp"

// -------------- //
// Main programme //
// -------------- //

int main(int argc, char *argv[])
{

// Initialize MPI
    MPI_Init(&argc, &argv);

// Start timekeeping
	double start = MPI_Wtime();
	double tmpdble1, tmpdble2;

// MPI variables
	int rank, size;

// Get the number of processes and the individual process ID
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

// Allocate variables (for superconducting junction the notation is _sc)
    int nsite, systype, biastype, nlead, ntstep, tgridtype, nwstep, 
        layers, lead_row, bridge_sites, snaps, pert, sc, finitet, 
        besselorder, NstochMax, padeorder, freq_mult1, freq_mult2, subs, rule, partpara;
    double lambda_strength, gamma_strength, mol_hop, lead_hop, dt, 
           bias_strength, mu, cutoff, beta, ampl_rat, acc, Dfluct, wcorr;
    cx_double sc_delta;
    vector<int> lcontact, rcontact, lcontact2, rcontact2, bridgeone, 
                bridgetwo, bridgethree, lcontact_sc, rcontact_sc, 
                lcontact2_sc, rcontact2_sc, bridgeone_sc, bridgetwo_sc, 
                bridgethree_sc;
    vector<double> constpart, amplitude, frequency, phase;
    vector<int> calc;
    vector<vector<double> > coords;
    double scw_t, scw_Vz, scw_aso, scw_Delta, scw_mu, scw_gate;

// Read input
    if(rank == 0) cout << endl;
    tmpdble1 = MPI_Wtime();
    ReadInput(nsite, systype, biastype, nlead, ntstep, tgridtype, 
              nwstep, layers, snaps, pert, sc, finitet, besselorder, NstochMax,
              lcontact, rcontact, lcontact2, rcontact2, bridgeone, 
              bridgetwo, bridgethree, lambda_strength, gamma_strength, 
              mol_hop, lead_hop, lead_row, bridge_sites, dt, 
              bias_strength, cutoff, sc_delta, beta, constpart, 
              amplitude, frequency, phase, freq_mult1, freq_mult2, ampl_rat, 
              padeorder, subs, acc, rule, partpara, calc, coords,
              scw_t, scw_Vz, scw_aso, scw_Delta, scw_mu, scw_gate, Dfluct, wcorr);

    // If the junction is superconducting, the contact indices need to be relabeled
    if(sc == 1)
    {
        SCContacts(nsite, lcontact, rcontact, lcontact2, rcontact2, 
                   bridgeone, bridgetwo, bridgethree, lcontact_sc, 
                   rcontact_sc, lcontact2_sc, rcontact2_sc, 
                   bridgeone_sc, bridgetwo_sc, bridgethree_sc);
    }

    tmpdble2 = MPI_Wtime();
    if(rank == 0) {StopWatch(tmpdble1, tmpdble2); cout << endl;}

// Allocate arrays (for superconducting junction the notation is _sc)
    cx_double bondcurrent_one_ss, bondcurrent_two_ss, 
              bondcurrent_three_ss, supercurrent_one_ss, 
              supercurrent_two_ss, supercurrent_three_ss,
              PumpCurrentLR, PumpCurrentLRstoch, PumpCurrentLRstochAdiabatic;
    vec gs_eigval(nsite), gs_eigval_sc(2*nsite), pade_eta(padeorder), 
        pade_zeta(padeorder);
    cx_vec bondcurrent_one(ntstep/size), bondcurrent_one_ft(nwstep/size),
           bondcurrent_two(ntstep/size), bondcurrent_two_ft(nwstep/size),
           bondcurrent_three(ntstep/size), bondcurrent_three_ft(nwstep/size),
           supercurrent_one(ntstep/size), supercurrent_one_ft(nwstep/size),
           supercurrent_two(ntstep/size), supercurrent_two_ft(nwstep/size),
           supercurrent_three(ntstep/size), supercurrent_three_ft(nwstep/size),
           NC(ntstep/size), NC_ft(nwstep/size), dipmom(ntstep/size), dipmom_ft(nwstep/size), I_ss(nlead);
    cx_mat Hu(nsite,nsite), gs_eigvec(nsite,nsite), Hp(nsite,nsite), 
           rho_ss(nsite,nsite), Hu_sc(2*nsite,2*nsite), 
           gs_eigvec_sc(2*nsite,2*nsite), Hp_sc(2*nsite,2*nsite), 
           rho_ss_sc(2*nsite,2*nsite), pairdensity(nsite,ntstep/size), 
           pairdensity_ft(nsite,nwstep/size), supermatrix(nsite,ntstep/size), 
           supermatrix_ft(nsite,nwstep/size), TDCurrent(nlead, ntstep/size), TDCurrentNew(nlead, ntstep/size), 
           TDBias(nlead, ntstep/size), TDCurrent_ft(nlead, nwstep/size), 
           PumpCurrent(nlead, nlead);
    cx_cube rho(nsite,nsite,ntstep/size), rho_sc(2*nsite,2*nsite,ntstep/size);
    field<cx_mat> Glss(ntstep/size,ntstep), Ggtr(ntstep/size,ntstep);

	field<cx_mat> SigmaGtr(ntstep/size,ntstep,nlead), SigmaLss(ntstep,ntstep/size,nlead),
	              GLssNew(ntstep,ntstep/size), GGtrNew(ntstep/size,ntstep), Cab(ntstep/size,ntstep),
	              LambdaPlus(ntstep,ntstep/size,nlead), LambdaMinus(ntstep/size,ntstep,nlead),
	              LambdaPlusD(ntstep/size,ntstep,nlead), LambdaMinusD(ntstep,ntstep/size,nlead);

// Call functions (perform the actual calculations, Fourier transforms,
// write to file etc.)
    tmpdble1 = MPI_Wtime();
    ConstructHamiltonian(nsite, systype, mol_hop, Hu, layers, Hp, pert, sc,
			 scw_t, scw_Vz, scw_aso, scw_Delta, scw_mu, scw_gate);
    // If the junction is superconducting, the Hamiltonian needs to 
    // include SC indices
    if(sc == 1)
    {
        SCHamiltonian(nsite, Hu, Hp, Hu_sc, Hp_sc, sc_delta);
    }
    tmpdble2 = MPI_Wtime();
    if(rank == 0) {StopWatch(tmpdble1, tmpdble2); cout << endl;}

    tmpdble1 = MPI_Wtime();
    // No superconductivity => proceed as before
    if(sc == 0)
    {
        CalculateGS(nsite, ntstep, mu, gamma_strength, bias_strength, Hu, 
                    systype, finitet, beta, gs_eigval, gs_eigvec);
    }
    // Superconductivity => number of sites is doubled and relabeled 
    // Hamiltonian is used
    if(sc == 1)
    {
        CalculateGS(2*nsite, ntstep, mu, gamma_strength, bias_strength, 
                    Hu_sc, systype, finitet, beta, gs_eigval_sc, 
                    gs_eigvec_sc);
    }

    tmpdble2 = MPI_Wtime();
    if(rank == 0) {StopWatch(tmpdble1, tmpdble2); cout << endl;}

    tmpdble1 = MPI_Wtime();
    // No superconductivity => proceed as before
    if(sc == 0)
    {
        CalculateQuantities(nsite, nlead, biastype, lcontact, rcontact, 
                            lcontact2, rcontact2, bridgeone, bridgetwo, 
                            bridgethree, lead_row, bridge_sites, 
                            bias_strength, ntstep, tgridtype, sc, finitet, 
                            besselorder, NstochMax, dt, lambda_strength, gamma_strength, 
                            lead_hop, mu, beta, Hu, Hp, pert, rho, rho_ss, 
                            gs_eigval, gs_eigvec, systype, bondcurrent_one_ss, 
                            bondcurrent_one, bondcurrent_two_ss, bondcurrent_two, 
                            bondcurrent_three_ss, bondcurrent_three, 
                            supercurrent_one_ss, supercurrent_one, 
                            supercurrent_two_ss, supercurrent_two, 
                            supercurrent_three_ss, supercurrent_three, 
                            pairdensity, supermatrix, mol_hop, cutoff, 
                            TDCurrent, I_ss, TDCurrentNew, TDBias, constpart, amplitude, 
                            frequency, phase, NC, freq_mult1, freq_mult2, ampl_rat, 
                            PumpCurrent, PumpCurrentLR, PumpCurrentLRstoch, PumpCurrentLRstochAdiabatic, partpara, calc, 
                            dipmom, coords, Dfluct, wcorr);
    }
    // Superconductivity => number of sites is doubled, contacts are 
    // the _sc ones and relabeled Hamiltonian is used
    if(sc == 1)
    {
        CalculateQuantities(2*nsite, nlead, biastype, lcontact_sc, 
                            rcontact_sc, lcontact2_sc, rcontact2_sc, 
                            bridgeone_sc, bridgetwo_sc, bridgethree_sc, 
                            lead_row, 2*bridge_sites, bias_strength, 
                            ntstep, tgridtype, sc, finitet, besselorder, NstochMax,
                            dt, lambda_strength, gamma_strength, lead_hop, 
                            mu, beta, Hu_sc, Hp_sc, pert, rho_sc, rho_ss_sc, 
                            gs_eigval_sc, gs_eigvec_sc, systype, 
                            bondcurrent_one_ss, bondcurrent_one, 
                            bondcurrent_two_ss, bondcurrent_two, 
                            bondcurrent_three_ss, bondcurrent_three, 
                            supercurrent_one_ss, supercurrent_one, 
                            supercurrent_two_ss, supercurrent_two, 
                            supercurrent_three_ss, supercurrent_three, 
                            pairdensity, supermatrix, mol_hop, cutoff, 
                            TDCurrent, I_ss, TDCurrentNew, TDBias, constpart, amplitude, 
                            frequency, phase, NC, freq_mult1, freq_mult2, ampl_rat, 
                            PumpCurrent, PumpCurrentLR, PumpCurrentLRstoch, PumpCurrentLRstochAdiabatic, partpara, calc, 
                            dipmom, coords, Dfluct, wcorr);
    }
    tmpdble2 = MPI_Wtime();
    if(rank == 0) {StopWatch(tmpdble1, tmpdble2); cout << endl;}

	// Pade expansion parameters are needed for the two-time Green's functions
    PadeExpansion(padeorder, pade_eta, pade_zeta);

	// Evaluate the two-time Green's functions
    if(calc[4] == 1)
    {
        tmpdble1 = MPI_Wtime();
        // No superconductivity => proceed as before
        if(sc == 0)
        {
            TwoTimeGreen(nsite, nlead, lcontact, rcontact, lcontact2, rcontact2, 
                         lead_row, bias_strength, ntstep, tgridtype, sc, dt, 
                         lambda_strength, gamma_strength, lead_hop, mu, beta, 
                         Hu, systype, gs_eigval, gs_eigvec, Glss, Ggtr, constpart, 
                         amplitude, frequency, phase, padeorder, pade_eta, 
                         pade_zeta, subs, acc, rule);
        }
        // Superconductivity => number of sites is doubled, contacts are 
        // the _sc ones and relabeled Hamiltonian is used
        if(sc == 1)
        {
            TwoTimeGreen(2*nsite, nlead, lcontact_sc, rcontact_sc, lcontact2_sc, rcontact2_sc, 
                         lead_row, bias_strength, ntstep, tgridtype, sc, dt, 
                         lambda_strength, gamma_strength, lead_hop, mu, beta, 
                         Hu_sc, systype, gs_eigval, gs_eigvec, Glss, Ggtr, constpart, 
                         amplitude, frequency, phase, padeorder, pade_eta, 
                         pade_zeta, subs, acc, rule);
        }
        tmpdble2 = MPI_Wtime();
        if(rank == 0) {StopWatch(tmpdble1, tmpdble2); cout << endl;}
    } // if calc two-time Green's function

	// Evaluate current correlation function
    if(calc[10] == 1)
    {
        tmpdble1 = MPI_Wtime();
        // No superconductivity => proceed as before
        if(sc == 0)
        {
			CorrelationFunction(nsite, nlead, lcontact, rcontact, lcontact2, rcontact2, lead_row, 
                                bias_strength, ntstep, tgridtype, sc, besselorder, dt, lambda_strength, 
                                gamma_strength, lead_hop, mu, beta, Hu, systype, gs_eigval, 
                                gs_eigvec, SigmaGtr, SigmaLss, LambdaPlus, LambdaMinus, LambdaPlusD, LambdaMinusD, GLssNew, 
                                GGtrNew, Cab, constpart, amplitude, frequency, phase, freq_mult1, 
                                freq_mult2, ampl_rat, padeorder, pade_eta, pade_zeta, partpara, dipmom, coords, calc);
        }
        // Superconductivity => number of sites is doubled, contacts are 
        // the _sc ones and relabeled Hamiltonian is used
        if(sc == 1)
        {
			CorrelationFunction(2*nsite, nlead, lcontact_sc, rcontact_sc, lcontact2_sc, rcontact2_sc, 
			                    lead_row, bias_strength, ntstep, tgridtype, sc, besselorder, dt, lambda_strength, 
                                gamma_strength, lead_hop, mu, beta, Hu_sc, systype, gs_eigval, 
                                gs_eigvec, SigmaGtr, SigmaLss, LambdaPlus, LambdaMinus, LambdaPlusD, LambdaMinusD, GLssNew, 
                                GGtrNew, Cab, constpart, amplitude, frequency, phase, freq_mult1, 
                                freq_mult2, ampl_rat, padeorder, pade_eta, pade_zeta, partpara, dipmom, coords, calc);
        }
        tmpdble2 = MPI_Wtime();
        if(rank == 0) {StopWatch(tmpdble1, tmpdble2); cout << endl;}
    } // if calc correlation functions

    tmpdble1 = MPI_Wtime();
    FourierTransform(nsite, nlead, ntstep, tgridtype, nwstep, sc, dt, 
                     bondcurrent_one_ss, bondcurrent_one, bondcurrent_one_ft, 
                     bondcurrent_two_ss, bondcurrent_two, bondcurrent_two_ft, 
                     bondcurrent_three_ss, bondcurrent_three, bondcurrent_three_ft, 
                     supercurrent_one_ss, supercurrent_one, supercurrent_one_ft, 
                     supercurrent_two_ss, supercurrent_two, supercurrent_two_ft, 
                     supercurrent_three_ss, supercurrent_three, supercurrent_three_ft, 
                     pairdensity, pairdensity_ft, supermatrix, supermatrix_ft,
                     TDCurrent, TDCurrent_ft, NC, NC_ft, calc, dipmom, dipmom_ft);
    tmpdble2 = MPI_Wtime();
    if(rank == 0) {StopWatch(tmpdble1, tmpdble2); cout << endl;}

    if(rank == 0) {cout << endl; cout << "Saving files and terminating... " 
                        << endl; cout << endl;}

    tmpdble1 = MPI_Wtime();
    // No superconductivity => proceed as before
    if(sc == 0)
    {
        WriteResults(nsite, nlead, ntstep, tgridtype, nwstep, snaps, sc, 
                     dt, rho, rho_ss, bondcurrent_one, bondcurrent_one_ft,
                     bondcurrent_two, bondcurrent_two_ft, bondcurrent_three, 
                     bondcurrent_three_ft, supercurrent_one, supercurrent_one_ft, 
                     supercurrent_two, supercurrent_two_ft, supercurrent_three, 
                     supercurrent_three_ft, pairdensity, pairdensity_ft, 
                     supermatrix, supermatrix_ft, TDCurrent, TDCurrentNew, TDBias, 
                     TDCurrent_ft, NC, NC_ft, PumpCurrent, PumpCurrentLR, PumpCurrentLRstoch, PumpCurrentLRstochAdiabatic, Glss, 
                     Ggtr, calc, Cab, GLssNew, GGtrNew, dipmom, dipmom_ft);
    }
    // Superconductivity => rho_sc instead of rho
    if(sc == 1)
    {
        WriteResults(nsite, nlead, ntstep, tgridtype, nwstep, snaps, sc, 
                     dt, rho_sc, rho_ss_sc, bondcurrent_one, bondcurrent_one_ft,
                     bondcurrent_two, bondcurrent_two_ft, bondcurrent_three, 
                     bondcurrent_three_ft, supercurrent_one, supercurrent_one_ft, 
                     supercurrent_two, supercurrent_two_ft, supercurrent_three, 
                     supercurrent_three_ft, pairdensity, pairdensity_ft, 
                     supermatrix, supermatrix_ft, TDCurrent, TDCurrentNew, TDBias, 
                     TDCurrent_ft, NC, NC_ft, PumpCurrent, PumpCurrentLR, PumpCurrentLRstoch, PumpCurrentLRstochAdiabatic, Glss, 
                     Ggtr, calc, Cab, GLssNew, GGtrNew, dipmom, dipmom_ft);
    }
    tmpdble2 = MPI_Wtime();
    if(rank == 0) {StopWatch(tmpdble1, tmpdble2); cout << endl;}

// Stop timekeeping
	double finish = MPI_Wtime();

// Stopwatch
	if(rank == 0) StopWatch(start, finish);

// Terminate MPI
	MPI_Finalize();

// Main programme ends
    return 0;
}
