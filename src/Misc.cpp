#include "Main.hpp"

double Delta(unsigned int i, unsigned int j)
{
	if(i == j) return 1.0;
	else return 0.0;
}

double Theta(double x)
{
	if(x > 0) return 1.0;
	else return 0.0;
}

double ThetaMidpoint(double x)
{
	if(x > 0) return 1.0;
	else if(x == 0) return 0.5;
	else return 0.0;
}

double Round(double x)
{
    return (x > 0.0) ? floor(x + 0.5) : ceil(x - 0.5);
}

void Seed()
{
    srand(time(0));
}

double Sign(double x)
{
    return (x > 0) ? 1 : ((x < 0) ? -1 : 0);
}

double UniformRandom()
{
    return rand() / double(RAND_MAX);
}

cx_double Fermi(double beta, cx_double z)
{
    return cx_double(1.0,0.0)/(exp(cx_double(beta,0.0)*z)+1.0);
}

void PadeExpansion(int padeorder, vec &pade_eta, vec &pade_zeta)
{
/*
 * Imported Mike's code
 * */

    const int functype = 0; // 0 = Fermi, 1 = Bose

    // Initialize A, B
    mat A(2*padeorder,2*padeorder), B(2*padeorder-1,2*padeorder-1);
    vec eigvalA, eigvalB, eigA(padeorder), eigB(padeorder-1), iota(padeorder);
    A.zeros();
    B.zeros();

    for(unsigned int j=1; j<2*padeorder; j++)
    {
        if(functype == 0)
        {
            A(j-1,j) = 1/(sqrt((2*j-1)*(2*j+1)));
            A(j,j-1) = 1/(sqrt((2*j-1)*(2*j+1)));
        }
        if(functype == 1)
        {
            A(j-1,j) = 1/(sqrt((2*j+1)*(2*j+3)));
            A(j,j-1) = 1/(sqrt((2*j+1)*(2*j+3)));
        }
    }
    
    for(unsigned int j=1; j<(2*padeorder-1); j++)
    {
        if(functype == 0)
        {
            B(j-1,j) = 1/(sqrt((2*j+1)*(2*j+3)));
            B(j,j-1) = 1/(sqrt((2*j+1)*(2*j+3)));
        }
        if(functype == 1)
        {
            B(j-1,j) = 1/(sqrt((2*j+3)*(2*j+5)));     
            B(j,j-1) = 1/(sqrt((2*j+3)*(2*j+5)));
        }
    }
    
    eig_sym(eigvalA,A);
    eig_sym(eigvalB,B);
    
    for(unsigned int j=0; j<padeorder; j++)
    {
        eigA(j) = -2/eigvalA(j);
        if(j<padeorder-1)
        {
            eigB(j) = -2/eigvalB(j);
        }
    }

    iota.ones();
    for(unsigned int j=0; j<padeorder-1; j++)
    {
        for(unsigned int i=0; i<j; i++)
        {
            iota(j) = iota(j)*(eigB(i)*eigB(i)-eigA(j)*eigA(j));
            iota(j) = iota(j)/(eigA(i)*eigA(i)-eigA(j)*eigA(j));
        }
        iota(j) = iota(j)*(eigB(j)*eigB(j)-eigA(j)*eigA(j));
        
        for(unsigned int i=j+1; i<padeorder-1; i++)
        {
            iota(j) = iota(j)*(eigB(i)*eigB(i)-eigA(j)*eigA(j));
            iota(j) = iota(j)/(eigA(i)*eigA(i)-eigA(j)*eigA(j));
        }

        iota(j) = iota(j)/(eigA(padeorder-1)*eigA(padeorder-1)-eigA(j)*eigA(j));
        if(functype == 0)
        {
            iota(j) = (padeorder/2)*(2*padeorder+1)*iota(j);
        }
        if(functype == 1)
        {
            iota(j) = (padeorder/2)*(2*padeorder+3)*iota(j);
        }
    }

    for(unsigned int j=0;j<padeorder-1;j++)
    {
        iota(padeorder-1) = iota(padeorder-1)*(eigB(j)*eigB(j)-eigA(padeorder-1)
                           *eigA(padeorder-1))/(eigA(j)*eigA(j)-eigA(padeorder-1)*eigA(padeorder-1));
    }
    if(functype == 0)
    {
        iota(padeorder-1)=(padeorder/2)*(2*padeorder+1)*iota(padeorder-1);
    }
    if(functype == 1)
    {
        iota(padeorder-1)=(padeorder/2)*(2*padeorder+3)*iota(padeorder-1);
    }

    for(unsigned int j=0; j<padeorder; j++)
    {
        pade_zeta(j) = eigA(j);
        pade_eta(j) = iota(j);
    }

// Compare Padé expansion to exact Fermi/Bose function
/*
    ofstream bose("output/bose.out", ios::out);
    bose <<"#w" << "\t\t" << "f(w)" << endl;
    double x=-10.0;
    double betatest=1.0;
    const int Nx = 201;
    double dx = (-1.0*x-x)/Nx;
    cx_double bosetemp;
    cx_double I(0.0,1.0);
    for(unsigned int i=0; i<Nx; i++)
    {
        bosetemp = cx_double(0.0,0.0);
        bose << setprecision(5) << scientific << x << "\t\t" << 1.0/(exp(betatest*1.0*x)-1.0) << "\t\t";
        for(unsigned int j=0; j<padeorder; j++)
        {
            if(i==0) cout << pade_eta(j) << " " << pade_zeta(j) << endl;
            bosetemp += ( pade_eta(j)/(-1.0*betatest*x + I*pade_zeta(j)) + pade_eta(j)/(-1.0*betatest*x - I*pade_zeta(j)) );
        }
        bose << -1.0/(-1.0*betatest*x) - 0.5 - real(bosetemp) << endl;
        x += dx;
    }
    bose.close();
*/
}
